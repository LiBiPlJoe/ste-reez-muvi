﻿using UnityEngine;
using System.Collections;

public class CellScript : MonoBehaviour {

    public CellLoc cellLocation;
	public Vector3 newPosition, newScale;
	Vector3 oldPosition, oldScale;
	float timeSinceStable;
	public bool toBeSeen; // independent of local concerns, does the UI want to see me?

    public string displayString;

    private Material currMaterial;
    public Material CurrMaterial {
        get { return currMaterial; }
        set {
            if (currMaterial != value && myRend != null) {
                currMaterial = value;
                myRend.material = currMaterial;
                myRend.material.mainTextureScale = new Vector2(-1f,-1f);
                oldScale = transform.localScale = new Vector3(0.9f, 0.9f, transform.localScale.z); // too subtle?
            }
        }
    }

    private Renderer myRend;

	public MapManager mapManager;

	void Awake () {
		Reset();
        myRend = GetComponent<Renderer>();
	}

	public void Reset() {
		oldPosition = newPosition = transform.position;
		oldScale = newScale = transform.localScale;
		timeSinceStable = 0f;
		toBeSeen = true;
	}

	void Update () {
		timeSinceStable += Time.deltaTime;
		if (transform.position != newPosition) {
			transform.position = Vector3.Slerp(oldPosition,newPosition,timeSinceStable);
			if (timeSinceStable >= 1f) transform.position = newPosition;
		}
		if (transform.localScale != newScale) {
			transform.localScale = Vector3.Slerp(oldScale,newScale,timeSinceStable);
			if (timeSinceStable >= 1f) transform.localScale = newScale;
		} 
        if (transform.localScale == newScale && transform.position == newPosition) {
			timeSinceStable = 0f;
			oldPosition = transform.position;
			oldScale = transform.localScale;
		}

		// disappear if desired
		myRend.enabled = toBeSeen;
	}


}

