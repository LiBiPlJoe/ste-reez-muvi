using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace DragonsSpine
{
    /// <summary>
    /// A Land is a collection of maps, and everything pertaining to the entire collection of maps.
    /// </summary>
    public class Land
    {
        #region Private Data
        protected readonly short m_facetID;
        protected readonly short m_landID;
        protected readonly string m_name; // name of this land (ie: Beginner's Game)
        protected readonly string m_shortDesc; // short description (ie: BG)
        protected readonly string m_longDesc; // a detailed description of this land
        protected readonly int[] m_hitDice; // none, fighter, thaum, wizard, ma, thief, knight
        protected readonly int[] m_manaDice;
        protected readonly int[] m_staminaDice;
        protected readonly int m_statCapOperand; // added to the stat cap formula (eg: hits cap = hitDice * level + statCapOperand)
        protected readonly int m_maximumAbilityScore; // the maximum a player may raise an ability score to in this land
        protected readonly int m_maximumShielding; // the maximum amount of shielding a player may have in this land
        protected readonly int m_maximumTempAbilityScore; // the maximum amount of temporary stat a player may have in this land
        protected Dictionary<short, Map> m_mapDict; // array of Map objects in this Land 
        #endregion

        #region Properties
        public short FacetID
        {
            get { return this.m_facetID; }
        }
        public short LandID
        {
            get { return this.m_landID; }
        }
        public string Name
        {
            get { return this.m_name; }
        }
        public string ShortDesc
        {
            get { return this.m_shortDesc; }
        }
        public string LongDesc
        {
            get { return this.m_longDesc; }
        }
        public int[] HitDice
        {
            get { return this.m_hitDice; }
        }
        public int[] ManaDice
        {
            get { return this.m_manaDice; }
        }
        public int[] StaminaDice
        {
            get { return this.m_staminaDice; }
        }
        public int StatCapOperand
        {
            get { return this.m_statCapOperand; }
        }
        public int MaxAbilityScore
        {
            get { return this.m_maximumAbilityScore; }
        }
        public int MaxShielding
        {
            get { return this.m_maximumShielding; }
        }
        public int MaxTempAbilityScore
        {
            get { return this.m_maximumTempAbilityScore; }
        }
        public Dictionary<short, Map> MapDictionary
        {
            get { return this.m_mapDict; }
        }
        public Dictionary<short, Map>.ValueCollection Maps
        {
            get { return this.m_mapDict.Values; }
        }
        #endregion

        #region Constructor
        public Land(short facetID, System.Data.DataRow dr)
        {
            m_facetID = facetID;
            m_landID = Convert.ToInt16(dr["landID"]);
            m_name = dr["name"].ToString();
            m_shortDesc = dr["shortDesc"].ToString();
            m_longDesc = dr["longDesc"].ToString();
            int len = Enum.GetNames(typeof(Character.ClassType)).Length;
            m_hitDice = new int[len];
            string[] s = dr["hitDice"].ToString().Split(" ".ToCharArray());
            for (int a = 0; a < m_hitDice.Length; a++)
            {
                m_hitDice[a] = Convert.ToInt32(s[a]);
            }
            m_manaDice = new int[len];
            s = dr["manaDice"].ToString().Split(" ".ToCharArray());
            for (int a = 0; a < m_manaDice.Length; a++)
            {
                m_manaDice[a] = Convert.ToInt32(s[a]);
            }
            m_staminaDice = new int[len];
            s = dr["staminaDice"].ToString().Split(" ".ToCharArray());
            for (int a = 0; a < m_manaDice.Length; a++)
            {
                m_staminaDice[a] = Convert.ToInt32(s[a]);
            }
            m_statCapOperand = Convert.ToInt32(dr["statCapOperand"]);
            m_maximumAbilityScore = Convert.ToInt32(dr["maximumAbilityScore"]);
            m_maximumShielding = Convert.ToInt32(dr["maximumShielding"]);
            m_maximumTempAbilityScore = Convert.ToInt32(dr["maximumTempAbilityScore"]);
            m_mapDict = new Dictionary<short, Map>();

        }
        #endregion

        public bool FillLand() // fill this land with the appropriate maps
        {
            try
            {
                DAL.DBWorld.LoadMaps(this);

                string mapBase = "..\\..\\maps";

                foreach (Map map in this.Maps)
                {
                    if (!map.LoadMap(mapBase + "\\" + map.Name + ".txt", this.FacetID, this.LandID, map.MapID))
                    {
                        Utils.Log("Land.FillLand() failed while loading map file for " + map.Name + ".", Utils.LogType.SystemFailure);
                    }
                    //else
                    //{
                    //    Utils.Log("Loaded map " + map.Name + " into the " + this.Name + ".", Utils.LogType.SystemGo);
                    //}
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        public void Add(Map map)
        {
            m_mapDict.Add(map.MapID, map);
            //Utils.Log("Added " + map.Name + " to " + this.Name + " in " + World.GetFacetByID(this.FacetID).Name + " Facet.", Utils.LogType.SystemGo);
        }

        public Map GetMapByID(short mapID)
        {
            if (this.MapDictionary.ContainsKey(mapID))
            {
                return this.MapDictionary[mapID];
            }
            Utils.Log("Land.GetMapByID(" + mapID + ") returned null (Land = " + this.Name + "[" + this.LandID + "]).", Utils.LogType.SystemWarning);
            return null;
        }

        public Map GetMapByIndex(int index)
        {
            int a = 0;
            foreach (Map map in this.Maps)
            {
                if (a == index)
                {
                    return map;
                }
                a++;
            }
            Utils.Log("Land.GetMapByIndex(" + index + ") returned null (Land = " + this.Name + "[" + this.LandID + "]).", Utils.LogType.SystemWarning);
            return null;
        }
    }
}
