using System;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine
{
	public class Segue
	{
        #region Private Data
        short landID;
        short mapID;
        short xCord;
        short yCord;
        int zCord;
        int height;
        #endregion

        #region Public Properties
        public short LandID
        {
            get { return this.landID; }
        }
        public short MapID
        {
            get { return this.mapID; }
        }
        public short X
        {
            get { return this.xCord; }
        }
        public short Y
        {
            get { return this.yCord; }
        }
        public int Z
        {
            get { return this.zCord; }
        }
        public int Height
        {
            get { return this.height; }
        }
        #endregion

        #region Constructor
        public Segue(short landID, short mapID, short xCord, short yCord, int zCord, int height)
        {
            this.landID = landID;
            this.mapID = mapID;
            this.xCord = xCord;
            this.yCord = yCord;
            this.zCord = zCord;
            this.height = height;
        } 
        #endregion

        #region Public Static Functions
        public static Segue GetUpSegue(Cell cell)
        {
            try
            {
                int[] zPlanes = new int[cell.Map.ZPlanes.Count];
                cell.Map.ZPlanes.CopyTo(zPlanes);
                Array.Sort(zPlanes); // sorts in ascending order
                int a = 0;
                Cell targetCell = null;
                do
                {
                    if (zPlanes[a] > cell.Z)
                    {
                        targetCell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X, cell.Y, zPlanes[a]);
                        if (targetCell != null)
                        {
                            if (targetCell.IsUpSegue) // found up segue
                            {
                                return new Segue(targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z, Math.Abs(targetCell.Z - cell.Z));
                            }
                            // search for a suitable segue
                            for (int xpos = 0; xpos <= 1; xpos++)
                            {
                                for (int ypos = 0; ypos <= 1; ypos++)
                                {
                                    Cell searchCell = Cell.GetCell(targetCell.FacetID, targetCell.LandID, targetCell.MapID, targetCell.X + xpos, targetCell.Y + ypos, targetCell.Z);
                                    if (searchCell != null && searchCell.IsUpSegue)
                                    {
                                        return new Segue(searchCell.LandID, searchCell.MapID, searchCell.X, searchCell.Y, searchCell.Z, Math.Abs(searchCell.Z - cell.Z));
                                    }

                                    searchCell = Cell.GetCell(targetCell.FacetID, targetCell.LandID, targetCell.MapID, targetCell.X - xpos, targetCell.Y - ypos, targetCell.Z);
                                    if (searchCell != null && searchCell.IsUpSegue)
                                    {
                                        return new Segue(searchCell.LandID, searchCell.MapID, searchCell.X, searchCell.Y, searchCell.Z, Math.Abs(searchCell.Z - cell.Z));
                                    }
                                }
                            }
                        }
                    }
                    a++;
                }
                while (a < zPlanes.Length);
                if (targetCell != null) // this is the fail safe
                {
                    return new Segue(targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z, Math.Abs(targetCell.Z - cell.Z));
                }
                Utils.Log("Failed to find a suitable Segue at Segue.GetUpSegue(Cell: " + cell.LandID + " " + cell.MapID + " " + cell.X + " " + cell.Y + " " + cell.Z + ")", Utils.LogType.SystemFailure);
                Utils.Log("Returned new Segue (KarmaRes)", Utils.LogType.SystemFailure);
                return new Segue(cell.LandID, cell.MapID, cell.Map.KarmaResX, cell.Map.KarmaResY, cell.Map.KarmaResZ, 0);
                //return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return new Segue(cell.LandID, cell.MapID, cell.Map.KarmaResX, cell.Map.KarmaResY, cell.Map.KarmaResZ, 0);
                //return null;
            }
        }

        public static Segue GetDownSegue(Cell cell)
        {
            try
            {
                int[] zPlanes = new int[cell.Map.ZPlanes.Count];
                cell.Map.ZPlanes.CopyTo(zPlanes);
                Array.Sort(zPlanes); // sorts in ascending order
                Array.Reverse(zPlanes); // reverse the array
                int a = 0;
                Cell targetCell = null;
                do
                {
                    if (zPlanes[a] < cell.Z)
                    {
                        targetCell = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X, cell.Y, zPlanes[a]);
                        if (targetCell != null)
                        {
                            if (targetCell.IsDownSegue)
                            {
                                return new Segue(targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z, cell.Z - targetCell.Z);
                            }

                            if(cell.DisplayGraphic == "%%")
                            {
                                if (Cell.GetCell(cell.FacetID, targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z) != null)
                                {
                                    return new Segue(targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z, cell.Z - targetCell.Z);
                                }
                                else
                                {
                                    Utils.Log("Failed to find a suitable Segue for air cell at Segue.GetDownSegue(Cell: " + cell.LandID + " " + cell.MapID + " " + cell.X + " " + cell.Y + " " + cell.Z + ")", Utils.LogType.SystemFailure);
                                    Utils.Log("Returned new Segue (KarmaRes)", Utils.LogType.SystemFailure);
                                    return new Segue(cell.LandID, cell.MapID, cell.Map.KarmaResX, cell.Map.KarmaResY, cell.Map.KarmaResZ, 0);
                                }
                            }

                            // search for a suitable segue
                            for (int xpos = 0; xpos <= 1; xpos++)
                            {
                                for (int ypos = 0; ypos <= 1; ypos++)
                                {
                                    Cell searchCell = Cell.GetCell(targetCell.FacetID, targetCell.LandID, targetCell.MapID, targetCell.X + xpos, targetCell.Y + ypos, targetCell.Z);
                                    if (searchCell != null && searchCell.IsDownSegue)
                                    {
                                        return new Segue(searchCell.LandID, searchCell.MapID, searchCell.X, searchCell.Y, searchCell.Z, cell.Z - searchCell.Z);
                                    }
                                    
                                    searchCell = Cell.GetCell(targetCell.FacetID, targetCell.LandID, targetCell.MapID, targetCell.X - xpos, targetCell.Y - ypos, targetCell.Z);
                                    if (searchCell != null && searchCell.IsDownSegue)
                                    {
                                        return new Segue(searchCell.LandID, searchCell.MapID, searchCell.X, searchCell.Y, searchCell.Z, cell.Z - searchCell.Z);
                                    }
                                }
                            }
                        }
                    }
                    a++;
                }
                while (a < zPlanes.Length);
                if (targetCell != null)
                {
                    return new Segue(targetCell.LandID, targetCell.MapID, targetCell.X, targetCell.Y, targetCell.Z, targetCell.Z - cell.Z);
                }
                Utils.Log("Failed to find a suitable Segue at Segue.GetDownSegue(Cell: " + cell.LandID + " " + cell.MapID + " " + cell.X + " " + cell.Y + " " + cell.Z + ")", Utils.LogType.SystemFailure);
                Utils.Log("Returned new Segue (KarmaRes)", Utils.LogType.SystemFailure);
                return new Segue(cell.LandID, cell.MapID, cell.Map.KarmaResX, cell.Map.KarmaResY, cell.Map.KarmaResZ, 0);
                //return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return new Segue(cell.LandID, cell.MapID, cell.Map.KarmaResX, cell.Map.KarmaResY, cell.Map.KarmaResZ, 0);
                //return null;
            }
        }
        #endregion
    }
}

