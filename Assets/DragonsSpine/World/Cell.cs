using System;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine
{
    /// <summary>
    /// This class is used for every map location in the game
    /// the whole idea is to keep this as small as we can since
    /// there will be thousands of these things
    /// </summary>
    public class Cell
    {
        #region Private Data
        short facet = 0;
        short land = 0; // 16 bits
        short map = 0; // 16 bits
        short xcord = 0; // 16 bits
        short ycord = 0; // 16 bits
        int zcord = 0; // 32 bits
        Segue segue = null;
        bool alwaysDark = false; // true if cell has permanent darkness
        bool ancestorFinish = false;	// true if cell allows completion of ancestoring process
        bool ancestorStart = false; // true if cell begins ancestoring process
        string cellGraphic = "  ";
        string description = ""; // text description shown to players when entering cell TODO: add description off/on toggle
        string displayGraphic = "  ";
        bool lair = false; // true if the janitor will ignore this cell
        bool lockedHorizontalDoor = false;
        bool lockedVerticalDoor = false;
        bool locker = false; // true if this is a lockers cell
        bool magicDead = false; // true if magic does not work
        bool mapPortal = false;	// true if this is a map portal cell
        bool mirror = false; // true if this cell has a mirror
        bool noRecall = false; // true if cell does not allow setting recall or recall from
        bool outOfBounds = false; // true if this cell was marked out of bounds during map load
        bool pvpEnabled = false; // true if PvP enabled (no mark or karma penalties)
        bool secretDoor = false; // true if this cell is a secret door
        bool singleCustomer = false;	// true if only one player is allowed in this cell at a time
        bool teleport = false; // true if this is a teleport cell (uses this cell's CellLock for access)
        bool underworldPortal = false; // true if cell allows access to the Underworld
        bool withinTownLimits = false;	// true if within town limits (terrain spells cast here by lawfuls make you neutral)
        bool outdoors = false; // true if is outdoor
        bool mailbox = false; // true if player can send and receive mail from this cell
        List<int> szList = new List<int>();
        private Dictionary<Effect.EffectType, Effect> m_effectsDict = new Dictionary<Effect.EffectType, Effect>();
        private List<Character> m_charactersList = new List<Character>(); // collection of Character objects in this cell
        private List<Item> m_itemsList = new List<Item>(); // collection of Item objects in this cell
        private List<GameObjects.GameObject> m_objectList = new List<DragonsSpine.GameObjects.GameObject>();
        #endregion

        #region Public Properties
        public short FacetID
        {
            get { return this.facet; }
            set { this.facet = value; }
        }
        public Facet Facet
        {
            get
            {
                return World.GetFacetByID(this.FacetID);
            }
        }
        public short LandID
        {
            get { return this.land; }
            set { this.land = value; }
        }
        public Land Land
        {
            get
            {
                return World.GetFacetByID(this.FacetID).GetLandByID(this.LandID);
            }
        }
        public short MapID
        {
            get { return this.map; }
            set { this.map = value; }
        }
        public Map Map
        {
            get
            {
                return World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID);
            }
        }
        public short X
        {
            get { return this.xcord; }
            set { this.xcord = value; }
        }
        public short Y
        {
            get { return this.ycord; }
            set { this.ycord = value; }
        }
        public int Z
        {
            get { return this.zcord; }
            set { this.zcord = value; }
        }
        public const string GRAPHIC_WATER = "~~";
        public const string GRAPHIC_AIR = "%%";
        public const string GRAPHIC_WEB = "ww";
        public const string GRAPHIC_CLOSED_DOOR_HORIZONTAL = "--";
        public const string GRAPHIC_OPEN_DOOR_HORIZONTAL = "\\ ";
        public const string GRAPHIC_CLOSED_DOOR_VERTICAL = "| ";
        public const string GRAPHIC_OPEN_DOOR_VERTICAL = "/ ";
        public const string GRAPHIC_ICE = "~.";
        public const string GRAPHIC_FIRE = "**";
        public const string GRAPHIC_WALL = "[]";
        public const string GRAPHIC_MOUNTAIN = "/\\";
        public const string GRAPHIC_IMPASSABLE_TREE = "TT";
        public const string GRAPHIC_SECRET_DOOR = "SD";
        public const string GRAPHIC_SECRET_MOUNTAIN = "SM";
        public const string GRAPHIC_LOCKED_DOOR_HORIZONTAL = "HD";
        public const string GRAPHIC_LOCKED_DOOR_VERTICAL = "VD";
        public const string GRAPHIC_COUNTER = "==";
        public const string GRAPHIC_COUNTER_PLACEABLE = "CC";
        public const string GRAPHIC_ALTAR = "mm";
        public const string GRAPHIC_ALTAR_PLACEABLE = "MM";
        public const string GRAPHIC_REEF = "WW";
        public const string GRAPHIC_GRATE = "##";
        public const string GRAPHIC_EMPTY = ". ";
        public string CellGraphic
        {
            get { return this.cellGraphic; }
            set { this.cellGraphic = value; }
        }
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }
        public string DisplayGraphic
        {
            get { return this.displayGraphic; }
            set
            {
                this.displayGraphic = value;
                if (DragonsSpineMain.ServerStatus == DragonsSpineMain.ServerState.Running)
                    this.Map.UpdateCellVisible(this);
            }
        }
        public bool HasMirror
        {
            get { return this.mirror; }
            set { this.mirror = value; }
        }
        public bool IsAlwaysDark
        {
            get { return this.alwaysDark; }
            set { this.alwaysDark = value; }
        }
        public bool IsAncestorFinish
        {
            get { return this.ancestorFinish; }
            set { this.ancestorFinish = value; }
        }
        public bool IsAncestorStart
        {
            get { return this.ancestorStart; }
            set { this.ancestorStart = value; }
        }
        public bool IsDownSegue
        {
            get
            {
                if (this.CellGraphic == "up")
                {
                    return true;
                }
                if (this.oneHandClimbUp || this.IsTwoHandClimbUp)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsLair
        {
            get { return this.lair; }
            set { this.lair = value; }
        }
        public bool IsLockedHorizontalDoor
        {
            get { return this.lockedHorizontalDoor; }
            set { this.lockedHorizontalDoor = value; }
        }
        public bool IsLockedVerticalDoor
        {
            get { return this.lockedVerticalDoor; }
            set { this.lockedVerticalDoor = value; }
        }
        public bool IsLocker
        {
            get { return this.locker; }
            set { this.locker = value; }
        }
        public bool IsMagicDead
        {
            get { return this.magicDead; }
            set { this.magicDead = value; }
        }
        public bool IsMapPortal
        {
            get { return this.mapPortal; }
            set { this.mapPortal = value; }
        }
        public bool IsNoRecall
        {
            get { return this.noRecall; }
            set { this.noRecall = value; }
        }
        public bool IsOpenDoor
        {
            get
            {
                if (this.DisplayGraphic == "\\ " || this.CellGraphic == "\\ ")
                {
                    return true;
                }
                else if (this.DisplayGraphic == "/ " || this.CellGraphic == "/ ")
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsOutOfBounds
        {
            get { return this.outOfBounds; }
            set { this.outOfBounds = value; }
        }
        public bool IsPVPEnabled
        {
            get { return this.pvpEnabled; }
            set { this.pvpEnabled = value; }
        }
        public bool IsSecretDoor
        {
            get { return this.secretDoor; }
            set { this.secretDoor = value; }
        }
        public bool IsSingleCustomer
        {
            get { return this.singleCustomer; }
            set { this.singleCustomer = value; }
        }
        public bool IsTeleport
        {
            get { return this.teleport; }
            set { this.teleport = value; }
        }
        public bool IsUnderworldPortal
        {
            get { return this.underworldPortal; }
            set { this.underworldPortal = value; }
        }
        public bool IsUpSegue
        {
            get
            {
                if (this.CellGraphic == "dn")
                {
                    return true;
                }
                if (this.IsOneHandClimbDown || this.IsTwoHandClimbDown)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsWithinTownLimits
        {
            get { return this.withinTownLimits; }
            set { this.withinTownLimits = value; }
        }
        public Segue Segue
        {
            get { return this.segue; }
            set { this.segue = value; }
        }
        public List<int> SpawnZoneList
        {
            get { return this.szList; }
        }
        public bool IsOutdoors
        {
            get { return outdoors; }
            set { outdoors = value; }
        }
        public bool HasMailbox
        {
            get { return mailbox; }
            set { mailbox = value; }
        }
        public Dictionary<Effect.EffectType, Effect> Effects
        {
            get { return m_effectsDict; }
        }
        public List<Character> Characters
        {
            get { return m_charactersList; }
        }
        public List<Item> Items
        {
            get { return m_itemsList; }
        }
        #endregion

        public CellLock cellLock = new CellLock(); // string matching key needed to access cell

        #region Constructors (3)
        public Cell(short facetID, short landID, short mapID, short x, short y, int z, string graphic)
        {
            this.LandID = landID;
            this.MapID = mapID;
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.CellGraphic = graphic;
            this.DisplayGraphic = graphic;
        }

        public Cell(short land, short map, short xcord, short ycord, string graphic)
        {
            this.LandID = land;
            this.MapID = map;
            this.X = xcord;
            this.Y = ycord;
            this.CellGraphic = graphic;
            this.DisplayGraphic = graphic;
        }

        public Cell(System.Data.DataRow dr)
        {
            this.X = Convert.ToInt16(dr["xCord"]);
            this.Y = Convert.ToInt16(dr["yCord"]);
            this.Z = Convert.ToInt32(dr["zCord"]);
            if (dr["segue"].ToString() != "")
            {
                string[] segueInfo = dr["segue"].ToString().Split("|".ToCharArray());
                this.segue = new Segue(Convert.ToInt16(segueInfo[0]), Convert.ToInt16(segueInfo[1]), Convert.ToInt16(segueInfo[2]),
                    Convert.ToInt16(segueInfo[3]), Convert.ToInt32(segueInfo[4]), Convert.ToInt32(segueInfo[5]));
            }
            this.description = dr["description"].ToString();
            if (dr["lock"].ToString() == "")
            {
                this.cellLock = new Cell.CellLock();
            }
            else
            {
                this.cellLock = new Cell.CellLock(dr["lock"].ToString());
            }
            this.mapPortal = Convert.ToBoolean(dr["portal"]);
            this.teleport = Convert.ToBoolean(dr["teleport"]);
            this.singleCustomer = Convert.ToBoolean(dr["singleCustomer"]);
            this.pvpEnabled = Convert.ToBoolean(dr["pvpEnabled"]);
            this.mailbox = Convert.ToBoolean(dr["mailbox"]);
        }
        #endregion

        public class CellLock // a class that restricts movement into a Cell object
        {
            public enum LockType { None, Door, ClassType, DailyCycle, LunarCycle, Level, Key, Flag, Alignment }

            #region Constructors (2)
            public CellLock()
            {

            }

            // lock types | class types | daily cycles | lunar cycles | level requirements
            public CellLock(string cellLockInfo)
            {
                int a;
                string[] lockInfo = cellLockInfo.Split("|".ToCharArray());
                #region Lock Types
                string[] lockTypesArray = lockInfo[0].Split(",".ToCharArray());
                this.lockTypes = new LockType[lockTypesArray.Length];
                for (a = 0; a < lockTypesArray.Length; a++)
                {
                    this.lockTypes[a] = (LockType)Enum.Parse(typeof(LockType), lockTypesArray[a], true);
                }
                #endregion
                #region Class Types Allowed
                if (lockInfo.Length > 1 && lockInfo[1] != "-1") // -1 means all classes allowed
                {
                    string[] classesAllowed = lockInfo[1].Split(",".ToCharArray());
                    this.classTypes = new Character.ClassType[classesAllowed.Length];
                    for (a = 0; a < classesAllowed.Length; a++)
                    {
                        this.classTypes[a] = (Character.ClassType)Enum.Parse(typeof(Character.ClassType), classesAllowed[a], true);
                    }
                }
                #endregion
                #region Daily Cycles Allowed
                if (lockInfo.Length > 2 && lockInfo[2] != "-1") // -1 means all daily cycles
                {
                    string[] dailyCyclesAllowed = lockInfo[2].Split(",".ToCharArray());
                    this.dailyCycles = new World.DailyCycle[dailyCyclesAllowed.Length];
                    for (a = 0; a < dailyCyclesAllowed.Length; a++)
                    {
                        this.dailyCycles[a] = (World.DailyCycle)Enum.Parse(typeof(World.DailyCycle), dailyCyclesAllowed[a], true);
                    }
                }
                #endregion
                #region Lunar Cycles Allowed
                if (lockInfo.Length > 3 && lockInfo[3] != "-1")
                {
                    string[] lunarCyclesAllowed = lockInfo[3].Split(",".ToCharArray());
                    this.lunarCycles = new World.LunarCycle[lunarCyclesAllowed.Length];
                    for (a = 0; a < lunarCyclesAllowed.Length; a++)
                    {
                        this.lunarCycles[a] = (World.LunarCycle)Enum.Parse(typeof(World.LunarCycle), lunarCyclesAllowed[a], true);
                    }
                }
                #endregion
                #region Level Requirements
                if (lockInfo.Length > 4 && Convert.ToInt32(lockInfo[4]) > 0)
                { this.minimumLevel = Convert.ToInt32(lockInfo[4]); }
                if (lockInfo.Length > 5 && Convert.ToInt32(lockInfo[5]) > 0)
                { this.maximumLevel = Convert.ToInt32(lockInfo[5]); }
                #endregion
                #region Other Optional Requirements
                if (lockInfo.Length > 6)
                {
                    this.key = lockInfo[6]; // also used to hold flag information
                }
                if (lockInfo.Length > 7)
                {
                    this.heldKey = Convert.ToBoolean(lockInfo[7]);
                }
                if (lockInfo.Length > 8)
                {
                    this.wornKey = Convert.ToBoolean(lockInfo[8]);
                }
                if (lockInfo.Length > 9)
                {
                    this.lockSuccess = lockInfo[9];
                }
                if (lockInfo.Length > 10)
                {
                    this.lockFailureString = lockInfo[10];
                }
                if (lockInfo.Length > 11)
                {
                    this.lockFailureXCord = Convert.ToInt16(lockInfo[11]);
                }
                if (lockInfo.Length > 12)
                {
                    this.lockFailureYCord = Convert.ToInt16(lockInfo[12]);
                }
                if (lockInfo.Length > 13)
                {
                    this.lockFailureEffect = lockInfo[13];
                }
                if (lockInfo.Length > 14)
                {
                    string[] alignments = lockInfo[14].Split(",".ToCharArray());
                    this.alignmentTypes = new Globals.eAlignment[alignments.Length];
                    for (a = 0; a < alignments.Length; a++)
                    {
                        this.alignmentTypes[a] = (Globals.eAlignment)Enum.Parse(typeof(Globals.eAlignment), alignments[a], true);
                    }
                }
                if (lockInfo.Length > 15)
                {
                    this.lockFailureZCord = Convert.ToInt32(lockInfo[15]);
                }
                #endregion
            }
            #endregion

            #region Public Data
            public LockType[] lockTypes = new LockType[] { LockType.None };
            public Character.ClassType[] classTypes = new Character.ClassType[] { Character.ClassType.None };
            public World.DailyCycle[] dailyCycles = new World.DailyCycle[] { World.DailyCycle.Morning, World.DailyCycle.Afternoon,
                World.DailyCycle.Evening, World.DailyCycle.Night };
            public World.LunarCycle[] lunarCycles = new World.LunarCycle[] { World.LunarCycle.Full, World.LunarCycle.New,
                World.LunarCycle.Waning_Crescent, World.LunarCycle.Waxing_Crescent };
            public int minimumLevel = 0;
            public int maximumLevel = 10000;
            public string key = "";
            public bool heldKey = false;
            public bool wornKey = false;
            public string lockSuccess = "";
            public string lockFailureString = "";
            public int lockFailureXCord = -1;
            public int lockFailureYCord = -1;
            public string lockFailureEffect = ""; //TODO
            public Globals.eAlignment[] alignmentTypes = new Globals.eAlignment[] { Globals.eAlignment.All };
            public int lockFailureZCord = -1;
            #endregion
        }

        public bool balmBerry = false; // true if balm berries spawn on this cell
        public bool manaBerry = false; // true if mana berries spawn on this cell
        public bool poisonBerry = false; // true if poison berries spawn on this cell
        public bool stamBerry = false; // true if stamina berries spawn on this cell
        public int dailyBerry = 1; // the amount of berries that will drop here each game day
        public int droppedBerry = 0; // the number of times in the day berries have been picked

        #region Climb Properties
        bool oneHandClimbDown = false;
        public bool IsOneHandClimbDown
        {
            get { return this.oneHandClimbDown; }
            set { this.oneHandClimbDown = value; }
        }
        bool oneHandClimbUp = false;
        public bool IsOneHandClimbUp
        {
            get { return this.oneHandClimbUp; }
            set { this.oneHandClimbUp = value; }
        }
        bool twoHandClimbDown = false;
        public bool IsTwoHandClimbDown
        {
            get { return this.twoHandClimbDown; }
            set { this.twoHandClimbDown = value; }
        }
        bool twoHandClimbUp = false;
        public bool IsTwoHandClimbUp
        {
            get { return this.twoHandClimbUp; }
            set { this.twoHandClimbUp = value; }
        }
        #endregion

        public BitArray visCells = new BitArray(49, true);	// create our visible cells array, default to true

        public void Add(Character ch)
        {
            m_charactersList.Add(ch);
        }
        public void Add(GameObjects.GameObject ob)
        {
            m_objectList.Add(ob);
        }
        public void Add(Item item)
        {
            // check if we are on a garbage can cell
            if (this.CellGraphic == "o ")
            {
                if (item.itemType == Globals.eItemType.Corpse)
                {
                    PC pc = PC.GetOnline(item.special);
                    if (pc != null)
                    {
                        Rules.DeadRest((Character)pc);
                    }
                }
                return;
            }

            if (item.itemType == Globals.eItemType.Coin)
            {
                foreach (Item addItem in this.Items)
                {
                    if (addItem.itemType == Globals.eItemType.Coin)
                    {
                        addItem.coinValue += item.coinValue;
                        addItem.dropRound = DragonsSpineMain.GameRound;
                        return;
                    }
                }
            }

            item.dropRound = DragonsSpineMain.GameRound;
            item.facet = this.FacetID;
            item.land = this.LandID;
            item.map = this.MapID;
            item.xcord = this.X;
            item.ycord = this.Y;
            item.zcord = this.Z;

            m_itemsList.Add(item);
        }

        public void Add(Effect effect)
        {
            m_effectsDict.Add(effect.effectType, effect);
        }

        public void Add(SpawnZone spawnZone)
        {
            this.szList.Add(spawnZone.ZoneID);
        }

        public void Remove(Character ch)
        {
            m_charactersList.Remove(ch);
        }
        public void Remove(GameObjects.GameObject ob)
        {
            m_objectList.Remove(ob);
        }

        public void Remove(Item item)
        {
            m_itemsList.Remove(item);
        }

        public void Remove(Effect.EffectType effectType)
        {
            m_effectsDict.Remove(effectType);
        }

        public void Remove(SpawnZone spawnZone)
        {
            szList.Remove(spawnZone.ZoneID);
        }

        public bool ContainsPlayerCorpse(string playerName)
        {
            foreach (Item item in Items)
            {
                if (item.itemType == Globals.eItemType.Corpse && item.special == playerName)
                    return true;
            }
            return false;
        }

        public void showMapOldKesProto(Character ch)
        {

            string mapstring = Map.KP_PICTURE_TERRAIN_UPDATE;
            string updatestring = Map.KP_CLEAN_ACTIVE + Map.KP_PICTURE_ICON_UPDATE + ch.dirPointer;
            string[] LETTER = new string[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q",
											  "R","S","T","U","V","W","X","Y","Z"};
            string[] Align = new string[] { "?", " ", "!", "*", "+", " " };
            int counter = 0;

            #region Client Layout
            //
            // On a redraw everything is sent
            // EX:
			// =1BY4  =1BY  =1BJ=1BY5=3DHits       : =1BY5J   153=1BY5RHits Taken   : =1BY5a     0=1BY6=3DExperience : =1BY6I8669670
			// =1BY6RMagic Points : =1BY6a    47=1BY7=3DStamina    : =1BY7J    69=1BY6 R =1BY7 L =1BUv1!A1!A1!A1!A8!E=7F
			// =1BP)!1a012a2!a!!!a724127b747d747=7F=1BY 9A djinn=1BY!9A Whumper=1BY"9A Y.Ironuzak.Sea=1BY#9A demon
			// =1BY$9djinn=1BY$fplate=1BMTo the south you hear a toad croaking. =1BL=1BUv1!A1!A1!A1!A8!E=7F=1BP=7F=1BY4 >=1BK s; l up" "!
			//
			// Otherwise only the changed items are sent.
			// EX:
			// =1BY4  =1BMYou hear a toad croaking. =1BY6a    50=1BL=1BUv1!A1!A1!A=7F=1BPx6fFeUUU=7F=1BY 9A djinn=1BY @=1BK=1BY!9A djinn
			// =1BY!@=1BK=1BY"9A toad=1BY"?=1BK=1BY4 >=1BKl up

            // Clear screen = ESC-Y(char)32(char)32 ESC-J (set pos 1,1 and send clear)

            // ESC-Y <l><c> = (char)31+<line/col> (char)32+(char)32 = pos 1,1
            // As you can see, ESC-Y+(char)52+(char)32 is sent every round in the begining.
            // this places the active line at 21,1
            //
            // Hits: Line is at (char)53(char)61 (53-31=22, 61-31=30 == 22,30)
            // #ofHP: Line is at (char)53(char)74 (53-31=22, 74-31=43 == 22,43) (6 chars long)
            // Hits Taken line is at (char)53(char)82 (53-31=22, 82-31=51 == 22,51)
            // #ofHP taken line is at (char)53(char)97 (53-31=22, 97-31=66 == 22,66) (6 chars long)
            // ExpText Line is at (char)54(char)61 (54-31=23, 61-31=30 == 23,30)
            // Exp# Line is at (char)54(char)73 (54-31=23, 73-31=42 == 23,42) (7 chars long)
            // MPText Line is at (char)54(char)82 (54-31=23, 82-31=51 == 23,51)
            // MP# Line is at (char)54(char)97 (54-31=23, 97-31=66 == 23,66) (6 chars long)
            // StamText Line is at (char)55(char)61 (55-31=24, 61-31=30 == 24,30)
            // Stam# Line is at (char)55(char)74 (55-31=24, 74-31=43 == 24,43) (6 chars long)
            // Rtext Line is at (char)54(char)32 (54-31=23, 32-31=1 == 23,1)
            // LText Line is at (char)55(char)32 (55-31=24, 32-31=1 == 24,1)
            // Update String: ESC-U<pointer><string><(char)127>
            // Map String: ESC-P<chars for pos 1,1 - 7,7> string is trunc'd at 7,7 --- P's
            // Contact lines (critter lines): start at (char)32(char)57 (32-31=1, 57-31=26 == 1,26)
            // col 71 for armor of mob on same cell as player
            // col 41 for right hand item
            // col 56 for left hand item
            //
            // Message Line - ESC-L at end of all messages or on blank message
            // Set pos 21,1 ESC-Y(char)52(char)32 > ESC-K

            // Client Screen Layout
            // 0.........1.........2.........3.........4.........5.........6.........7.........8.........9
            // 1PPPPPPPPPPPPPP.....2.....A djinn.......4.........5.........6.........7.........8.........9
            // 2PPPPPPPPPPPPPP.....2.....A Whumper.....4.........5.........6.........7.........8.........9
            // 3PPPPPPPPPPPPPP.....2.....A Y.Ironuzak.Sea........5.........6.........7.........8.........9
            // 4PPPPPPPPPPPPPP.....2.....A demon.......4.........5.........6.........7.........8.........9
            // 5PPPPPPPPPPPPPP.....2.....djinn.........4RhandItem5.....LeftHandItem..7plate....8.........9
            // 6PPPPPPPPPPPPPP.....2.........3.........4.........5.........6.........7.........8.........9
            // 7PPPPPPPPPPPPPP.....2.........3.........4.........5.........6.........7.........8.........9
            // 8.........1.........2.........3.........4.........5.........6.........7.........8.........9
            // 9.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //10.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //11.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //12.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //13.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //14.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //15.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //16.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //17.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //18.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //19.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //20.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //21> First Message Line (Active line set at each new round)...6.........7.........8.........9
            //22.........1.........2.........Hits       : ######.5Hits Taken   : ######........8.........9
            //23R .......1.........2.........Experience :#######.5Magic Points : ######........8.........9
            //24L .......1.........2.........Stamina    : ######.5.........6.........7.........8.........9
            //25.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //26.........1.........2.........3.........4.........5.........6.........7.........8.........9
            //27.........1.........2.........3.........4.........5.........6.........7.........8.........9
            #endregion

            if (ch.IsPeeking)
            {
                if (ch.CurrentCell != ch.PeekTarget.CurrentCell)
                {
                    ch.CurrentCell = ch.PeekTarget.CurrentCell;
                }
            }

            Cell[] cellArray = Cell.GetVisibleCellArray(ch.CurrentCell, 3);

            #region Create the Map String <ESC> P <tstr>
            //int skip = 0;
            if (ch.updateMap)
            {
                for (int j = 0; j < 49; j++)
                {
                    if (cellArray[j] == null || this.visCells[j] == false)
                    {
                        mapstring += Map.KPNOVIS;
                        //skip++;
                    }
                    else
                    {
                        //if(skip !=0)
                        //{
                        //	mapstring += (char)(96+skip);
                        //}
                        //skip = 0;
                        if (cellArray[j].Effects.Count > 0)
                        {
                            if (cellArray[j].IsAlwaysDark)
                            {
                                if (ch.HasNightVision)
                                {
                                    mapstring += Map.kesProtoConvertGraphic(cellArray[j].DisplayGraphic);
                                }
                                else
                                {
                                    mapstring += Map.KPDARKNESS;
                                }
                            }
                            else if (ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                                    ch.CurrentCell.IsAlwaysDark)
                            {
                                if (ch.HasNightVision)
                                {
                                    mapstring += Map.kesProtoConvertGraphic(cellArray[j].DisplayGraphic);
                                }
                                else
                                {
                                    mapstring += Map.KPDARKNESS;
                                }
                            }
                            else
                            {
                                mapstring += Map.kesProtoConvertGraphic(cellArray[j].DisplayGraphic);
                            }
                        }
                        else
                        {
                            if (cellArray[j].IsAlwaysDark)
                            {
                                if (ch.HasNightVision)
                                {
                                    mapstring += Map.kesProtoConvertGraphic(cellArray[j].CellGraphic);
                                }
                                else
                                {
                                    mapstring += Map.KPDARKNESS;
                                }
                            }
                            else if (cellArray[j].Effects.ContainsKey(Effect.EffectType.Darkness) || cellArray[j].IsAlwaysDark)
                            {
                                if (ch.HasNightVision)
                                {
                                    mapstring += Map.kesProtoConvertGraphic(cellArray[j].CellGraphic);
                                }
                                else
                                {
                                    mapstring += Map.KPDARKNESS;
                                }
                            }
                            else
                            {
                                mapstring += Map.kesProtoConvertGraphic(cellArray[j].CellGraphic);
                            }
                        }
                    }

                }
            }
            mapstring += (char)127;
            #endregion
            #region Create the Message String (ESC-M+<Message>)
            string curMessage = ch.getDisplayText();

            string messageString = "";
            string newText = "";
            if (curMessage == "")
            {
                messageString = Map.KP_NEXT_LINE + Map.KP_CLEAN_ACTIVE;
            }
            else
            {
                string[] messageArray = curMessage.Split("\n\r".ToCharArray());
                foreach (string mText in messageArray)
                {
                    newText = "";
                    if (mText != "")
                    {
                        if (mText.Length > 65)
                        {
                            newText = mText.Insert(mText.LastIndexOf(" ", 65), Map.KP_NEXT_LINE);
                            newText = newText.Insert(0, Map.KP_NEXT_LINE);
                        }
                        else
                        {
                            newText = mText.Insert(0, Map.KP_NEXT_LINE);
                        }
                    }
                    messageString += newText;
                    ch.ClearDisplay();
                }
            }
            #endregion
            #region Create the Update String <ESC> U <dir> <istr> <end> Picture Icon Update String
            //
            // Update Map String (ESC-U <dir> <iStr> <end>
            // This function is used to send the critters and treasure data
            // for the picture.  The <dir> bytes is the player course arrow.
            // <istr> is composed of an arbitary number of three character
            // packets that are:
            // byte 1 : picture position 31 + (1..49)
            // byte 2 : icon number
            //          treasure pile    = 32
            //          a critter        = 33
            //          several critters = 34
            // (this will be expanded in the future to indicate
            // the body type of the critter.  The number can be
            // taken out of the display list)
            // byte 3 : display character A..L
            // The end of the string <end> is an ascii 127
            //
            int mobnum = 0;
            for (int j = 0; j < 49; j++)
            {
                if (cellArray[j] == null || this.visCells[j] == false)
                {
                    // do nothing
                }
                else
                {
                    if (cellArray[j].Characters.Count == 1)
                    {
                        if (cellArray[j] != ch.CurrentCell)
                        {
                            updatestring += (char)(32 + j);
                            updatestring += (char)33;
                            updatestring += LETTER[mobnum];
                            mobnum++;
                        }
                    }
                    else if (cellArray[j].Characters.Count > 1)
                    {
                        if (cellArray[j] != ch.CurrentCell)
                        {
                            updatestring += (char)(32 + j);
                            updatestring += (char)34;
                            updatestring += LETTER[mobnum];
                            mobnum++;
                        }


                    }
                    if (cellArray[j].Items.Count > 0)
                    {
                        updatestring += (char)(32 + j);
                        updatestring += (char)32 + "$";
                    }
                }
            }
            updatestring += (char)127;
            #endregion
            #region Create the Critter Listing <ESC>Y
            mobnum = 0;
            string critstring = "";
            int alignnum = 0;
            counter = 0;
            for (int x = 1; x < 10; x++)
            {
                critstring += Map.KP_DIRECT_CURS;
                critstring += (char)(31 + x);
                critstring += (char)56;
                critstring += Map.KP_ERASE_END_LINE;
            }
            for (int j = 0; j < 49; j++)
            {
                if (cellArray[j] == null || this.visCells[j] == false)
                {
                    // do nothing
                }
                else
                {
                    if (cellArray[j].Characters.Count > 0)
                    {
                        if (cellArray[j] != ch.CurrentCell)
                        {
                            #region Not on the character's cell
                            if (cellArray[j].Characters.Count == 1)
                            {
                                #region Only 1 mob in the cell
                                foreach (Character mob in cellArray[j].Characters)
                                {
                                    alignnum = (int)mob.Alignment;
                                    if (mob.BaseProfession == Character.ClassType.Thief) { alignnum = 0; }
                                    if (mob.BaseProfession == Character.ClassType.Thief && ch.BaseProfession != Character.ClassType.Knight)
                                    {
                                        if (ch.Level > Skills.GetSkillLevel(mob.magic)) { alignnum = (int)mob.Alignment; }
                                    }
                                    if (mob.IsHidden)
                                    {
                                        #region Mob is hidden
                                        if (Skills.GetSkillLevel(mob.magic) < ch.Level - 3)
                                        {
                                            if (Map.findTargetInNextCells(mob, ch.Name) != null && Skills.GetSkillLevel(mob.magic) < ch.Level)
                                            {
                                                critstring += Map.KP_DIRECT_CURS;
                                                critstring += (char)(32 + mobnum);
                                                critstring += (char)56;
                                                critstring += Align[alignnum] + LETTER[counter] + " " + mob.Name + Map.KP_ERASE_END_LINE;
                                                mobnum++;
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Mob is visible
                                        if (!mob.IsInvisible)
                                        {
                                            critstring += Map.KP_DIRECT_CURS;
                                            critstring += (char)(32 + mobnum);
                                            critstring += (char)56;
                                            critstring += Align[alignnum] + LETTER[counter] + " " + mob.Name + Map.KP_ERASE_END_LINE;
                                            mobnum++;
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region More than 1 mob in the cell
                                ArrayList npcs = new ArrayList();
                                ArrayList nums = new ArrayList();
                                ArrayList aligns = new ArrayList();
                                int num = 1;
                                string name = "";

                                Character mob;
                                for (int x = 0; x < cellArray[j].Characters.Count; x++)
                                {
                                    mob = cellArray[j].Characters[x];
                                    alignnum = (int)mob.Alignment;
                                    if (mob.BaseProfession == Character.ClassType.Thief) { alignnum = 0; }
                                    if (mob.BaseProfession == Character.ClassType.Thief && ch.BaseProfession != Character.ClassType.Knight)
                                    {
                                        if (ch.Level > Skills.GetSkillLevel(mob.magic)) { alignnum = (int)mob.Alignment; }
                                    }
                                    if (mob.IsHidden)
                                    {
                                        if (Skills.GetSkillLevel(mob.magic) < ch.Level - 3)
                                        {
                                            if (Map.findTargetInNextCells(mob, ch.Name) != null && Skills.GetSkillLevel(mob.magic) < ch.Level)
                                            {

                                                name = mob.Name;
                                                num = 1;
                                                if (npcs.Count > 0)
                                                {
                                                    int v = 0;
                                                    bool match = false;
                                                    while (v < npcs.Count)
                                                    {
                                                        if (npcs[v].ToString() == name)
                                                        {
                                                            nums[v] = (int)nums[v] + 1;
                                                            match = true;
                                                        }
                                                        v++;
                                                    }
                                                    if (match == false)
                                                    {
                                                        npcs.Add(name);
                                                        nums.Add(num);
                                                        aligns.Add(mob.Alignment);
                                                    }
                                                }
                                                else
                                                {
                                                    npcs.Add(name);
                                                    nums.Add(num);
                                                    aligns.Add(mob.Alignment);
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!mob.IsInvisible)
                                        {
                                            //
                                            name = mob.Name;
                                            num = 1;
                                            if (npcs.Count > 0)
                                            {
                                                int v = 0;
                                                bool match = false;
                                                while (v < npcs.Count)
                                                {
                                                    if (npcs[v].ToString() == name)
                                                    {
                                                        nums[v] = (int)nums[v] + 1;
                                                        match = true;
                                                    }
                                                    v++;
                                                }
                                                if (match == false)
                                                {
                                                    npcs.Add(name);
                                                    nums.Add(num);
                                                    aligns.Add(mob.Alignment);
                                                }
                                            }
                                            else
                                            {
                                                npcs.Add(name);
                                                nums.Add(num);
                                                aligns.Add(mob.Alignment);
                                            }
                                        }
                                    }
                                }
                                for (int i = 0; i < npcs.Count; i++)
                                {

                                    critstring += Map.KP_DIRECT_CURS;
                                    critstring += (char)(32 + mobnum);
                                    critstring += (char)56;
                                    if ((int)nums[i] > 1)
                                        critstring += Align[(int)aligns[i]] + LETTER[counter] + " " + nums[i] + " " + Multinames((string)npcs[i]) + Map.KP_ERASE_END_LINE;
                                    else
                                        critstring += Align[(int)aligns[i]] + LETTER[counter] + " " + npcs[i] + Map.KP_ERASE_END_LINE;
                                    mobnum++;
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region On the character's cell
                            int linenum = counter;
                            foreach (Character crit in cellArray[j].Characters)
                            {
                                if (crit != ch)
                                {
                                    if (!crit.IsInvisible)
                                    {
                                        alignnum = (int)crit.Alignment;
                                        if (crit.BaseProfession == Character.ClassType.Thief) { alignnum = 0; }
                                        if (crit.BaseProfession == Character.ClassType.Thief && ch.BaseProfession != Character.ClassType.Knight)
                                        {
                                            if (ch.Level > Skills.GetSkillLevel(crit.magic)) { alignnum = (int)crit.Alignment; }
                                        }
                                        critstring += Map.KP_DIRECT_CURS;
                                        critstring += (char)(32 + mobnum);
                                        critstring += (char)56;
                                        critstring += Align[alignnum] + crit.Name + Map.KP_ERASE_END_LINE; // Align + name
                                        critstring += Map.KP_DIRECT_CURS + ((char)(32 + linenum)); // ESC-Y <line>
                                        critstring += (char)(31 + 41) + crit.rightHandItemName() + Map.KP_ERASE_END_LINE; // Col + righthand
                                        critstring += Map.KP_DIRECT_CURS + ((char)(32 + linenum)); // ESC-Y <line>
                                        critstring += (char)(31 + 56) + crit.leftHandItemName() + Map.KP_ERASE_END_LINE; // Col + lefthand
                                        critstring += Map.KP_DIRECT_CURS + ((char)(32 + linenum)); // Esc-Y <Line>
                                        critstring += (char)(31 + 71) + crit.GetVisibleArmorName() + Map.KP_ERASE_END_LINE; // Col + Armor
                                        linenum++;
                                        mobnum++;
                                    }
                                }
                            }
                            #endregion
                        }
                        if (ch.CurrentCell != cellArray[j])
                        {
                            counter++;
                        }
                    }
                }

            }
            #endregion
            #region Create the Status Line strings
            // HitText Line 'Hits       : '
            string hitsTextLine = Map.KP_DIRECT_CURS + (char)53 + (char)61 + "Hits       : ";
            // HitsTakenText Line 'Hits Taken   : '
            string hitsTakenTextLine = Map.KP_DIRECT_CURS + (char)53 + (char)82 + "Hits Taken   : ";
            // ExpText Line 'Experience : '
            string ExpTextLine = Map.KP_DIRECT_CURS + (char)54 + (char)61 + "Experience : ";
            // MPText Line 'Magic Points : '
            string MPTextLine = Map.KP_DIRECT_CURS + (char)54 + (char)82 + "Magic Points : ";
            // StamText Line 'Stamina    : '
            string stamTextLine = Map.KP_DIRECT_CURS + (char)55 + (char)61 + "Stamina    : ";
            #region Setup the desired lengths
            // Max hits
            string hitsMax = ch.HitsFull.ToString();
            while (hitsMax.Length < 6)
            {
                hitsMax = hitsMax.Insert(0, " ");
            }
            //hitsLeft
            int hTn = ch.HitsFull - ch.Hits;
            string hitsLeft = hTn.ToString();
            while (hitsLeft.Length < 6)
            {
                hitsLeft = hitsLeft.Insert(0, " ");
            }
            //stamMax
            string stamMax = ch.StaminaFull.ToString();
            while (stamMax.Length < 6)
            {
                stamMax = stamMax.Insert(0, " ");
            }
            //stamLeft
            string stamLeft = ch.Stamina.ToString();
            while (stamLeft.Length < 6)
            {
                stamLeft = stamLeft.Insert(0, " ");
            }
            //manaMax
            string manaMax = ch.ManaFull.ToString();
            while (manaMax.Length < 6)
            {
                manaMax = manaMax.Insert(0, " ");
            }
            //manaLeft
            string manaLeft = ch.Mana.ToString();
            while (manaLeft.Length < 6)
            {
                manaLeft = manaLeft.Insert(0, " ");
            }
            //exp
            string chexp = updateExp(ch);
            while (chexp.Length < 7)
            {
                chexp = chexp.Insert(0, " ");
            }

            #endregion
            // numhits
            string numHits = Map.KP_DIRECT_CURS + (char)53 + (char)74 + hitsMax;
            // numhitstaken
            string numHitsTaken = Map.KP_DIRECT_CURS + (char)53 + (char)97 + hitsLeft;
            // numExp
            string numExp = Map.KP_DIRECT_CURS + (char)54 + (char)73 + chexp;
            // numMP
            string numMP = Map.KP_DIRECT_CURS + (char)54 + (char)97 + manaLeft;
            // numStam
            string numStam = Map.KP_DIRECT_CURS + (char)55 + (char)74 + stamLeft;
            // Right Hand
            string rHandText = Map.KP_DIRECT_CURS + (char)54 + (char)32 + "R ";
            // Left Hand
            string lHandText = Map.KP_DIRECT_CURS + (char)55 + (char)32 + "L ";
            string rHandItem = Map.KP_DIRECT_CURS + (char)54;
            rHandItem += (char)34 + ch.rightHandItemName();
            string lHandItem = Map.KP_DIRECT_CURS + (char)55;
            lHandItem += (char)34 + ch.leftHandItemName();

            #endregion
            #region Build the Kproto String
            string redraw = Map.KP_DIRECT_CURS + (char)32 + (char)32 + Map.KP_ERASE_TO_END;
            string setCur = Map.KP_DIRECT_CURS + (char)52 + (char)32 + " ";
            string chstatus = "";
            if (ch.Stunned > 0) { chstatus = " You are stunned."; }
            if (ch.IsBlind) { chstatus = " You are blind."; }
            if (ch.IsFeared) { chstatus = " You are afraid."; }
            string cmdline = Map.KP_DIRECT_CURS + ((char)(52)) + " >" + chstatus + Map.KP_ERASE_END_LINE;

            string kProtoString = setCur;

            if (ch.updateAll)
            {
                ch.updateExp = true;
                ch.updateLeft = true;
                ch.updateMap = true;
                ch.updateMP = true;
                ch.updateRight = true;
                ch.updateStam = true;
                kProtoString = setCur + redraw + hitsTextLine + hitsTakenTextLine + ExpTextLine +
                    MPTextLine + stamTextLine + rHandText + lHandText;
                ch.updateAll = false;
            }
            if (ch.updateExp)
            {
                kProtoString += numExp;
                //ch.updateExp = false;
            }
            if (ch.updateHits)
            {
                kProtoString += numHits;
                kProtoString += numHitsTaken;
                //ch.updateHits = false;
            }
            if (ch.updateMP)
            {
                kProtoString += numMP;
                //ch.updateMP = false;
            }
            if (ch.updateStam)
            {
                kProtoString += numStam;
                //ch.updateStam = false;
            }
            if (ch.updateRight)
            {
                kProtoString += rHandItem;
                //ch.updateRight = false;
            }
            if (ch.updateLeft)
            {
                kProtoString += lHandItem;
                //ch.updateLeft = false;
            }

            kProtoString += updatestring + mapstring + critstring + messageString + cmdline;
            #endregion

            #region Send the character the Kproto String

            ch.Write(kProtoString);
            //ch.clearDisplay();
            #endregion
            return;
        }

        public static string Multinames(string name)
        {
            if (name.ToLower() == "wolf")
            {
                return "wolves";
            }
            else if (name.ToLower().EndsWith("s"))
            {
                return name;
            }
            else
            {
                return name + "s";
            }
        }

        public void showMap(Character ch)
        {
            try
            {
                //IF this is an NPC, break outa here right away
                if (!ch.IsPC) { return; }

                // handle darkness and blindness
                if ((ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                     ch.CurrentCell.IsAlwaysDark) && !ch.HasNightVision)
                {
                    Map.clearMap(ch);
                }
                else if (ch.IsBlind)
                {
                    Map.clearMap(ch);
                }
                else
                {
                    showNormalDisplay(ch);
                }

                Map.writeAtXY(ch, 12, 6, ch.dirPointer);

                clearDisplayWindow(ch); // clear the text window from bottom up.

                // Show Displays
                updateRight(ch); // update the player status display
                updateLeft(ch);
                updateHits(ch);
                updateHitsLeft(ch);
                updateStam(ch);
                updateExp(ch);
                updateMagicPoints(ch);


                if (ch.Stunned == 0)
                {
                    if (ch.IsFeared)
                    {
                        Map.writeAtXY(ch, 1, 11, ch.getDisplayText());
                        ch.ClearDisplay();
                        Map.writeAtXY(ch, 1, 21, " -> You are scared!");
                        return;
                    }
                    if (ch.IsBlind)
                    {
                        ch.WriteToDisplay("You are blind!");
                    }
                    Map.writeAtXY(ch, 1, 11, ch.getDisplayText());
                    ch.ClearDisplay();
                    Map.writeAtXY(ch, 1, 21, " ->");
                    ch.Write(ch.getInputBuffer());
                    return;
                }
                else
                {
                    Map.writeAtXY(ch, 1, 11, ch.getDisplayText());
                    ch.ClearDisplay();
                    Map.writeAtXY(ch, 1, 21, " -> You are stunned!");
                    return;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void showNormalDisplay(Character ch)
        {
            string mapstring = "";
            string[] LETTER = new string[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q",
											  "R","S","T","U","V","W","X","Y","Z"};
            string[] Align = new string[] { "?", " ", "!", "*", "+", " " };
            Globals.eAlignment visibleAlign = Globals.eAlignment.Lawful; //used to determine if thieves are seen as neutral by knights
            int counter = 0;
            int writeX = 6;
            int writeY = 3;
            ch.Write(Map.CLS);

            ch.seenList.Clear();

            if (ch.IsPeeking)
            {
                if (ch.CurrentCell != ch.PeekTarget.CurrentCell)
                {
                    ch.CurrentCell = ch.PeekTarget.CurrentCell;
                }
            }

            Cell[] cellArray = Cell.GetVisibleCellArray(ch.CurrentCell, 3);

            #region  Print out the map
            for (int j = 0; j < 49; j++)
            {
                if (counter == 7)
                {
                    writeY++;
                    writeX = 6;
                    counter = 0;
                }
                if (cellArray[j] == null || this.visCells[j] == false)
                {
                    mapstring = "  ";
                }
                else
                {
                    if (cellArray[j].Effects.Count > 0)
                    {
                        mapstring = Map.returnTile(cellArray[j].DisplayGraphic);
                        if (ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                                    ch.CurrentCell.IsAlwaysDark)
                        {
                            if (!ch.HasNightVision)
                            {
                                mapstring = "  ";
                            }
                            else
                            {
                                mapstring = Map.returnTile(cellArray[j].CellGraphic);
                            }
                        }
                    }
                    else
                    {
                        mapstring = Map.returnTile(cellArray[j].CellGraphic);
                        if ((ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                                    ch.CurrentCell.IsAlwaysDark) && !ch.HasNightVision)
                        {
                            mapstring = "  ";
                        }
                    }
                }
                Map.writeAtXY(ch, writeX, writeY, mapstring);
                writeX += 2;
                counter++;
            }
            #endregion
            #region Print out any creatures in the cell
            int mobletter = 0;
            writeX = 6;
            writeY = 3;
            counter = 0;
            int line = 0;
            string updatestring = "";
            for (int j = 0; j < 49; j++)
            {
                updatestring = "";
                if (line == 7)
                {
                    writeX = 6;
                    writeY++;
                    line = 0;
                }
                if (cellArray[j] == null || this.visCells[j] == false)
                {
                    updatestring = "";
                }
                else
                {
                    if (cellArray[j].Characters.Count == 1)
                    {
                        #region Single Mob in cell
                        if (ch.CurrentCell != cellArray[j])
                        {
                            foreach (Character critter in cellArray[j].Characters)
                            {
                                visibleAlign = critter.Alignment;

                                if (critter.BaseProfession == Character.ClassType.Thief)
                                {
                                    if (!Rules.DetectAlignment(critter, ch))
                                    {
                                        visibleAlign = Globals.eAlignment.Lawful;
                                    }
                                }

                                if (Rules.DetectHidden(critter, ch) && Rules.DetectInvisible(critter, ch))
                                {
                                    updatestring = LETTER[mobletter];
                                    Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Name);
                                    counter++;
                                    mobletter++;
                                    ch.seenList.Add(critter);
                                }
                                //else
                                //{
                                //    if (!critter.IsInvisible)
                                //    {
                                //        updatestring = LETTER[mobletter];
                                //        Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Name);
                                //        counter++;
                                //        mobletter++;
                                //    }
                                //}
                            }
                        }
                        #endregion
                    }

                    if (cellArray[j].Characters.Count > 1)
                    {
                        #region More than one mob in the cell
                        if (ch.CurrentCell != cellArray[j])
                        {
                            #region Mobs not on characters cell
                            foreach (Character critter in cellArray[j].Characters)
                            {
                                if (critter.IsPC || critter.Group == null ||
                                    (critter.Group != null && critter.Group.GroupLeaderID == critter.worldNpcID))
                                {
                                    visibleAlign = critter.Alignment;
                                    if (critter.BaseProfession == Character.ClassType.Thief)
                                    {
                                        if (!Rules.DetectAlignment(critter, ch))
                                        {
                                            visibleAlign = Globals.eAlignment.Lawful;
                                        }
                                    }

                                    if (Rules.DetectHidden(critter, ch) && Rules.DetectInvisible(critter, ch))
                                    {
                                        updatestring = LETTER[mobletter];
                                        if (critter.Group == null || critter.IsPC)
                                        {
                                            Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Name);
                                            ch.seenList.Add(critter);
                                        }
                                        else
                                        {
                                            Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Group.GroupNPCList.Count.ToString() + " " + Cell.Multinames(critter.Name));
                                            ch.seenList.Add(critter);
                                        }
                                        counter++;
                                    }
                                    //else
                                    //{
                                    //    if (!critter.IsInvisible)
                                    //    {
                                    //        updatestring = LETTER[mobletter];
                                    //        if (critter.Group == null || critter.IsPC)
                                    //        {
                                    //            Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Name);
                                    //        }
                                    //        else
                                    //        {
                                    //            Map.writeAtXY(ch, 24, 2 + counter, Align[(int)visibleAlign] + LETTER[mobletter] + " " + critter.Group.GroupNPCList.Count.ToString() + " " + Cell.Multinames(critter.Name));
                                    //        }
                                    //        counter++;
                                    //    }
                                    //}
                                }
                            }
                            #endregion
                            mobletter++;
                        }
                        else
                        {
                            #region Mobs on characters cell
                            foreach (Character critter in cellArray[j].Characters)
                            {
                                visibleAlign = critter.Alignment;
                                if (critter.BaseProfession == Character.ClassType.Thief)
                                {
                                    if (!Rules.DetectAlignment(critter, ch))
                                    {
                                        visibleAlign = Globals.eAlignment.Lawful;
                                    }
                                }

                                if (ch != critter && Rules.DetectInvisible(critter, ch))
                                {
                                    Map.writeAtXY(ch, 25, 2 + counter, Align[(int)visibleAlign] + " " + critter.Name);
                                    Map.writeAtXY(ch, 45, 2 + counter, critter.rightHandItemName());
                                    Map.writeAtXY(ch, 57, 2 + counter, critter.leftHandItemName());
                                    Map.writeAtXY(ch, 68, 2 + counter, critter.GetVisibleArmorName());
                                    counter++;
                                    ch.seenList.Add(critter);
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
                if (updatestring != "")
                {
                    Map.writeAtXY(ch, writeX, writeY, updatestring);
                }
                writeX += 2;
                line++;
            }
            #endregion
            #region display any items in the cells
            counter = 0;
            writeX = 7;
            writeY = 3;
            for (int j = 0; j < 49; j++)
            {
                if (counter == 7)
                {
                    writeY++;
                    writeX = 7;
                    counter = 0;
                }
                if (cellArray[j] == null || this.visCells[j] == false)
                {
                    updatestring = "";
                }
                else if (cellArray[j].Items.Count > 0)
                {
                    if (ch.CurrentCell == cellArray[j])
                    {
                        updatestring = "$";
                    }
                    else if (cellArray[j].CellGraphic != "~~" ||
                        cellArray[j].DisplayGraphic != "~~" ||
                        cellArray[j].CellGraphic != "[]" ||
                        cellArray[j].DisplayGraphic != "[]")
                    {
                        updatestring = "$";
                    }
                }
                else
                {
                    updatestring = "";
                }
                if (updatestring != "")
                {
                    Map.writeAtXY(ch, writeX, writeY, updatestring);
                }
                counter++;
                writeX += 2;
            }
            #endregion
            return;
        }

        public void updateRight(Character ch)
        {
            string clr = "                             ";
            Map.writeAtXY(ch, 1, 23, clr);
            Map.writeAtXY(ch, 1, 23, " " + Map.BGRN + "R " + ch.rightHandItemName() + Map.CEND);
            return;
        }

        public void updateLeft(Character ch)
        {
            string clr = "                             ";
            Map.writeAtXY(ch, 1, 24, clr);
            Map.writeAtXY(ch, 1, 24, " " + Map.BGRN + "L " + ch.leftHandItemName() + Map.CEND);
            return;
        }

        public void updateHits(Character ch)
        {
            string clr = "      ";
            Map.writeAtXY(ch, 42, 23, clr);
            Map.writeAtXY(ch, 30, 23, Map.CMAG + "Hits       : " + ch.Hits + "/" + ch.HitsFull + "    " + Map.CEND);
            return;
        }

        public void updateHitsLeft(Character ch)
        {
            string clr = "      ";
            Map.writeAtXY(ch, 72, 23, clr);
            Map.writeAtXY(ch, 60, 23, Map.CMAG + "Hits Taken : " + (int)(ch.HitsFull - ch.Hits) + Map.CEND);
            return;
        }

        public string updateExp(Character ch)
        {
            string clr = "        ";

            string chexp = "0";
            if (ch.Experience > 999999)
            {
                long expK = ch.Experience / 1000;
                chexp = Convert.ToString(expK) + "K";
            }
            else
            {
                chexp = Convert.ToString(ch.Experience);
            }
            if (ch.protocol == "normal")
            {
                Map.writeAtXY(ch, 42, 24, clr);
                Map.writeAtXY(ch, 30, 24, Map.CMAG + "Experience : " + chexp + "  " + Map.CEND);
                return "";
            }
            else if (ch.protocol == DragonsSpineMain.APP_PROTOCOL || ch.protocol == "old-kesmai")
            {
                return chexp;
            }
            else
            {
                Map.writeAtXY(ch, 42, 24, clr);
                Map.writeAtXY(ch, 30, 24, Map.CMAG + "Experience : " + chexp + "  " + Map.CEND);
                return "";
            }
        }

        public void updateStam(Character ch)
        {
            string clr = "     ";
            Map.writeAtXY(ch, 42, 25, clr);
            Map.writeAtXY(ch, 30, 25, Map.CMAG + "Stamina    : " + ch.Stamina + Map.CEND);
            return;
        }

        public void updateMagicPoints(Character ch)
        {
            string clr = "     ";
            if (ch.ManaMax > 0)
            {
                Map.writeAtXY(ch, 72, 24, clr);
                Map.writeAtXY(ch, 60, 24, Map.CMAG + "Magic Points: " + ch.Mana + Map.CEND);
            }
            return;
        }

        public void clearDisplayWindow(Character ch) //change to a real buffer that gets displayed each round, clearing at the end.
        {
            int x;

            string clr = "                                                                                             ";
            for (x = 22; x > 9; x--)
            {
                Map.writeAtXY(ch, 1, x, clr);
            }
        }

        public void SendShout(string text)
        {
            Cell cell = null;

            for (int ypos = -6; ypos <= 6; ypos += 1)
            {
                for (int xpos = -6; xpos <= 6; xpos += 1)
                {
                    try
                    {
                        if (Cell.CellRequestInBounds(this, xpos, ypos))
                        {
                            cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                            if (cell != null)
                            {
                                for (int a = 0; a < cell.Characters.Count; a++)
                                {
                                    Character chr = cell.Characters[a];
                                    chr.WriteToDisplay(Character.GetTextDirection(chr, this.X, this.Y) + text);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utils.LogException(e);
                    }
                }
            }
        }

        public static void Move(Character ch, string[] dir, short landID, short mapID, int xcord, int ycord, int zcord)
        {
            string tile;

             {
                ch.updateMap = true;
            }

            Cell tCell = Cell.GetCell(ch.FacetID, landID, mapID, xcord, ycord, zcord);

            if (tCell == null)
            {
                Utils.Log("Cell.Move cell is null land = " + landID + " map = " + mapID + " x = " + xcord + " y = " + ycord + " z = " + zcord, Utils.LogType.SystemFailure);
                return;
            }

            if (tCell.Effects.Count > 0)
            {
                tile = tCell.DisplayGraphic;
            }
            else
            {
                tile = tCell.CellGraphic;
            }

            try
            {
                #region Handle moving TO a cell (tCell)
                if (tCell.IsTeleport)
                {
                    #region Handle moving into a teleport
                    if (!ch.IsPC)
                    {
                        // perform a normal move for mobs, since we dont want them to use teleporters
                        ch.CurrentCell = tCell;
                        if (ch.floating < 2)
                        {
                            ch.floating = 2;
                        }
                        return;
                    }

                    Segue segue = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, xcord, ycord, zcord).Segue;// Segue.GetSegue1(ch.LandID, ch.MapID, xcord, ycord);

                    try
                    {
                        if (segue != null)
                        {
                            if (Cell.PassesCellLock(ch, tCell.cellLock, false)) // teleport the character if there is a match
                            {
                                ch.CurrentCell = Cell.GetCell(ch.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                                if (tCell.cellLock.lockSuccess != "") { ch.WriteToDisplay(tCell.cellLock.lockSuccess); }
                            }
                            else // place the character on the teleport cell only
                            {
                                ch.CurrentCell = tCell;
                                if (tCell.cellLock.lockFailureString.Length > 0)
                                {
                                    ch.WriteToDisplay(tCell.cellLock.lockFailureString);
                                }
                                if (ch.floating < 2)
                                {
                                    ch.floating = 2;
                                }
                                return;
                            }
                        }
                        dir[0] = "";
                        dir[1] = "";
                        dir[2] = "";
                        return;
                    }
                    catch (Exception e)
                    {
                        Utils.LogException(e);
                        dir[0] = "";
                        dir[1] = "";
                        dir[2] = "";
                    }
                    #endregion
                }

                if (Array.IndexOf(tCell.cellLock.lockTypes, Cell.CellLock.LockType.None) == -1) // there is a CellLock
                {
                    if (Array.IndexOf(tCell.cellLock.lockTypes, Cell.CellLock.LockType.Door) != -1)
                    {
                        if (tCell.Effects.ContainsKey(Effect.EffectType.Unlocked_Horizontal_Door) || tCell.Effects.ContainsKey(Effect.EffectType.Unlocked_Vertical_Door))
                        {
                            ch.CurrentCell = tCell;
                            if (ch.floating < 2)
                            {
                                ch.floating = 2;
                            }
                            return;
                        }
                        else
                        {
                            ch.WriteToDisplay("The door is locked.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        }
                    }

                    if (tCell.IsSecretDoor && !tCell.Effects.ContainsKey(Effect.EffectType.Find_Secret_Door))
                    {
                        goto checkTileType;
                    }

                    if (!tCell.IsTeleport) // NOT a teleport
                    {
                        if (PassesCellLock(ch, tCell.cellLock, true)) // passes the cellLock
                        {
                            ch.CurrentCell = tCell;
                            if (ch.floating < 2)
                            {
                                ch.floating = 2;
                            }
                            return;
                        }
                        else
                        {
                            if (tCell.cellLock.lockFailureString.Length > 0)
                            {
                                ch.WriteToDisplay(tCell.cellLock.lockFailureString);
                            }
                            // this is where we do CellLock failure effects
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        }
                    }
                }

                if (tCell.IsWithinTownLimits && ch.Alignment != Globals.eAlignment.Lawful && !ch.IsPC)
                {
                    #region Prevent non lawful mobs from entering a lawful town
                    //ch.WriteToDisplay("You are not allowed to enter the town limits.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }

                if ((tCell.IsLockedHorizontalDoor || tCell.IsLockedVerticalDoor) && !tCell.IsOpenDoor)
                {
                    #region Locked Door
                    ch.WriteToDisplay("The door is locked.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    #endregion
                }

                if (tCell.Characters.Count > 0 && tCell.IsSingleCustomer && ch.IsPC && !tCell.IsPVPEnabled) // moving into a single customer cell
                {
                    #region Handle moving into a single customer cell
                    foreach (Character customer in tCell.Characters)
                    {
                        // if the customer isn't an implementor, and the character moving isn't an implementor
                        if (customer.IsPC && customer.ImpLevel < Globals.eImpLevel.GM && ch.ImpLevel < Globals.eImpLevel.GM)
                        {
                            ch.WriteToDisplay("Please wait your turn.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        }
                    }
                    #endregion
                }

                if (tCell.Characters.Count > 5 && ch.IsPC) // dont let a pc enter a cell with more than 5 ppl/mobs in it
                {
                    #region Handle PC's moving into crowded cells
                    ch.WriteToDisplay("You are blocked by the crowd.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }

                if (tCell.Characters.Count > 7 && !ch.IsPC) // dont let a mob enter a cell with more than 7 ppl/mobs in it
                {
                    #region Handle Mobs moving into crowded cells
                    //ch.WriteToDisplay("You are blocked by the crowd.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }

                if (ch.LastCommand != null && ch.CommandType == Command.CommandType.Crawl)
                {
                    #region Handle a Crawl command that would run into a wall
                    switch (tile)
                    {
                        case "~~":
                            ch.WriteToDisplay("You are stopped by water.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        case "%%":
                            ch.WriteToDisplay("You detect open air.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        case "[]":
                            ch.WriteToDisplay("You are stopped by a wall.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        case "MM":
                        case "mm":
                            ch.WriteToDisplay("You are stopped by an alter.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        case "CC":
                        case "==":
                            ch.WriteToDisplay("You are stopped by a counter.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                        case "/\\":
                            ch.WriteToDisplay("You are stopped by solid earth.");
                            dir[0] = "";
                            dir[1] = "";
                            dir[2] = "";
                            return;
                    }
                    #endregion
                }

                checkTileType:
                // moves into a wall
                if (tile.Equals("[]") || tile.Equals("##") || tile.Equals("/\\") || tile.Equals("mm") || tile.Equals("==") || tile.Equals("CC") || tile.Equals("MM") && !ch.IsImmortal)
                {
                    #region Handle moving into a Wall
                    if (tCell.Effects.Count > 0)
                    {
                        if (tCell.DisplayGraphic.Equals(". "))
                        {
                            ch.CurrentCell = tCell;
                        }
                        return;
                    }

                    if (!ch.IsImmortal && !ch.IsInvisible) // let's keep the service department happy
                    {
                        ch.WriteToDisplay("WHUMP! You are stunned!");
                        if (!ch.IsPC && !ch.IsBlind && !ch.IsFeared)
                        {
                            Utils.Log(ch.GetLogString() + " ran into a wall with cellGraphic " + tile, Utils.LogType.Unknown);
                        }
                        if (ch.race != "")
                        {
                            ch.SendToAllInSight(ch.Name + " runs into a wall.");
                        }
                        else
                        {
                            ch.SendToAllInSight("The " + ch.Name + " runs into a wall.");
                        }

                        ch.Stunned = (short)(Rules.RollD(1, 3)+1);
                    }
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }
                else if (tile.Equals("%%"))
                {
                    #region Handle moving into Air

                    if (ch.CanFly)
                    {
                        ch.CurrentCell = tCell;
                        return;
                    }

                    Segue segue = null;
                    int totalHeight = 0;
                    int currentHeight = ch.CurrentCell.Z;
                    int countLoop = 0;
                    int curx = xcord;
                    int cury = ycord;
                    int curz = zcord;
                    do
                    {
                        segue = Segue.GetDownSegue(Cell.GetCell(ch.FacetID, landID, mapID, curx, cury, curz));
                        //segue = Segue.GetDownSegue(ch.CurrentCell);
                        countLoop++;
                        if (segue != null)
                        {
                            ch.CurrentCell = Cell.GetCell(ch.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                            curx = segue.X;
                            cury = segue.Y;
                            curz = segue.Z;
                            totalHeight += segue.Height;
                        }
                        else
                        {
                            return;
                        }
                    }
                    while (Cell.GetCell(ch.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z).CellGraphic == "%%" && countLoop < 100);

                    if (segue != null)
                    {
                        ch.CurrentCell = Cell.GetCell(ch.FacetID, landID, mapID, segue.X, segue.Y, segue.Z);
                        ch.WriteToDisplay("You have fallen " + Math.Abs(totalHeight) + " feet!");
                        if (segue.Height == 0)
                        {
                            ch.WriteToDisplay("There has been an error with your fall and you have been teleported to a safe spot. Please report this.");
                        }
                        // there will be damage if character does not have featherfall
                        if (!ch.HasFeatherFall)
                        {
                            string EncumbDesc = Rules.GetEncumbrance(ch);
                            int stunMod = 1;
                            int fallDmg = totalHeight;
                            // there will be more damage and stun if encumbered
                            if (EncumbDesc != "lightly")
                            {
                                int maxEncumb = ((ch.Strength + ch.TempStrength) * 9) + ((ch.Dexterity + ch.TempDexterity) * 2) +
                                    ((ch.Constitution + ch.TempConstitution) * 2);
                                fallDmg += (int)(ch.encumbrance - maxEncumb);
                                if (EncumbDesc == "moderately") { stunMod = 3; }
                                if (EncumbDesc == "heavily") { stunMod = 6; }
                            }
                            if (segue.Height > 10 || stunMod > 1)
                            {
                                ch.Stunned += (short)(Rules.dice.Next(stunMod) + stunMod);
                            }
                            #region Emit Falling Sound
                            if (ch.gender == Globals.eGender.Female)
                            {
                                ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FallingFemale));
                            }
                            else if (ch.gender == Globals.eGender.Male)
                            {
                                ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FallingMale));
                            }
                            #endregion
                            Rules.DoDamage(ch, ch, fallDmg, false);
                        }
                    }

                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }
                else if (tile.Equals("WW"))
                {
                    #region Handle moving into a Reef
                    ch.WriteToDisplay("The path is blocked.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    return;
                    #endregion
                }
                else if (tile.Equals("TT"))
                {
                    #region Handle moving into a Solid Tree
                    ch.WriteToDisplay("The path is blocked.");
                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";
                    #endregion
                }
                else if (tile.Equals("~~"))
                {
                    #region Handle moving into Water
                    if (ch.CanFly || ch.CanBreatheWater)
                    {
                        ch.CurrentCell = tCell;
                        if (!ch.CanFly)
                        {
                            ch.CurrentCell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.Splash));
                            ch.WriteToDisplay("Splash!");
                        }
                        return;
                    }

                    if (ch.IsPC)
                    {
                        ch.WriteToDisplay("Splash!");
                    }

                    ch.CurrentCell = tCell;
                    ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.Splash));

                    dir[0] = "";
                    dir[1] = "";
                    dir[2] = "";

                    if (ch.IsPC)
                    {
                        if (!ch.IsImmortal)
                        {
                            if (!ch.CanFly)
                            {
                                if (!ch.CanBreatheWater)
                                {
                                    if (ch.floating == 4)
                                    {
                                        ch.floating -= 1;
                                    }
                                    else if(ch.floating == 3)
                                    {
                                        ch.floating -= 1;
                                        ch.WriteToDisplay("You are sinking below the surface!");
                                    }
                                    else if (ch.floating == 2)
                                    {
                                        ch.floating -= 1;
                                        ch.WriteToDisplay("You are submerged!");
                                    }
                                    else if (ch.floating == 1)
                                    {
                                        ch.floating -= 1;
                                        ch.WriteToDisplay("You are submerged under water and cannot breathe!");

                                    }
                                    else if (ch.floating < 1)
                                    {
                                        ch.WriteToDisplay("You have drowned!");
                                        ch.SendToAllInSight(ch.Name + " has drowned to death.");
                                        if (!ch.IsDead)
                                        {
                                            Rules.DoDeath(ch, null);
                                        }
                                    }
                                    else
                                    {
                                        ch.floating = 3; // just in case.
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                else if (tile.Equals("--"))
                {
                    #region Handle moving into a Door
                    ch.CurrentCell = tCell;
                    ch.CurrentCell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenDoor));
                    ch.CurrentCell.DisplayGraphic = "\\ ";
                    ch.CurrentCell.CellGraphic = "\\ ";
                    #endregion
                }
                else if (tile.Equals("| "))
                {
                    #region Handle moving into a Door
                    ch.CurrentCell = tCell;
                    ch.CurrentCell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenDoor));
                    ch.CurrentCell.DisplayGraphic = "/ ";
                    ch.CurrentCell.CellGraphic = "/ ";
                    #endregion
                }
                else if (tile.Equals("[_") || tile.Equals("_]"))
                {
                    #region Handle moving into ruins
                    ch.CurrentCell = tCell;
                    if (dir[2] != "")
                    {
                        ch.WriteToDisplay("Your movement is slowed by the rubble.");
                        dir[2] = "";
                    }
                    #endregion
                }
                else if(tile.Equals(".\\"))
                {
                    ch.CurrentCell = tCell;
                    if (dir[2] != "")
                    {
                        ch.WriteToDisplay("Your movement is slowed by the sand.");
                        dir[2] = "";
                    }
                }
                else
                {
                    #region Handle a Normal Move
                    ch.CurrentCell = tCell;
                    if (ch.floating < 2)
                    {
                        ch.floating = 2;
                    }
                    return;
                    #endregion
                }
                #endregion
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void EmitSound(string soundFile)
        {
            if (soundFile == null || soundFile == "")
            {
                return;
            }
            else if (soundFile.Length < 4)
            {
                Utils.Log("Sound file length less than 4 at cell.EmitSound(" + soundFile + ")", Utils.LogType.SystemWarning);
                return;
            }
            Cell cell = null;
            for (int ypos = -6; ypos <= 6; ypos++)
            {
                for (int xpos = -6; xpos <= 6; xpos++)
                {
                    if (Cell.CellRequestInBounds(this, xpos, ypos))
                    {
                        cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                        if (cell != null && cell.Characters.Count > 0)
                        {
                            for (int a = 0; a < cell.Characters.Count; a++)
                            {
                                Character chr = cell.Characters[a];
                                if (chr.protocol == DragonsSpineMain.APP_PROTOCOL)
                                {
                                    int distance = Cell.GetCellDistance(this.X, this.Y, cell.X, cell.Y);
                                    int direction = Convert.ToInt32(Map.GetDirection(this, cell));
                                    chr.Write(Protocol.SOUND + soundFile + Protocol.VSPLIT + distance + Protocol.VSPLIT + direction + Protocol.SOUND_END);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static Cell[] GetVisibleCellArray(Cell cell, int d)
        {
            Cell[] cellArray = new Cell[50];

            int bitcount = 0;

            for (int ypos = -d; ypos <= d; ypos++)
            {
                for (int xpos = -d; xpos <= d; xpos++)
                {
                    if (!Cell.CellRequestInBounds(cell, xpos, ypos))
                    {
                        cellArray[bitcount] = null;
                    }
                    else
                    {
                        cellArray[bitcount] = Cell.GetCell(cell.FacetID, cell.LandID, cell.MapID, cell.X + xpos, cell.Y + ypos, cell.Z);
                    }
                    bitcount++;
                }
            }

            return cellArray;
        }

        public static bool CellRequestInBounds(Cell cell, int xAdd, int yAdd)
        {
            if (cell == null || cell.Map == null)
            {
                return false;
            }

            int xMax = cell.Map.XCordMax[cell.Z];
            int xMin = cell.Map.XCordMin[cell.Z];
            int yMax = cell.Map.YCordMax[cell.Z];
            int yMin = cell.Map.YCordMin[cell.Z];

            if (xAdd >= 0)
            {
                if (cell.X + xAdd > xMax)
                {
                    return false;
                }
            }

            if (xAdd < 0)
            {
                if (cell.X + xAdd < xMin)
                {
                    return false;
                }
            }

            if (yAdd >= 0)
            {
                if (cell.Y + yAdd > yMax)
                {
                    return false;
                }
            }

            if (yAdd < 0)
            {
                if (cell.Y + yAdd < yMin)
                {
                    return false;
                }
            }

            return true;
        }

        public static Cell GetCell(short facetID, short landID, short mapID, int x, int y, int z)
        {
            try
            {
                string key = "0,0,0";
                //key = x.ToString() + "," + y.ToString() + "," + z.ToString(); // crashing on too many nested calls
                key = x + "," + y + "," + z; // same as above without the nested calls to ToString() - 1 crash here so far
                if (World.GetFacetByID(facetID).GetLandByID(landID).GetMapByID(mapID).cells.ContainsKey(key))
                {
                    return World.GetFacetByID(facetID).GetLandByID(landID).GetMapByID(mapID).cells[key];
                }
                //Utils.Log("Failure in Cell.GetCell(" + facetID + ", " + landID + ", " + mapID + ", " + x + ", " + y + ", " + z + ") returned null.", Utils.LogType.SystemFailure);
                return null;
            }
            catch (Exception e)
            {
                Utils.Log("Failure in Cell.GetCell(" + facetID + ", " + landID + ", " + mapID + ", " + x + ", " + y + ", " + z + ").", Utils.LogType.SystemFailure);
                Utils.LogException(e);
                return null;
            }
        }

        public static int GetCellDistance(int startXCord, int startYCord, int goalXCord, int goalYCord) // determine distance between two cells
        {
            try
            {
                if (startXCord == goalXCord && startYCord == goalYCord)
                {
                    return 0;
                }

                if (goalXCord <= Int32.MinValue || goalYCord <= Int32.MinValue)
                {
                    return 0;
                }

                int xAbsolute = Math.Abs(startXCord - goalXCord); // get absolute value of StartX - GoalX
                int yAbsolute = Math.Abs(startYCord - goalYCord); // get absolute value of StartY - GoalY

                if (xAbsolute > yAbsolute)
                {
                    return xAbsolute;
                }
                else
                {
                    return yAbsolute;
                }
            }
            catch (Exception e)
            {
                Utils.Log("Failure at getCellDistance(" + startXCord + ", " + startYCord + ", " + goalXCord + ", " + goalYCord + ")", Utils.LogType.SystemFailure);
                Utils.LogException(e);
                return 0;
            }
        }

        public static bool PassesCellLock(Character ch, Cell.CellLock cellLock, bool inform)
        {
            try
            {
                if (ch.IsImmortal) { return true; } // if immortal the ch may pass freely
                if (!ch.IsPC) { return false; } // NPCs fail the check automatically

                if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.None) != -1) // check if there is no cellLock on this teleport
                {
                    return true;
                }
                else
                {
                    // check Character.classType restrictions
                    if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.ClassType) != -1)
                    {
                        if (Array.IndexOf(cellLock.classTypes, ch.BaseProfession) == -1)
                        {
                            if (inform)
                            {
                                ch.WriteToDisplay("You failed to meet the profession restriction of a cell lock.");
                            }
                            return false;
                        }
                    }
                    // check World.Alignment restrictions
                    {
                        if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.Alignment) != -1)
                        {
                            if (Array.IndexOf(cellLock.alignmentTypes, ch.Alignment) == -1)
                            {
                                if (inform)
                                {
                                    ch.WriteToDisplay("You failed to meet the alignment restriction of a cell lock.");
                                }
                                return false;
                            }
                        }
                    }
                    // check World.dailyCycle restrictions
                    if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.DailyCycle) != -1)
                    {
                        if (Array.IndexOf(cellLock.dailyCycles, World.CurrentDailyCycle) == -1)
                        {
                            if (inform)
                            {
                                ch.WriteToDisplay("You failed to meet the daily cycle restriction of a cell lock.");
                            }
                            return false;
                        }
                    }
                    // check World.lunarCycle restrictions
                    if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.LunarCycle) != -1)
                    {
                        if (Array.IndexOf(cellLock.lunarCycles, World.CurrentLunarCycle) == -1)
                        {
                            if (inform)
                            {
                                ch.WriteToDisplay("You failed to meet the lunar cycle restriction of a cell lock.");
                            }
                            return false;
                        }
                    }
                    // check level requirements for teleport
                    if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.Level) != -1)
                    {
                        if (ch.Level < cellLock.minimumLevel)
                        {
                            if (inform)
                            {
                                ch.WriteToDisplay("You failed to meet the minimum level restriction of a cell lock.");
                            }
                            return false; } // minimumLevel
                        if (ch.Level > cellLock.maximumLevel)
                        {
                            if (inform)
                            {
                                ch.WriteToDisplay("You failed to meet the maximum level restriction of a cell lock.");
                            }
                            return false; } // maximumLevel
                    }
                    // check key requirements - item.name or item.key
                    if (Array.IndexOf(cellLock.lockTypes, CellLock.LockType.Key) != -1)
                    {
                        if (cellLock.heldKey)
                        {
                            if (ch.RightHand == null && ch.LeftHand == null)
                            {
                                if (inform)
                                {
                                    ch.WriteToDisplay("You failed to meet the key restriction of a cell lock.");
                                }
                                return false;
                            }
                            else if (ch.RightHand != null && ch.LeftHand == null)
                            {
                                if (!ch.RightHand.key.Contains(cellLock.key) && ch.RightHand.name != cellLock.key)
                                {
                                    if (inform)
                                    {
                                        ch.WriteToDisplay("You failed to meet the key restriction of a cell lock.");
                                    }
                                    return false;
                                }
                            }
                            else if (ch.RightHand == null && ch.LeftHand != null)
                            {
                                if (!ch.LeftHand.key.Contains(cellLock.key) && ch.LeftHand.name != cellLock.key)
                                {
                                    if (inform)
                                    {
                                        ch.WriteToDisplay("You failed to meet the key restriction of a cell lock.");
                                    }
                                    return false;
                                }
                            }
                            else if (ch.RightHand != null && ch.LeftHand != null)
                            {
                                if (!ch.RightHand.key.Contains(cellLock.key) && ch.RightHand.name != cellLock.key &&
                                    !ch.LeftHand.key.Contains(cellLock.key) && ch.LeftHand.name != cellLock.key)
                                {
                                    if (inform)
                                    {
                                        ch.WriteToDisplay("You failed to meet the key restriction of a cell lock.");
                                    }
                                    return false;
                                }
                            }
                        }
                        else if (cellLock.wornKey) // worn item key can be item.key or item.name
                        {
                            bool wornMatch = false;

                            foreach (Item item in ch.wearing)
                            {
                                if (item.key.Contains(cellLock.key) || item.name == cellLock.key)
                                {
                                    wornMatch = true; break;
                                }
                            }
                            if (!wornMatch)
                            {
                                foreach (Item item in ch.GetRings())
                                {
                                    if (item.key.Contains(cellLock.key) || item.name == cellLock.key)
                                    {
                                        wornMatch = true; break;
                                    }
                                }
                            }
                            if (!wornMatch)
                            {
                                if (inform)
                                {
                                    ch.WriteToDisplay("You failed to meet the key restriction of a cell lock.");
                                }
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return true;
            }
        }
    }

	public struct CellLite {
		public string displayGraphic;
		public bool   isMapPortal;
		public short  x;
		public short  y;
		public int    z;
        public int lastRoundChanged;
	}

}
