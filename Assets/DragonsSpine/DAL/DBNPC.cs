using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DBNPC.
	/// </summary>
	public class DBNPC
	{
		public DBNPC()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		internal static bool LoadNPCDictionary()
		{			
			try
			{
				SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select_All", DataAccess.GetSQLConnection());
				DataTable dtNPCs = sp.ExecuteDataTable();
				foreach(DataRow dr in dtNPCs.Rows)
				{
                    int npcid =Convert.ToInt32(dr["npcID"]);
                    NPC.Add(npcid, dr);
				}
                Utils.Log("Loaded NPCs (" + NPC.NPCDictionary.Count.ToString() + ")...", Utils.LogType.SystemGo);
				return true;
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return false;
			}
		}

		internal static NPC GetNPCByID(int NpcID)
		{
			try
			{
				SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select", DataAccess.GetSQLConnection());
				sp.AddParameter("@npcID",				SqlDbType.Int,		4,	ParameterDirection.Input, NpcID);
				DataTable dtNPCs = sp.ExecuteDataTable();
                return new NPC(dtNPCs.Rows[0]);
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return null;
			}
		}

        internal static ArrayList GetRandomNPCs(int diff)
        {
            ArrayList mylist = new ArrayList();
            int num = 6;
            SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select_Random", DataAccess.GetSQLConnection());
            ArrayList npclist = new ArrayList();
            sp.AddParameter("@difficulty", SqlDbType.Int, 8, ParameterDirection.Input, diff);
            DataTable dtNPCs = sp.ExecuteDataTable();
            foreach (DataRow dr in dtNPCs.Rows)
            {
                npclist.Add(new NPC(dr));
            }
            for (int x = 0; x < num; x++)
            {
                mylist.Add(npclist[Rules.dice.Next(npclist.Count)]);
            }
            return mylist;
        }

		public static List<NPC> GetNpcs(string npcName)
		{
			List<NPC> npclist = new List<NPC>();

			using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
			using (SqlStoredProcedure sp = new SqlStoredProcedure("prApp_LiveNPC_by_MapID", tempConnection)) {
				sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, 0);
				sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, 0);
				DataTable dtNPCs = sp.ExecuteDataTable();
				foreach (DataRow dr in dtNPCs.Rows)
				{
					if (dr["name"].ToString() == npcName) {
						NPC npc = new NPC();
						npc.worldNpcID = Convert.ToInt32(dr["uniqueId"]);
						npc.lastActiveRound = Convert.ToInt32(dr["lastActiveRound"]);
						npc.Name = dr["name"].ToString();
						npc.X = Convert.ToInt16(dr["xCord"]);
						npc.Y = Convert.ToInt16(dr["yCord"]);
						npc.Z = Convert.ToInt16(dr["zCord"]);
						npclist.Add(npc);
					}
				}
			}
			return npclist;
		}

		public static List<NPCLite> GetAllNpcs(int mapNum)
		{
			List<NPCLite> npclist = new List<NPCLite>();
			
			using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
			using (SqlStoredProcedure sp = new SqlStoredProcedure("prApp_LiveNPC_by_MapID", tempConnection)) {
				sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, 0);
				sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, mapNum);
				using (DataTable dtNPCs = sp.ExecuteDataTable()) {
					foreach (DataRow dr in dtNPCs.Rows)
					{
						NPCLite npc = new NPCLite();
						npc.worldNpcID = Convert.ToInt32(dr["uniqueId"]);
						npc.lastActiveRound = Convert.ToInt32(dr["lastActiveRound"]);
						npc.Name = dr["name"].ToString();
						npc.X = Convert.ToInt16(dr["xCord"]);
						npc.Y = Convert.ToInt16(dr["yCord"]);
						npc.Z = Convert.ToInt16(dr["zCord"]);

						npc.HitsFull	= Convert.ToInt32(dr["fullHits"]);
						npc.Hits		= Convert.ToInt32(dr["hits"]);

						npc.hasMostHated = !(dr.IsNull(dtNPCs.Columns["mostHatedId"]));
						if (npc.hasMostHated) {
							npc.mostHatedId = Convert.ToInt32(dr["mostHatedId"]);
						}
						npclist.Add(npc);
					}
				}
			}
			return npclist;
		}

        public static List<NPCLite> GetAllNpcsAndPcs(int mapNum) {
            List<NPCLite> npclist = new List<NPCLite>();

            using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
            using (SqlStoredProcedure sp = new SqlStoredProcedure("prApp_LiveNPC_by_MapID", tempConnection)) {
                sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, 0);
                sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, mapNum);
                using (DataTable dtNPCs = sp.ExecuteDataTable()) {
                    foreach (DataRow dr in dtNPCs.Rows) {
                        NPCLite npc = new NPCLite();
                        npc.worldNpcID = Convert.ToInt32(dr["uniqueId"]);
                        npc.lastActiveRound = Convert.ToInt32(dr["lastActiveRound"]);
                        npc.Name = dr["name"].ToString();
                        npc.X = Convert.ToInt16(dr["xCord"]);
                        npc.Y = Convert.ToInt16(dr["yCord"]);
                        npc.Z = Convert.ToInt16(dr["zCord"]);

                        npc.HitsFull = Convert.ToInt32(dr["fullHits"]);
                        npc.Hits = Convert.ToInt32(dr["hits"]);

                        npc.hasMostHated = !(dr.IsNull(dtNPCs.Columns["mostHatedId"]));
                        if (npc.hasMostHated) {
                            npc.mostHatedId = Convert.ToInt32(dr["mostHatedId"]);
                        }
                        npclist.Add(npc);
                    }
                }
            }

            using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
            using (SqlStoredProcedure sp = new SqlStoredProcedure("prApp_LivePC_by_MapID", tempConnection)) {
                sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, 0);
                sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, mapNum);
                using (DataTable dtNPCs = sp.ExecuteDataTable()) {
                    foreach (DataRow dr in dtNPCs.Rows) {
                        NPCLite npc = new NPCLite();
                        npc.worldNpcID = Convert.ToInt32(dr["uniqueId"]);
                        npc.lastActiveRound = Convert.ToInt32(dr["lastActiveRound"]);
                        npc.Name = dr["name"].ToString();
                        npc.X = Convert.ToInt16(dr["xCord"]);
                        npc.Y = Convert.ToInt16(dr["yCord"]);
                        npc.Z = Convert.ToInt16(dr["zCord"]);

                        npc.HitsFull = Convert.ToInt32(dr["fullHits"]);
                        npc.Hits = Convert.ToInt32(dr["hits"]);

                        npc.isPC = true;
                        npclist.Add(npc);
                    }
                }
            }
            
            return npclist;
        }

        //internal static Quests convertDataToQuest(DataRow dritem)
        //{
        //    Quests nq = new Quests();
        //    nq.failQuestString = Convert.ToString(dritem["failQuestString"]);
        //    nq.finishQuestString = Convert.ToString(dritem["finishQuestString"]);
        //    nq.giveQuestString = Convert.ToString(dritem["giveQuestString"]);
        //    nq.QuestID = Convert.ToInt32(dritem["QuestID"]);
        //    nq.QuestItems = Convert.ToString(dritem["QuestItems"]);
        //    nq.QuestItemValue = Convert.ToInt32(dritem["QuestItemValue"]);
        //    nq.QuestName = Convert.ToString(dritem["QuestName"]);
        //    nq.QuestRewardItem = Convert.ToString(dritem["QuestRewardItem"]);
        //    nq.QuestRewardType = Convert.ToInt32(dritem["QuestRewardType"]);
        //    nq.QuestRewardValue = Convert.ToInt32(dritem["QuestRewardValue"]);
        //    nq.startQuestString = Convert.ToString(dritem["startQuestString"]);
        //    return nq;
        //}
	}
}	

