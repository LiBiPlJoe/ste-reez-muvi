using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DragonsSpine.DAL
{
    static public class DBMail
    {
        internal static List<Mail> GetMailByReceiverID(int receiverID) // retrieves locker record from PlayerLocker table
        {
            try
            {
                List<Mail> mailList = new List<Mail>();
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Mail_Select_By_ReceiverID", DataAccess.GetSQLConnection());
                sp.AddParameter("@receiverID", SqlDbType.Int, 4, ParameterDirection.Input, receiverID);
                DataTable dtMail = sp.ExecuteDataTable();
                foreach (DataRow dr in dtMail.Rows)
                {
                    mailList.Add(new Mail(dr));
                }
                return mailList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static List<Mail> GetMailBySenderID(int senderID) // retrieves locker record from PlayerLocker table
        {
            try
            {
                List<Mail> mailList = new List<Mail>();
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Mail_Select_By_SenderID", DataAccess.GetSQLConnection());
                sp.AddParameter("@senderID", SqlDbType.Int, 4, ParameterDirection.Input, senderID);
                DataTable dtMail = sp.ExecuteDataTable();
                foreach (DataRow dr in dtMail.Rows)
                {
                    mailList.Add(new Mail(dr));
                }
                return mailList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static Mail GetMailByMailID(long mailID)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Mail_Select_By_MailID", DataAccess.GetSQLConnection());
                sp.AddParameter("@receiverID", SqlDbType.BigInt, 8, ParameterDirection.Input, mailID);
                DataTable dtMail = sp.ExecuteDataTable();
                if (dtMail.Rows.Count > 0)
                {
                    return new Mail(dtMail.Rows[0]);
                }
                return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static List<MailAttachment> GetMailAttachments(long mailID)
        {
            List<MailAttachment> attList = new List<MailAttachment>();

            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_MailAttachment_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@mailID", SqlDbType.BigInt, 8, ParameterDirection.Input, mailID);
                DataTable dtMailAtt = sp.ExecuteDataTable();
                foreach (DataRow dr in dtMailAtt.Rows)
                {
                    attList.Add(new MailAttachment(dr));
                }
                return attList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return attList;
            }
        }

        internal static int InsertMail(Mail mail)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Mail_Insert", DataAccess.GetSQLConnection());
                sp.AddParameter("@senderID", SqlDbType.Int, 4, ParameterDirection.Input, mail.SenderID);
                sp.AddParameter("@receiverID", SqlDbType.Int, 4, ParameterDirection.Input, mail.ReceiverID);
                sp.AddParameter("@timeSent", SqlDbType.DateTime, 8, ParameterDirection.Input, mail.TimeSent);
                sp.AddParameter("@subject", SqlDbType.NVarChar, 50, ParameterDirection.Input, mail.Subject);
                sp.AddParameter("@body", SqlDbType.VarChar, 4000, ParameterDirection.Input, mail.Body);
                sp.AddParameter("@attachment", SqlDbType.Bit, 1, ParameterDirection.Input, mail.HasAttachment);
                sp.AddParameter("@readByReceiver", SqlDbType.Bit, 1, ParameterDirection.Input, false);
                return sp.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }
    }
}
