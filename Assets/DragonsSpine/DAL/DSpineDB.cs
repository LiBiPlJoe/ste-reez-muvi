using System;
using System.Data;
using System.Data.SqlClient;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DSpineDB.
	/// </summary>
	public class DSpineDB
	{
		public DSpineDB()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		internal static int InsertSegue(int Land1,int Map1,int yCord1,int xCord1,int Land2,int Map2,int yCord2,int xCord2,int height)
		{
			try
			{
				SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Segue_Insert", DataAccess.GetSQLConnection());
				sp.AddParameter("@Land1ID",		SqlDbType.Int,		4,	ParameterDirection.Input, Land1);
				sp.AddParameter("@Map1ID",		SqlDbType.Int,		4,	ParameterDirection.Input, Map1);
				sp.AddParameter("@Ycord1",		SqlDbType.Int,		4,	ParameterDirection.Input, yCord1);
				sp.AddParameter("@Xcord1",		SqlDbType.Int,		4,	ParameterDirection.Input, xCord1);
				sp.AddParameter("@Land2ID",		SqlDbType.Int,		4,	ParameterDirection.Input, Land2);
				sp.AddParameter("@Map2ID",		SqlDbType.Int,		4,	ParameterDirection.Input, Map2);
				sp.AddParameter("@Ycord2",		SqlDbType.Int,		4,	ParameterDirection.Input, yCord2);
				sp.AddParameter("@Xcord2",		SqlDbType.Int,		4,	ParameterDirection.Input, xCord2);
				sp.AddParameter("@Height",		SqlDbType.Int,		4,	ParameterDirection.Input, height);
									
					
				return sp.ExecuteNonQuery();
			}
			catch
			{
				//Log the error
				return -1;
			}
		}







	}
}



