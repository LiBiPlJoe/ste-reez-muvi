using System;
using System.Data;
using System.Data.SqlClient;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DataAccess.
	/// </summary>
	public class DataAccess
	{
//		public DataAccess()
//		{
//			//
//			// TODO: Add constructor logic here
//			//
//		}
//
//
//
		public static SqlConnection GetSQLConnection()
		{
			// string sConnStr = "User ID='db1_readonly';Password='IMakeMaps';Initial Catalog='DSpineBackup';Data Source='AQUALUNG\\SQLEXPRESS';Connect Timeout=15";
			string sConnStr = "User ID='db1_readonly';Password='IMakeMaps';Initial Catalog='dragonsspine';Data Source='localhost\\MYTESTSQL';Connect Timeout=30";
			// Utils.Log(sConnStr, Utils.LogType.SystemFailure);
			try
			{
				SqlConnection sCon = new SqlConnection(sConnStr);
				sCon.Open();
                return sCon;
			}
			catch(Exception e)
			{
				Utils.LogException(e);
				string error =  e.Message;
				return new SqlConnection();
			}
		}

		public static SqlConnection GetSQLConnection(string sConnStr)
		{
			SqlConnection sCon = new SqlConnection(sConnStr);
			return sCon;
		}


	}
}
