using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace DragonsSpine.GameClients
{
    /// <summary>
    /// The TelnetClient class represents clients using TCP only.
    /// </summary>
    public class TelnetClient : GameClient
    {
        protected const short DISPLAY_BUFFER_SIZE = 10;

        protected TcpClient m_tcpClient;

        protected Queue m_outputQueue;

        protected string[] m_displayBuffer;

        protected string m_displayText;

        public TelnetClient(IPAddress ipAddress, int port, GameObjects.GamePlayer player)
            : base(ipAddress, port, player)
        {
            m_outputQueue = new Queue();

            m_tcpClient = new TcpClient(m_ipEndPoint);

            m_displayBuffer = new string[DISPLAY_BUFFER_SIZE];

            m_displayText = "";
        }

        public override void Write(string message) // sends a text message to the player
        {
            m_outputQueue.Enqueue(message);
        }

        public override void WriteLine(string message, Protocol.TextType textType) // write a line with a carriage return, include protocol
        {
            try
            {
                message = message + "\r\n"; // add a carriage return line feed to message

                if (m_player.ClientProtocol != DragonsSpineMain.APP_PROTOCOL || m_player.PlayerState == Globals.ePlayerState.PLAYING) // if not using the protocol send the message
                {
                    if (m_player.PlayerState == Globals.ePlayerState.PLAYING)
                    {
                        WriteToDisplay(message, textType);
                    }
                    else
                    {
                        Write(message);
                    }
                    return;
                }

                // Format the protocol message.
                message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);

                // Send the protocol message.
                this.Write(message);

            }
            catch (Exception e)
            {
                this.Write(message);
                Utils.LogException(e);
            }
        }

        public override void WriteToDisplay(string message, Protocol.TextType textType) // write a line with a carriage return, include protocol
        {
            try
            {
                message = message + "\r\n"; // add a carriage return line feed to message

                if (m_player.ClientProtocol != DragonsSpineMain.APP_PROTOCOL || m_player.PlayerState == Globals.ePlayerState.PLAYING) // if not using the protocol send the message
                {
                    if (m_player.PlayerState == Globals.ePlayerState.PLAYING)
                    {
                        WriteToDisplay(message);
                    }
                    else
                    {
                        Write(message);
                    }
                    return;
                }

                message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);
                this.Write(message); // send the protocol message

            }
            catch (Exception e)
            {
                this.Write(message);
                Utils.LogException(e);
            }
        }

        public override void WriteToDisplay(string message) // text that will be displayed at the end of the round
        {
            // If message is empty, log it and return void.
            if (message == "")
            {
                Utils.Log("WriteToDisplay message = null.", Utils.LogType.Unknown);
                return;
            }

            // Filter message by replacing strings.
            message = FilterMessage(message);

            // If player is not playing then use WriteLine method
            if (m_player.PlayerState != Globals.ePlayerState.PLAYING)
            {
                WriteLine(message, Protocol.TextType.Status);
                return;
            }

            if (m_player.ClientProtocol == DragonsSpineMain.APP_PROTOCOL)
            {
                Write(Protocol.GAME_TEXT + message + Protocol.GAME_TEXT_END);
                return;
            }

            int ctr;

            for (ctr = DISPLAY_BUFFER_SIZE - 1; ctr > 0; ctr--)
            {
                m_displayBuffer[ctr] = m_displayBuffer[ctr - 1];
            }

            m_displayBuffer[0] = message + "\n\r";

            m_displayText = message + "\n\r";

            for (ctr = 1; ctr < DISPLAY_BUFFER_SIZE; ctr++)
            {
                m_displayText = m_displayBuffer[ctr] + m_displayText;
            }
        }

        public override void WriteLine(string message) // write a line with a carriage return line feed, exclude protocol
        {
            Write(message + "\r\n");
        }
    }
}
