using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace DragonsSpine.GameClients
{
    /// <summary>
    /// The HybridClient class represents clients using both TCP and UDP.
    /// </summary>
    public class HybridClient : TelnetClient
    {
        private const int MAX_UDP_BYTES = 1024;

        private UdpClient m_udpClient;

        public HybridClient(IPAddress ipAddress, int port, GameObjects.GamePlayer player)
            : base(ipAddress, port, player)
        {
            m_udpClient = new UdpClient(m_ipEndPoint);
        }

        /// <summary>
        /// Send a message to the client.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public override void Write(string message)
        {
            byte[] data = new byte[MAX_UDP_BYTES];
            data = Encoding.ASCII.GetBytes(message);
            m_udpClient.Send(data, data.Length, m_ipEndPoint);
        }

        /// <summary>
        /// Send a message to the client with a carriage return and line feed, including protocol.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="textType">The type of message being sent.</param>
        public override void WriteLine(string message, Protocol.TextType textType)
        {
            message = message + "\r\n";
            message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);
            Write(message);
        }

        /// <summary>
        /// Send a message to the client with a carriage return and line feed, including protocol.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="textType">The type of message being sent.</param>
        public override void WriteToDisplay(string message, Protocol.TextType textType)
        {
            message = message + "\r\n";
            message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);
            Write(message);
        }

        /// <summary>
        /// Send a message to the client with a carriage return and line feed, excluding protocol.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public override void WriteLine(string message)
        {
            Write(message + "\r\n");
        }
    }
}
