using System;
using System.Timers;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
// using System.Windows.Forms;
using System.Threading;
using System.Configuration;
using DragonsSpine.Config;
using System.Management;

namespace DragonsSpine
{
    public class DragonsSpineMain
    {
        public static string APP_VERSION = ConfigurationManager.AppSettings["APP_VERSION"];
        public static string CLIENT_VERSION = ConfigurationManager.AppSettings["CLIENT_VERSION"];
        public static string APP_NAME = ConfigurationManager.AppSettings["APP_NAME"];
        public static string APP_PROTOCOL = ConfigurationManager.AppSettings["APP_PROTOCOL"];
        public static string SQL_CONNECTION = ConfigurationManager.AppSettings["SQL_CONNECTION"];

        #region Private Data
        static ServerState m_serverStatus;
        private static DragonsSpineMain m_instance;
        static int m_gameRound = 0;
        static int m_npcRound = 0;
        static double m_masterRoundInterval = 5000;
        static System.Timers.Timer m_roundTimer;
        static System.Timers.Timer m_saveTimer;
        static System.Timers.Timer m_janitorTimer;
        static System.Timers.Timer m_chronoTimer;
        static System.Timers.Timer m_lunarTimer;
        static System.Timers.Timer m_inactivityTimer;
        static System.Timers.Timer m_displayTimer;
        private static GameServerSettings m_settings;

        #endregion

        #region Public Data
        public enum ServerState { Starting, Running, Locked, ShuttingDown, Restarting };

        public static ServerState ServerStatus
        {
            get { return m_serverStatus; }
            set { m_serverStatus = value; }
        }
        /// <summary>
        /// Gets the instance of the game server.
        /// </summary>
        public static DragonsSpineMain Instance
        {
            get { return m_instance; }
        }

        /// <summary>
        ///  Gets the server settings.
        /// </summary>
        public GameServerSettings Settings
        {
            get { return m_settings; }
        }

        public static int GameRound
        {
            get { return m_gameRound; }
            set { m_gameRound++; }
        }
        public static int NPCRound
        {
            get { return m_npcRound; }
            set { m_npcRound++; }
        }
        public static bool ProcessEmptyWorld = false;
        public static double MasterRoundInterval
        {
            get { return DragonsSpineMain.m_masterRoundInterval; }
            set { DragonsSpineMain.m_masterRoundInterval = value; }
        }
        #endregion

        public static int Main(string[] args)
        {
            SetInstance(new DragonsSpineMain());
            m_settings = GameServerSettings.Load();

            #region Initialize Console
            WinConsole.Initialize();

            //// to make Debug or Trace output to the console, do the following.
            ////Debug.Listeners.Remove("default");
            ////Debug.Listeners.Add(new TextWriterTraceListener(new ConsoleWriter(...)));

            Console.SetError(new ConsoleWriter(Console.Error, ConsoleColor.Red | ConsoleColor.Intensified | ConsoleColor.WhiteBG, ConsoleFlashMode.FlashUntilResponse, true));
            //WinConsole.Color = ConsoleColor.Blue | ConsoleColor.Intensified | ConsoleColor.BlueBG;
            WinConsole.Flash(true);
            #endregion
            //WinConsole.Visible = false;
            DragonsSpineMain main = new DragonsSpineMain(); // create our main class
            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.Starting;
            Utils.Log(APP_NAME + " " + APP_VERSION, Utils.LogType.SystemGo);

            #region Compile Scripts
            Utils.Log("Compiling scripts.", Utils.LogType.SystemGo);
            if (!CompileScripts())
            {
                Utils.Log("CompileScripts() failed.", Utils.LogType.SystemFatalError);
                return -1;
            }
            else
            {
                Utils.Log("Compiled!", Utils.LogType.SystemGo);
            }
            #endregion

            #region Load World
            if (!World.LoadWorld()) // load World
            {
                Utils.Log("World.loadWorld() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Load Banned IP List
            if (!World.LoadBannedIPList()) // load banned IP list - not a fatal error on fail
            {
                Utils.Log("World.LoadBannedIPList() failed.", Utils.LogType.SystemFailure);
            } 
            #endregion

            #region Load Items
            if (!Item.LoadItems())
            {
                Utils.Log("Item.loadItems() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Load Quests
            if (!Quest.LoadQuests())
            {
                Utils.Log("Quest.LoadQuests() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Load Spawn Zones
            if (!SpawnZone.LoadSpawnZones())
            {
                Utils.Log("SpawnZoneLink.loadSpawnZoneLinks() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Load Newbies
            if (!CharGen.LoadNewbies())
            {
                Utils.Log("CharGen.LoadNewbies failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Load Spells
            if (!Spell.LoadSpells())
            {
                Utils.Log("Spell.loadSpells() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Establish Spawn Zones
            if (!Facet.EstablishSpawnZones())
            {
                Utils.Log("Facet.EstablishSpawnZones() failed.", Utils.LogType.SystemFatalError);
                return -1;
            } 
            #endregion

            #region Create NPC Catalog
            Utils.Log("Creating NPC Catalog.", Utils.LogType.SystemGo);
            NPC.LoadNPCDictionary(); 
            #endregion

            #region Spawn NPCs
            Utils.Log("Spawning NPCs.", Utils.LogType.SystemGo);
            NPC.DoSpawn(); 
            #endregion

            #region Clear Stores / Restock stores
            if (ConfigurationManager.AppSettings["ClearStores"].ToLower() == "true") // clear all store items that are not original store items
                StoreItem.ClearStores();

            if (ConfigurationManager.AppSettings["RestockStores"].ToLower() == "true") // restock store merchants
                StoreItem.RestockStores();
            #endregion

            StartTimers();


            IO io = new IO(3000); // get our network stuff ready
            ProtoClientIO p_io = new ProtoClientIO();

            if (!io.Open())
            {
                Utils.Log("Failed to start network services.", Utils.LogType.SystemFatalError);
                return -1;
            }
            p_io.startProtoServer();
            main.RunGame(io);

            return 0;
        }

        private static void StartTimers()
        {
            m_displayTimer = new System.Timers.Timer();
            m_displayTimer.Elapsed += new ElapsedEventHandler(UpdateServerStatus);
            m_displayTimer.Interval = 30000;
            m_displayTimer.Start();

            m_roundTimer = new System.Timers.Timer();
            m_roundTimer.Elapsed += new ElapsedEventHandler(RoundEvent); // player rounds (5 seconds)
            m_roundTimer.Interval = 5000;
            m_roundTimer.Start();
            Utils.Log("Master round timer started.", Utils.LogType.SystemGo);

            m_saveTimer = new System.Timers.Timer();
            m_saveTimer.Elapsed += new ElapsedEventHandler(SaveEvent);
            m_saveTimer.Interval = 30000;
            m_saveTimer.Start();
            Utils.Log("Save timer started.", Utils.LogType.SystemGo);

            m_janitorTimer = new System.Timers.Timer();
            m_janitorTimer.Elapsed += new ElapsedEventHandler(JanitorEvent);  // janitor timer
            m_janitorTimer.Interval = 10000;
            m_janitorTimer.Start();
            Utils.Log("Janitor round timer started.", Utils.LogType.SystemGo);

            m_chronoTimer = new System.Timers.Timer();
            m_chronoTimer.Elapsed += new ElapsedEventHandler(World.ShiftDailyCycle); // time change (30 minutes)
            m_chronoTimer.Interval = 1800000;
            m_chronoTimer.Start();
            Utils.Log("Chronology timer started.", Utils.LogType.SystemGo);

            m_lunarTimer = new System.Timers.Timer();
            m_lunarTimer.Elapsed += new ElapsedEventHandler(World.ShiftLunarCycle); // moon phases timer (90 minutes)
            m_lunarTimer.Interval = 7200000;
            m_lunarTimer.Start();
            Utils.Log("Lunar cycle timer started.", Utils.LogType.SystemGo);

            m_inactivityTimer = new System.Timers.Timer();
            m_inactivityTimer.Elapsed += new ElapsedEventHandler(InactivityEvent);
            m_inactivityTimer.Interval = 10000;
            m_inactivityTimer.Start();
            Utils.Log("Inactivity timer started.", Utils.LogType.SystemGo);
        }

        protected void RunGame(IO io) // the primary loop function for the game
        {
            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.Running;

            start: Utils.Log("Starting main game loop.", Utils.LogType.SystemGo);

            try
            {
                while (DragonsSpineMain.ServerStatus <= DragonsSpineMain.ServerState.Locked)
                {
                    this.CleanupLists();
                    io.HandleNewConnections();
                    io.GetInput();
                    io.ProcessRealTimeCommands();
                    io.SendOutput();
                    System.Threading.Thread.Sleep(100);
                }
            }
            catch (Exception e)
            {
                Utils.Log("Exception Data: " + e.Data + " Source: " + e.Source + " TargetSite: " + e.TargetSite, Utils.LogType.Exception);
                Utils.LogException(e);
                goto start; //this is more efficient than embedding the try/catch inside of the while loop
            }
            IO.Close();
            System.Environment.Exit(0);
        }

        private void CleanupLists()
        {
            #region Remove
            try
            {
                if (IO.pplToRemoveFromLogin)
                {
                    foreach (Character ch in new ArrayList(IO.removeFromLogin))
                    {
                        Character.loginList.Remove(ch);
                    }
                    IO.removeFromLogin.Clear();
                    IO.pplToRemoveFromLogin = false;
                }
                if (IO.pplToRemoveFromCharGen)
                {
                    foreach (Character ch in new ArrayList(IO.removeFromCharGen))
                    {
                        Character.charGenList.Remove(ch);
                    }
                    IO.removeFromCharGen.Clear();
                    IO.pplToRemoveFromCharGen = false;
                }
                if (IO.pplToRemoveFromMenu)
                {
                    foreach (Character ch in new ArrayList(IO.removeFromMenu))
                    {
                        Character.menuList.Remove(ch);
                    }
                    IO.removeFromMenu.Clear();
                    IO.pplToRemoveFromMenu = false;
                }
                if (IO.pplToRemoveFromLimbo)
                {
                    foreach (Character ch in new ArrayList(IO.removeFromLimbo))
                    {
                        Character.confList.Remove(ch);
                    }
                    IO.removeFromLimbo.Clear();
                    IO.pplToRemoveFromLimbo = false;
                }
                if (IO.pplToRemoveFromWorld)
                {
                    try
                    {
                        foreach (Character ch in new ArrayList(IO.removeFromWorld))
                        {
                            Character.allCharList.Remove(ch);
                            if (ch.IsPC)
                            {
                                Character.pcList.Remove(ch);
                            }
                            else
                            {
                                Character.NPCList.Remove(ch);
                            }
                        }
                        IO.removeFromWorld.Clear();
                        IO.pplToRemoveFromWorld = false;
                    }
                    catch (Exception e)
                    {
                        Utils.LogException(e);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.Log("Error in Cleanup Lists <REMOVE>", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
            #endregion
            #region Add
            try
            {
                if (IO.pplToAddToLogin)
                {
                    foreach (Character ch in new ArrayList(IO.addToLogin))
                    {
                        Character.loginList.Add(ch);
                    }
                    IO.addToLogin.Clear();
                    IO.pplToAddToLogin = false;
                }
                if (IO.pplToAddToCharGen)
                {
                    foreach (Character ch in new ArrayList(IO.addToCharGen))
                    {
                        Character.charGenList.Add(ch);
                    }
                    IO.addToCharGen.Clear();
                    IO.pplToAddToCharGen = false;
                }
                if (IO.pplToAddToMenu)
                {
                    foreach (Character ch in new ArrayList(IO.addToMenu))
                    {
                        Character.menuList.Add(ch);
                        if (ch.afk) { ch.ResetAFK(); }
                    }
                    Protocol.UpdateUserLists(); // send updated user lists to protocol users
                    IO.addToMenu.Clear();
                    IO.pplToAddToMenu = false;
                }
                if (IO.pplToAddToLimbo)
                {
                    foreach (Character ch in new ArrayList(IO.addToLimbo))
                    {
                        Character.confList.Add(ch);
                    }
                    Protocol.UpdateUserLists(); // send updated user lists to protocol users
                    IO.addToLimbo.Clear();
                    IO.pplToAddToLimbo = false;
                }
                if (IO.pplToAddToWorld)
                {
                    foreach (Character ch in new ArrayList(IO.addToWorld))
                    {
                        Character.allCharList.Add(ch);
                        if (ch.IsPC)
                        {
                            Character.pcList.Add(ch);
                            if (ch.afk) { ch.ResetAFK(); }
                            Protocol.UpdateUserLists(); // send updated user lists to protocol users
                        }
                        else
                        {
                            Character.NPCList.Add(ch);
                        }
                    }
                    IO.addToWorld.Clear();
                    IO.pplToAddToWorld = false;
                }
            }
            catch (Exception e)
            {
                Utils.Log("Error in Cleanuplists <ADD>", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
            #endregion
        }

        #region Timer Events

        private static void NpcEvent(object sender, ElapsedEventArgs eventArgs)
        {
            DragonsSpineMain.NPCRound++;
            #region Old NPC Event
            //foreach (NPC npc in new List<Character>(Character.NPCList))
            //{
            //    Character ch = npc as Character;
            //    if (ch.effectList.ContainsKey(Effect.EffectType.Wizard_Eye))
            //        continue;

            //    //if (World.GetNumberPlayersInMap(ch.MapID) == 0) continue;
            //    if (!ProcessEmptyWorld)
            //    {
            //        if (World.GetNumberPlayersInMap(ch.MapID) == 0)
            //            continue;
            //    }

            //    if (ch.CurrentCell != null)
            //        ch.Map.UpdateCellVisible(ch.CurrentCell);

            //    if (npc.RoundsRemaining > 0)
            //    {
            //        npc.RoundsRemaining--;

            //        if (npc.RoundsRemaining <= 0)
            //        {
            //            if (npc.special.ToLower().Contains("figurine"))
            //            {
            //                Rules.DespawnFigurine(npc);
            //                continue;
            //            }
            //            else if (npc.special.ToLower().Contains("despawn"))
            //            {
            //                npc.RemoveFromWorld();
            //                continue;
            //            }
            //        }
            //    }

            //    ch.debug = 0; // reset each round
            //    ch.cmdWeight = 0;

            //    if (ch.IsSummoned) // only summoned npcs gain age
            //    {
            //        ch.Age++;
            //    }

            //    if (ch.Stunned == 0)
            //    {
            //        if (ch.IsFeared)
            //            Creature.AIMakePCMove(ch);
            //        else
            //        {
            //            if (ch.Group == null || (ch.Group != null && ch.Group.GroupLeaderID == ch.worldNpcID))
            //                ch.doAI();
            //        }
            //    }
            //    else ch.Stunned -= 1;

            //    if (ch.Group != null && ch.Group.GroupLeaderID == ch.worldNpcID)
            //    {
            //        foreach (NPC groupNPC in new List<NPC>(ch.Group.GroupNPCList))
            //            if (groupNPC.worldNpcID != ch.worldNpcID)
            //                if (groupNPC.CurrentCell != ch.CurrentCell)
            //                    groupNPC.CurrentCell = ch.CurrentCell;
            //    }

            //    #region Follow Mode for Controlled NPCs
            //    if (ch.PetOwner != null && ch.canCommand)
            //    {
            //        Character owner = ch.PetOwner;
            //        if (owner != null)
            //        {
            //            if (Map.FindTargetInView(ch, owner.Name, false, false) != null)
            //            {
            //                ch.CurrentCell = owner.CurrentCell; // place the npc in the pc's cell
            //                if (!ch.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // perma-root
            //                {
            //                    Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, ch, -1, null);
            //                }
            //            }
            //            else
            //            {
            //                if (ch.questList.Count > 0)
            //                {
            //                    if (ch.questList[0].FailStrings.Count > 0) // shout a fail string
            //                    {
            //                        ch.SendShout(ch.Name + ": " + ch.questList[0].FailStrings[Convert.ToInt16(Rules.dice.Next(1,
            //                            ch.questList[0].FailStrings.Count))]);
            //                    }
            //                }

            //                if (ch.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // remove perma-root
            //                {
            //                    ch.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
            //                }
            //            }
            //        }
            //    }
            //    #endregion

            //    ch.numAttackers = 0;

            //    if (ch.CurrentCell != null)
            //        ch.Map.UpdateCellVisible(ch.CurrentCell);
            //}
            #endregion
        }
        private static void RoundEvent(object sender, ElapsedEventArgs eventArgs)
        {
            // Increase the Game round for time tracking 
            World.magicCordLastRound.Clear();
            foreach (String cord in new List<string>(World.magicCordThisRound))
            {
                World.magicCordLastRound.Add(cord);
            }
            World.magicCordThisRound.Clear();
            DragonsSpineMain.GameRound++;
            // Players (Character Class) processes own timer per character
            #region Old Player Event
            //int i = 0;            
            //try
            //{
            //    foreach(Character ch in new List<Character>(Character.pcList)) // process the commands for players in the world
            //    {
            //        //Character ch = Character.pcList[i];

            //        if (ch.CurrentCell != null)
            //            ch.Map.UpdateCellVisible(ch.CurrentCell);

            //        Character.ValidatePlayer(ch);

            //        if (ch.IsDead && !ch.CurrentCell.ContainsPlayerCorpse(ch.Name))
            //        {
            //            foreach (Character chold in new List<Character>(Character.pcList))
            //            {
            //                if (chold.RightHand != null && chold.RightHand.itemType == Globals.eItemType.Corpse &&
            //                    chold.RightHand.special == ch.Name)
            //                {
            //                    ch.CurrentCell = chold.CurrentCell;
            //                    ch.corpseIsCarried = true;
            //                }
            //                else if (chold.LeftHand != null && chold.LeftHand.itemType == Globals.eItemType.Corpse &&
            //                    chold.LeftHand.special == ch.Name)
            //                {
            //                    ch.CurrentCell = chold.CurrentCell;
            //                    ch.corpseIsCarried = true;
            //                }
            //            }
            //        }
            //        else ch.corpseIsCarried = false;

            //        ch.cmdWeight = 0; // reset the number of commands entered

            //        if (!ch.InUnderworld && !ch.IsDead) // age the character if they are not in the Underworld and not dead
            //        {
            //            ch.Age++;
            //            Rules.DoAgingEffect(ch);
            //        }

            //        ch.RoundsPlayed++; // add to the total roundsPlayed

            //        // every x rounds the player suffers skill loss from time - DISABLED
            //        //if (!ch.IsDead && !ch.InUnderworld && ch.RoundsPlayed % Globals.SKILL_LOSS_DIVISOR == 0)
            //        //{
            //        //    Skills.SkillLossOverTime(ch);
            //        //}

            //        if (ch.preppedSpell != null)
            //        {
            //            ch.Mana--;
            //            ch.updateMP = true;
            //            if (ch.Mana <= 0)
            //            {
            //                ch.Mana = 0;
            //                ch.preppedSpell = null;
            //                ch.WriteToDisplay("Your spell has been lost.");
            //            }
            //        }
            //        if (ch.Stunned == 0 && !ch.IsFeared)
            //        {
            //            //IO.ProcessCommands(ch);

            //            #region Follow Mode for Players
            //            if (ch.IsPC)
            //            {
            //                if (ch.FollowName != "" && Array.IndexOf(Command.breakFollowCommands, ch.CommandType) != -1)
            //                {
            //                    ch.BreakFollowMode();
            //                }
            //                else if (ch.FollowName != "")
            //                {
            //                    PC pc = PC.GetOnline(ch.FollowName);
            //                    if (pc == null)
            //                    {
            //                        ch.BreakFollowMode();
            //                    }
            //                    else if (pc.IsDead)
            //                    {
            //                        ch.BreakFollowMode();
            //                    }
            //                    else
            //                    {
            //                        if (Map.FindTargetInView(ch, pc.Name, false, true) != null)
            //                        {
            //                            ch.CurrentCell = pc.CurrentCell;
            //                            ch.Timeout = Character.INACTIVITY_TIMEOUT;
            //                        }
            //                        else
            //                        {
            //                            ch.BreakFollowMode();
            //                        }
            //                    }
            //                }
            //            }
            //            #endregion
            //        }
            //        else
            //        {
            //            if (ch.Stunned > 0)
            //                ch.Stunned -= 1;
            //            else if (ch.IsFeared)
            //                Creature.AIMakePCMove(ch);
            //        }

            //        ch.numAttackers = 0; // reset the number of attackers after commands are processed

            //        //Character.ValidatePlayer(ch);

            //        // update the visible cells
            //        if (ch.CurrentCell != null)
            //            ch.Map.UpdateCellVisible(ch.CurrentCell);
            //    }

            //    foreach (Character ch in new List<Character>(Character.pcList))
            //    {
            //        if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
            //            Protocol.ShowMap(ch);
            //        else if (ch.protocol == "old-kesmai")
            //            ch.CurrentCell.showMapOldKesProto(ch);
            //        else if (ch.CurrentCell != null)
            //            ch.CurrentCell.showMap(ch);
            //    }


            //}
            //catch (Exception e)
            //{
            //    Utils.LogException(e);
            //}
            #endregion
        }
        private static void SaveEvent(object sender, ElapsedEventArgs eventArgs)
        {
            foreach (PC pc in new List<Character>(Character.pcList))
            {
                pc.Save();
            }

        }
        private static void JanitorEvent(object sender, ElapsedEventArgs eventArgs)
        {
            try
            {
                foreach (Facet facet in World.Facets)
                {
                    foreach (SpawnZone szl in facet.Spawns.Values)
                        if (szl.NumberInZone < szl.MaxAllowedInZone)
                            szl.Timer++;

                    foreach (Land land in facet.Lands)
                    {
                        foreach (Map map in land.Maps)
                        {
                            if (map != null)
                            {
                                foreach (Cell cell in map.cells.Values)
                                {
                                    if (cell != null)
                                    {
                                        if (!cell.IsLair && cell.CellGraphic != "MM" && cell.CellGraphic != "CC" && cell.Items.Count > 0)
                                        {
                                            for (int a = 0; a < cell.Items.Count; a++)
                                            {
                                                Item item = cell.Items[a];
                                                // decay for corpses
                                                if (item.itemType == Globals.eItemType.Corpse)
                                                {
                                                    if (item.itemID >= 600000)
                                                    {
                                                        if (item.dropRound < DragonsSpineMain.GameRound - World.PlayerCorpseDecayTimer)
                                                        {
                                                            PC pc = PC.GetOnline(item.special);
                                                            if (pc == null)
                                                            {
                                                                Item.dumpCorpse(item, cell);
                                                                cell.Remove(item);
                                                            }
                                                            else if (!pc.IsDead)
                                                            {
                                                                // TODO?
                                                            }
                                                            else
                                                            {
                                                                Rules.DeadRest(pc);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (item.dropRound < DragonsSpineMain.GameRound - World.NPCCorpseDecayTimer)
                                                        {
                                                            Item.dumpCorpse(item, cell);
                                                            cell.Remove(item);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //decay for items that aren't attuned
                                                    if (item.attunedID <= 0)
                                                    {
                                                        if (item.dropRound < DragonsSpineMain.GameRound - World.ItemDecayTimer)
                                                        {
                                                            if (item.itemType == Globals.eItemType.Coin)
                                                            {
                                                                if (World.Lottery == null)
                                                                {
                                                                    World.Lottery = new long[World.GetFacetByIndex(0).Lands.Count];
                                                                }
                                                                World.Lottery[cell.LandID] = World.Lottery[cell.LandID] + (long)(item.coinValue / 2);
                                                            }
                                                            cell.Remove(item);
                                                        }
                                                    }
                                                    //decay for attuned items
                                                    else
                                                    {
                                                        if (item.dropRound < DragonsSpineMain.GameRound - World.AttunedItemDecayTimer)
                                                        {
                                                            cell.Remove(item);
                                                            Utils.Log("Janitor removed attuned item: " + item.GetLogString() + " PlayerID: " + item.attunedID + ".", Utils.LogType.SystemWarning);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //the janitor is also a gardener, and places berries where they should be
                                        if (cell.balmBerry || cell.manaBerry || cell.poisonBerry || cell.stamBerry && cell.droppedBerry < cell.dailyBerry)
                                        {
                                            bool hasBalmBerries = false;
                                            bool hasManaBerries = false;
                                            bool hasPoisonBerries = false;
                                            bool hasStamBerries = false;

                                            foreach (Item berryItem in cell.Items)
                                            {
                                                if (berryItem.itemID == Item.ID_BALMBERRY && cell.balmBerry) { hasBalmBerries = true; break; }
                                                if (berryItem.itemID == Item.ID_POISONBERRY && cell.poisonBerry) { hasPoisonBerries = true; break; }
                                                if (berryItem.itemID == Item.ID_MANABERRY && cell.manaBerry) { hasManaBerries = true; break; }
                                                if (berryItem.itemID == Item.ID_STAMINABERRY && cell.stamBerry) { hasStamBerries = true; break; }
                                            }

                                            if (!hasBalmBerries && !hasManaBerries && !hasPoisonBerries && !hasStamBerries)
                                            {
                                                Item newBerries = new Item();

                                                if (cell.balmBerry && !hasBalmBerries) { newBerries = Item.CopyItemFromDictionary(Item.ID_BALMBERRY); }
                                                else if (cell.poisonBerry && !hasPoisonBerries) { newBerries = Item.CopyItemFromDictionary(Item.ID_POISONBERRY); }
                                                else if (cell.manaBerry && !hasManaBerries) { newBerries = Item.CopyItemFromDictionary(Item.ID_MANABERRY); }
                                                else if (cell.stamBerry && !hasStamBerries) { newBerries = Item.CopyItemFromDictionary(Item.ID_STAMINABERRY); }
                                                cell.Add(newBerries);
                                                cell.droppedBerry++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                Utils.Log("Failure in JanitorEvent while pruning cells.", Utils.LogType.SystemFailure);
            }

            NPC.DoSpawn();
        }
        private static void InactivityEvent(object sender, ElapsedEventArgs eventArgs)
        {
            int a;

            for (a = 0; a < Character.pcList.Count; a++) // loop through all characters in the game and do maintenence
            {
                Character ch = Character.pcList[a];
                if (ch.afk || (ch.corpseIsCarried && ch.socketConnected())) continue;
                ch.Timeout--;
                if (ch.Timeout < 0 || !ch.socketConnected())
                {
                    if (ch.IsDead)
                    {
                        Command.ParseCommand(ch, "rest", "");
                    }

                    ch.RemoveFromWorld();
                    ch.RemoveFromServer();
                    Utils.Log(ch.GetLogString() + " disconnected from the world for inactivity.", Utils.LogType.Timeout);
                }
            }
            for (a = 0; a < Character.loginList.Count; a++) // check for inactivity at the login prompt
            {
                Character ch = Character.loginList[a];
                ch.Timeout--;
                if (ch.Timeout < 0 || !ch.socketConnected())
                {
                    if (!ch.socketConnected())
                    {
                        Utils.Log(ch.HostName + " (" + ch.IPAddress + ") lost connection, removing from login.", Utils.LogType.Disconnect);
                    }
                    else
                    {
                        Utils.Log(ch.HostName + " (" + ch.IPAddress + ") disconnected from login for inactivity.", Utils.LogType.Timeout);
                    }
                    ch.RemoveFromLogin();
                    ch.RemoveFromServer();
                }
            }
            for (a = 0; a < Character.confList.Count; a++) // check for inactivity in limbo
            {
                Character ch = Character.confList[a];
                if (ch.afk) continue;

                ch.Timeout--;

                if (ch.Timeout < 0 || !ch.socketConnected())
                {
                    if (!ch.socketConnected())
                    {
                        if (!ch.IsInvisible)
                        {
                            ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                        }
                        Utils.Log(ch.GetLogString() + " lost connection, removing from limbo.", Utils.LogType.Disconnect);
                    }
                    else
                    {
                        if (!ch.IsInvisible)
                        {
                            ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                        }
                        Utils.Log(ch.GetLogString() + " disconnected from limbo for inactivity.", Utils.LogType.Timeout);
                    }
                    ch.RemoveFromLimbo();
                    ch.RemoveFromServer();
                }
            }
            for (a = 0; a < Character.charGenList.Count; a++) // check for inactivity at the character generator
            {
                Character ch = Character.charGenList[a];
                ch.Timeout--;
                if (ch.Timeout < 0 || !ch.socketConnected())
                {
                    if (!ch.socketConnected())
                    {
                        if (ch.Name != "Nobody")
                        {
                            Utils.Log(ch.GetLogString() + " lost connection, removing from chargen.", Utils.LogType.Disconnect);
                        }
                        else
                        {
                            Utils.Log(ch.HostName + " (" + ch.IPAddress + ") lost connection, removing from chargen.", Utils.LogType.Disconnect);
                        }
                    }
                    else
                    {
                        if (ch.Name != "Nobody")
                        {
                            Utils.Log(ch.GetLogString() + " disconnected from chargen for inactivity.", Utils.LogType.Timeout);
                        }
                        else
                        {
                            Utils.Log(ch.HostName + " (" + ch.IPAddress + ") disconnected from chargen for inactivity.", Utils.LogType.Timeout);
                        }
                    }
                    ch.RemoveFromCharGen();
                    ch.RemoveFromServer();

                }
            }
            for (a = 0; a < Character.menuList.Count; a++) // check for inactivity at the menu
            {
                Character ch = Character.menuList[a];
                ch.Timeout--;
                if (ch.Timeout < 0 || !ch.socketConnected())
                {
                    if (!ch.socketConnected())
                    {
                        Utils.Log(ch.GetLogString() + " lost connection, removing from menu.", Utils.LogType.Disconnect);
                    }
                    else
                    {
                        Utils.Log(ch.GetLogString() + " disconnected from the menu for inactivity.", Utils.LogType.Timeout);
                    }
                    ch.RemoveFromMenu();
                    ch.RemoveFromServer();
                }
            }
        }
        private static void UpdateServerStatus(object sender, ElapsedEventArgs eventArgs)
        {
            string DeviceID = "";
            string Usage = "??";
            string npc_count = Character.NPCList.Count.ToString();
            int player_count = Character.pcList.Count + Character.menuList.Count + Character.confList.Count + Character.charGenList.Count;
            //ManagementScope mgmtScope;
            //mgmtScope = new ManagementScope(@"\\.\root\cimv2");
            //mgmtScope.Connect();
            //ManagementPath mp = new ManagementPath("Win32_Processor");
            //ManagementClass mc = new ManagementClass(mgmtScope, mp, null); 
            //ManagementObjectCollection procs = mc.GetInstances();
            //foreach (ManagementObject mo in procs)
            //{
                
            //    foreach (PropertyData pd in mo.Properties)
            //    {
            //        if (pd.Name == "DeviceID")
            //            DeviceID = pd.Value.ToString();
            //        if (pd.Name == "LoadPercentage")
            //            Usage = pd.Value.ToString();
            //    }
            //}
            Console.WriteLine(DateTime.Now.ToLocalTime()+": NPCs: [" + npc_count + "] | Players: [" + player_count.ToString() + "] | CPU: ["+Usage+"%] | Rnd: [" + DragonsSpineMain.GameRound + "]");

        }

        #endregion

        /// <summary>
        /// Comiples the scripts DLL.
        /// </summary>
        /// <returns>True if successful.</returns>
        public static bool CompileScripts()
        {
            string scriptDirectory = Utils.GetStartupPath() + Path.DirectorySeparatorChar + "Scripts";

            if (!Directory.Exists(scriptDirectory))
            {
                Directory.CreateDirectory(scriptDirectory);
            }

            string[] parameters = Instance.Settings.ScriptAssemblies.Split(',');

            return ScriptManager.CompileScripts(false, scriptDirectory, Instance.Settings.ScriptCompilationTarget, parameters);
        }
        public static void SetInstance(DragonsSpineMain server)
        {
            m_instance = server;
            
        }
    }
}
