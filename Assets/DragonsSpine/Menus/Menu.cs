namespace DragonsSpine
{
	using System;
	using System.Net;
	using System.Net.Sockets;
	using System.Collections;
    using System.Collections.Generic;
	
	public class Menu
	{
        public static List<PC> menuList = new List<PC>();

		public static void PrintMainMenu(Character ch)
		{
            DateTime lastOnline = Account.GetLastOnline(ch.accountID);

			if(ch.protocol == DragonsSpineMain.APP_PROTOCOL)
			{
				ch.Write(Protocol.DETECT_CLIENT);

                Protocol.sendWorldNews(ch);

                if (!ch.sentWorldInformation)
                {
                    ch.Write(Protocol.WORLD_INFORMATION);
                    Protocol.sendWorldVersion(ch);
                    Protocol.sendWorldSpells(ch);
                    Protocol.sendWorldLands(ch);
                    Protocol.sendWorldMaps(ch);
                    Protocol.sendWorldCharGen(ch);
                    Protocol.SendAccountInfo(ch);
                    Protocol.SendCharacterList(ch);
                    ch.sentWorldInformation = true;
                }

                if (ch.PCState == Globals.ePlayerState.MENU)
                {
                    ch.Write(Protocol.MENU_MAIN);
                }
                // TODO: send last online

                Protocol.SendCurrentCharacterID(ch); // send current character ID

                Protocol.SendUserList(ch);                
			}
			else
			{
                if (ch.protocol != "old-kesmai")
                {
                    ch.Write(Protocol.DETECT_PROTOCOL);
                }
				Map.clearMap(ch);
				ch.WriteLine(DragonsSpineMain.APP_NAME+ " ("+DragonsSpineMain.APP_VERSION+") Main Menu");
                if (lastOnline.ToString() == "1/1/1900 12:00:00 AM")
                {
                    ch.WriteLine("Welcome " + ch.account + "!");
                }
                else
                {
                    ch.WriteLine("Welcome back " + ch.account + "!");
                    ch.WriteLine("Your last visit to " + DragonsSpineMain.APP_NAME + " was on "+ lastOnline.ToShortDateString() + " at " +lastOnline.ToShortTimeString()+".");
                }
				ch.WriteLine("Current Character: "+ch.Name+" Level: "+ch.Level+" Class: "+ch.classFullName+" Land: " + ch.Land.Name + " Map: "+ch.Map.Name);
                // new mail message
                if (Mail.HasNewMail(DAL.DBMail.GetMailByReceiverID(ch.PlayerID)))
                    ch.WriteLine("You have new mail. Please visit a mailbox in the game world to read it.");
				ch.WriteLine("");
				ch.WriteLine("1. Enter Game");
				ch.WriteLine("2. Enter Conference Room");
				ch.WriteLine("3. Disconnect");
				ch.WriteLine("4. View Account");
				ch.WriteLine("5. Change Protocol ("+ch.protocol+")");
				ch.WriteLine("6. Change/Create/Delete Character");
				ch.WriteLine("");
				ch.Write("Command: ");
			}

            Account.SetLastOnline(ch.accountID);
		}

		public static void PrintCharMenu(Character ch)
		{
			if(ch.protocol != DragonsSpineMain.APP_PROTOCOL)
			{
				ch.WriteLine("Dragon's Spine ("+DragonsSpineMain.APP_VERSION+") Character Menu");
				ch.WriteLine("Current Character: "+ch.Name+" Level: "+ch.Level+" Class: "+ch.classFullName+" Land: " + ch.Land.Name + " Map: " + ch.Map.Name);
				ch.WriteLine("");
				ch.WriteLine("1. Create New Character");
				ch.WriteLine("2. Change Character");
				ch.WriteLine("3. Delete Character");
				ch.WriteLine("4. Return to Main Menu");
				ch.WriteLine("");
				ch.Write("Command: ");
			}
		}

		public static void PrintAccountMenu(Character ch, string message)
		{
			if(ch.protocol != DragonsSpineMain.APP_PROTOCOL)
			{
				Map.clearMap(ch);
				ch.WriteLine("Dragon's Spine ("+DragonsSpineMain.APP_VERSION+") Account Menu");
				ch.WriteLine("");
				ch.WriteLine("Account: "+ch.account+" Current Marks: "+ch.currentMarks);
				ch.WriteLine("");
				if(message != ""){ch.WriteLine(message); ch.WriteLine("");}
				ch.WriteLine("1. Change Password");
				ch.WriteLine("2. Return to Main Menu");
                ch.WriteLine("3. Read/Send Mail (*Disabled*)");
				ch.WriteLine("");
			}
		}
	}
}