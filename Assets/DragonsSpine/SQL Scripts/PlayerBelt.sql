DROP TABLE [PlayerBelt]

CREATE TABLE [PlayerBelt] 
(
	[BeltID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID),
	[BeltSlot]	[int]	NULL ,
	[BeltItem]	[int]	NULL ,
	[Attuned]	[int]	NULL ,
	[Special]	[nvarchar] (255) NULL ,
	[CoinValue] [float] NULL ,
	[Charges]	[int] NULL ,
	[Venom]		[int] NULL ,
	[WillAttune][bit] NULL ,
	[FigExp]	[bigint] NULL
) 
ON [PRIMARY]
GO





