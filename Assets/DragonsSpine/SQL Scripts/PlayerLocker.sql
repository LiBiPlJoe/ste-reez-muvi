DROP TABLE [PlayerLocker]

CREATE TABLE [PlayerLocker] 
(
	[LockerID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID),
	[LockerSlot]	[int]	NULL ,
	[LockerItem]	[int]	NULL ,
	[Attuned]	[int]	NULL ,
	[Special]	[nvarchar](255)	NULL ,
	[CoinValue]	[float]	NULL ,
	[Charges]	[int]	NULL ,
	[WillAttune][bit]	NULL ,
	[FigExp]	[bigint]NULL
) 
ON [PRIMARY]
GO





