DROP TABLE [PlayerWearing]

CREATE TABLE [PlayerWearing] 
(
	[WearingID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID),
	[WearingSlot]	[int]	NULL,
	[WearingItem]	[int]	NULL,
	[Attuned]	[int]	NULL,
	[WearLocationCode]	[float]	NULL,
	[Special]	[nvarchar] (255) NULL,
	[CoinValue]	[float] NULL,
	[Charges]	[int]	NULL,
	[Venom]	[int]	NULL,
	[WillAttune][bit]	NULL
) 
ON [PRIMARY]
GO





