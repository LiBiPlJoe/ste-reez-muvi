if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_ItemTypeCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_ItemTypeCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_BlockRankCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_BlockRankCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_BlockRankCode1]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_BlockRankCode1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_BlockRankCode2]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_BlockRankCode2
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_EffectTypeCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_EffectTypeCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_LootTypeCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_LootTypeCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_SizeCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_SizeCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Item_WearLocationCode]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_WearLocationCode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ItemTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ItemTypeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AlignCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AlignCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BaseTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BaseTypeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BlockRankCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BlockRankCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CellInfo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CellInfo]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CharacterClassCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CharacterClassCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EffectTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[EffectTypeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Item]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Item]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Land]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Land]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LootTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[LootTypeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Map]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Map]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NPC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[NPC]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NpcTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[NpcTypeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Segue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Segue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SizeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SizeCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SpawnZoneLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SpawnZoneLink]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WearLocationCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[WearLocationCode]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ItemTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[ItemTypeCode] (
	[ItemTypeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AlignCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[AlignCode] (
	[AlignCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BaseTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[BaseTypeCode] (
	[NpcTypeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BlockRankCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[BlockRankCode] (
	[BlockRankCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CellInfo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[CellInfo] (
	[CellInfoID] [int] IDENTITY (1, 1) NOT NULL ,
	[LandID] [int] NOT NULL ,
	[MapID] [int] NOT NULL ,
	[Xcord] [int] NOT NULL ,
	[Ycord] [int] NOT NULL ,
	[CellDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CharacterClassCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[CharacterClassCode] (
	[CharacterClassCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EffectTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[EffectTypeCode] (
	[EffectTypeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO








if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Item]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[CatalogItemID] (
	[CatalogItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[ItemTypeCode] [int] NOT NULL ,
	[WearLocationCode] [int] NOT NULL ,
	[Weight] [int] NULL ,
	[CoinValue] [int] NULL ,
	[SizeCode] [int] NULL ,
	[BlockRankCode] [int] NOT NULL ,
	[EffectTypeCode] [int] NOT NULL ,
	[EffectAmount] [int] NULL ,
	[LootLandID] [int] NULL ,
	[LootMapID] [int] NULL ,
	[LootTypeCode] [int] NULL ,
	[ItemName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ShortDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LongDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

			<skillType>magic</skillType>
			<bookType>1</bookType>
			<currPage>0</currPage>
			<maxPages>50</maxPages>
			<lootLand>0</lootLand>
			<lootMap>0</lootMap>
			<lootType>9</lootType>
			<vRandLow>0</vRandLow>
			<vRandHigh>0</vRandHigh>





if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Land]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Land] (
	[LandID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ShortDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LongDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LootTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LootTypeCode] (
	[LootTypeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Map]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Map] (
	[MapID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ShortDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LongDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NPC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[NPC] (
	[NpcID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MovementString] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Special] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NpcTypeCode] [int] NOT NULL ,
	[BaseTypeCode] [int] NOT NULL ,
	[Gold] [int] NULL ,
	[CharacterClassCode] [int] NOT NULL ,
	[Experience] [int] NULL ,
	[Hits] [int] NULL ,
	[HitsMax] [int] NULL ,
	[AlignCode] [int] NULL ,
	[Stamina] [int] NULL ,
	[Speed] [int] NULL ,
	[Strength] [int] NULL ,
	[Dexterity] [int] NULL ,
	[Intelligence] [int] NULL ,
	[Wisdom] [int] NULL ,
	[Constitution] [int] NULL ,
	[Charisma] [int] NULL ,
	[Unarmed] [int] NULL ,
	[LootVeryCommon] [int] NULL ,
	[SpawnArmorItemID] [int] NULL ,
	[SpawnLeftHandItemID] [int] NULL ,
	[spawnRightHandItemID] [int] NULL ,
	[MaceSkill] [int] NULL ,
	[BowSkill] [int] NULL ,
	[DaggerSkill] [int] NULL ,
	[RapierSkill] [int] NULL ,
	[TwohandedSkill] [int] NULL ,
	[StaffSkill] [int] NULL ,
	[ShurikenSkill] [int] NULL ,
	[SwordSkill] [int] NULL ,
	[ThreestaffSkill] [int] NULL ,
	[HalberdSkill] [int] NULL ,
	[ThieverySkill] [int] NULL ,
	[MagicSkill] [int] NULL ,
	[Difficulty] [int] NULL ,
	[IsAnimal] [bit] NOT NULL ,
	[TanningResult]	[varchar] (255) NULL ,
	[IsUndead] [bit] NOT NULL ,
	[IsHidden] [bit] NOT NULL ,
	[CanFly] [bit] NOT NULL ,
	[CanBreatheWater] [bit] NOT NULL ,
	[IsLairCritter] [bit] NOT NULL ,
	[IsMovable] [bit] NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NpcTypeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[NpcTypeCode] (
	[NpcTypeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Segue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Segue] (
	[SegueID] [int] IDENTITY (1, 1) NOT NULL ,
	[LandID] [int] NOT NULL ,
	[MapID] [int] NOT NULL ,
	[Xcord1] [int] NOT NULL ,
	[Ycord1] [int] NOT NULL ,
	[Xcord2] [int] NOT NULL ,
	[Ycord2] [int] NOT NULL ,
	[Height] [int] NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SizeCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[SizeCode] (
	[SizeCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SpawnZoneLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[SpawnZoneLink] (
	[SpawnZoneLinkID] [int] IDENTITY (1, 1) NOT NULL ,
	[NPCID] [int] NOT NULL ,
	[SpawnTimer] [int] NOT NULL ,
	[MaxAllowedInZone] [int] NOT NULL ,
	[SpawnMessage] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WearLocationCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[WearLocationCode] (
	[WearLocationCode] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
END

GO

ALTER TABLE [dbo].[ItemTypeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_ItemTypeCode] PRIMARY KEY  CLUSTERED 
	(
		[ItemTypeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[AlignCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_AlignCode] PRIMARY KEY  CLUSTERED 
	(
		[AlignCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BaseTypeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_BaseTypeCode] PRIMARY KEY  CLUSTERED 
	(
		[NpcTypeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BlockRankCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_BlockRankCode] PRIMARY KEY  CLUSTERED 
	(
		[BlockRankCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[CellInfo] WITH NOCHECK ADD 
	CONSTRAINT [PK_CellInfo] PRIMARY KEY  CLUSTERED 
	(
		[CellInfoID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[CharacterClassCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_CharacterClassCode] PRIMARY KEY  CLUSTERED 
	(
		[CharacterClassCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[EffectTypeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_EffectTypeCode] PRIMARY KEY  CLUSTERED 
	(
		[EffectTypeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Item] WITH NOCHECK ADD 
	CONSTRAINT [PK_Item] PRIMARY KEY  CLUSTERED 
	(
		[ItemID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Land] WITH NOCHECK ADD 
	CONSTRAINT [PK_Land] PRIMARY KEY  CLUSTERED 
	(
		[LandID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LootTypeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_LootTypeCode] PRIMARY KEY  CLUSTERED 
	(
		[LootTypeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Map] WITH NOCHECK ADD 
	CONSTRAINT [PK_Map] PRIMARY KEY  CLUSTERED 
	(
		[MapID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[NPC] WITH NOCHECK ADD 
	CONSTRAINT [PK_NPC] PRIMARY KEY  CLUSTERED 
	(
		[NpcID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[NpcTypeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_NpcTypeCode] PRIMARY KEY  CLUSTERED 
	(
		[NpcTypeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Segue] WITH NOCHECK ADD 
	CONSTRAINT [PK_Segue] PRIMARY KEY  CLUSTERED 
	(
		[SegueID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[SizeCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_SizeCode] PRIMARY KEY  CLUSTERED 
	(
		[SizeCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[SpawnZoneLink] WITH NOCHECK ADD 
	CONSTRAINT [PK_SpawnZoneLink] PRIMARY KEY  CLUSTERED 
	(
		[SpawnZoneLinkID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[WearLocationCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_WearLocationCode] PRIMARY KEY  CLUSTERED 
	(
		[WearLocationCode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[NPC] ADD 
	CONSTRAINT [DF__NPC__IsAnimal__7D78A4E7] DEFAULT (0) FOR [IsAnimal],
	CONSTRAINT [DF__NPC__IsUndead__7E6CC920] DEFAULT (0) FOR [IsUndead],
	CONSTRAINT [DF__NPC__IsHidden__7F60ED59] DEFAULT (0) FOR [IsHidden],
	CONSTRAINT [DF__NPC__CanFly__00551192] DEFAULT (0) FOR [CanFly],
	CONSTRAINT [DF__NPC__CanBreatheW__014935CB] DEFAULT (0) FOR [CanBreatheWater],
	CONSTRAINT [DF__NPC__IsLairCritt__023D5A04] DEFAULT (0) FOR [IsLairCritter],
	CONSTRAINT [DF__NPC__IsMovable__03317E3D] DEFAULT (0) FOR [IsMovable]
GO

ALTER TABLE [dbo].[Item] ADD 
	CONSTRAINT [FK_Item_BlockRankCode] FOREIGN KEY 
	(
		[BlockRankCode]
	) REFERENCES [dbo].[BlockRankCode] (
		[BlockRankCode]
	),
	CONSTRAINT [FK_Item_BlockRankCode1] FOREIGN KEY 
	(
		[BlockRankCode]
	) REFERENCES [dbo].[BlockRankCode] (
		[BlockRankCode]
	),
	CONSTRAINT [FK_Item_BlockRankCode2] FOREIGN KEY 
	(
		[BlockRankCode]
	) REFERENCES [dbo].[BlockRankCode] (
		[BlockRankCode]
	),
	CONSTRAINT [FK_Item_EffectTypeCode] FOREIGN KEY 
	(
		[EffectTypeCode]
	) REFERENCES [dbo].[EffectTypeCode] (
		[EffectTypeCode]
	),
	CONSTRAINT [FK_Item_ItemTypeCode] FOREIGN KEY 
	(
		[ItemTypeCode]
	) REFERENCES [dbo].[ItemTypeCode] (
		[ItemTypeCode]
	),
	CONSTRAINT [FK_Item_LootTypeCode] FOREIGN KEY 
	(
		[LootTypeCode]
	) REFERENCES [dbo].[LootTypeCode] (
		[LootTypeCode]
	),
	CONSTRAINT [FK_Item_SizeCode] FOREIGN KEY 
	(
		[SizeCode]
	) REFERENCES [dbo].[SizeCode] (
		[SizeCode]
	),
	CONSTRAINT [FK_Item_WearLocationCode] FOREIGN KEY 
	(
		[WearLocationCode]
	) REFERENCES [dbo].[WearLocationCode] (
		[WearLocationCode]
	)
GO




EXECUTE pr__SYS_MakeDeleteRecordProc AlignCode, 1 
EXECUTE pr__SYS_MakeInsertRecordProc AlignCode, 1 
EXECUTE pr__SYS_MakeSelectRecordProc AlignCode, 1 
EXECUTE pr__SYS_MakeUpdateRecordProc AlignCode, 1 

EXECUTE pr__SYS_MakeDeleteRecordProc BaseTypeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc BaseTypeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc BaseTypeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc BaseTypeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc CellInfo, 1
EXECUTE pr__SYS_MakeInsertRecordProc CellInfo, 1
EXECUTE pr__SYS_MakeSelectRecordProc CellInfo, 1
EXECUTE pr__SYS_MakeUpdateRecordProc CellInfo, 1

EXECUTE pr__SYS_MakeDeleteRecordProc CatalogItem, 1
EXECUTE pr__SYS_MakeInsertRecordProc CatalogItem, 1
EXECUTE pr__SYS_MakeSelectRecordProc CatalogItem, 1
EXECUTE pr__SYS_MakeUpdateRecordProc CatalogItem, 1

EXECUTE pr__SYS_MakeDeleteRecordProc CharacterClassCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc CharacterClassCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc CharacterClassCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc CharacterClassCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc EffectTypeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc EffectTypeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc EffectTypeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc EffectTypeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Item, 1
EXECUTE pr__SYS_MakeInsertRecordProc Item, 1
EXECUTE pr__SYS_MakeSelectRecordProc Item, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Item, 1

EXECUTE pr__SYS_MakeDeleteRecordProc ItemTypeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc ItemTypeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc ItemTypeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc ItemTypeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Land, 1
EXECUTE pr__SYS_MakeInsertRecordProc Land, 1
EXECUTE pr__SYS_MakeSelectRecordProc Land, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Land, 1

EXECUTE pr__SYS_MakeDeleteRecordProc LootTypeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc LootTypeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc LootTypeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc LootTypeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Map, 1
EXECUTE pr__SYS_MakeInsertRecordProc Map, 1
EXECUTE pr__SYS_MakeSelectRecordProc Map, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Map, 1

EXECUTE pr__SYS_MakeDeleteRecordProc NPC, 1
EXECUTE pr__SYS_MakeInsertRecordProc NPC, 1
EXECUTE pr__SYS_MakeSelectRecordProc NPC, 1
EXECUTE pr__SYS_MakeUpdateRecordProc NPC, 1

EXECUTE pr__SYS_MakeDeleteRecordProc NpcTypeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc NpcTypeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc NpcTypeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc NpcTypeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Segue, 1
EXECUTE pr__SYS_MakeInsertRecordProc Segue, 1
EXECUTE pr__SYS_MakeSelectRecordProc Segue, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Segue, 1

EXECUTE pr__SYS_MakeDeleteRecordProc SizeCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc SizeCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc SizeCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc SizeCode, 1

EXECUTE pr__SYS_MakeDeleteRecordProc SpawnZone, 1
EXECUTE pr__SYS_MakeInsertRecordProc SpawnZone, 1
EXECUTE pr__SYS_MakeSelectRecordProc SpawnZone, 1
EXECUTE pr__SYS_MakeUpdateRecordProc SpawnZone, 1

EXECUTE pr__SYS_MakeDeleteRecordProc SpawnZoneLink, 1
EXECUTE pr__SYS_MakeInsertRecordProc SpawnZoneLink, 1
EXECUTE pr__SYS_MakeSelectRecordProc SpawnZoneLink, 1
EXECUTE pr__SYS_MakeUpdateRecordProc SpawnZoneLink, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Store, 1
EXECUTE pr__SYS_MakeInsertRecordProc Store, 1
EXECUTE pr__SYS_MakeSelectRecordProc Store, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Store, 1

EXECUTE pr__SYS_MakeDeleteRecordProc Spell, 1
EXECUTE pr__SYS_MakeInsertRecordProc Spell, 1
EXECUTE pr__SYS_MakeSelectRecordProc Spell, 1
EXECUTE pr__SYS_MakeUpdateRecordProc Spell, 1

EXECUTE pr__SYS_MakeDeleteRecordProc WearLocationCode, 1
EXECUTE pr__SYS_MakeInsertRecordProc WearLocationCode, 1
EXECUTE pr__SYS_MakeSelectRecordProc WearLocationCode, 1
EXECUTE pr__SYS_MakeUpdateRecordProc WearLocationCode, 1








