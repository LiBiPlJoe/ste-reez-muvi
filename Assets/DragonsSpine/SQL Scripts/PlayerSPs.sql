if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_AccountIP_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_AccountIP_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_AccountPW_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_AccountPW_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Account_Check]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Account_Check]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Account_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Account_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Account_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Account_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Account_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Account_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Account_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Account_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerBelt_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerBelt_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerBelt_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerBelt_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerBelt_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerBelt_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerBelt_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerBelt_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerEffects_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerEffects_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerEffects_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerEffects_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerEffects_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerEffects_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerEffects_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerEffects_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerLocker_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerLocker_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerLocker_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerLocker_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerLocker_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerLocker_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerLocker_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerLocker_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerRings_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerRings_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerRings_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerRings_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerRings_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerRings_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerRings_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerRings_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSack_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSack_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSack_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSack_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSack_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSack_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSack_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSack_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSpells_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSpells_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSpells_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSpells_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSpells_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSpells_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerSpells_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerSpells_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerWearing_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerWearing_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerWearing_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerWearing_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerWearing_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerWearing_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_PlayerWearing_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_PlayerWearing_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Check]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Check]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Select_By_Account]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Select_By_Account]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Select_By_Name]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Select_By_Name]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prApp_Player_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prApp_Player_Update]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a IPAddress in Account
----------------------------------------------------------------------------
CREATE PROC prApp_AccountIP_Update
	@AccountID int,
	@IPAddress nvarchar(16)
AS

UPDATE	Account
SET		IPAddress = @IPAddress
WHERE 	AccountID = @AccountID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update an  Account Password
----------------------------------------------------------------------------
CREATE PROC prApp_AccountPW_Update
	@AccountID int,
	@Password  nvarchar(20)
AS

UPDATE	Account
SET		Password = @Password
WHERE 	AccountID = @AccountID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE prApp_Account_Check 
	@AccountName nvarchar (20) =NULL
AS

SELECT	*
FROM		Account
WHERE	AccountName = @AccountName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

----------------------------------------------------------------------------
-- Delete a single record from Account
----------------------------------------------------------------------------
CREATE PROC prApp_Account_Delete
	@AccountName nvarchar (20)
AS

DELETE	Account
WHERE 	AccountName = @AccountName
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into Account
----------------------------------------------------------------------------
CREATE PROC prApp_Account_Insert
	@AccountName  nvarchar(20),
	@Password nvarchar(20),
	@IPAddress nvarchar(16)
AS

INSERT Account(AccountName, Password, IPAddress)
VALUES (@AccountName, @Password, @IPAddress)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Select a single record from Account
----------------------------------------------------------------------------
CREATE PROC prApp_Account_Select
	@AccountName  nvarchar (20)=NULL
AS

SELECT	*
FROM	Account
WHERE 	AccountName = @AccountName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record in Account
----------------------------------------------------------------------------
CREATE PROC prApp_Account_Update
	@AccountName nvarchar(20) = NULL,
	@Password  nvarchar(20) = NULL,
	@IPAddress nvarchar(16) = NULL
AS

UPDATE	Account
SET	Password = @Password,
	IPAddress = @IPAddress
WHERE 	AccountName = @AccountName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerBelt
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerBelt_Delete
	@playerID int
AS

DELETE	PlayerBelt
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

----------------------------------------------------------------------------
-- Insert a single record into PlayerBelt
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerBelt_Insert
	@PlayerID int,
	@BeltSlot int,
	@BeltItem int,
	@Attuned int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
AS

INSERT PlayerBelt(PlayerID,BeltSlot, BeltItem, Attuned, Special, CoinValue, Charges, Venom, WillAttune)
VALUES (@PlayerID, @BeltSlot, @BeltItem, @Attuned, @Special, @CoinValue, @Charges, @Venom, @WillAttune)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

--------------------------------------------------------------------------
-- Select a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerBelt_Select
	@PlayerID int,
	@BeltSlot int
AS

SELECT	*
FROM	PlayerBelt
WHERE 	
PlayerID = @PlayerID
AND
BeltSlot = @BeltSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerBelt
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerBelt_Update
	@PlayerID int,
	@BeltSlot int,
	@BeltItem int,
	@Attuned int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
	
AS
UPDATE PlayerBelt
SET      
	BeltItem = @BeltItem,
	Attuned = @Attuned,
	Special = @Special,
	CoinValue = @CoinValue,
	Charges = @Charges,
	Venom = @Venom,
	WillAttune = @WillAttune
	
WHERE
PlayerID = @PlayerID
AND
BeltSlot = @BeltSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

----------------------------------------------------------------------------
-- Delete a single record from PlayerEffects
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerEffects_Delete
	@playerID int
AS

DELETE	PlayerEffects
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

----------------------------------------------------------------------------
-- Insert a single record into PlayerEffects
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerEffects_Insert
	@PlayerID int,
	@EffectSlot int,
	@EffectID int,
	@Amount int,
	@Duration int
AS

INSERT PlayerEffects(PlayerID, EffectSlot, EffectID, Amount, Duration)
VALUES (@PlayerID, @EffectSlot, @EffectID, @Amount, @Duration)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

-- Select a single record from PlayerEffects
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerEffects_Select
	@PlayerID int,
	@EffectSlot int
AS

SELECT	*
FROM	PlayerEffects
WHERE 	
PlayerID = @PlayerID
AND
EffectSlot = @EffectSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerEffects
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerEffects_Update
	@PlayerID int,
	@EffectSlot int,
	@EffectID int,
	@Amount int,
	@Duration int
	
AS
UPDATE PlayerEffects
SET      
	EffectID = @EffectID,
	Amount = @Amount,
	Duration = @Duration
	
WHERE
PlayerID = @PlayerID
AND
EffectSlot = @EffectSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerLocker_Delete
	@playerID int
AS

DELETE	PlayerLocker
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into PlayerLocker
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerLocker_Insert
	@PlayerID int,
	@LockerSlot int,
	@LockerItem int,
	@Attuned int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@WillAttune bit
AS

INSERT PlayerLocker(PlayerID, LockerSlot, LockerItem, Attuned, Special, CoinValue, Charges, WillAttune)
VALUES (@PlayerID, @LockerSlot, @LockerItem, @Attuned, @Special, @CoinValue, @Charges, @WillAttune)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


---------------------------------------------------------------------------
-- Select a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerLocker_Select
	@PlayerID int,
	@LockerSlot int
AS

SELECT	*
FROM	PlayerLocker
WHERE 	
PlayerID = @PlayerID
AND
LockerSlot = @LockerSlot

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerLocker
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerLocker_Update
	@PlayerID int,
	@LockerSlot int,
	@LockerItem int,
	@Attuned int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@WillAttune bit
	
AS
UPDATE PlayerLocker
SET      
	LockerItem = @LockerItem,
	Attuned = @Attuned
	Special = @Special
	CoinValue = @CoinValue
	Charges = @Charges
	WillAttune = @WillAttune
	
WHERE
PlayerID = @PlayerID
AND
LockerSlot = @LockerSlot

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerRings
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerRings_Delete
	@playerID int
AS

DELETE	PlayerRings
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into PlayerRings
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerRings_Insert
	@PlayerID int,
	@RingFInger int,
	@RingItem int,
	@Attuned int,
	@isRecall bit,
	@wasRecall bit,
	@recallLand int,
	@recallMap int,
	@recallX int,
	@recallY int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@WillAttune bit
AS

INSERT PlayerRings(PlayerID, RingFinger, RingItem, Attuned, isRecall, wasRecall, recallLand, recallMap, recallX, recallY, Special, CoinValue, Charges, WillAttune)
VALUES (@PlayerID, @RingFinger, @RingItem, @Attuned, @isRecall, @wasRecall @recallLand, @recallMap, @recallX, @recallY, @Special, @CoinValue, @Charges, @WillAttune)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


---------------------------------------------------------------------------
-- Select a single record from PlayerRings
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerRings_Select
	@PlayerID int,
	@RingFinger  int
AS

SELECT	*
FROM	PlayerRings
WHERE 	
PlayerID = @PlayerID
AND
RingFinger = @RingFinger
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerRings
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerRings_Update
	@PlayerID int,
	@RingFinger int,
	@RingItem int,
	@Attuned int,
	@isRecall bit,
	@wasRecall bit,
	@recallLand int,
	@recallMap int,
	@recallX int,
	@recallY int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@WillAttune bit
	
AS
UPDATE PlayerRings
SET      
	RingItem = @RingItem,
	Attuned = @Attuned,
	isRecall = @isRecall,
	wasRecall = @wasRecall,
	recallLand = @recallLand,
	recallMap = @recallMap,
	recallX = @recallX,
	recallY = @recallY,
	Special = @Special,
	CoinValue = @CoinValue,
	Charges = @Charges,
	WillAttune = @WillAttune
	
WHERE
PlayerID = @PlayerID
AND
RingFinger = @RingFinger
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerSack
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSack_Delete
	@playerID int
AS

DELETE	PlayerSack
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into PlayerSack
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSack_Insert
	@PlayerID int,
	@SackSlot int,
	@SackItem int,
	@Attuned int,
	@SackGold float,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
AS

INSERT PlayerSack(PlayerID, SackSlot, SackItem, Attuned, SackGold, Special, CoinValue, Charges, Venom, WillAttune)
VALUES (@PlayerID, @SackSlot, @SackItem, @Attuned, @SackGold, @Special, @CoinValue, @Charges, @Venom, @WillAttune)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


---------------------------------------------------------------------------
-- Select a single record from PlayerSack
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSack_Select
	@PlayerID int,
	@SackSlot int
AS

SELECT	*
FROM	PlayerSack
WHERE 	
PlayerID = @PlayerID
AND
SackSlot = @SackSlot

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerSack
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSack_Update
	@PlayerID int,
	@SackSlot int,
	@SackItem int,
	@Attuned int,
	@SackGold float,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
	
AS
UPDATE PlayerSack
SET      
	SackItem = @SackItem,
	Attuned = @Attuned,
	SackGold = @SackGold,
	Special = @Special,
	CoinValue = @CoinValue,
	Charges = @Charges,
	Venom = @Venom,
	WillAttune = @WillAttune
	
WHERE
PlayerID = @PlayerID
AND
SackSlot = @SackSlot


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerSpells
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSpells_Delete
	@playerID int
AS

DELETE	PlayerSpells
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

----------------------------------------------------------------------------
-- Insert a single record into PlayerSpells
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSpells_Insert
	@PlayerID int,
	@SpellSlot int,
	@SpellID int,
	@ChantString nvarchar(255)=NULL
AS

INSERT PlayerSpells(PlayerID, SpellSlot, SpellID, ChantString)
VALUES (@PlayerID, @SpellSlot, @SpellID, @ChantString)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


---------------------------------------------------------------------------
-- Select a single record from PlayerSpells
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSpells_Select
	@PlayerID int,
	@SpellSlot int
AS

SELECT	*
FROM	PlayerSpells
WHERE 	
PlayerID = @PlayerID
AND
SpellSlot = @SpellSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerSpells
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerSpells_Update
	@PlayerID int,
	@SpellSlot int,
	@SpellID int,
	@ChantString nvarchar(255)=NULL
	
AS
UPDATE PlayerSpells
SET      
	SpellID = @SpellID,
	ChantString = @ChantString
	
WHERE
PlayerID = @PlayerID
AND
SpellSlot = @SpellSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from PlayerWearing
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerWearing_Delete
	@playerID int
AS

DELETE	PlayerWearing
WHERE 	PlayerID = @playerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into PlayerWearing
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerWearing_Insert
	@PlayerID int,
	@WearingSlot int,
	@WearingItem int,
	@Attuned int,
	@WearLocationCode int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
AS

INSERT PlayerWearing(PlayerID, WearingSlot,WearingItem, Attuned, WearLocationCode, Special, CoinValue, Charges, Venom, WillAttune)
VALUES (@PlayerID, @WearingSlot, @WearingItem, @Attuned, @WearLocationCode, @Special, @CoinValue, @Charges, @Venom, @WillAttune)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


---------------------------------------------------------------------------
-- Select a single record from PlayerSack
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerWearing_Select
	@PlayerID int,
	@WearingSlot int
AS

SELECT	*
FROM	PlayerWearing
WHERE 	
PlayerID = @PlayerID
AND
WearingSlot = @WearingSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into PlayerWearing
----------------------------------------------------------------------------
CREATE PROC prApp_PlayerWearing_Update
	@PlayerID int,
	@WearingSlot int,
	@WearingItem int,
	@Attuned int,
	@WearLocationCode int,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit
	
AS
UPDATE PlayerWearing
SET      
	WearingItem = @WearingItem,
	Attuned = @Attuned,
	WearLocationCode = @WearLocationCode,
	Special = @Special,
	CoinValue = @CoinValue,
	Charges = @Charges,
	Venom = @Venom,
	WillAttune = @WillAttune
	
WHERE
PlayerID = @PlayerID
AND
WearingSlot = @WearingSlot
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE prApp_Player_Check 
	@Name nvarchar (20) =NULL
AS

SELECT	*
FROM		Player
WHERE	Name = @Name

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Delete a single record from Player
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Delete
	@playerID int
AS

DELETE	Player
WHERE 	PlayerID = @playerID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Insert a single record into Player
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Insert
	@AccountID int,
	@Account nvarchar(20)=NULL,
	@Protocol nvarchar(20)=NULL,
	@Name nvarchar(255) = NULL,
	@gender int= NULL,
	@Race nvarchar(2) = NULL,
	@CharClass nvarchar(2) = NULL,
	@CharClassFull nvarchar(15)=NULL,
	@ClassID int= NULL,
	@Align int= NULL,
	@ConfRoom int= NULL,
	@ImpLevel int= NULL,
	@ShowTitle bit,
	@Echo bit,
	@Anonymous bit,
	@TimeOut int= NULL,
	@Land int= NULL,
	@Map int= NULL,
	@XCord int= NULL,
	@YCord int= NULL,
	@DirPointer char(1)=NULL,
	@CurPos nvarchar(3)=NULL,
	@Stunned int = NULL,
	@Floating int = NULL,
	@IsDead bit,
	@IsHidden bit,
	@IsInvisible bit,
	@NightVision bit,
	@FeatherFall bit,
	@BreatheWater bit,
	@Blind bit,
	@Poisoned bit,
	@Shield int = NULL,
	@FireProtection int = NULL,
	@IceProtection int = NULL,
	@DeathProtection int = NULL,
	@FearProtection int = NULL,
	@BlindProtection int = NULL,
	@StunProtection int = NULL,
	@LightningProtection int = NULL,
	@PoisonProtection int = NULL,
	@FighterSpecial varchar(255) = NULL,
	@KnightRing bit,
	@Level int = NULL,
	@Exp bigint = NULL,
	@Hits int = NULL,
	@HitsMax int = NULL,
	@Stamina int = NULL,
	@StamLeft int = NULL,
	@Mana int = NULL,
	@ManaMax int = NULL,
	@Age int = NULL,
	@RoundsPlayed bigint = NULL,
	@NumKills int = NULL,
	@NumDeaths int = NULL,
	@BankGold float = NULL,
	@Strength int = NULL,
	@Dexterity int = NULL,
	@Intelligence int = NULL,
	@Wisdom int = NULL,
	@Constitution int = NULL,
	@Charisma int = NULL,
	@TempStrength int = NULL,
	@TempDexterity int = NULL,
	@TempIntelligence int = NULL,
	@TempWisdom int = NULL,
	@TempConstitution int = NULL,
	@TempCharisma int = NULL,
	@StrAdd int = NULL,
	@DexAdd int = NULL,
	@Mace bigint = NULL,
	@Bow bigint = NULL,
	@Flail bigint = NULL,
	@Dagger bigint = NULL,
	@Rapier bigint = NULL,
	@Twohanded bigint = NULL,
	@Staff bigint = NULL,
	@Shuriken bigint = NULL,
	@Sword  bigint = NULL,
	@Threestaff bigint = NULL,
	@Halberd bigint = NULL,
	@Unarmed bigint = NULL,
	@Thievery bigint = NULL,
	@Magic bigint = NULL,
	@trainedMace bigint = NULL,
	@trainedBow bigint = NULL,
	@trainedFlail bigint = NULL,
	@trainedDagger bigint = NULL,
	@trainedRapier bigint = NULL,
	@trainedTwoHanded bigint = NULL,
	@trainedStaff bigint = NULL,
	@trainedShuriken bigint = NULL,
	@trainedSword  bigint = NULL,
	@trainedThreestaff bigint = NULL,
	@trainedHalberd bigint = NULL,
	@trainedUnarmed bigint = NULL,
	@trainedThievery bigint = NULL,
	@trainedMagic bigint = NULL,
	@SavedHitsMax int = NULL,
	@SavedManaMax int = NULL,
	@BirthDay datetime = NULL,
	@LastSave datetime = NULL,
	@MPRegen int = NULL,
	@Encumb int = NULL,
	@LHItemID int = NULL,
	@LHItemAttuned int = NULL,
	@RHItemID int = NULL,
	@RHItemAttuned int = NULL
	
AS

INSERT Player( AccountID, Account, Protocol, Name, gender, Race, CharClass, CharClassFull, ClassID, Align, ConfRoom, ImpLevel, ShowTitle, Echo, Anonymous, TimeOut, Land, Map, XCord, YCord, DirPointer, CurPos, Stunned, Floating, IsDead, IsHidden, IsInvisible, NightVision, FeatherFall, BreatheWater, Blind, Poisoned, Shield, FireProtection, IceProtection, DeathProtection, FearProtection, BlindProtection, StunProtection, LightningProtection, PoisonProtection, FighterSpecial, KnightRing, Level, Exp, Hits, HitsMax, Stamina, StamLeft, Mana, ManaMax, Age, RoundsPlayed, NumKills, NumDeaths, BankGold, Strength, Dexterity, Intelligence, Wisdom, Constitution, Charisma, TempStrength, TempDexterity, TempIntelligence, TempWisdom, TempConstitution, TempCharisma,StrAdd, DexAdd, Mace, Bow, Flail, Dagger, Rapier, Twohanded, Staff, Shuriken, Sword, Threestaff, Halberd, Unarmed, Thievery, Magic, trainedMace, trainedBow, trainedFlail, trainedDagger, trainedRapier, trainedTwoHanded, trainedStaff, trainedShuriken, trainedSword, trainedThreestaff, trainedHalberd, trainedUnarmed, trainedThievery, trainedMagic, SavedHitsMax, SavedManaMax, Birthday, LastSave, MPRegen, Encumb, LHItemID, LHItemAttuned, RHItemID, RHItemAttuned)
VALUES (@AccountID, @Account, @Protocol, @Name, @gender, @Race, @CharClass, @CharClassFull, @ClassID, @Align, @ConfRoom, @ImpLevel, COALESCE(@ShowTitle, 0), COALESCE(@Echo, 0), COALESCE(@Anonymous, 0), @TimeOut, @Land, @Map, @XCord, @YCord, @DirPointer, @CurPos, @Stunned, @Floating, COALESCE(@IsDead, 0), COALESCE(@IsHidden, 0), COALESCE(@IsInvisible, 0), COALESCE(@NightVision, 0), COALESCE(@FeatherFall, 0), COALESCE(@BreatheWater, 0), COALESCE(@Blind, 0), @Poisoned, @Shield, @FireProtection, @IceProtection, @DeathProtection, @FearProtection, @BlindProtection, @StunProtection, @LightningProtection, @PoisonProtection, @FighterSpecial, COALESCE(@KnightRing, 0), @Level, @Exp, @Hits, @HitsMax, @Stamina, @StamLeft, @Mana, @ManaMax, @Age, @RoundsPlayed, @NumKills, @NumDeaths, @BankGold, @Strength, @Dexterity, @Intelligence, @Wisdom, @Constitution, @Charisma, @TempStrength, @TempDexterity, @TempIntelligence, @TempWisdom, @TempConstitution, @TempCharisma, @StrAdd, @DexAdd, @Mace, @Bow, @Flail, @Dagger, @Rapier, @Twohanded, @Staff, @Shuriken, @Sword, @Threestaff, @Halberd, @Unarmed, @Thievery, @Magic, @trainedMace, @trainedBow, @trainedFlail, @trainedDagger, @trainedRapier, @trainedTwoHanded, @trainedStaff, @trainedShuriken, @trainedSword, @trainedThreestaff, @trainedHalberd, @trainedUnarmed, @trainedThievery, @trainedMagic, @SavedHitsMax, @SavedManaMax, @Birthday, @LastSave, @MPRegen, @Encumb, @LHItemID, @LHItemAttuned, @RHItemID, @RHItemAttuned)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Select a single record from Player
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Select
	@PlayerID int
AS

SELECT	*
FROM	Player
WHERE 	PlayerID = @PlayerID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Select all records from Player on an Account
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Select_By_Account
	@AccountID  int
AS

SELECT	*
FROM	Player
WHERE 	AccountID = @AccountID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Select a single record from Player by name
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Select_By_Name
	@Name nvarchar(255)=NULL
AS

SELECT	*
FROM	Player
WHERE 	Name = @Name

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


----------------------------------------------------------------------------
-- Update a single record into Player
----------------------------------------------------------------------------
CREATE PROC prApp_Player_Update
	@PlayerID int,
	@Account nvarchar(20)=NULL,
	@Protocol nvarchar(20)=NULL,
	@Name nvarchar(255)=NULL,
	@gender int= NULL,
	@Race nvarchar(2) = NULL,
	@CharClass nvarchar(2) = NULL,
	@CharClassFull nvarchar(15)=NULL,
	@ClassID int= NULL,
	@Align int= NULL,
	@ImpLevel int= NULL,
	@Echo bit,
	@Anonymous bit,
	@TimeOut int= NULL,
	@Land int= NULL,
	@Map int= NULL,
	@XCord int= NULL,
	@YCord int= NULL,
	@DirPointer char(1)=NULL,
	@CurPos nvarchar(3)=NULL,
	@Stunned int = NULL,
	@Floating int = NULL,
	@IsDead bit,
	@IsHidden bit,
	@IsInvisible bit,
	@NightVision bit,
	@FeatherFall bit,
	@BreatheWater bit,
	@Blind bit,
	@Poisoned bit,
	@Shield int=NULL,
	@FireProtection int=NULL,
	@IceProtection int=NULL,
	@DeathProtection int=NULL,
	@FearProtection int=NULL,
	@BlindProtection int=NULL,
	@StunProtection int=NULL,
	@LightningProtection int=NULL,
	@PoisonProtection int=NULL,
	@FighterSpecial varchar(255) = NULL,
	@KnightRing int = NULL,
	@Level int = NULL,
	@Exp int = NULL,
	@Hits int = NULL,
	@HitsMax int = NULL,
	@Stamina int = NULL,
	@StamLeft int = NULL,
	@Mana int = NULL,
	@ManaMax int = NULL,
	@Age int = NULL,
	@RoundsPlayed bigint = NULL,
	@NumKills int = NULL,
	@NumDeaths int = NULL,
	@BankGold float = NULL,
	@Strength int = NULL,
	@Dexterity int = NULL,
	@Intelligence int = NULL,
	@Wisdom int = NULL,
	@Constitution int = NULL,
	@Charisma int = NULL,
	@TempStrength int = NULL,
	@TempDexterity int = NULL,
	@TempIntelligence int = NULL,
	@TempWisdom int = NULL,
	@TempConstitution int = NULL,
	@TempCharisma int = NULL,
	@StrAdd int = NULL,
	@DexAdd int = NULL,
	@Mace bigint = NULL,
	@Bow bigint = NULL,
	@Flail bigint = NULL,
	@Dagger bigint = NULL,
	@Rapier bigint = NULL,
	@Twohanded bigint = NULL,
	@Staff bigint = NULL,
	@Shuriken bigint = NULL,
	@Sword  bigint = NULL,
	@Threestaff bigint = NULL,
	@Halberd bigint = NULL,
	@Unarmed bigint = NULL,
	@Thievery bigint = NULL,
	@Magic bigint = NULL,
	@trainedMace bigint = NULL,
	@trainedBow bigint = NULL,
	@trainedFlail bigint = NULL,
	@trainedDagger bigint = NULL,
	@trainedRapier bigint = NULL,
	@trainedTwoHanded bigint = NULL,
	@trainedStaff bigint = NULL,
	@trainedShuriken bigint = NULL,
	@trainedSword  bigint = NULL,
	@trainedThreestaff bigint = NULL,
	@trainedHalberd bigint = NULL,
	@trainedUnarmed bigint = NULL,
	@trainedThievery bigint = NULL,
	@trainedMagic bigint = NULL,
	@SavedHitsMax int = NULL,
	@SavedManaMax int = NULL,
	@LastSave datetime = NULL,
	@MPRegen int = NULL,
	@Encumb int = NULL,
	@LHItemID int = NULL,
	@LHItemAttuned int = NULL,
	@RHItemID int = NULL,
	@RHItemAttuned int = NULL
AS
UPDATE Player
SET      
	Account = @Account,
	Protocol = @Protocol,
	gender = @gender,
	Race = @Race,
	CharClass = @CharClass,
	CharClassFull = @CharClassFull,
	ClassID = @ClassID,
	Align = @Align,
	ImpLevel = @ImpLevel,
	Echo = @Echo,
	Anonymous = @Anonymous,
	TimeOut = @TimeOut,
	Land = @Land,
	Map = @Map,
	XCord = @XCord,
	YCord = @YCord,
	DirPointer = @DirPointer,
	CurPos = @CurPos,
	Stunned = @Stunned,
	Floating = @Floating,
	IsDead = @IsDead,
	IsHidden = @IsHidden,
	IsInvisible = @IsInvisible,
	NightVision = @NightVision,
	FeatherFall = @FeatherFall,
	BreatheWater = @BreatheWater,
	Blind = @Blind,
	Poisoned = @Poisoned,
	Shield = @Shield,
	FireProtection = @FireProtection,
	IceProtection = @IceProtection,
	DeathProtection = @DeathProtection,
	FearProtection = @FearProtection,
	BlindProtection = @BlindProtection,
	StunProtection = @StunProtection,
	LightningProtection = @LightningProtection,
	PoisonProtection = @PoisonProtection,
	FighterSpecial = @FighterSpecial,
	KnightRing = @KnightRing,
	Level = @Level,
	Exp = @Exp,
	Hits = @Hits,
	HitsMax = @HitsMax,
	Stamina = @Stamina,
	StamLeft = @StamLeft,
	Mana = @Mana,
	ManaMax = @ManaMax,
	Age = @Age,
	RoundsPlayed = @RoundsPlayed,
	NumKills = @NumKills,
	NumDeaths = @NumDeaths,
	BankGold = @BankGold,
	Strength = @Strength,
	Dexterity = @Dexterity,
	Intelligence = @Intelligence,
	Wisdom = @Wisdom,
	Constitution = @Constitution,
	Charisma = @Charisma,
	TempStrength = @TempStrength,
	TempDexterity = @TempDexterity,
	TempIntelligence = @TempIntelligence,
	TempWisdom = @TempWisdom,
	TempConstitution = @TempConstitution,
	TempCharisma = @TempCharisma,
	StrAdd = @StrAdd,
	DexAdd = @DexAdd,
	Mace = @Mace,
	Bow = @Bow,
	Flail = @Flail,
	Dagger = @Dagger,
	Rapier = @Rapier,
	Twohanded = @Twohanded,
	Staff = @Staff,
	Shuriken = @Shuriken,
	Sword = @Sword,
	Threestaff = @Threestaff,
	Halberd = @Halberd,
	Unarmed = @Unarmed,
	Thievery = @Thievery,
	Magic = @Magic,
	trainedMace = @trainedMace,
	trainedBow = @trainedBow,
	trainedFlail = @trainedFlail,
	trainedDagger = @trainedDagger,
	trainedRapier = @trainedRapier,
	trainedTwoHanded = @trainedTwoHanded,
	trainedStaff = @trainedStaff,
	trainedShuriken = @trainedShuriken,
	trainedSword = @trainedSword,
	trainedThreestaff = @trainedThreestaff,
	trainedHalberd = @trainedHalberd,
	trainedUnarmed = @trainedUnarmed,
	trainedThievery = @trainedThievery,
	trainedMagic = @trainedMagic,
	SavedHitsMax = @SavedHitsMax,
	SavedManaMax = @SavedManaMax,
	LastSave = @LastSave,
	MPRegen = @MPRegen,
	Encumb = @Encumb,
	LHItemID = @LHItemID,
	LHItemAttuned = @LHItemAttuned,
	RHItemID = @RHItemID,
	RHItemAttuned = @RHItemAttuned

WHERE
PlayerID = @PlayerID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

