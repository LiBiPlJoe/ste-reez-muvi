DELETE TABLE [Item$]

CREATE TABLE [Item] (
	[ItemID] [int] NULL ,
	[ItemTypeCode] [int] NULL ,
	[WearLocationCode] [int] NULL ,
	[Weight] [int] NULL ,
	[CoinValue] [int] NULL ,
	[SizeCode] [int] NULL ,
	[BlockRankCode] [int] NULL ,
	[EffectTypeCode] [int] NULL ,
	[EffectAmount] [int] NULL ,
	[LootLandID] [int] NULL ,
	[LootMapID] [int] NULL ,
	[LootTypeCode] [int] NULL ,
	[ItemName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ShortDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LongDesc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO


