DROP TABLE [SpawnZoneLink]

CREATE TABLE [SpawnZoneLink] 
(
	[SpawnZoneLinkID] 		[int] NULL ,
	[Name] 			[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[MovementString] 	[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[Special] 		[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[NpcTypeCode] 		[int] NULL ,
	[BaseTypeCode] 		[int] NULL ,
	[Gold] 			[int] NULL ,
	[CharacterClassCode] 	[int] NULL ,
	[Experience] 		[int] NULL ,
	[Hits] 			[int] NULL ,

	[IsHidden] 		[bit] NOT NULL  DEFAULT (0),
	[CanFly] 		[bit] NOT NULL  DEFAULT (0),
	[CanBreatheWater]	[bit] NOT NULL  DEFAULT (0),
	[IsLairCritter]		[bit] NOT NULL  DEFAULT (0),
	[IsMovable] 		[bit] NOT NULL  DEFAULT (0)

) 
ON [PRIMARY]
GO


		<SpawnZoneLink>
			<SpawnZoneID>1</SpawnZoneID>
			<NPCID>1</NPCID>
			<SpawnTimer>90</SpawnTimer>
			<MaxAllowedInZone>1</MaxAllowedInZone>
			<SpawnMessage>You feel the earth shudder under your feet.</SpawnMessage>
		</SpawnZoneLink>