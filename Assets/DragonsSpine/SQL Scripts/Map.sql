DROP TABLE Map

CREATE TABLE [Map] 
(
	[MapID] 		[int] NULL ,
	[Name] 			[nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[ShortDesc] 		[nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, 
	[LongDesc] 		[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
	
) 
ON [PRIMARY]
GO

