using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    static public class Globals
    {
        public const int UNDERWORLD_LANDID = 2;
        public const int PRAETOSEBA_MAPID = 4;

        public const int MAX_EXP_LEVEL = 30;
        public const int SKILL_LOSS_DIVISOR = 5000;
        public const int MAX_STORE_INVENTORY = 12; // maximum store inventory amount

        public enum eMerchantType
        {
            None,
            Pawn,
            Barkeep,
            Weapon,
            Armor,
            Apothecary,
            Book,
            Jewellery,
            General,
            Magic
        }

        public enum eTrainerType
        {
            None,
            Spell,
            Weapon,
            Martial_Arts,
            Knight,
            Sage,
            HP_Doctor
        }

        public enum eInteractiveType
        {
            None,
            Banker,
            Tanner,
            Balm,
            Recall_Ring,
            Mugwort,
            Confessor
        }
        public enum eSpellType
        {
            Abjuration,
            Alteration,
            Conjuration,
            Divination,
            Evocation
        }
        public enum eBookType { None, Normal, Spellbook }
        public enum eTargetType
        {
            Area_Effect,
            Group,
            Point_Blank_Area_Effect,
            Self,
            Single
        }

        public enum ePlayerState
        {
            LOGIN,
            NEWCHAR,
            CONFERENCE,
            PLAYING,
            NEWCHARVERIFY,
            PICKRACE,
            PICKGENDER,
            ROLLSTATS,
            PICKCLASS,
            PICKFIRSTNAME,
            PICKEMAIL,
            VERIFYEMAIL,
            PICKPASSWORD,
            VERIFYPASSWORD,
            MENU,
            PICKACCOUNT,
            CHECKPASSWORD,
            CHANGECHAR,
            CHANGECHAR2,
            ACCOUNTMAINT,
            DELETECHAR,
            DELETECHAR2,
            CHANGEPASSWORD,
            CHANGEPASSWORD2,
            CHANGEPASSWORD3,
            PROTO_CHARGEN
        };

        public enum eSpecies
        {
            Unknown,
            FireDragon,
            IceDragon,
            LightningDrake,
            TundraYeti,
            Sandwyrm,
            Arachnid,
            Demon,
            WindDragon,
            Giantkin,
            Humanoid,
            Unnatural,
            Magical,
            Reptile,
            WildAnimal,
            DomesticAnimal,
            Amphibian,
            Fish,
            Insect,
            Avian,
            Thisson,
            Ydmos
        }

        public enum eIconColor
        {
            Green,
            Yellow,
            Brown,
            Gray,
            Red,
            Purple
        }

        public enum eGender
        {
            It,
            Male,
            Female,
            Random
        }

        public enum eHomeland
        {
            Illyria,
            Mu,
            Lemuria,
            Leng,
            Draznia,
            Hovath,
            Mnar,
            Barbarian
        }

        public enum eSkillType
        {
            None,
            Bow,
            Dagger,
            Flail,
            Halberd,
            Mace,
            Rapier,
            Shuriken,
            Staff,
            Sword,
            Threestaff,
            Two_Handed,
            Unarmed,
            Thievery,
            Magic,
            Bash
        }

        public enum eImpLevel
        {
            USER,
            AGM,
            GM,
            DEVJR,
            DEV
        }

        public enum eAlignment
        {
            None,
            Lawful,
            Neutral,
            Chaotic,
            Evil,
            Amoral,
            All
        }

        #region Max Wearable
        public static int[] Max_Wearable = {
            0, // None
            1, // Head
            1, // Neck
            1, // Nose
            2, // Ear
            1, // Face
            1, // Shoulders
            1, // Back
            1, // Torso
            2, // Bicep
            1, // Forearms
            2, // Wrist
            8, // Finger
            1, // Waist
            1, // Legs
            1, // Calves
            1, // Shins
            1, // Feet
            1  // Hands
        }; 
        #endregion

        public enum eAttuneType
        {
            /// <summary>
            /// Item is already attuned or will never attune.
            /// </summary>
            None,
            /// <summary>
            /// Item will bind to the first player that attacks with it.
            /// </summary>
            Attack,
            /// <summary>
            /// Item will bind to the player that killed the current item's owner.
            /// </summary>
            Slain,
            /// <summary>
            ///  Item will bind when taken for the first time.
            /// </summary>
            Take,
            /// <summary>
            /// Item will bind when worn for the first time.
            /// </summary>
            Wear,
            /// <summary>
            /// Item will bind when given as a quest reward.
            /// </summary>
            Quest
        }

        /// <summary>
        /// The order of the WearLocation enum corresponds with the Max_Wearable array
        /// </summary>
        public enum eWearLocation
        {
            /// <summary>
            /// Item cannot be worn.
            /// </summary>
            None,
            Head,
            Neck,
            Nose,
            Ear,
            Face,
            Shoulders,
            Back,
            Torso,
            Bicep,
            Forearms,
            Wrist,
            Finger,
            Waist,
            Legs,
            Calves,
            Shins,
            Feet,
            Hands
        };

        public enum eWearOrientation
        {
            None,
            RightRing1,
            RightRing2,
            RightRing3,
            RightRing4,
            LeftRing1,
            LeftRing2,
            LeftRing3,
            LeftRing4,
            Left,
            Right
        }
        /// <summary>
        /// The inventory slot enumeration.
        /// </summary>
        public enum eInventorySlot
        {
            None,
            RightHand,
            LeftHand,
            Head,
            Neck,
            Nose,
            RightEar,
            LeftEar,
            Face,
            Shoulders,
            Back,
            Torso,
            Bicep,
            Forearms,
            RightWrist,
            LeftWrist,
            RightFinger1,
            RightFinger2,
            RightFinger3,
            RightFinger4,
            LeftFinger1,
            LeftFinger2,
            LeftFinger3,
            LeftFinger4,
            Waist,
            Legs,
            Calves,
            Shins,
            Feet,
            Hands
        };
        public enum eItemType
        { 
            Weapon,
            Wearable,
            Container,
            Miscellaneous,
            Edible,
            Potable,
            Corpse,
            Coin,
            WearableWeapon
        }

        public enum eItemBaseType
        {
            Unknown, // 0
            Bow,
            Dagger,
            Flail,
            Halberd,
            Mace, // 5
            Rapier,
            Shuriken,
            Staff,
            Sword,
            Threestaff, // 10
            TwoHanded,
            Thievery,
            Magic,
            Unarmed,
            Armor, // 15
            Helm,
            Bracelet,
            Shield,
            Boots,
            Bottle, // 20
            Amulet,
            Ring,
            Book,
            Food,
            Gem, // 25
            Figurine,
            Scroll,
        }

        public enum eItemSize
        {
            Belt_Only,
            Sack_Only,
            Belt_Or_Sack,
            No_Container,
            Belt_Large_Slot_Only
        }

        public enum eArmorType
        {
            None,
            Feathers,
            Fur,
            Cloth,
            Hide,
            Leather,
            Chain,
            Plate,
            Scales,
            Rock,
            Stone,
            Mithril,
            Chitin
        }
    }
}
