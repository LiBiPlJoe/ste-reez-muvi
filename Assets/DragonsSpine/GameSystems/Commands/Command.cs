using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Net.Sockets;
using System.Collections.Specialized;

namespace DragonsSpine
{
    public class Command
    {
        public static CommandType[] breakFollowCommands = new CommandType[] { CommandType.Attack, CommandType.Jumpkick, CommandType.Kick,
            CommandType.Poke, CommandType.Rest, CommandType.Crawl, CommandType.Movement, CommandType.Chant, CommandType.Quit, CommandType.Cast,
            CommandType.NPCInteraction, CommandType.Bash };
        public static object lockObject = new object();
        public enum CommandType
        {
            None,
            Attack,
            Jumpkick,
            Kick,
            Poke,
            Shoot,
            Throw,
            Rest,
            Crawl,
            Movement,
            Chant,
            Quit,
            Cast,
            NPCInteraction,
            Bash
        }

        public Command()
        {

        }

        public Command(Character chr)
        {
            this.chr = chr;
        }

        Character chr = null;

        public static bool ParseCommand(Character chr, string command, string args)
        {
            lock (lockObject)
            {
                if (!chr.IsPC)
                {
                    chr.LastCommand = command + " " + args;
                    chr.cmdWeight = 0; // always reset an NPC's command weight...
                }

                Command cmd = new Command(chr);

                chr.CommandType = CommandType.None; // reset commandType

                int chatpos = 0;

                int i = 0;

                string fullinput = command + " " + args;

                string newArgs = "";

                if (command.ToLower().StartsWith("say"))
                {
                    command = "/";
                }

                if (chr.protocol == DragonsSpineMain.APP_PROTOCOL || chr.cmdWeight <= 3)
                {
                    if (!command.StartsWith("$"))
                    {
                        if (fullinput.IndexOf("\"") != -1 && fullinput.IndexOf("/") != -1)
                        {
                            if (fullinput.IndexOf("/") < fullinput.IndexOf("\""))
                            {
                                goto SlashChat;
                            }
                        }

                        chatpos = fullinput.IndexOf("\"");//, 0, fullinput.Length);

                        if (chatpos > -1) // Break out the chat string
                        {
                            string chatCommand = fullinput.Substring(chatpos + 1);

                            if (chatpos > 0)
                            {
                                string[] commandOne = fullinput.Substring(0, chatpos).Split(" ".ToCharArray());// newCommand[0].Split(" ".ToCharArray());

                                for (i = 1; i <= commandOne.GetUpperBound(0); i++)
                                {
                                    newArgs = newArgs + " " + commandOne[i];
                                }

                                command = commandOne[0].Trim();

                                args = newArgs.Trim();

                                cmd.InterpretCommand(command, args);
                            }

                            cmd.InterpretCommand("/", chatCommand);

                            return true;
                        }

                    SlashChat:
                        chatpos = fullinput.IndexOf("/");

                        if (chatpos > -1) // Break out the chat string
                        {
                            string chatCommand = fullinput.Substring(chatpos + 1);

                            if (chatpos > 0)
                            {
                                string[] commandOne = fullinput.Substring(0, chatpos).Split(" ".ToCharArray());// newCommand[0].Split(" ".ToCharArray());

                                for (i = 1; i <= commandOne.GetUpperBound(0); i++)
                                {
                                    newArgs = newArgs + " " + commandOne[i];
                                }

                                command = commandOne[0].Trim();

                                args = newArgs.Trim();

                                cmd.InterpretCommand(command, args);
                            }

                            cmd.InterpretCommand("/", chatCommand);
                            return true;
                        }
                    }

                    cmd.InterpretCommand(command, args);
                    if (chr.protocol == DragonsSpineMain.APP_PROTOCOL)
                    {
                        Protocol.ShowMap(chr);
                    }
                    return true;
                }
                return false;
            }
        }

        public bool InterpretCommand(string command, string args)
        {
            int andStrPos = 0;
            int i = 0;
            string sCmdAndArgs = command + " " + args;
            string newArgs = "";            

            if (chr.IsDead)
            {
                if (command.ToLower() != "rest" && command.ToLower() != "quit")
                {
                    chr.WriteToDisplay("You are dead, you can either wait to be resurrected or rest.");
                    return true;
                }
            }

            if (chr.IsPeeking)
            {
                command = "rest";
                args = "";
            }

            if (chr.IsWizardEye)
            {
                command = command.ToLower();
                switch (command.ToLower())
                {
                    case "n":
                    case "north":
                    case "s":
                    case "south":
                    case "e":
                    case "east":
                    case "w":
                    case "west":
                    case "ne":
                    case "northeast":
                    case "nw":
                    case "northwest":
                    case "se":
                    case "southeast":
                    case "sw":
                    case "southwest":
                    case "d":
                    case "down":
                    case "u":
                    case "up":
                    case "a":
                    case "again":
                    case "l":
                    case "look":
                    case "rest":
                        break;
                    default:
                        chr.WriteToDisplay("You cannot use that command while polymorphed.");
                        return true;

                }
            }

            if (chr.protocol == DragonsSpineMain.APP_PROTOCOL)
            {
                if (Protocol.CheckGameCommand(chr, command, args))
                {
                    return true;
                }
            }

            // catch some commands before they go through the parser

            #region if speech
            if (command.StartsWith("/") || command.StartsWith("\""))
            {
                if (args.IndexOf("/!") != -1)
                {
                    chr.SendShout(chr.Name + ": " + args.Substring(0, args.IndexOf("/!")));
                    if (chr.IsPC)
                    {
                        chr.WriteToDisplay("You shout: " + args.Substring(0, args.IndexOf("/!")));
                        Utils.Log(chr.Name + ": " + args.Substring(0, args.IndexOf("/!")), Utils.LogType.PlayerChat);
                    }
                    return true;
                }
                else if (args.IndexOf("\"!") != -1)
                {
                    chr.SendShout(chr.Name + ": " + args.Substring(0, args.IndexOf("\"!")));
                    if (chr.IsPC)
                    {
                        chr.WriteToDisplay("You shout: " + args.Substring(0, args.IndexOf("\"!")));
                        Utils.Log(chr.Name + ": " + args.Substring(0, args.IndexOf("\"!")), Utils.LogType.PlayerChat);
                    }
                    return true;
                }
                else
                {
                    chr.SendToAllInSight(chr.Name + ": " + args);
                    if (chr.IsPC)
                    {
                        chr.WriteToDisplay("You say: " + args);
                        Utils.Log(chr.Name + ": " + args, Utils.LogType.PlayerChat);
                    }
                    return true;
                }
            }
            #endregion

            #region else if tell
            else if (command.StartsWith("tell"))
            {
                tell(args);
                return true;
            } 
            #endregion

            #region else if macro
            else if (command.StartsWith("$"))
            {
                macro(command, args);
                return true;
            }
            #endregion

            #region else if npc interaction
            else if (command.EndsWith(","))
            {
                chr.NPCSpeech(command, args);
                return true;
            }
            #endregion

            else
            {

                #region Check for command joining and split the commands out
                //If command has the ; for joining two commands
                if (sCmdAndArgs.IndexOf(" and ") != -1)
                {
                    sCmdAndArgs = sCmdAndArgs.Replace(" and ", ";");
                }
                andStrPos = sCmdAndArgs.IndexOf(";", 0, sCmdAndArgs.Length);

                if (andStrPos > 0)
                {
                    //Break out the two commands
                    String[] sCommands = sCmdAndArgs.Split(";".ToCharArray());

                    if (sCommands[0].Length > 0)
                    {
                        //get the command and the args
                        sCommands[0] = sCommands[0].Trim();//.ToLower();

                        string[] sArgss = sCommands[0].Split(" ".ToCharArray());

                        for (i = 1; i <= sArgss.GetUpperBound(0); i++)
                        {
                            newArgs = newArgs + " " + sArgss[i];
                        }

                        Command.ParseCommand(chr, sArgss[0].ToString().Trim(), newArgs.Trim());
                        chr.FirstJoinedCommand = chr.CommandType;
                    }
                    if (sCommands[1].Length > 0)
                    {
                        //get the command and the args
                        sCommands[1] = sCommands[1].Trim();//.ToLower();

                        string[] sArgss = sCommands[1].Split(" ".ToCharArray());

                        newArgs = "";

                        for (i = 1; i <= sArgss.GetUpperBound(0); i++)
                        {
                            newArgs = newArgs + " " + sArgss[i];
                        }

                        Command.ParseCommand(chr, sArgss[0].ToString().Trim(), newArgs.Trim());
                        chr.FirstJoinedCommand = CommandType.None;
                    }
                    return true;
                }
                #endregion

                // lower case
                command = command.ToLower();

                MethodInfo[] methodInfo = typeof(Command).GetMethods();

                try
                {
                    foreach (MethodInfo m in methodInfo)
                    {
                        int length = m.Name.Length;
                        if (m.Name.IndexOf("_") != -1)
                        {
                            length = m.Name.IndexOf("_");
                        }
                        if (m.Name.ToLower().Substring(0, length) == command)
                        {
                            //if (m.Name.EndsWith("_x"))
                            //{
                            //    if (chr.isCommandProcessDelayed() && !chr.IsImmortal) // no delay for immortal flagged characters
                            //    {
                            //        int seconds = (int)(chr.getCommandDelayRemaining() / 1000);
                            //        string delayInfo = "Your command time debt has not been paid yet. You have " + seconds + " second";
                            //        if (seconds > 1)
                            //        {
                            //            delayInfo += "s remaining.";
                            //        }
                            //        else
                            //        {
                            //            delayInfo += " remaining.";
                            //        }
                            //        chr.WriteToDisplay(delayInfo);
                            //        chr.sendSound(Sound.GetCommonSound(Sound.CommonSound.Beep));
                            //        return false;
                            //    }
                            //    chr.startCommandDelayTimer();
                            //}

                            chr.cmdWeight += 1; // add one to the command weight

                            object[] obj = new object[1];

                            obj[0] = args;

                            try
                            {
                                m.Invoke(this, obj);
                            }
                            catch (Exception e)
                            {
                                Utils.LogException(e);
                            }
                            return true;
                        }
                    }
                }
                catch (Exception e)
                {
                    chr.WriteToDisplay("Error processing your command. Please report this.");
                    Utils.Log("Failure at Command.interpretCommand(" + command + ", " + args + ")", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
                    return false;
                }
            }

            #region Chants
            #region Demon Summoning Chant
            if (command == "alsi" && args.StartsWith("ku nushi ilani"))
            {
                chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
                if (chr.gender == Globals.eGender.Female)
                {
                    chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FemaleSpellWarm));
                }
                else
                {
                    chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MaleSpellWarm));
                }
                int dNum = Rules.dice.Next(6);
                switch (dNum)
                {
                    case 0:
                        NPC.ChantDemonSummon(NPC.DemonName.Asmodeus, chr);
                        break;
                    case 1:
                        NPC.ChantDemonSummon(NPC.DemonName.Damballa, chr);
                        break;
                    case 2:
                        NPC.ChantDemonSummon(NPC.DemonName.Glamdrang, chr);
                        break;
                    case 3:
                        NPC.ChantDemonSummon(NPC.DemonName.Pazuzu, chr);
                        break;
                    case 4:
                        NPC.ChantDemonSummon(NPC.DemonName.Perdurabo, chr);
                        break;
                    case 5:
                        NPC.ChantDemonSummon(NPC.DemonName.Thamuz, chr);
                        break;
                    case 6:
                    default:
                        NPC.ChantDemonSummon(NPC.DemonName.Samael, chr);
                        break;
                }
                //chr.WriteToDisplay("You hear a voice in your head, \"Not now, I'm busy.\"");
                return true;
            }
            #endregion

            #region Ancestor Start Chant
            else if (command + " " + args == "ashak ashtug nushi ilani")
            {
                    chr.CommandType = CommandType.Chant;
                    chr.cmdWeight += 3;
                    if (chr.cmdWeight == 3)
                    {
                        if (chr.CurrentCell.IsAncestorStart)
                        {
                            //everyone hears the chant
                            chr.SendToAllInSight(chr.Name + ": ashak ashtug nushi ilani");
                            //flag this character as an ancestor (disables Underworld quests)
                            chr.IsAncestor = true;
                            //send 'em to the Underworld
                            Rules.EnterUnderworld(chr);
                        }
                        else
                        {
                            chr.WriteToDisplay("Your vision blurs for a moment.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Performing death rites requires your full concentration.");
                    }
                return true;
            }
            #endregion

            #region Underworld Portal Chant
            else if (command + " " + args == "urruku ya zi xul")
            {
                    chr.CommandType = CommandType.Chant;
                    //check command weight
                    chr.cmdWeight += 3;
                    if (chr.cmdWeight == 3)
                    {
                        if (chr.CurrentCell.IsUnderworldPortal)
                        {
                            //everyone hears the chant
                            chr.SendToAllInSight(chr.Name + ": urruku ya zi xul");
                            //send 'em to the Underworld
                            Rules.EnterUnderworld(chr);
                        }
                        else
                        {
                            chr.WriteToDisplay("Your vision blurs for a moment.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Beckoning the spirits of the Underworld requires your full concentration.");
                    }
                return true;
            }
            #endregion

            #region Map Portal Chant
            else if (command + " " + args == "ashtug ninda anghizidda arrflug")
            {
                    chr.CommandType = CommandType.Chant;
                    chr.SendToAllInSight(chr.Name + ": ashtug ninda anghizidda arrflug");
                    
                    chr.cmdWeight += 3;
                    if (chr.cmdWeight == 3)
                    {
                        //portal failure if character is an ancestor in the Underworld
                        if (chr.InUnderworld && chr.IsAncestor)
                        {
                            chr.WriteToDisplay("Your time in the realm of the living has ended. You will never return.");
                            return true;
                        }

                        //portal failure if character is in the Underworld and has not completed all four quests
                        if (chr.InUnderworld && (!chr.UW_hasIntestines || !chr.UW_hasLiver || !chr.UW_hasLungs || !chr.UW_hasStomach))
                        {
                            chr.WriteToDisplay("You cannot return to the realm of the living until you are whole.");
                            return true;
                        }
                        if (chr.InUnderworld && (chr.UW_hasIntestines || chr.UW_hasLiver || chr.UW_hasLungs || chr.UW_hasStomach))
                        {
                            // send them back to kesmai
                            Rules.ReturnFromUnderworld(chr);
                            return true;
                        }

                        if (chr.CurrentCell.IsMapPortal)
                        {
                            #region fail to portal if holding a corpse
                            if (chr.RightHand != null && chr.RightHand.itemType == Globals.eItemType.Corpse)
                            {
                                chr.WriteToDisplay("Your vision blurs for a moment.");
                                return true;
                            }
                            if (chr.LeftHand != null && chr.LeftHand.itemType == Globals.eItemType.Corpse)
                            {
                                chr.WriteToDisplay("Your vision blurs for a moment.");
                                return true;
                            } 
                            #endregion

                            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MapPortal));

                            Segue segue = chr.CurrentCell.Segue;

                            if (segue != null)
                            {
                                if (segue.LandID != chr.CurrentCell.LandID)
                                {
                                    Rules.OneWayPortal(chr, World.GetFacetByID(chr.FacetID).GetLandByID(segue.LandID));
                                }

                                chr.SendSoundToAllInRange(Sound.GetCommonSound(Sound.CommonSound.MapPortal)); // send sound as portal occurs
                                chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z); // change cell to segue

                                if (chr.IsHidden) // break hide
                                {
                                    chr.IsHidden = false;
                                }

                                int recallReset = 0;

                                // reset recall rings when portal used
                                foreach (Item ring in chr.GetRings())
                                {
                                    if (ring.isRecall)
                                    {
                                        ring.isRecall = false;
                                        ring.wasRecall = true;
                                        recallReset++;
                                    }
                                }

                                if (recallReset > 0)
                                {
                                    chr.SendSound(Sound.GetCommonSound(Sound.CommonSound.RecallReset));

                                    if (recallReset == 1)
                                    {
                                        chr.WriteToDisplay("Your recall ring has been cleared! You must remove and reset it.");
                                    }
                                    else if (recallReset > 1)
                                    {
                                        chr.WriteToDisplay("Your recall rings have been cleared! You must remove and reset them.");
                                    }
                                }

                                chr.dirPointer = "^";
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            chr.WriteToDisplay("Your vision blurs for a moment.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Enabling portal magic requires your full concentration.");
                    }
                return true;
            } 
            #endregion
            #endregion

            #region Spell Chant Exists
            if (chr.spellList.Exists(command + " " + args))
            {
                if (chr.InUnderworld)
                {
                    chr.WriteToDisplay("I don't understand your command.");
                    return true;
                }

                if (chr.preppedSpell != null)
                {
                    chr.WriteToDisplay("You already have a spell warmed. Either cast it or rest to clear your mind.");
                    return true;
                }
                else
                {
                    chr.cmdWeight += 3;
                    if (chr.cmdWeight == 3)
                    {
                        chr.CommandType = CommandType.Chant;
                        chr.prepSpell(chr.spellList.GetInt(command + " " + args));
                        World.magicCordThisRound.Add(chr.FacetID + "|" + chr.LandID + "|" + chr.MapID + "|" + chr.X + "|" + chr.Y + "|" + chr.Z);
                        chr.PreppedRound = DragonsSpineMain.GameRound;
                        int bitcount = 0;
                        Cell curCell = null;
                        // this is the same logic from chr.sendToAllInSight, however we add spell info in some situations
                        // loop through all visible cells

                        for (int ypos = -3; ypos <= 3; ypos += 1)
                        {
                            for (int xpos = -3; xpos <= 3; xpos += 1)
                            {
                                if (Cell.CellRequestInBounds(chr.CurrentCell, xpos, ypos))
                                {
                                    if (chr.CurrentCell.visCells[bitcount]) // check the PC list, and char list of the cell
                                    {
                                        curCell = Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, chr.X + xpos, chr.Y + ypos, chr.Z);

                                        if (curCell != null)
                                        {
                                            foreach (Character character in new List<Character>(curCell.Characters)) // search for the character in the charlist of the cell
                                            {
                                                if (character != chr && character.IsPC) // players sending messages to other players
                                                {
                                                    if (Array.IndexOf(character.ignoreList, chr.PlayerID) == -1) // ignore list
                                                    {
                                                        if ((chr.Group != null && chr.preppedSpell != null && chr.Group.GroupMemberIDList.Contains(character.PlayerID)) || character.ImpLevel >= Globals.eImpLevel.GM)
                                                        {
                                                            character.WriteToDisplay(chr.Name + ": " + command + " " + args + " <" + chr.preppedSpell.Name + ">");
                                                        }
                                                        else
                                                        {
                                                            character.WriteToDisplay(chr.Name + ": " + command + " " + args);
                                                        }
                                                    }
                                                }
                                                else if (character != chr && character.IsPC && !chr.IsPC) // non players sending messages to other players
                                                {
                                                    character.WriteToDisplay(chr.Name + ": " + command + " " + args);
                                                }
                                                //TODO: else... detect spell casting by sensitive NPC
                                            }
                                        }
                                    }
                                    bitcount++;
                                }
                            }
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Warming a spell requires your full concentration.");
                    }
                }
                return true;
            }
            #endregion

            Utils.Log(chr.GetLogString() + " Invalid command: " + command + " args: " + args, Utils.LogType.CommandFailure);
            chr.WriteToDisplay("I don't understand your command. For a full list of game commands visit the Dragon's Spine forums.");
            return false;
        }

        public void macro(string command, String args)
        {
            if (command == "macro")
            {
                chr.WriteToDisplay("Macro Format: $list to view, $# to use, or $# <text> to set, where # can range from 0 to " + Character.MAX_MACROS + ".");
                return;
            }

            if (command.Length >= 5 && command.Substring(1, 4).ToLower() == "list")
            {
                if (chr.macros.Count == 0)
                {
                    chr.WriteToDisplay("You do not have any macros set.");
                    return;
                }

                chr.WriteToDisplay("Macros List");
                for (int a = 0; a < chr.macros.Count; a++)
                {
                    chr.WriteToDisplay(a + " = " + chr.macros[a].ToString());
                }
                return;
            }

            int macrosIndex;

            try
            {
                macrosIndex = Convert.ToInt32(command.Substring(1));
            }
            catch
            {
                chr.WriteToDisplay("Invalid macro format. Format: $# to use or $# <text> to set, where # can range from 0 to " + Character.MAX_MACROS + ".");
                return;
            }

            if (macrosIndex > Character.MAX_MACROS)
            {
                chr.WriteToDisplay("The current maximum amount of macros each character may have is " + (Character.MAX_MACROS + 1) + ".");
                return;
            }

            if (macrosIndex < 0)
            {
                chr.WriteToDisplay("Invalid macro format. Format: $# to use or $# <text> to set, where # can range from 0 to " + Character.MAX_MACROS + ".");
                return;
            }

            try
            {
                if (args == null || args.Length == 0)
                {
                    if (chr.macros.Count >= macrosIndex + 1)
                    {
                        if (chr.macros[macrosIndex].ToString().IndexOf(" ") != -1)
                        {
                            command = chr.macros[macrosIndex].ToString().Substring(0, chr.macros[macrosIndex].ToString().IndexOf(" "));
                            args = chr.macros[macrosIndex].ToString().Substring(chr.macros[macrosIndex].ToString().IndexOf(" ") + 1);
                            ParseCommand(chr, command, args);
                        }
                        else
                        {
                            ParseCommand(chr, chr.macros[macrosIndex].ToString(), "");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Macro " + macrosIndex + " is not set.");
                    }
                }
                else
                {
                    if (chr.macros.Count >= macrosIndex + 1)
                    {
                        if (args.IndexOf(Protocol.ISPLIT) != -1)
                        {
                            chr.WriteToDisplay("Your macro contains an illegal character. The character " + Protocol.ISPLIT + " is reserved.");
                            return;
                        }
                        chr.macros[macrosIndex] = args;
                        chr.WriteToDisplay("Macro " + macrosIndex + " has been set to \"" + args + "\".");
                    }
                    else
                    {
                        if (args.IndexOf(Protocol.ISPLIT) != -1)
                        {
                            chr.WriteToDisplay("Your macro contains an illegal character. The character " + Protocol.ISPLIT + " is reserved.");
                            return;
                        }
                        chr.macros.Add(args);
                        chr.WriteToDisplay("Macro " + macrosIndex + " has been set to \"" + args + "\".");
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void mail(string args)
        {
            // character is not at a mailbox (all regular users must be at a mailbox)
            if (!chr.CurrentCell.HasMailbox && chr.ImpLevel == Globals.eImpLevel.USER)
            {
                chr.WriteToDisplay("You are not standing next to a mailbox.");
                return;
            }

            // args are null or empty
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Type \"mail help\" for more information.");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            switch (sArgs[0].ToLower())
            {
                case "help":
                    break;
                case "create":
                    break;
                case "list":
                    break;
                default:
                    break;
            }
        }

        public void attack_x(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Attack what?");
            }
            else
            {
                kill_x(args);
            }
        }

        public void inventory_x(string args)
        {
            show("inventory");
        }

        public void inv_x(string args)
        {
            show("inventory");
        }

        public void jk_x(string args)
        {
            jumpkick_x(args);
        }

        public void jumpkick_x(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Jumpkick who?");
                return;
            }

            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                chr.WriteToDisplay("Command weight limit exceeded. Jumpkick command not processed.");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs[0] == null)
            {
                chr.WriteToDisplay("Who do you want to jumpkick?");
                return;
            }

            Character target = null;

            if (sArgs.Length == 2)
            {
                int countTo = 0;

                try
                {
                    countTo = Convert.ToInt32(sArgs[0]);
                    target = Map.FindTargetInView(chr, sArgs[1].ToLower(), countTo);
                }
                catch
                {
                    target = Map.FindTargetInView(chr, sArgs[0].ToLower(), false, false);
                }
            }
            else
            {
                target = Map.FindTargetInView(chr, sArgs[0].ToLower(), false, false);
            }

            if (target == null)
            {
                chr.WriteToDisplay("You don't see a " + args[0] + " here.");
            }
            else
            {
                if (target.CurrentCell != chr.CurrentCell)
                {
                    Creature pathTest = new Creature();
                    pathTest.CurrentCell = chr.CurrentCell;
                    if (!pathTest.BuildMoveList(target.X, target.Y, target.Z))
                    {
                        chr.WriteToDisplay("The path is blocked.");
                        pathTest.RemoveFromWorld();
                        return;
                    }
                    pathTest.RemoveFromWorld();
                }

                chr.CommandType = CommandType.Jumpkick;

                chr.Stamina -= Cell.GetCellDistance(chr.X, chr.Y, target.X, target.Y);

                string encumbDesc = Rules.GetEncumbrance(chr);
                if (encumbDesc == "moderately")
                    chr.Stamina -= 2;
                else if (encumbDesc == "heavily")
                    chr.Stamina -= 4;
                else if (encumbDesc == "severely")
                    chr.Stamina -= 6;

                if (chr.Stamina < 0)
                {
                    Rules.DoDamage(chr, chr, Math.Abs(chr.Stamina), false);
                    chr.Stamina = 0;
                }

                chr.CurrentCell = target.CurrentCell;

                chr.updateMap = true;

                Rules.DoJumpKick(chr, target);
            }
        }

        public void kick_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Who do you want to kick?");
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs[0] == null)
            {
                chr.WriteToDisplay("Who do you want to kick?");
                return;
            }

            Character target = null;

            if (sArgs.Length == 2)
            {
                int countTo = 0;

                try
                {
                    countTo = Convert.ToInt32(sArgs[0]);
                    target = Map.FindTargetInView(chr, sArgs[1].ToLower(), countTo);
                }
                catch
                {
                    target = Map.FindTargetInCell(chr, sArgs[0].ToLower());
                }
            }
            else
            {
                target = Map.FindTargetInCell(chr, sArgs[0].ToLower());
            }

            if (target == null)
            {
                chr.WriteToDisplay("You don't see a " + args[0] + " here.");
            }
            else
            {
                chr.CommandType = CommandType.Kick;

                chr.Stamina -= 1;

                string encumbDesc = Rules.GetEncumbrance(chr);
                if(encumbDesc == "moderately")
                    chr.Stamina -= 1;
                else if(encumbDesc == "heavily")
                    chr.Stamina -= 2;
                else if(encumbDesc == "severely")
                    chr.Stamina -= 3;

                if (chr.Stamina < 0)
                {
                    Rules.DoDamage(chr, chr, Math.Abs(chr.Stamina), false);
                    chr.Stamina = 0;
                }

                Rules.Combat(chr, target, chr.GetInventoryItem(Globals.eWearLocation.Feet));
            }
        }

        public void load_x(string args)
        {
            nock_x(args);
        }

        public void nock_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (chr.RightHand == null)
            {
                if (chr.LeftHand == null)
                {
                    chr.WriteToDisplay("You are not holding a bow.");
                }
                else
                {
                    chr.WriteToDisplay("The bow you want to nock should be in your right hand.");
                }
                return;
            }

            if (chr.RightHand.skillType != Globals.eSkillType.Bow)
            {
                chr.WriteToDisplay("You are not holding a bow.");
                return;
            }

            if (chr.RightHand.nocked)
            {
                chr.WriteToDisplay("The " + chr.RightHand.name + " is already nocked.");
                return;
            }

            if (chr.LeftHand != null)
            {
                chr.WriteToDisplay("Your left hand must be empty.");
                return;
            }

            if (chr.RightHand.IsAttunedToOther(chr))
            {
                chr.CurrentCell.Add(chr.RightHand);
                chr.WriteToDisplay("The " + chr.RightHand.name + " leaps from your hand!");
                chr.RightHand = null;
                return;
            }

            if (!chr.RightHand.AlignmentCheck(chr))
            {
                chr.CurrentCell.Add(chr.RightHand);
                chr.WriteToDisplay("The " + chr.RightHand.name + " singes your hands and falls to the ground!");
                chr.UnEquipRightHand(chr.RightHand);
                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                return;
            }

            chr.RightHand.nocked = true;

            if (chr.RightHand.longDesc.ToLower().IndexOf("crossbow") != -1)
            {
                chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.NockCrossbow));
            }
            else
            {
                chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.NockBow));
            }
        }

        public void shoot_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (args == null || args == "")
            {
                chr.WriteToDisplay("Shoot what?");
                return;
            }

            String[] sArgs = args.Split(" ".ToCharArray());
            //TODO: look in right and left hands, as well as the belt for an item to shoot/throw
            if (chr.RightHand == null) { chr.WriteToDisplay("You are not holding something that you can shoot."); return; }
            if (chr.RightHand.skillType != Globals.eSkillType.Bow)
            {
                throw_x(args);
                return;
            }
            if (!chr.RightHand.nocked)
            {
                chr.WriteToDisplay("The " + chr.RightHand.name + " is not nocked.");
                return;
            }
            if (chr.RightHand.attunedID != 0)
            {
                if (chr.IsPC && chr.RightHand.IsAttunedToOther(chr))
                {
                    chr.CurrentCell.Add(chr.RightHand);
                    chr.WriteToDisplay("The " + chr.RightHand.name + " leaps from your hands!");
                    chr.UnEquipRightHand(chr.RightHand);
                    return;
                }
            }
            if (chr.RightHand.alignment != Globals.eAlignment.None)
            {
                if (!chr.RightHand.AlignmentCheck(chr))
                {
                    chr.CurrentCell.Add(chr.RightHand);
                    chr.WriteToDisplay("The " + chr.RightHand.name + " singes your hands and falls to the ground!");
                    chr.UnEquipRightHand(chr.RightHand);
                    Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                    return;
                }
            }
                Character target = Map.FindTargetInView(chr, sArgs[0], false, false);
                if (target == null)
                {
                    chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                    return;
                }
                chr.CommandType = CommandType.Shoot;
                chr.RightHand.nocked = false;
                Rules.Combat(chr, target, chr.RightHand); // change to bow combat

        }

        private void SpawnFigurine(Item item, Cell target, Character chr)
        {
            string message = "";
            NPC fig = NPC.CreateNPC(Item.ID_SUMMONEDMOB, target.FacetID, target.LandID, target.MapID, target.X, target.Y, target.Z);
            Item armor = null;
            fig.Level = (short)(Skills.GetSkillLevel(item.figExp) + 1);
            fig.Experience = item.figExp;
            fig.aiType = NPC.AIType.Summoned;            
            fig.IsSummoned = true;
            fig.IsUndead = false;
            fig.RoundsRemaining = Rules.RollD(fig.Level, 6);
            fig.Age = 0;
            fig.Alignment = chr.Alignment;
            fig.special = item.special;
            fig.canCommand = false;
            fig.BaseProfession = Character.ClassType.Fighter;
            fig.classFullName = "Fighter";
            fig.IsMobile = true;
            fig.animal = true;
            fig.unarmed = Skills.GetSkillToNext(fig.Level);
            fig.HitsMax = Rules.GetHitsGain(fig, fig.Level);
            fig.ManaMax = Rules.GetManaGain(fig, fig.Level);
            fig.StaminaMax = Rules.GetStaminaGain(fig, fig.Level);

            if (item.special.ToLower().IndexOf("snake") > -1)
            {
                armor = Item.CopyItemFromDictionary(8113); // scales
                fig.Name = "snake";
                fig.idleSound = "0017";
                fig.deathSound = "0041";
                fig.attackSound = "0029";
                fig.speed = 3;
                fig.Strength = 3 + fig.Level;
                fig.Dexterity = 12 + fig.Level;
                fig.Constitution = 4 + fig.Level;
                fig.Wisdom = 2 + fig.Level;
                fig.Charisma = 2 + fig.Level;
                fig.Intelligence = 2 + fig.Level;
                message = "the hiss of a snake.";
                fig.shortDesc = "snake";
                fig.longDesc = "a snake";
                fig.attackString1 = "the snake bites you!"; // normal
                fig.attackString2 = "the snake bites you!"; // smash if str > 20
                fig.attackString3 = "the snake bites you!"; // normal (bite)
                fig.attackString4 = "the snake bites you!"; // can poison
                fig.attackString5 = "the snake bites you!"; // normal
                fig.attackString6 = "the snake bites you!"; // normal
                fig.blockString1 = "You are blocked by the snakes scales."; // normal
                fig.blockString2 = "You are blocked by the snakes scales."; // normal
                fig.blockString3 = "You are blocked by the snakes scales."; // normal
                fig.species = Globals.eSpecies.Reptile;
            }
            else if (item.special.ToLower().IndexOf("tiger") > -1)
            {
                armor = Item.CopyItemFromDictionary(7204); // fur
                fig.Name = "tiger";
                fig.idleSound = "0015";
                fig.deathSound = "0039";
                fig.attackSound = "0027";
                fig.speed = 2;
                fig.Strength = 10 + fig.Level;
                fig.Dexterity = 11 + fig.Level;
                fig.Constitution = 10 + fig.Level;
                fig.Wisdom = 3 + fig.Level;
                fig.Charisma = 3 + fig.Level;
                fig.Intelligence = 3 + fig.Level;
                fig.shortDesc = "tiger";
                fig.longDesc = "a tiger";
                message = "a tiger roar.";
                fig.attackString1 = "the tiger bites you!"; // normal
                fig.attackString2 = "the tiger hits with a claw!"; // smash if str > 20
                fig.attackString3 = "the tiger bites you!"; // normal (bite)
                fig.attackString4 = "the bites bites you!"; // can poison
                fig.attackString5 = "the rakes you with its claws!"; // normal
                fig.attackString6 = "the mauls you!"; // normal
                fig.blockString1 = "You are blocked by the tiger's fur."; // normal
                fig.blockString2 = "You are blocked by the tiger's paw."; // normal
                fig.blockString3 = "You are blocked by the tiger's paw."; // normal
                fig.species = Globals.eSpecies.WildAnimal;
            }
            else if (item.special.ToLower().IndexOf("griffin") > -1)
            {
                armor = Item.CopyItemFromDictionary(6200); // feathers
                fig.Name = "griffin";
                fig.idleSound = "0009";
                fig.deathSound = "0033";
                fig.attackSound = "0021";
                fig.speed = 2;
                fig.Strength = 11 + fig.Level;
                fig.Dexterity = 10 + fig.Level;
                fig.Constitution = 9 + fig.Level;
                fig.Wisdom = 3 + fig.Level;
                fig.Charisma = 3 + fig.Level;
                fig.Intelligence = 3 + fig.Level;
                fig.shortDesc = "griffin";
                fig.longDesc = "a griffin";
                fig.attackString1 = "the griffin bites you!"; // normal
                fig.attackString2 = "the griffin rakes you with it's claws!"; // smash if str > 20
                fig.attackString3 = "the griffin bites you!"; // normal (bite)
                fig.attackString4 = "the griffin bites you!"; // can poison
                fig.attackString5 = "the griffin claws you!"; // normal
                fig.attackString6 = "the griffin scratches you!"; // normal
                fig.blockString1 = "You are blocked by griffin's hide."; // normal
                fig.blockString2 = "You are blocked by griffin's paw."; // normal
                fig.blockString3 = "You are blocked by griffin's paw."; // normal
                message = "a griffin screech.";
                fig.species = Globals.eSpecies.Avian;
            }
            else if (item.special.ToLower().IndexOf("firedragon") > -1)
            {
                armor = Item.CopyItemFromDictionary(8107); // Young dragon
                fig.Name = "dragon";
                fig.idleSound = "0019";
                fig.deathSound = "0043";
                fig.attackSound = "0031";
                fig.speed = 4;
                fig.Strength = 14 + fig.Level;
                fig.Dexterity = 12 + fig.Level;
                fig.Constitution = 13 + fig.Level;
                fig.Wisdom = 12 + fig.Level;
                fig.Charisma = 10 + fig.Level;
                fig.Intelligence = 12 + fig.Level;
                fig.shortDesc = "dragon";
                fig.longDesc = "a dragon";
                message = "a dragon roar.";
                fig.attackString1 = "the dragon bites you!"; // normal
                fig.attackString2 = "the dragon buffets you with its wings!"; // smash if str > 20
                fig.attackString3 = "the dragon mauls you!"; // normal (bite)
                fig.attackString4 = "the dragon rakes you with its claws!"; // can poison
                fig.attackString5 = "the dragon bites you!"; // normal
                fig.attackString6 = "the dragon whips you with it's tail!"; // normal
                fig.blockString1 = "You are blocked by the dragon's scales."; // normal
                fig.blockString2 = "You are blocked by the dragon's tail."; // normal
                fig.blockString3 = "You are blocked by the dragon's claw."; // normal
                fig.castMode = NPC.CastMode.Unlimited;
                fig.species = Globals.eSpecies.FireDragon;
                fig.magic = Skills.GetSkillToNext(fig.Level);
            }
            else if (item.special.ToLower().IndexOf("drake") > -1)
            {
                armor = Item.CopyItemFromDictionary(8109); // drake
                fig.Name = "drake";
                fig.idleSound = "0019";
                fig.deathSound = "0043";
                fig.attackSound = "0031";
                fig.shortDesc = "drake";
                fig.longDesc = "a drake";
                fig.castMode = NPC.CastMode.Unlimited;
                fig.species = Globals.eSpecies.LightningDrake;
                fig.speed = 4;
                fig.Strength = 15 + fig.Level;
                fig.Dexterity = 13 + fig.Level;
                fig.Constitution = 14 + fig.Level;
                fig.Wisdom = 14 + fig.Level;
                fig.Charisma = 11 + fig.Level;
                fig.Intelligence = 13 + fig.Level;
                fig.attackString1 = "the drake bites you!"; // normal
                fig.attackString2 = "the drake buffets you with its wings!"; // smash if str > 20
                fig.attackString3 = "the drake mauls you!"; // normal (bite)
                fig.attackString4 = "the drake rakes you with its claws!"; // can poison
                fig.attackString5 = "the drake bites you!"; // normal
                fig.attackString6 = "the drake whips you with it's tail!"; // normal
                fig.blockString1 = "You are blocked by the drake's scales."; // normal
                fig.blockString2 = "You are blocked by the drake's tail."; // normal
                fig.blockString3 = "You are blocked by the drake's claw."; // normal
                message = "a drake roar.";
                fig.magic = Skills.GetSkillToNext(fig.Level);
            }

            fig.Hits = fig.HitsFull;
            fig.Mana = fig.ManaFull;
            fig.Stamina = fig.StaminaFull;
            fig.WearItem(armor);
            fig.CurrentCell = target;
            //fig.CurrentCell.Add(fig);
            fig.CurrentCell.SendShout(message);
            fig.CurrentCell.EmitSound(fig.attackSound);
            Utils.Log(fig.GetLogString() + " [Final Hits: " + fig.HitsFull + "] [Final Mana: " + fig.ManaFull + "] Rounds: "+fig.RoundsRemaining, Utils.LogType.ItemFigurineUse);
        }

        public void throw_x(string args)
        {
            int num = 0;
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Throw what?");
                return;
            }

            chr.cmdWeight += 1;

            if (chr.cmdWeight > 3)
            {
                chr.WriteToDisplay("Command weight limit exceeded. Th command not processed.");
                return;
            }

            bool rightHand = false;

            Item item = null;

            Character target = null;

            string[] sArgs = args.Split(" ".ToCharArray());

            if (chr.LeftHand != null && (sArgs[0].ToLower() == "left" || sArgs[0].ToLower() == chr.LeftHand.name.ToLower()))
                item = chr.LeftHand;

            else if (chr.RightHand != null && (sArgs[0].ToLower() == "right" || sArgs[0].ToLower() == chr.RightHand.name.ToLower()))
            {
                item = chr.RightHand;
                rightHand = true;
            }

            if (item == null) // item check failed
            {
                chr.WriteToDisplay("You are not holding a " + sArgs[0] + ".");
                return;
            }

            if (sArgs.Length == 1)
            {
                chr.WriteToDisplay("Where do you want to throw " + item.name + "?");
                return;
            }

            #region Thrown item is attuned
            if (chr.IsPC && item.IsAttunedToOther(chr))
            {
                chr.CurrentCell.Add(item);
                chr.WriteToDisplay("The " + item.name + " leaps from your hand!");
                if (rightHand)
                {
                    chr.RightHand = null;
                }
                else
                {
                    chr.LeftHand = null;
                }
                return;
            }
            #endregion

            #region Thrown item is aligned
            if (!item.AlignmentCheck(chr))
            {
                chr.CurrentCell.Add(item);
                chr.WriteToDisplay("The " + item.name + " singes your hand and falls to the ground!");
                if (rightHand)
                {
                    chr.RightHand = null;
                    Rules.DoDamage(chr, chr, Rules.dice.Next(1, 4), false);
                }
                else
                {
                    chr.LeftHand = null;
                    Rules.DoDamage(chr, chr, Rules.dice.Next(1, 4), false);
                }
                return;
            }
            #endregion

            #region Throw an item at a target
            if (sArgs.Length >= 3 && sArgs[1] == "at" && sArgs[2] != null)
            {
                if (sArgs[2].Length == 1) // this would be a number - target becomes sArgs[3]
                {
                    num = Convert.ToInt32(sArgs[2]);
                    target = Map.FindTargetInView(chr, sArgs[3], false, false);
                }
                else
                {
                    target = Map.FindTargetInView(chr, sArgs[2], false, false);
                }

                Cell targetCell = null;

                if (target != null)
                {
                    chr.CommandType = CommandType.Throw;

                    #region Thrown item is a weapon
                    if (item.itemType == Globals.eItemType.Weapon) // if throwing a weapon at something
                    {
                        if (target != chr)
                        {
                            targetCell = target.CurrentCell;
                            Rules.Combat(chr, target, item);
                        }
                        else
                        {
                            chr.WriteToDisplay("You cannot throw things at yourself.");
                            return;
                        }
                    }
                    #endregion

                    #region else if Figurine thrown at a target
                    else if (item.baseType == Globals.eItemBaseType.Figurine) // if thrown item is figurine
                    {
                        // breakable chance if figurine is fragile
                        if (item.fragile)
                        {
                            // 2d100 roll for fig break
                            int figBreakRoll = Rules.RollD(2, 100);

                            if (chr.IsLucky && Rules.RollD(1, 100) >= 50)
                                figBreakRoll++;

                            // two 1's are rolled, character is not lucky
                            if (figBreakRoll == 2)
                            {
                                target.SendShout("something shatter into a hundred pieces.");
                                target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                                Utils.Log("[" + chr.PlayerID + "]" + chr.Name + "(" + chr.accountID + ") broke a figurine. Item ID: " + item.itemID.ToString() + " FigExp: " + item.figExp.ToString(), Utils.LogType.ItemFigurineUse);
                            }
                            else
                            {
                                SpawnFigurine(item, target.CurrentCell, chr);
                            }
                        }
                        else
                        {
                            SpawnFigurine(item, target.CurrentCell, chr);
                        }
                    }
                    #endregion

                    #region else if Bottle thrown at a target

                    else if (item.baseType == Globals.eItemBaseType.Bottle) // what to do if the thrown item is a bottle
                    {
                        if (Rules.CheckFumble(chr, item))
                        {
                            target = chr;
                            chr.SendToAllInSight(chr.Name + " fumbles!");
                            chr.WriteToDisplay("You fumble!");
                        }

                        if (item.effectType.Length > 0)
                        {
                            string[] effectTypes = item.effectType.Split(" ".ToCharArray());
                            string[] effectAmounts = item.effectAmount.Split(" ".ToCharArray());
                            string[] effectDurations = item.effectDuration.Split(" ".ToCharArray());

                            for (int a = 0; a < effectTypes.Length; a++)
                            {
                                Effect.EffectType effectType = (Effect.EffectType)Convert.ToInt32(effectTypes[a]);
                                if (effectType == Effect.EffectType.Nitro)
                                {
                                    Spell.CastGenericAreaSpell(target.CurrentCell, "", Effect.EffectType.Nitro, Convert.ToInt32(effectAmounts[a]), "");
                                }
                                else if (effectType == Effect.EffectType.Naphtha)
                                {
                                    ArrayList cells = new ArrayList();
                                    cells.Add(target.CurrentCell);
                                    Effect effect = new Effect(Effect.EffectType.Fire, "**", Convert.ToInt32(effectAmounts[a]), Convert.ToInt32(effectDurations[a]), chr, cells);
                                }
                            }
                        }

                        target.SendShout("the sound of glass shattering.");
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                    }
                    #endregion

                    else
                    {
                        chr.WriteToDisplay("You miss!");
                        if (chr.race != "")
                        {
                            target.WriteToDisplay(chr.Name + " misses you.");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + chr.Name + " misses you.");
                        }

                        if (targetCell != null && !targetCell.Items.Contains(item))
                        {
                            targetCell.Add(item);
                        }
                    }
                }
                else
                {
                    chr.WriteToDisplay("You don't see " + sArgs[2] + " here.");
                    return;
                }

                // ** item may have been removed from hand if fumbled result in Rules.doCombat
                if (rightHand && chr.RightHand != null)
                {
                    if (!item.returning)
                    {
                        chr.UnEquipRightHand(item);
                        if (targetCell != null)
                        {
                            targetCell.Add(item);
                        }
                    }
                }
                else if (!rightHand && chr.LeftHand != null)
                {
                    if (!item.returning)
                    {
                        chr.UnEquipLeftHand(item);
                        if (targetCell != null)
                        {
                            targetCell.Add(item);
                        }
                    }
                }
                return;
            }
            #endregion

            #region Throw an item in a direction
            // not throwing an item AT a target
            args = "";

            for (int a = 1; a < sArgs.Length; a++)
            {
                args += sArgs[a] + " ";
            }

            Cell cell = Map.GetCellRelevantToCell(chr.CurrentCell, args.Substring(0, args.Length - 1), false);

            if (cell != chr.CurrentCell)
            {
                Creature pathTest = new Creature();
                pathTest.CurrentCell = chr.CurrentCell;
                if (!pathTest.BuildMoveList(cell.X, cell.Y, cell.Z))
                    cell = chr.CurrentCell;
                pathTest.RemoveFromWorld();
            }

            if (item.effectType.Length > 0)
            {
                string[] effectTypes = item.effectType.Split(" ".ToCharArray());
                string[] effectAmounts = item.effectAmount.Split(" ".ToCharArray());
                string[] effectDurations = item.effectDuration.Split(" ".ToCharArray());

                for(int a = 0; a < effectTypes.Length; a++)
                {
                    Effect.EffectType effectType = (Effect.EffectType)Convert.ToInt32(effectTypes[a]);
                    if(effectType == Effect.EffectType.Nitro)
                    {
                        Spell.CastGenericAreaSpell(cell, "", Effect.EffectType.Nitro, Convert.ToInt32(effectAmounts[a]), "concussion", chr);
                    }
                    else if (effectType == Effect.EffectType.Naphtha)
                    {
                        ArrayList cells = new ArrayList();
                        cells.Add(cell);
                        Effect effect = new Effect(Effect.EffectType.Fire, "**", Convert.ToInt32(effectAmounts[a]), Convert.ToInt32(effectDurations[a]), chr, cells);
                    }
                }
                if (item.baseType == Globals.eItemBaseType.Bottle)
                {
                    cell.SendShout("the sound of glass shattering.");
                    cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                    item = null;
                }
                else if (item.fragile)
                {
                    int breakRoll = Rules.RollD(1, 100);

                    if (item.baseType == Globals.eItemBaseType.Figurine)
                    {
                        breakRoll += Rules.GetExpLevel(item.figExp);
                        // less chance for a lucky character to break a figurine
                        if (chr.IsLucky && Rules.RollD(1, 100) >= 50)
                            breakRoll += 20;
                    }

                    if (breakRoll > 25)
                        cell.Add(item);
                    else
                    {
                        target.SendShout("something shatter into pieces.");
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                        if (item.baseType == Globals.eItemBaseType.Figurine)
                        {
                            Utils.Log("[" + chr.PlayerID + "]" + chr.Name + "(" + chr.account + ") broke a figurine. Item ID: " + item.itemID.ToString() + " FigExp: " + item.figExp.ToString(), Utils.LogType.ItemFigurineUse);
                        }
                    }
                }
                else
                {
                    cell.Add(item);
                }
            }
            else
            {
                #region else if Figurine thrown at a target
                    if (item.baseType == Globals.eItemBaseType.Figurine) // if thrown item is figurine
                    {
                        // breakable chance if figurine is fragile
                        if (item.fragile)
                        {
                            // 2d100 roll for fig break
                            int figBreakRoll = Rules.RollD(2, 100);

                            if (chr.IsLucky && Rules.RollD(1, 100) >= 50)
                                figBreakRoll++;

                            // two 1's are rolled, character is not lucky
                            if (figBreakRoll == 2)
                            {
                                target.SendShout("something shatter into a hundred pieces.");
                                target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                                Utils.Log("[" + chr.PlayerID + "]" + chr.Name + "(" + chr.accountID + ") broke a figurine. Item ID: " + item.itemID.ToString() + " FigExp: " + item.figExp.ToString(), Utils.LogType.ItemFigurineUse);
                            }
                            else
                            {
                                SpawnFigurine(item, cell, chr);
                            }
                        }
                        else
                        {
                            SpawnFigurine(item, cell, chr);
                        }
                    }
                    #endregion
                else if (item.fragile)
                {
                    int breakRoll = Rules.RollD(1, 100);
                    if (item.baseType == Globals.eItemBaseType.Figurine)
                    {
                        breakRoll += Rules.GetExpLevel(item.figExp);
                        // less chance for a lucky character to break a figurine
                        if (chr.IsLucky && Rules.RollD(1, 100) >= 50)
                            breakRoll += 20;
                    }

                    if (breakRoll > 25)
                        cell.Add(item);
                    else
                    {
                        target.SendShout("something shatter into pieces.");
                        target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                        if (item.baseType == Globals.eItemBaseType.Figurine)
                        {
                            Utils.Log(chr.GetLogString() + " broke a figurine. " + item.GetLogString() + " FigExp: " + item.figExp.ToString(), Utils.LogType.ItemFigurineUse);
                        }
                    }
                }
                else
                {
                    cell.Add(item);
                }
            }
            if (rightHand)
            {
                chr.RightHand = null;
            }
            else
            {
                chr.LeftHand = null;
            }
            #endregion
        }

        public void bash(string args)
        {
            if (chr.BaseProfession != Character.ClassType.Fighter)
            {
                chr.WriteToDisplay("You do not have that ability.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Bash what?");
            }

            // remove command weight if level 9+
            if (chr.Level < 9)
            {
                chr.cmdWeight += 1;

                if (chr.cmdWeight > 3)
                {
                    chr.WriteToDisplay("Command weight limit exceeded. Bash command not processed.");
                    return;
                }
            }

            // if first command is movement and bash skill level is less than 12 (Expert) then fail a move and bash attempt
            if (chr.FirstJoinedCommand == CommandType.Movement && Skills.GetSkillLevel(chr.bash) < 12)
            {
                chr.WriteToDisplay("You are not skilled enough to move and bash in the same round.");
                return;
            }

            if (DragonsSpineMain.GameRound - chr.lastBashRound < 2)
            {
                chr.WriteToDisplay("Your bash timer has not reset yet. (" + Convert.ToString(2 - (DragonsSpineMain.GameRound - chr.lastBashRound)) + " round(s) remaining)");
                return;
            }

            Item shield = chr.LeftHand;

            if (shield == null)
            {
                chr.WriteToDisplay("You cannot bash if you do not have an item equipped in your left hand.");
                return;
            }

            if (shield.skillType != Globals.eSkillType.Bash)
            {
                chr.WriteToDisplay("You cannot bash with " + shield.shortDesc + ".");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = null;

            if (sArgs.Length == 2)
            {
                int countTo = 0;

                try
                {
                    countTo = Convert.ToInt32(sArgs[0]);
                    target = Map.FindTargetInView(chr, sArgs[1].ToLower(), countTo);
                }
                catch
                {
                    target = Map.FindTargetInCell(chr, sArgs[0].ToLower());
                }
            }
            else target = Map.FindTargetInCell(chr, sArgs[0].ToLower());

            if (target == null)
            {
                chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                return;
            }

            if (target.merchantType > Merchant.MerchantType.None)
            {
                chr.WriteToDisplay("You cannot bash a merchant...yet.");
                return;
            }

            if (target.trainerType > 0)
            {
                chr.WriteToDisplay("You cannot bash a trainer...yet.");
                return;
            }

            if (shield.IsAttunedToOther(chr)) // check if a weapon is attuned
            {
                chr.CurrentCell.Add(shield);
                chr.WriteToDisplay("The " + shield.name + " leaps from your hand!");
                chr.UnEquipLeftHand(shield);
                return;
            }

            if (!shield.AlignmentCheck(chr)) // check if a weapon has an alignment
            {
                chr.CurrentCell.Add(shield);
                chr.WriteToDisplay("The " + shield.name + " singes your hand and falls to the ground!");
                chr.UnEquipLeftHand(shield);
                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                return;
            }

            if (chr.Stamina < 2)
            {
                chr.WriteToDisplay("You do not have enough stamina to bash.");
                return;
            }

            // successful bash
            chr.Stamina -= 2;
            chr.CommandType = CommandType.Bash;
            chr.lastBashRound = DragonsSpineMain.GameRound;
            Rules.Combat(chr, target, shield);
        }

        public void kill_x(string args)
        {
            chr.cmdWeight += 1;

            if (chr.cmdWeight > 3)
                return;

            if (chr.FirstJoinedCommand == CommandType.Movement)
            {
                chr.WriteToDisplay("You do not have the ability to move and attack in the same round.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Kill what?");
            }
            else
            {
                chr.CommandType = CommandType.Attack;

                Item weapon = chr.RightHand;

                string[] sArgs = args.Split(" ".ToCharArray());

                Character target = null;

                if (weapon == null) // check for gauntlets if hands are empty
                    weapon = chr.GetInventoryItem(Globals.eWearLocation.Hands);

                if (sArgs.Length == 2)
                {
                    int countTo = 0;

                    try
                    {
                        countTo = Convert.ToInt32(sArgs[0]);
                        target = Map.FindTargetInView(chr, sArgs[1].ToLower(), countTo);
                    }
                    catch
                    {
                        target = Map.FindTargetInCell(chr, sArgs[0].ToLower());
                    }
                }
                else target = Map.FindTargetInCell(chr, sArgs[0].ToLower());

                if (target == null)
                {
                    chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                    return;
                }

                //if (target.merchantType > Merchant.MerchantType.None)
                //{
                //    chr.WriteToDisplay("You cannot attack a merchant...yet.");
                //    return;
                //}

                //if (target.trainerType > 0)
                //{
                //    chr.WriteToDisplay("You cannot attack a trainer...yet.");
                //    return;
                //}

                if (weapon != null)
                {
                    if (weapon.skillType == Globals.eSkillType.Bow) // check if a bow is nocked
                    {
                        chr.WriteToDisplay("You must nock and shoot a bow.");
                        return;
                    }

                    if (weapon.IsAttunedToOther(chr)) // check if a weapon is attuned
                    {
                        chr.CurrentCell.Add(weapon);
                        chr.WriteToDisplay("The " + weapon.name + " leaps from your hand!");
                        chr.UnEquipRightHand(weapon);
                        return;
                    }

                    if (!weapon.AlignmentCheck(chr)) // check if a weapon has an alignment
                    {
                        chr.CurrentCell.Add(weapon);
                        chr.WriteToDisplay("The " + weapon.name + " singes your hand and falls to the ground!");
                        if (weapon.wearLocation == Globals.eWearLocation.Hands)
                        {
                            chr.RemoveWornItem(weapon);
                        }
                        else
                        {
                            chr.UnEquipRightHand(weapon);
                        }
                        Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                        return;
                    }
                }

                Rules.Combat(chr, target, weapon);
            }
        }

        public void f_x(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Fight what?");
                return;
            }
            kill_x(args);
            return;
        }

        public void fight_x(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Fight what?");
            }
            else
            {
                kill_x(args);
            }
            return;
        }

        public void poke_x(string args)
        {
            if (chr.RightHand == null)
            {
                chr.WriteToDisplay("You are not holding a weapon in your right hand.");
                return;
            }

            if (chr.RightHand.baseType != Globals.eItemBaseType.Halberd)
            {
                chr.WriteToDisplay("You cannot poke with that weapon.");
                return;
            }

            if (args == null || args == "")
            {
                chr.WriteToDisplay("Poke what?");
                return;
            }

            chr.cmdWeight += 1;

            if (chr.cmdWeight > 4)
            {
                return;
            }

            if (Rules.GetEncumbrance(chr) != "lightly")
            {
                if (chr.FirstJoinedCommand == CommandType.Movement)
                {
                    chr.WriteToDisplay("You cannot move and poke while " + Rules.GetEncumbrance(chr) + " encumbered.");
                    return;
                }
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = new Character();

            target = Map.findTargetInNextCells(chr, sArgs[0]);

            if (target == null)
            {
                chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                return;
            }
            if (chr.RightHand.IsAttunedToOther(chr))
            {
                chr.CurrentCell.Add(chr.RightHand);
                chr.WriteToDisplay("The " + chr.RightHand.name + " leaps from your hands!");
                chr.UnEquipRightHand(chr.RightHand);
                return;
            }
            if (!chr.RightHand.AlignmentCheck(chr))
            {
                chr.CurrentCell.Add(chr.RightHand);
                chr.WriteToDisplay("The " + chr.RightHand.name + " singes your hand and falls to the ground!");
                chr.UnEquipRightHand(chr.RightHand);
                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                return;
            }

            chr.CommandType = CommandType.Poke;

            Rules.Combat(chr, target, chr.RightHand);
            return;
        }

        public void turn(string args)
        {
            flip(args);
        }

        public void flip(string args)
        {
            Book book;

            int pagesToFlip = 0;
            int iArgCount = 0;

            if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Book) // get the book from hand
            {
                book = (Book)chr.RightHand;
            }
            else if (chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Book)
            {
                book = (Book)chr.LeftHand;
            }
            else
            {
                chr.WriteToDisplay("You are not holding a book.");
                return;
            }

            try
            {
                if (args == null)
                {
                    args = "1"; // flip all alone should advance one page
                }

                String[] sArgs = args.Split(" ".ToCharArray());
                iArgCount = sArgs.GetUpperBound(0) + 1;
                // Start filtering out too many/wrong args 
                if (iArgCount > 3 || (iArgCount == 3 && (sArgs[0].ToLower() != "book" && sArgs[0].ToLower() != "spellbook"))) // FLIP Book TO #
                {
                    chr.WriteToDisplay("Usage: Flip [back / forward / to] [number]");
                    return;
                }
                if (iArgCount == 3) // only way this is true is if first arg is "book" or "spellbook", so we eliminate it
                {
                    sArgs[0] = sArgs[1];
                    sArgs[1] = sArgs[2];
                    iArgCount = 2;
                }

                if (iArgCount == 2) // make sure the second argument, if there is one, is numeric
                {
                    try
                    {
                        pagesToFlip = Convert.ToInt32(sArgs[1]);
                    }
                    catch
                    {
                        chr.WriteToDisplay("Format: Flip [back / forward / to] [number]");
                        return;
                    }

                }
                switch (sArgs[0].ToLower())
                {
                    case "back":
                        if (iArgCount == 1) // FLIP BACK
                        {
                            pagesToFlip = -1;
                        }
                        else // FLIP BACK #
                        {
                            pagesToFlip = pagesToFlip * -1;
                        }
                        break;
                    case "forward":
                        pagesToFlip = pagesToFlip * 1;
                        break;
                    case "to":
                        pagesToFlip = pagesToFlip - book.currentPage;//pagesToFlip = pagesToFlip - 1;//pagesToFlip = pagesToFlip - currentPage;
                        break;
                    case "page":  // FLIP PAGE - turn one page
                        pagesToFlip = 1;
                        break;
                    case "book":  // READ Book ends up here.  So just read the current page.
                        break;
                    default: // either it's a number or it's bad input
                        try
                        {
                            pagesToFlip = Convert.ToInt32(sArgs[0]);
                        }
                        catch
                        {
                            chr.WriteToDisplay("Format: Flip [back / forward / to] [number]");
                            return;
                        }
                        break;
                }
                if (book.currentPage + pagesToFlip <= 0) // the book does not have a negative amount of pages, set to 0
                {
                    book.currentPage = 1;
                }
                else if (book.bookType != Book.BookType.Spellbook && book.currentPage + pagesToFlip > (int)book.pages.Length / 2)
                {
                    book.currentPage = (int)(book.pages.Length / 2) - 1;
                }
                else
                {
                    book.currentPage = book.currentPage + pagesToFlip;
                }

                if (book.bookType == Book.BookType.Spellbook)
                {
                    if (book.attunedID != chr.PlayerID) // do not allow a player to read another player's spellbook
                    {
                        chr.WriteToDisplay("The book is sealed shut.");
                        return;
                    }
                    else
                    {
                        if (book.currentPage > chr.spellList.Count) // page is blank
                        {
                            chr.WriteToDisplay("That page is blank.");
                            return;
                        }
                        int spellID = chr.spellList.GetSpellID(book.currentPage - 1);
                        chr.WriteToDisplay("Page " + (book.currentPage) + ":");
                        chr.WriteToDisplay("The incantation for " + Spell.GetSpell(spellID).Name + " (" + Spell.GetSpell(spellID).SpellCommand + ")");
                        chr.WriteToDisplay(chr.spellList.GetString(book.currentPage - 1));
                    }
                }
                else
                {
                    chr.WriteToDisplay(book.ReadPage());
                }
            }
            catch (Exception e)
            {
                Utils.Log("Command.flip(" + args + ") by " + chr.GetLogString(), Utils.LogType.CommandFailure);
                Utils.LogException(e);
                chr.WriteToDisplay("That page was not found.");
                return;
            }
        }

        public void read(string args)
        {
            if (args.IndexOf("book") != -1)
            {
                flip(args);
                return;
            }

            bool rightHand = true;

            if (args == null)
            {
                if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Book)
                {
                    flip(args);
                    return;
                }

                if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Scroll)
                {
                    goto readScroll;
                }

                if(chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Book)
                {
                    flip(args);
                    return;
                }

                if(chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Scroll)
                {
                    rightHand = false;
                }
            }

            readScroll:
            if (rightHand)
            {
                if (chr.RightHand == null || chr.RightHand.baseType != Globals.eItemBaseType.Scroll)
                {
                    return;
                }

                if (chr.RightHand.pages == null || chr.RightHand.pages[0] == "")
                {
                    chr.WriteToDisplay("The scroll is blank.");
                }
                else
                {
                    chr.WriteToDisplay(chr.RightHand.pages[0]);
                }
            }
            else
            {
                if (chr.LeftHand == null || chr.LeftHand.baseType != Globals.eItemBaseType.Scroll)
                {
                    return;
                }

                if (chr.LeftHand.pages == null || chr.LeftHand.pages[0] == "")
                {
                    chr.WriteToDisplay("The scroll is blank.");
                }
                else
                {
                    chr.WriteToDisplay(chr.LeftHand.pages[0]);
                }
            }
        }

        public void close(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("What do you want to close?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs[0] == "door")
            {
                Cell doorCell = Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[1], true);
                
                string newGraphic = doorCell.CellGraphic;

                if (doorCell.CellGraphic == "| " || doorCell.CellGraphic == "--")
                {
                    chr.WriteToDisplay("The door is already closed.");
                    return;
                }
                else if (doorCell.CellGraphic != "/ " && doorCell.CellGraphic != @"\ ")
                {
                    chr.WriteToDisplay("You don't see a door there.");
                    return;
                }
                else if (Cell.GetCellDistance(chr.X, chr.Y, doorCell.X, doorCell.Y) > 1)
                {
                    chr.WriteToDisplay("The door is too far away.");
                    return;
                }
                else if (doorCell.Items.Count > 0 || doorCell.Characters.Count > 0)
                {
                    chr.WriteToDisplay("The doorway is blocked.");
                    return;
                }

                switch (doorCell.CellGraphic)
                {
                    case "/ ":
                        newGraphic = "| ";
                        break;
                    case "\\ ":
                        newGraphic = "--";
                        break;
                    default:
                        break;
                }
                doorCell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.CloseDoor));
                doorCell.CellGraphic = newGraphic;
                doorCell.DisplayGraphic = newGraphic;
            }
            else if (sArgs[0].ToLower() == "bottle")
            {
                if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.CloseBottle((Bottle)chr.RightHand);
                }
                else if (chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.CloseBottle((Bottle)chr.LeftHand);
                }
                else
                {
                    chr.WriteToDisplay("You are not holding a bottle.");
                }

                return;
            }
            else if (sArgs[0].ToLower() == "book" || sArgs[0].ToLower() == "spellbook")
            {
                Book book = new Book();

                // Get the book from hand
                if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Book)
                {
                    book = (Book)chr.RightHand;
                    book.currentPage = 0;
                    chr.WriteToDisplay("You close the book.");
                }
                else if (chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Book)
                {
                    book = (Book)chr.LeftHand;
                    book.currentPage = 0;
                    chr.WriteToDisplay("You close the book.");
                }
                else
                {
                    chr.WriteToDisplay("You are not holding a book.");
                    return;
                }

                return;
            }
        }

        public void open(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("What do you want to open?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs[0].ToLower() == "bottle" || sArgs[0].ToLower() == "balm" || sArgs[0].ToLower() == "flask")
            {
                if (chr.RightHand != null && chr.RightHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.OpenBottle((Bottle)chr.RightHand, chr);
                }
                else if (chr.LeftHand != null && chr.LeftHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.OpenBottle((Bottle)chr.LeftHand, chr);
                }
                else
                {
                    chr.WriteToDisplay("You are not holding a bottle.");
                }

                return;
            }
            else if (sArgs[0].ToLower() == "book")
            {
                flip(args);
            }
            else if (sArgs[0].ToLower() == "door")
            {
                if (sArgs.Length == 1)
                {
                    chr.WriteToDisplay("Format: open door <direction>");
                    return;
                }

                Cell cell = Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[1], true);
                string newGraphic = cell.CellGraphic;

                if (cell.DisplayGraphic == "\\ " || cell.DisplayGraphic == "/ ")
                {
                    chr.WriteToDisplay("The door is already open.");
                    return;
                }
                else if (cell.DisplayGraphic != "| " && cell.DisplayGraphic != "--")
                {
                    chr.WriteToDisplay("You don't see a door there.");
                    return;
                }

                switch (cell.DisplayGraphic)
                {
                    case "| ":
                        newGraphic = "/ ";
                        break;
                    case "--":
                        newGraphic = "\\ ";
                        break;
                    default:
                        break;
                }
                cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenDoor));
                cell.DisplayGraphic = newGraphic;
                cell.CellGraphic = newGraphic;
            }
        }

        public void drink_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (chr.RightHand != null && chr.RightHand.itemType == Globals.eItemType.Potable)
            {
                if (chr.RightHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.DrinkBottle((Bottle)chr.RightHand, chr);
                }
            }
            else if (chr.LeftHand != null && chr.LeftHand.itemType == Globals.eItemType.Potable)
            {
                if (chr.LeftHand.baseType == Globals.eItemBaseType.Bottle)
                {
                    Bottle.DrinkBottle((Bottle)chr.LeftHand, chr);
                }
            }
            else //TODO: expand here to allow drinking from a fluid source (eg: fountains)
            {
                chr.WriteToDisplay("You are not holding a bottle.");
            }
        }

        public void eat_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (chr.RightHand != null && chr.RightHand.itemType == Globals.eItemType.Edible)
            {
                Food.EatFood(chr.RightHand, chr);
                chr.RightHand = null;
            }
            else if (chr.LeftHand != null && chr.LeftHand.itemType == Globals.eItemType.Edible)
            {
                Food.EatFood(chr.LeftHand, chr);
                chr.LeftHand = null;
            }
            else
            {
                chr.WriteToDisplay("You are not holding something to eat.");
            }
        }

        public void redraw(string args)
        {
            if (chr.protocol == DragonsSpineMain.APP_PROTOCOL || chr.protocol == "old-kesmai")
            {
                chr.updateAll = true;
                return;
            }
            else
            {
                Map.clearMap(chr);
                chr.CurrentCell.showMap(chr);
            }
        }

        public void c_x(string args)
        {
            cast_x(args);
        }

        public void cast_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            chr.CommandType = CommandType.Cast;

            string outOfCharges = "";

            if (args == null)
            {
                goto normalSpell;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            #region Cast a spell from an Item
            try
            {
                #region Right Hand
                if (chr.RightHand != null && chr.RightHand.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.RightHand.spell).SpellCommand)
                {
                    if (chr.RightHand.charges == 0)
                    {
                        outOfCharges += "The " + chr.RightHand.name + " is out of charges. ";
                        goto checkLeftHand;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.RightHand.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.RightHand.charges > 0)
                    {
                        chr.RightHand.charges--;
                    }
                    if (chr.RightHand.special.ToLower().Contains("scroll"))
                        chr.RightHand = null;
                    return;
                }
                #endregion
            checkLeftHand:
                #region Left Hand
                if (chr.LeftHand != null && chr.LeftHand.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.LeftHand.spell).SpellCommand)
                {
                    if (chr.LeftHand.charges == 0)
                    {
                        outOfCharges += "The " + chr.LeftHand.name + " is out of charges. ";
                        goto checkRightRing1;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.LeftHand.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.LeftHand.charges > 0)
                    {
                        chr.LeftHand.charges--;
                    }
                    if (chr.LeftHand.special.ToLower().Contains("scroll"))
                        chr.LeftHand = null;
                    return;
                }
                #endregion
            checkRightRing1:
                #region Right Ring 1
                if (chr.RightRing1 != null && chr.RightRing1.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.RightRing1.spell).SpellCommand)
                {
                    if (chr.RightRing1.charges == 0)
                    {
                        outOfCharges += "The " + chr.RightRing1.name + " is out of charges. ";
                        goto checkRightRing2;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.RightRing1.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.RightRing1.charges > 0)
                    {
                        chr.RightRing1.charges--;
                    }
                    return;
                }
                #endregion
            checkRightRing2:
                #region Right Ring 2
                if (chr.RightRing2 != null && chr.RightRing2.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.RightRing2.spell).SpellCommand)
                {
                    if (chr.RightRing2.charges == 0)
                    {
                        outOfCharges += "The " + chr.RightRing2.name + " is out of charges. ";
                        goto checkRightRing3;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.RightRing2.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.RightRing2.charges > 0)
                    {
                        chr.RightRing2.charges--;
                    }
                    return;
                }
                #endregion
            checkRightRing3:
                #region Right Ring 3
                if (chr.RightRing3 != null && chr.RightRing3.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.RightRing3.spell).SpellCommand)
                {
                    if (chr.RightRing3.charges == 0)
                    {
                        outOfCharges += "The " + chr.RightRing3.name + " is out of charges. ";
                        goto checkRightRing4;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.RightRing3.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.RightRing3.charges > 0)
                    {
                        chr.RightRing3.charges--;
                    }
                    return;
                }
                #endregion
            checkRightRing4:
                #region Right Ring 4
                if (chr.RightRing4 != null && chr.RightRing4.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.RightRing4.spell).SpellCommand)
                {
                    if (chr.RightRing4.charges == 0)
                    {
                        outOfCharges += "The " + chr.RightRing4.name + " is out of charges. ";
                        goto checkLeftRing1;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.RightRing4.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.RightRing4.charges > 0)
                    {
                        chr.RightRing4.charges--;
                    }
                    return;
                }
                #endregion
            checkLeftRing1:
                #region Left Ring 1
                if (chr.LeftRing1 != null && chr.LeftRing1.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.LeftRing1.spell).SpellCommand)
                {
                    if (chr.LeftRing1.charges == 0)
                    {
                        outOfCharges += "The " + chr.LeftRing1.name + " is out of charges. ";
                        goto checkLeftRing2;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.LeftRing1.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.LeftRing1.charges > 0)
                    {
                        chr.LeftRing1.charges--;
                    }
                    return;
                }
                #endregion
            checkLeftRing2:
                #region Left Ring 2
                if (chr.LeftRing2 != null && chr.LeftRing2.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.LeftRing2.spell).SpellCommand)
                {
                    if (chr.LeftRing2.charges == 0)
                    {
                        outOfCharges += "The " + chr.LeftRing2.name + " is out of charges. ";
                        goto checkLeftRing3;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.LeftRing2.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.LeftRing2.charges > 0)
                    {
                        chr.LeftRing2.charges--;
                    }
                    return;
                }
                #endregion
            checkLeftRing3:
                #region Left Ring 3
                if (chr.LeftRing3 != null && chr.LeftRing3.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.LeftRing3.spell).SpellCommand)
                {
                    if (chr.LeftRing3.charges == 0)
                    {
                        outOfCharges += "The " + chr.LeftRing3.name + " is out of charges. ";
                        goto checkLeftRing4;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.LeftRing3.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.LeftRing3.charges > 0)
                    {
                        chr.LeftRing3.charges--;
                    }
                    return;
                }
                #endregion
            checkLeftRing4:
                #region Left Ring 4
                if (chr.LeftRing4 != null && chr.LeftRing4.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(chr.LeftRing4.spell).SpellCommand)
                {
                    if (chr.LeftRing4.charges == 0)
                    {
                        outOfCharges += "The " + chr.LeftRing4.name + " is out of charges. ";
                        goto checkInventory;
                    }
                    Spell storedSpell = chr.preppedSpell;
                    chr.preppedSpell = Spell.GetSpell(chr.LeftRing4.spell);
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    chr.preppedSpell = storedSpell;
                    if (chr.LeftRing4.charges > 0)
                    {
                        chr.LeftRing4.charges--;
                    }
                    return;
                }
                #endregion
            checkInventory:
                #region Inventory
                for (int a = 0; a < chr.wearing.Count; a++)
                {
                    Item item = (Item)chr.wearing[a];
                    if (item != null && item.spell > 0 && sArgs[0].ToLower() == Spell.GetSpell(item.spell).SpellCommand)
                    {
                        if (item.charges == 0)
                        {
                            outOfCharges += "The " + item.name + " is out of charges. ";
                        }
                        else
                        {
                            Spell storedSpell = chr.preppedSpell;
                            chr.preppedSpell = Spell.GetSpell(item.spell);
                            if (args == null)
                            {
                                args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                            }
                            chr.preppedSpell.CastSpell(chr, args);
                            chr.preppedSpell = storedSpell;
                            if (item.charges > 0)
                            {
                                item.charges--;
                            }
                            chr.wearing[a] = item;
                            return;
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
            #endregion

            #region Knight Spell
            if (chr.BaseProfession == Character.ClassType.Knight) // caster is a knight
            {
                    if (chr.knightRing) // knight is wearing their ring
                    {
                        try
                        {
                            if (chr.IsPC) // npc knights already prepped their spell in Creature.prepareSpell
                            {
                                Spell spell = Spell.GetSpell(sArgs[0].ToLower());

                                if (spell != null && spell.IsClassSpell(chr.BaseProfession))
                                {
                                    chr.preppedSpell = Spell.GetSpell(sArgs[0].ToLower());
                                }
                                else
                                {
                                    if (outOfCharges != "")
                                    {
                                        chr.WriteToDisplay(outOfCharges);
                                    }
                                    else
                                    {
                                        chr.WriteToDisplay("You do not know that spell.");
                                    }
                                    return;
                                }
                            }

                            if (chr.preppedSpell != null)
                            {
                                if (chr.Alignment == Globals.eAlignment.Lawful)
                                {
                                    if (chr.Mana < chr.preppedSpell.ManaCost) // knight does not have enough mana
                                    {
                                        chr.WriteToDisplay("You do not have enough mana to cast the spell."); // message
                                        chr.preppedSpell = null; // remove the prepped spell
                                        return;
                                    }
                                    chr.preppedSpell.CastSpell(chr, args);
                                    chr.Mana -= chr.preppedSpell.ManaCost;
                                    chr.preppedSpell = null;
                                    chr.updateMP = true;
                                }
                                else
                                {
                                    Item ring = null;
                                    for (int rnum = 1; rnum < 5; rnum++)
                                    {
                                        // check right
                                        ring = chr.GetSpecificRing(true, rnum);
                                        if (ring != null && ring.itemID == Item.ID_KNIGHTRING)
                                        {
                                            chr.WriteToDisplay("Your " + ring.identifiedName + " explodes!");
                                            Rules.DoSpellDamage(chr, chr, null, Rules.RollD(2, 12), "concussion");
                                            chr.SetSpecificRing(true, rnum, null);
                                        }
                                        // check left
                                        ring = chr.GetSpecificRing(false, rnum);
                                        if (ring != null && ring.itemID == Item.ID_KNIGHTRING)
                                        {
                                            chr.WriteToDisplay("Your " + ring.identifiedName + " explodes!");
                                            Rules.DoSpellDamage(chr, chr, null, Rules.RollD(2, 12), "concussion");
                                            chr.SetSpecificRing(false, rnum, null);
                                        }
                                    }
                                }
                            }
                            return;
                        }
                        catch (Exception e)
                        {
                            Utils.Log("Command.cast(" + args + ") by " + chr.GetLogString(), Utils.LogType.CommandFailure);
                            Utils.LogException(e);
                            return;
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You are not wearing your knight ring.");
                        return;
                    }
            }
            #endregion

        normalSpell:
            #region Normal Spell
            try
            {
                // cast the prepped spell
                if (chr.preppedSpell == null)
                {
                    if (outOfCharges != "")
                    {
                        chr.WriteToDisplay(outOfCharges);
                        chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't have a spell warmed.");
                    }
                    return;
                }
                if (chr.BaseProfession == Character.ClassType.Thaumaturge && chr.IsPC)
                {
                    if (Rules.CheckSpellFailure(Skills.GetSkillLevel(chr.magic), chr.preppedSpell.RequiredLevel))
                    {
                        chr.Mana -= chr.preppedSpell.ManaCost;
                        chr.preppedSpell = null;
                        chr.WriteToDisplay("The spell fizzles.");
                        return;
                    }
                }
                if (chr.Mana < chr.preppedSpell.ManaCost)
                {
                    chr.preppedSpell = null;
                    chr.WriteToDisplay("The spell fails.");
                    return;
                }
                else
                {
                    if (args == null)
                    {
                        args = chr.preppedSpell.SpellCommand + " " + chr.Name;
                    }

                    if (!chr.IsPC)
                    {
                        ArtificialIntel.NPCCastNormalSpell(chr as NPC, args);
                        return;
                    }

                    // give skill experience for casting a spell that requires mana

                    double magicSkillBonus = 1;

                    switch (chr.BaseProfession)
                    {
                        case Character.ClassType.Thaumaturge:
                            if (chr.Wisdom + chr.TempWisdom == 16) { magicSkillBonus = 1.5; } // magicSkillBonus
                            else if (chr.Wisdom + chr.TempWisdom == 17) { magicSkillBonus = 2.0; }
                            else if (chr.Wisdom + chr.TempWisdom >= 18) { magicSkillBonus = 2.5; }
                            break;
                        case Character.ClassType.Wizard:
                        case Character.ClassType.Thief:
                            if (chr.Intelligence + chr.TempIntelligence == 16) { magicSkillBonus = 1.5; } // magicSkillBonus
                            else if (chr.Intelligence + chr.TempIntelligence == 17) { magicSkillBonus = 2.0; }
                            else if (chr.Intelligence + chr.TempIntelligence >= 18) { magicSkillBonus = 2.5; }
                            break;
                    }

                    int bonusSkillExperience = 0;

                    bonusSkillExperience = (int)((float)chr.trainedMagic * (.02 * magicSkillBonus));
                    chr.trainedMagic -= (int)((float)chr.trainedMagic * .09);
                    if (chr.trainedMagic < 0)
                    {
                        chr.trainedMagic = 0;
                    }

                    Skills.GiveSkillExp(chr, Globals.eSkillType.Magic, chr.preppedSpell.ManaCost * Skills.GetSkillLevel(chr.magic) + bonusSkillExperience); // give exp for casting
                    
                    // give random experience if current map is magic intense
                    if (chr.Map.HasRandomMagicIntensity)
                    {
                        if (Rules.RollD(1, 20) == Rules.RollD(1, 20))
                        {
                            if (chr.BaseProfession == Character.ClassType.Wizard)
                            {
                                chr.WriteToDisplay("Static electricity gathers around you and then dissipates gradually.");
                            }
                            else if (chr.BaseProfession == Character.ClassType.Thaumaturge)
                            {
                                chr.WriteToDisplay("An almost overwhelming feeling of serenity passes through you.");
                            }
                            else if (chr.BaseProfession == Character.ClassType.Thief)
                            {
                                chr.WriteToDisplay("You feel a rush of adrenalin coursing through your body.");
                            }
                            Skills.GiveSkillExp(chr, Globals.eSkillType.Magic, chr.preppedSpell.ManaCost * (Skills.GetSkillLevel(chr.magic) * Rules.RollD(1, 4)));
                        }
                    }
                    chr.preppedSpell.CastSpell(chr, args);
                    if (chr != null) // after the spell is cast the NPC could be dead
                    {
                        chr.Mana -= chr.preppedSpell.ManaCost;
                        chr.preppedSpell = null;
                    }
                }
                if (chr != null) // NPC could be null due to death
                {
                    chr.cmdWeight += 3;
                }
                return;
            }
            catch (Exception e)
            {
                Utils.Log("Command.cast(" + args + ") by " + chr.GetLogString(), Utils.LogType.CommandFailure);
                Utils.LogException(e);
                return;
            }
            #endregion
        }

        public void crawl_x(string args)
        {
            chr.cmdWeight += 2;
            if (chr.cmdWeight > 3)
            {
                return;
            }
            String[] sArgs = args.Split(" ".ToCharArray());
            if (sArgs[0] == null)
            {
                chr.WriteToDisplay("Crawl where?");
                return;
            }

            chr.CommandType = CommandType.Crawl;

            Map.MoveChar(chr, sArgs[0], "crawl");
        }

        public void push_x(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Push what?");
                return;
            }

            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (Item.IsItemOnGround(sArgs[0].ToString().ToLower(), chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z))
            {
                if (sArgs.Length < 2 || sArgs[1] == null)
                {
                    chr.WriteToDisplay("Where do you want to push the " + sArgs[0] + "?");
                    return;
                }

                Item item = Item.GetItemFromGround(sArgs[0].ToString(), chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);

                if (item == null)
                {
                    chr.WriteToDisplay("You can't find the " + sArgs[0] + " here?");
                    return;
                }

                Map.MoveChar(chr, sArgs[1].ToString(), "");

                if (item.attuneType == Globals.eAttuneType.Take)
                {
                    item.AttuneItem(chr);
                }

                chr.CurrentCell.Add(item);
            }
            else
            {
                chr.WriteToDisplay("You don't see a " + sArgs[1] + " here?");
            }
        }

        #region Direction Movement
        public void n(string args)
        {
            Map.MoveChar(chr, "n", args);
        }

        public void north(string args)
        {
            Map.MoveChar(chr, "n", args);
        }

        public void ne(string args)
        {
            Map.MoveChar(chr, "ne", args);
        }

        public void northeast(string args)
        {
            Map.MoveChar(chr, "ne", args);
        }

        public void e(string args)
        {
            Map.MoveChar(chr, "e", args);
        }

        public void east(string args)
        {
            Map.MoveChar(chr, "e", args);
        }

        public void se(string args)
        {
            Map.MoveChar(chr, "se", args);
        }

        public void southeast(string args)
        {
            Map.MoveChar(chr, "se", args);
        }

        public void s(string args)
        {
            Map.MoveChar(chr, "s", args);
        }

        public void south(string args)
        {
            Map.MoveChar(chr, "s", args);
        }

        public void sw(string args)
        {
            Map.MoveChar(chr, "sw", args);
        }

        public void southwest(string args)
        {
            Map.MoveChar(chr, "sw", args);
        }

        public void w(string args)
        {
            Map.MoveChar(chr, "w", args);
        }

        public void west(string args)
        {
            Map.MoveChar(chr, "w", args);
        }

        public void nw(string args)
        {
            Map.MoveChar(chr, "nw", args);
        }

        public void northwest(string args)
        {
            Map.MoveChar(chr, "nw", args);
        } 
        #endregion

        public void swim_x(string args)
        {

            chr.floating = 4;
            if (args != null || args.Length > 0)
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                Map.MoveChar(chr, sArgs[0], "");
            }
            else
            {
                chr.WriteToDisplay("You begin to tread water.");
            }
        }

        public void d_x(string args)
        {
            down_x(args);
        }

        public void down_x(string args)
        {
            chr.cmdWeight += 2;
            if (chr.cmdWeight == 3)
            {
                #region Transfer to new Map on Down
                if (chr.CurrentCell.CellGraphic.Equals("ZZ") || chr.CurrentCell.CellGraphic.Equals("XX"))
                {
                    Segue segue = chr.CurrentCell.Segue;// Segue.GetSegue1(chr.LandID, chr.MapID, chr.X, chr.Y);
                    if (segue != null)
                    {
                        chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                    }
                    else
                    {
                        return;
                    }
                    return;
                }
                #endregion

                #region Transfer Player to Random Map
                if (chr.CurrentCell.CellGraphic.Equals("RD") || chr.CurrentCell.CellGraphic.Equals("RU"))
                {

                    Segue segue = chr.CurrentCell.Segue;// Segue.GetSegue1(chr.CurrentCell.LandID, chr.CurrentCell.MapID, chr.CurrentCell.X, chr.CurrentCell.Y);
                    if (segue != null)
                    {
                        chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                        return;
                    }
                    else
                    {
                     return;
                    }
                }
                #endregion

                if (chr.CurrentCell.CellGraphic == "dn")
                {
                    Map.MoveChar(chr, "down", args);
                }
                else
                {
                    chr.WriteToDisplay("You don't see any stairs here.");
                }
            }

        }

        public void u_x(string args)
        {
            up_x(args);
        }

        public void up_x(string args)
        {
            chr.cmdWeight += 2;
            if (chr.cmdWeight == 3)
            {
                #region Transfer to new map on Up
                if (chr.CurrentCell.CellGraphic.Equals("ZZ") || chr.CurrentCell.CellGraphic.Equals("XX"))
                {

                    Segue segue = chr.CurrentCell.Segue;// Segue.GetSegue1(chr.CurrentCell.LandID, chr.CurrentCell.MapID, chr.CurrentCell.X, chr.CurrentCell.Y);
                    if (segue != null)
                    {
                        chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                    }
                    else
                    {
                        //segue = Segue.GetSegue2(chr.CurrentCell.LandID, chr.CurrentCell.MapID, chr.CurrentCell.X, chr.CurrentCell.Y);
                        //if (segue != null)
                        //{
                        //    chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.Land1, segue.Map1, segue.xCord1, segue.yCord1, segue.zCord1);
                        //}
                        return;
                    }
                    return;
                }
                #endregion
                #region Transfer to Random Map Up
                if (chr.CurrentCell.CellGraphic.Equals("RD") || chr.CurrentCell.CellGraphic.Equals("RU"))
                {

                    Segue segue = chr.CurrentCell.Segue;// Segue.GetSegue1(chr.CurrentCell.LandID, chr.CurrentCell.MapID, chr.CurrentCell.X, chr.CurrentCell.Y);
                    if (segue != null)
                    {
                        chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.LandID, segue.MapID, segue.X, segue.Y, segue.Z);
                    }
                    else
                    {
                        //segue = Segue.GetSegue2(chr.CurrentCell.LandID, chr.CurrentCell.MapID, chr.CurrentCell.X, chr.CurrentCell.Y);
                        //if (segue != null)
                        //{
                        //    chr.CurrentCell = Cell.GetCell(chr.FacetID, segue.Land1, segue.Map1, segue.xCord1, segue.yCord1, segue.zCord1);
                        //}
                        return;
                    }

                    return;
                }
                #endregion
                if (chr.CurrentCell.CellGraphic == "up")
                {
                    Map.MoveChar(chr, "up", args);
                }
                else
                {
                    chr.WriteToDisplay("You don't see any stairs here.");
                }
            }
        }

        public void climb_x(string args)
        {
            chr.cmdWeight += 2;
            if (args == null)
            {
                chr.WriteToDisplay("Climb what?");
            }
            else if(chr.cmdWeight == 3)
            {
                string[] sArgs = args.Split(" ".ToCharArray());
                if (sArgs[0].ToLower() == "up" || sArgs[0].ToLower() == "u")
                {
                    if (chr.CurrentCell.IsOneHandClimbUp)
                    {
                        if (chr.RightHand == null || chr.LeftHand == null)
                        {
                            Map.MoveChar(chr, "climb up", "");
                        }
                        else
                        {
                            chr.WriteToDisplay("You have slipped and fallen!");
                        }
                    }
                    else if (chr.CurrentCell.IsTwoHandClimbUp)
                    {
                        if (chr.RightHand == null && chr.LeftHand == null)
                        {
                            Map.MoveChar(chr, "climb up", "");
                        }
                        else
                        {
                            chr.WriteToDisplay("You have slipped and fallen!");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("There is nothing to climb here.");
                        return;
                    }
                }
                else if (sArgs[0].ToLower() == "down" || sArgs[0].ToLower() == "d")
                {
                    if (chr.CurrentCell.IsOneHandClimbDown)
                    {
                        if (chr.RightHand == null || chr.LeftHand == null)
                        {
                            Map.MoveChar(chr, "climb down", "");
                        }
                        else
                        {
                            chr.WriteToDisplay("You have slipped and fallen!");
                        }
                    }
                    else if (chr.CurrentCell.IsTwoHandClimbDown)
                    {
                        if (chr.RightHand == null && chr.LeftHand == null)
                        {
                            Map.MoveChar(chr, "climb down", "");
                        }
                        else
                        {
                            chr.WriteToDisplay("Both hands must be empty to climb down here.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("There is nothing to climb here.");
                    }
                }
                else
                {
                    chr.WriteToDisplay("You can't climb that!");
                }
            }
        }

        public void commands(string args)
        {
            try
            {
                MethodInfo[] methodInfo = typeof(Command).GetMethods();
                ArrayList commandsList = new ArrayList();
                foreach (MethodInfo m in methodInfo)
                {
                    if (!m.Name.StartsWith("imp"))
                    {
                        if (m.Name.IndexOf("_") != -1)
                        {
                            commandsList.Add(m.Name.Substring(0, m.Name.IndexOf("_")));
                        }
                        else
                        {
                            commandsList.Add(m.Name);
                        }
                    }
                }
                commandsList.Remove("Equals");
                commandsList.Remove("GetHashCode");
                commandsList.Remove("GetType");
                commandsList.Remove("ToString");
                commandsList.Remove("interpretCommand");
                commandsList.Remove("parseCommand");
                commandsList.Sort();

                string listing = "";
                for (int a = 0; a < commandsList.Count; a++)
                {
                    listing += (string)commandsList[a] + ", ";
                }
                listing = listing.Substring(0, listing.Length - 2);

                if (chr.PCState == Globals.ePlayerState.PLAYING)
                {
                    chr.WriteToDisplay(listing);
                }
                else if (chr.PCState == Globals.ePlayerState.CONFERENCE)
                {
                    chr.WriteLine("", Protocol.TextType.Help);
                    chr.WriteLine("Game Commands", Protocol.TextType.Help);
                    chr.WriteLine("-------------", Protocol.TextType.Help);
                    chr.WriteLine(listing, Protocol.TextType.Help);
                    chr.WriteLine("", Protocol.TextType.Help);
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void drop(string args)
        {
            Item coins = null;

            if (args == null || args == "")
            {
                chr.WriteToDisplay("Drop what?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            int ItemLoc = chr.inHand(args.ToLower());

            if (sArgs.Length < 2)
            {
                // if the item is in the player's hand
                if (ItemLoc == 1 || (sArgs[0] == "right" && chr.RightHand != null)) // Trap "Drop Right"
                {
                    chr.CurrentCell.Add(chr.RightHand);
                    chr.UnEquipRightHand(chr.RightHand);
                }
                else if (ItemLoc == 2 || (sArgs[0] == "left" && chr.LeftHand != null)) // Trap "Drop Left"
                {
                    chr.CurrentCell.Add(chr.LeftHand);
                    chr.UnEquipLeftHand(chr.LeftHand);
                }
                else
                {
                    chr.WriteToDisplay("You are not carrying that.");
                }
            }
            else if (sArgs.Length == 2)
            {
                // drop # <item>
                if (sArgs[1] == "coins" || sArgs[1] == "coin")
                {
                    if (Convert.ToInt32(sArgs[0]) < 1)
                    {
                        Command.ParseCommand(chr, "drop", "coins");
                        return;
                    }

                    if (chr.RightHand != null && chr.RightHand.itemType == Globals.eItemType.Coin)
                    {
                        if (chr.RightHand.coinValue == Convert.ToInt32(sArgs[0]))
                        {
                            Command.ParseCommand(chr, "drop", "coins");
                            return;
                        }
                        else if (chr.RightHand.coinValue > Convert.ToInt32(sArgs[0]))
                        {
                            coins = Item.CopyItemFromDictionary(Item.ID_COINS);
                            chr.RightHand.coinValue -= Convert.ToInt32(sArgs[0]);
                            coins.coinValue = Convert.ToInt32(sArgs[0]);
                            chr.CurrentCell.Add(coins);
                            return;
                        }
                        else
                        {
                            chr.WriteToDisplay("You are not holding that many coins.");
                            return;
                        }
                    }
                    else if (chr.LeftHand != null && chr.LeftHand.itemType == Globals.eItemType.Coin)
                    {
                        if (chr.LeftHand.coinValue == Convert.ToInt32(sArgs[0]))
                        {
                            Command.ParseCommand(chr, "drop", "coins");
                            return;
                        }
                        else if (chr.LeftHand.coinValue > Convert.ToInt32(sArgs[0]))
                        {
                            coins = Item.CopyItemFromDictionary(Item.ID_COINS);
                            chr.LeftHand.coinValue -= Convert.ToInt32(sArgs[0]);
                            coins.coinValue = Convert.ToInt32(sArgs[0]);
                            chr.CurrentCell.Add(coins);
                            return;
                        }
                        else
                        {
                            chr.WriteToDisplay("You are not holding that many coins.");
                            return;
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You are not holding any coins.");
                        return;
                    }
                }
                else
                {
                    chr.WriteToDisplay("You are not carrying that many " + sArgs[1] + ".");
                    return;
                }
            }
            else if (sArgs.Length > 2)
            {
                put(args);
            }
        }

        #region Demon names
        // Asmodeus, Pazuzu, Glamdrang, Damballa, Thamuz, Perdurabo, Samael
        public void asmodeus(string args)
        {
            NPC.ChantDemonSummon(NPC.DemonName.Asmodeus, chr);

        }
        public void pazuzu(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Pazuzu, chr);

        }
        public void glamdrang(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Glamdrang, chr);

        }
        public void damballa(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Damballa, chr);

        }
        public void thamuz(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Thamuz, chr);

        }
        public void perdurabo(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Perdurabo, chr);

        }
        public void samael(string args)
        {
            chr.SendToAllInSight(chr.Name + ": alsi ku nushi ilani");
            NPC.ChantDemonSummon(NPC.DemonName.Samael, chr);

        }
        #endregion

        public void who(string args)
        {
            int count = 0;
            string nameOfMap = "";
            string nameOfClass = "";
            bool isAnonymous = false;
            string anonymous = "";
            bool isVisible = true;
            string visibility = "";

            if (args == null || args.Length == 0)
            {
                nameOfMap = chr.Map.Name;
                nameOfClass = "";
                foreach (Character ch in Character.pcList)
                {
                    isAnonymous = ch.IsAnonymous;
                    isVisible = !ch.IsInvisible;
                    if (chr.ImpLevel > ch.ImpLevel)
                    {
                        isAnonymous = false;
                        isVisible = true;
                        if (ch.IsAnonymous) { anonymous = "[ANON]"; }
                        else { anonymous = ""; }
                        if (ch.ImpLevel >= Globals.eImpLevel.GM) { anonymous = Conference.GetStaffTitle(ch); }
                        if (ch.IsInvisible) { visibility = "[INVIS]"; }
                        else { visibility = ""; }
                    }

                    if (ch.IsPC && !isAnonymous && ch.MapID == chr.MapID && isVisible)
                    {
                        string final = Conference.GetStaffTitle(ch) + ch.Name;
                        final = final.PadRight(18, ' ');
                        final += " [" +ch.Level + "] " + ch.classFullName;
                        final = final.PadRight(40, ' ');
                        if (ch.IsDead && !isAnonymous)
                        {
                            final += " (dead)";
                        }
                        final += " " + anonymous + visibility;
                        chr.WriteToDisplay(final);
                        count++;
                    }
                }
                if (count > 1 || count < 1)
                {
                    chr.WriteToDisplay("There are " + count.ToString() + " players in " + chr.Facet.Name + " " + nameOfMap + ".");
                }
                else
                {
                    chr.WriteToDisplay("There is one player in " + chr.Facet.Name + " " + nameOfMap + ".");
                }
                chr.WriteToDisplay("Type WHO ALL for more information.");
            }
            else if (args == "all")
            {
                foreach (Character ch in Character.pcList)
                {
                    nameOfMap = World.GetFacetByID(ch.FacetID).Name + " " + World.GetFacetByID(ch.FacetID).GetLandByID(ch.LandID).GetMapByID(ch.MapID).Name;
                    isAnonymous = ch.IsAnonymous;
                    isVisible = !ch.IsInvisible;
                    if (chr.ImpLevel >= Globals.eImpLevel.GM)
                    {
                        isAnonymous = false;
                        isVisible = true;
                        if (ch.IsAnonymous) { anonymous = "[ANON]"; }
                        else { anonymous = ""; }
                        if (ch.ImpLevel > Globals.eImpLevel.USER) { anonymous = Conference.GetStaffTitle(ch); }
                        if (ch.IsInvisible) { visibility = "[INVIS]"; }
                        else { visibility = ""; }
                    }
                    if (isAnonymous)
                    {
                        nameOfMap = "ANONYMOUS";
                        nameOfClass = "ANONYMOUS";
                    }
                    else
                    {
                        nameOfClass = "[" + ch.Level + "] " + ch.classFullName;
                    }

                    if (ch.IsPC && isVisible)
                    {
                        string final = Conference.GetStaffTitle(ch) + ch.Name;
                        final = final.PadRight(18, ' ');
                        final += nameOfClass;
                        final = final.PadRight(40, ' ');
                        final += nameOfMap;
                        final = final.PadRight(65, ' ');
                        if(ch.IsDead && !isAnonymous)
                        {
                            final += " (dead)";
                        }
                        final += " " + anonymous + visibility;
                        chr.WriteToDisplay(final);
                        count++;
                    }
                }
                if (count > 1 || count < 1)
                {
                    chr.WriteToDisplay("There are " + count.ToString() + " players in all of the lands.");
                }
                else
                {
                    chr.WriteToDisplay("There is 1 player in the lands.");
                }

            }
        }

        public void sheathe(string args)
        {
            belt(args);
        }

        public void belt(string args)
        {
            //Check if they passed any args
            if (args == null)
            {
                chr.WriteToDisplay("Belt what?");
            }
            // check if belt is full - only 2 slots
            else
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                Item pItem;

                //Did they ask to belt "left" or "right"? If so, check if hand is holding something.
                //If it is, change the string to the actual item name so we can use object helper functions.
                if (sArgs[0] == "right" || sArgs[0] == "left")
                {
                    if (sArgs[0] == "left" && chr.LeftHand != null)
                            sArgs[0] = chr.LeftHand.name;
                    else if (sArgs[0] == "right" && chr.RightHand != null)
                            sArgs[0] = chr.RightHand.name;
                }

                pItem = chr.getItemFromHand(sArgs[0]);  //copy item from the players hand to check if legal.

                if (pItem == null)
                {
                    if (sArgs[0] == "left" || sArgs[0] == "right") //the hand was empty.
                    {
                        chr.WriteToDisplay("You have nothing in your " + sArgs[0] + " hand to belt.");
                    }
                    else
                    {
                        chr.WriteToDisplay("You are not holding a " + sArgs[0] + ".");
                    }
                }
                else if (pItem.size == Globals.eItemSize.Sack_Only || pItem.size == Globals.eItemSize.No_Container)
                {
                    if (pItem.baseType >= Globals.eItemBaseType.Bow && pItem.baseType < Globals.eItemBaseType.Thievery)
                    {
                        chr.WriteToDisplay("You cannot belt the " + pItem.name + ". Try to wear it on your back.");
                    }
                    else { chr.WriteToDisplay("You cannot belt " + pItem.shortDesc + "."); }
                }
                else if (chr.beltList.Count >= Character.MAX_BELT)
                {
                    chr.WriteToDisplay("Your belt is full.");
                }
                else
                {
                    //Now that it's all legit, actually take the item out of their hands and belt it.
                    int handLoc = chr.inHand(sArgs[0]);
                    pItem = chr.getFromHand(sArgs[0]);
                    bool beltOk = true;
                    if (pItem.size == Globals.eItemSize.Belt_Large_Slot_Only)
                    {
                        foreach (Item bItem in chr.beltList)
                        {
                            if (bItem.size == Globals.eItemSize.Belt_Large_Slot_Only)
                            {
                                chr.WriteToDisplay("You already have one large item on your belt.");
                                if (handLoc == 1)
                                {
                                    chr.RightHand = pItem;
                                }
                                else if (handLoc == 2)
                                {
                                    chr.LeftHand = pItem;
                                }
                                beltOk = false;
                                break;
                            }
                        }
                    }
                    if (beltOk)
                    {
                        if (pItem.name != "crossbow") { pItem.nocked = false; }
                        chr.BeltItem(pItem);
                    }
                }
            }
        }

        public void wield(string args)
        {
            draw(args);
        }

        public void draw(string args)
        {
            try
            {
                //Check if they passed any args
                if (args == null)
                {
                    chr.WriteToDisplay("Draw what?");
                }
                if (chr.RightHand != null && chr.LeftHand != null)
                {
                    chr.WriteToDisplay("Your hands are full.");
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());

                    Item tItem = chr.RemoveFromBelt(sArgs[0]);

                    if (tItem != null)
                    {
                        if (chr.RightHand == null)
                        {
                            chr.EquipRightHand(tItem);
                        }
                        else if (chr.LeftHand == null)
                        {
                            chr.EquipLeftHand(tItem);
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't have a " + sArgs[0] + " on your belt.");
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }

        }

        public void put(string args)
        {
            args = args.ToLower();
            string putTarget = null;
            string putItem = null;
            string putLocation = null;
            long putAmount = 0;		//This is mostly for coin
            string putType = "";

            //Check if they passed any args
            if (args == null)
            {
                chr.WriteToDisplay("Put what where?");
            }
            else
            {
                try
                {
                    string[] sArgs = args.Split(" ".ToCharArray());

                    //We need putItem, putTarget, putType, and putAmount if it exists

                    switch (sArgs.Length)
                    {
                        case 0:
                            break;
                        case 1:
                            chr.WriteToDisplay("Put " + sArgs[0] + " where?");
                            break;
                        case 2:
                            chr.WriteToDisplay("Do what?");
                            break;
                        case 3:
                            if (sArgs[0] == "left") // trap "put left/right in sack/locker"
                            {
                                if (chr.LeftHand == null)
                                {
                                    chr.WriteToDisplay("Your left hand is empty.");
                                    return;
                                }
                                else { putItem = chr.LeftHand.name; }
                            }
                            else if (sArgs[0] == "right")
                            {
                                if (chr.RightHand == null)
                                {
                                    chr.WriteToDisplay("Your right hand is empty.");
                                    return;
                                }
                                else { putItem = chr.RightHand.name; }
                            }
                            else { putItem = sArgs[0]; }
                            putType = sArgs[1];
                            putTarget = sArgs[2];
                            break;
                        case 4:
                            if (sArgs[0] == "ring")
                            {
                                putItem = sArgs[0];
                                putType = sArgs[1];
                                putLocation = sArgs[2];
                                putTarget = sArgs[3];
                            }
                            else if (sArgs[1].Contains("coin"))
                            {
                                putAmount = Convert.ToInt64(sArgs[0], 10);
                                putItem = sArgs[1];
                                putType = sArgs[2];
                                putTarget = sArgs[3];
                            }
                            else // put bracelet on left wrist / put band on left arm
                            {
                                putItem = sArgs[0]; // bracelet
                                putType = sArgs[1]; // on
                                putLocation = sArgs[2]; // left
                                putTarget = sArgs[3]; // wrist
                            }
                            break;
                        default:
                            chr.WriteToDisplay("Do what?");
                            break;
                    }
                    //0 = no argument
                    //1 = put IN
                    //2 = put ON
                    //3 = put UNDER
                    switch (putType.ToLower())
                    {
                        case "":
                            chr.WriteToDisplay("Put what where?");
                            break;
                        case "in":
                            #region "in"
                            int hand = chr.inHand(putItem);
                            if (hand == 0)
                            {
                                chr.WriteToDisplay("You aren't holding a " + putItem + ".");
                            }
                            else if (putTarget.ToLower() == "sack")
                            {
                                if (hand == 1)
                                {
                                    if (putItem == "coins")
                                    {
                                        foreach (Item itm in chr.sackList)
                                        {
                                            if (itm.name == "coins")
                                            {
                                                itm.coinValue += chr.RightHand.coinValue;
                                                chr.UnEquipRightHand(itm);
                                            }
                                        }
                                    }
                                    if (chr.RightHand != null)
                                    {
                                        if (chr.RightHand.size == Globals.eItemSize.Sack_Only || chr.RightHand.size == Globals.eItemSize.Belt_Or_Sack)
                                        {
                                            if (chr.SackCountMinusGold < Character.MAX_SACK || chr.RightHand.itemType == Globals.eItemType.Coin)
                                            {
                                                chr.SackItem(chr.RightHand);
                                                chr.UnEquipRightHand(chr.RightHand);
                                            }
                                            else
                                            {
                                                chr.WriteToDisplay("Your sack is full.");
                                            }
                                        }
                                        else { chr.WriteToDisplay("The " + chr.RightHand.name + " won't fit in your sack."); }
                                    }
                                }
                                else if (hand == 2)
                                {
                                    if (putItem == "coins")
                                    {
                                        foreach (Item itm in chr.sackList)
                                        {
                                            if (itm.name == "coins")
                                            {
                                                itm.coinValue += chr.LeftHand.coinValue;
                                                chr.UnEquipLeftHand(chr.LeftHand);
                                            }
                                        }
                                    }
                                    if (chr.LeftHand != null)
                                    {
                                        if (chr.LeftHand.size == Globals.eItemSize.Sack_Only || chr.LeftHand.size == Globals.eItemSize.Belt_Or_Sack)
                                        {
                                            if (chr.SackCountMinusGold < Character.MAX_SACK || chr.LeftHand.itemType == Globals.eItemType.Coin)
                                            {
                                                chr.SackItem(chr.LeftHand);
                                                chr.UnEquipLeftHand(chr.LeftHand);
                                            }
                                            else
                                            {
                                                chr.WriteToDisplay("Your sack is full.");
                                            }
                                        }
                                        else { chr.WriteToDisplay("The " + chr.LeftHand.name + " won't fit in your sack."); }
                                    }
                                }
                                return;
                            }
                            else if (chr.CurrentCell.IsLocker && putTarget.ToLower() == "locker")
                            {
                                if (putItem == "coins" || putItem == "coin")
                                {
                                    chr.WriteToDisplay("Coins must be deposited in the bank.");
                                    return;
                                }
                                if (putItem == "corpse")
                                {
                                    chr.WriteToDisplay("You notice a sign on the wall, 'Please do not store dead bodies in your locker. Thanks, +janitor'");
                                    return;
                                }
                                if (hand == 1)
                                {
                                    if (chr.lockerList.Count >= Character.MAX_LOCKER)
                                    {
                                        chr.WriteToDisplay("Your locker is full.");
                                    }
                                    else
                                    {
                                        chr.lockerList.Add(chr.RightHand);
                                        chr.UnEquipRightHand(chr.RightHand);
                                    }
                                }
                                else if (hand == 2)
                                {
                                    if (chr.lockerList.Count >= 20)
                                    {
                                        chr.WriteToDisplay("Your locker is full.");
                                    }
                                    else
                                    {
                                        chr.lockerList.Add(chr.LeftHand);
                                        chr.UnEquipLeftHand(chr.LeftHand);
                                    }
                                }
                                return;
                            }
                            else
                            {
                                chr.WriteToDisplay("You don't see a " + putTarget + " here.");
                            }
                            break; 
                            #endregion
                        case "on":
                            if (putTarget == "counter" || putTarget == "altar" || putTarget == "alter")
                            {
                                if (Map.nearCounter(chr))
                                {
                                    //If we are dealing with money, take care of it
                                    if (putAmount > 0 && (putItem == "coins" || putItem == "coin"))
                                    {
                                        hand = chr.inHand("coins");
                                        if (hand == 0)
                                        {
                                            chr.WriteToDisplay("You are not holding any coins.");
                                        }
                                        else
                                        {
                                            if (chr.RightHand.coinValue >= putAmount)
                                            {
                                                Item coins = Item.CopyItemFromDictionary(Item.ID_COINS);
                                                coins.coinValue = putAmount;
                                                chr.RightHand.coinValue -= putAmount;
                                                if (chr.RightHand.coinValue == 0)
                                                {
                                                    chr.UnEquipRightHand(chr.RightHand);
                                                }
                                                Map.PutItemOnCounter(chr, coins);
                                                return;
                                            }
                                            else if (chr.LeftHand.coinValue >= putAmount)
                                            {
                                                Item coins = Item.CopyItemFromDictionary(Item.ID_COINS);
                                                coins.coinValue = putAmount;
                                                chr.LeftHand.coinValue -= putAmount;
                                                if (chr.LeftHand.coinValue == 0)
                                                {
                                                    chr.UnEquipLeftHand(chr.LeftHand);
                                                }
                                                Map.PutItemOnCounter(chr, coins);
                                                return;
                                            }
                                            else
                                            {
                                                chr.WriteToDisplay("You don't have " + putAmount + " coins.");
                                                return;
                                            }
                                        }
                                    }

                                    //else, we are dealing with items
                                    hand = chr.inHand(putItem);
                                    if (hand == 0)
                                    {
                                        chr.WriteToDisplay("You are not holding a " + putItem + ".");
                                        return;
                                    }
                                    else
                                    {
                                        if (hand == 1)
                                        {
                                            Map.PutItemOnCounter(chr, chr.RightHand);
                                            chr.UnEquipRightHand(chr.RightHand);
                                        }
                                        else if (hand == 2)
                                        {
                                            Map.PutItemOnCounter(chr, chr.LeftHand);
                                            chr.UnEquipLeftHand(chr.LeftHand);
                                        }
                                        return;
                                    }
                                }
                                chr.WriteToDisplay("You are not near a " + putTarget + ".");
                                return;
                            }
                            #region putTarget "right" or "left"
                            if (putTarget == "right" || putTarget == "left")
                            {
                                hand = chr.inHand(putItem);
                                if (hand == 0)
                                {
                                    chr.WriteToDisplay("You are not holding a " + putItem + ".");
                                    return;
                                }
                                if (hand == 1)
                                {
                                    if (putTarget == "right") { chr.WriteToDisplay("You must have the ring in your left hand."); return; }
                                    Item tItem;
                                    tItem = chr.RightHand;

                                    switch (putLocation) // check which finger and make sure its empty
                                    {
                                        case "1":
                                            if (chr.LeftRing1 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipRightHand(chr.RightHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); } // attune the ring
                                            chr.LeftRing1 = tItem;
                                            chr.RightHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                //apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.LeftRing1);
                                            }
                                            //currently no message is given if the recall does not set
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "2":
                                            if (chr.LeftRing2 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipRightHand(chr.RightHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); } // attune item
                                            chr.LeftRing2 = tItem;
                                            chr.RightHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.LeftRing2);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "3":
                                            if (chr.LeftRing3 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipRightHand(chr.RightHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.LeftRing3 = tItem;
                                            chr.RightHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.LeftRing3);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "4":
                                            if (chr.LeftRing4 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipRightHand(chr.RightHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.LeftRing4 = tItem;
                                            chr.RightHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.LeftRing4);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        default:
                                            chr.WriteToDisplay("You cannot put a ring there.");
                                            break;
                                    }

                                    return;
                                }
                                if (hand == 2)
                                {
                                    if (putTarget == "left") { chr.WriteToDisplay("You must have the ring in your right hand."); return; }
                                    Item tItem = chr.LeftHand;
                                    switch (putLocation) // check which finger and make sure its empty
                                    {
                                        case "1":
                                            if (chr.RightRing1 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipLeftHand(chr.LeftHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.RightRing1 = tItem;
                                            chr.LeftHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.RightRing1);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "2":
                                            if (chr.RightRing2 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipLeftHand(chr.LeftHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.RightRing2 = tItem;
                                            chr.LeftHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.RightRing2);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "3":
                                            if (chr.RightRing3 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipLeftHand(chr.LeftHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.RightRing3 = tItem;
                                            chr.LeftHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.RightRing3);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        case "4":
                                            if (chr.RightRing4 != null)
                                            {
                                                chr.WriteToDisplay("You already have a ring there.");
                                                return;
                                            }
                                            if (tItem.isRecall && chr.CurrentCell.IsNoRecall)
                                            {
                                                chr.WriteToDisplay("A powerful force prevents you from using recall magic!");
                                                return;
                                            }
                                            // Check if the ring is attuned
                                            if (tItem.IsAttunedToOther(chr))
                                            {
                                                if (tItem.itemID == Item.ID_KNIGHTRING) //what happens if it's a knight ring
                                                {
                                                    chr.WriteToDisplay("The ring explodes!");
                                                    Rules.DoSpellDamage(null, chr, null, Rules.dice.Next(1, 20), "concussion");
                                                    chr.UnEquipLeftHand(chr.LeftHand);
                                                    return;
                                                }
                                                else
                                                {
                                                    chr.WriteToDisplay("The ring is soulbound to another individual.");
                                                    return;
                                                }
                                            }
                                            // Check if the ring has alignment
                                            if (!tItem.AlignmentCheck(chr))
                                            {
                                                chr.WriteToDisplay("The ring singes your finger and will not remain in place.");
                                                Rules.DoDamage(chr, chr, Rules.RollD(1, 4), false);
                                                return;
                                            }
                                            if (tItem.attuneType == Globals.eAttuneType.Wear) { tItem.AttuneItem(chr); }
                                            chr.RightRing4 = tItem;
                                            chr.LeftHand = null;
                                            if (tItem.effectType.Length > 0)
                                            {
                                                // apply effects
                                                Effect.AddWornEffectToCharacter(chr, chr.RightRing4);
                                            }
                                            if (tItem.isRecall)
                                            {
                                                Item.SetRecallValues(tItem, chr);
                                            }
                                            break;
                                        default:
                                            chr.WriteToDisplay("You cannot put a ring there.");
                                            break;
                                    }
                                    return;
                                }

                            } 
                            #endregion
                            else
                            {
                                hand = chr.inHand(putItem);
                                Item item = null;

                                if (hand == 0)
                                {
                                    chr.WriteToDisplay("You are not holding a " + putItem + ".");
                                    return;
                                }

                                switch (putTarget.ToLower())
                                {
                                    case "wrist":
                                        foreach (Item wItem in chr.wearing)
                                        {
                                            if (wItem.wearLocation == Globals.eWearLocation.Wrist)
                                            {
                                                if (wItem.wearOrientation == Globals.eWearOrientation.Left && putLocation.ToLower() == "left")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your left " + putTarget +".");
                                                    return;
                                                }
                                                else if (wItem.wearOrientation == Globals.eWearOrientation.Right && putLocation.ToLower() == "right")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your right " + putTarget + ".");
                                                    return;
                                                }
                                            }
                                        }

                                        if (hand == 1)
                                            item = chr.RightHand;
                                        else item = chr.LeftHand;

                                        item.wearOrientation = (Globals.eWearOrientation)Enum.Parse(typeof(Globals.eWearOrientation), putLocation, true);
                                        chr.WearItem(item);
                                        break;
                                    case "bicep":
                                        foreach (Item wItem in chr.wearing)
                                        {
                                            if (wItem.wearLocation == Globals.eWearLocation.Bicep)
                                            {
                                                if (wItem.wearOrientation == Globals.eWearOrientation.Left && putLocation.ToLower() == "left")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your left " + putTarget + ".");
                                                    return;
                                                }
                                                else if (wItem.wearOrientation == Globals.eWearOrientation.Right && putLocation.ToLower() == "right")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your right " + putTarget + ".");
                                                    return;
                                                }
                                            }
                                        }

                                        if (hand == 1)
                                            item = chr.RightHand;
                                        else item = chr.LeftHand;

                                        item.wearOrientation = (Globals.eWearOrientation)Enum.Parse(typeof(Globals.eWearOrientation), putLocation, true);
                                        chr.WearItem(item);
                                        break;
                                    case "ear":
                                        foreach (Item wItem in chr.wearing)
                                        {
                                            if (wItem.wearLocation == Globals.eWearLocation.Ear)
                                            {
                                                if (wItem.wearOrientation == Globals.eWearOrientation.Left && putLocation.ToLower() == "left")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your left " + putTarget + ".");
                                                    return;
                                                }
                                                else if (wItem.wearOrientation == Globals.eWearOrientation.Right && putLocation.ToLower() == "right")
                                                {
                                                    chr.WriteToDisplay("You are already wearing a " + putItem + " on your right " + putTarget + ".");
                                                    return;
                                                }
                                            }
                                        }

                                        if (hand == 1)
                                            item = chr.RightHand;
                                        else item = chr.LeftHand;

                                        item.wearOrientation = (Globals.eWearOrientation)Enum.Parse(typeof(Globals.eWearOrientation), putLocation, true);
                                        chr.WearItem(item);
                                        break;
                                    default:
                                        chr.WriteToDisplay("You can't put a " + putItem + " on a " + putTarget + ".");
                                        break;
                                }
                            }
                            break;
                        case "under":
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    Utils.Log("Failure at Command.put( " + args + " ) by " + chr.GetLogString(), Utils.LogType.SystemFailure);
                    Utils.LogException(e);
                }
            }
        }

        public void dump_x(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            args = args.Replace(" all", "");
            String[] sArgs = args.Split(" ".ToCharArray());
            int classUBound = chr.sackList.Count;

            try
            {
                if (sArgs.Length < 2)  // "Dump <item>" should just dump on ground
                {
                    foreach (Item item in new List<Item>(chr.sackList))
                    {
                        if (item.name == sArgs[0].Remove(sArgs[0].Length - 1, 1) || item.name == sArgs[0])
                        {
                            chr.CurrentCell.Add(item);
                            chr.sackList.Remove(item);
                        }
                    }
                    //for (int x = classUBound - 1; x >= 0; x--)
                    //{
                    //    Item item = (Item)chr.sackList[x];
                    //    if (item.name == sArgs[0].Remove(sArgs[0].Length - 1, 1) || item.name == sArgs[0])
                    //    {
                    //        chr.CurrentCell.Add(item);
                    //        chr.sackList.RemoveAt(x);
                    //    }

                    //}
                    return;
                }
                switch (sArgs[1])  // "Dump <item> <prep> <location> Right now, only counters and ground allowed.
                {
                    case "on":
                    case "in":
                        for (int x = classUBound - 1; x >= 0; x--)
                        {
                            Item item = (Item)chr.sackList[x];
                            if (item.name == sArgs[0].Remove(sArgs[0].Length - 1, 1) || item.name == sArgs[0])
                            {
                                if (Map.nearCounter(chr))
                                {
                                    Map.PutItemOnCounter(chr, item);
                                    chr.sackList.RemoveAt(x);
                                }
                                else
                                {
                                    chr.CurrentCell.Add(item);
                                    chr.sackList.RemoveAt(x);
                                }
                            }

                        }
                        break;
                    default:
                        for (int x = classUBound - 1; x >= 0; x--)
                        {
                            Item item = (Item)chr.sackList[x];
                            if (item.name == sArgs[0].Remove(sArgs[0].Length - 1, 1) || item.name == sArgs[0])
                            {
                                chr.CurrentCell.Add(item);
                                chr.sackList.RemoveAt(x);
                            }

                        }
                        break;
                }
                return;
            }
            catch (Exception e)
            {
                Utils.Log("Command.dump(" + args + ") by " + chr.GetLogString(), Utils.LogType.CommandFailure);
                Utils.LogException(e);
            }
        }

        public void wear(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Wear what?");
                return;
            }

            bool canWear = true;

            Item tmpItem = chr.getItemFromHand(args);

            if (tmpItem == null)
            {
                chr.WriteToDisplay("You are not holding a " + args + ".");
                return;
            }

            if (tmpItem.name == "ring")
            {
                chr.WriteToDisplay("Use \"put ring on # <left | right>\".");
                return;
            }

            if (tmpItem.wearLocation == Globals.eWearLocation.None)
            {
                chr.WriteToDisplay("You cannot wear "+tmpItem.shortDesc+".");
                return;
            }

            if (tmpItem.attunedID > 0 && tmpItem.attunedID != chr.PlayerID)
            {
                return;
            }

            string[] wearLocs = Enum.GetNames(typeof(Globals.eWearLocation));
            for (int a = 0; a < wearLocs.Length; a++)
            {
                if (wearLocs[a] == tmpItem.wearLocation.ToString())
                {
                    if (Globals.Max_Wearable.Length >= a + 1)
                    {
                        if (Globals.Max_Wearable[a] > 1)
                        {
                            chr.WriteToDisplay("Use \"put " + tmpItem.name + " on <left | right> " + tmpItem.wearLocation.ToString().ToLower() + "\".");
                            return;
                        }
                    }
                }
            }

            foreach (Item item in chr.wearing)
            {
                if (item.wearLocation == tmpItem.wearLocation)
                {
                    canWear = false;
                    break;
                }
            }

            if (canWear)
                chr.WearItem(tmpItem);
            else
                chr.WriteToDisplay("You are already wearing something there.");
        }

        public void show(string args)
        {
            try
            {
                int i = 0;
                int z = 0;
                double itemcount = 0;
                bool moreThanOne = false;
                string dispMsg = "";

                if (args == null || args.Equals("help"))
                {
                    chr.WriteToDisplay("Show Commands:");
                    chr.WriteToDisplay("show belt");
                    chr.WriteToDisplay("show locker");
                    chr.WriteToDisplay("show stats");
                    chr.WriteToDisplay("show skills");
                    chr.WriteToDisplay("show inventory");
                    chr.WriteToDisplay("show rings");
                    chr.WriteToDisplay("show resists");
                    chr.WriteToDisplay("show protection");
                    chr.WriteToDisplay("show effects");
                    chr.WriteToDisplay("show ac");
                    chr.WriteToDisplay("show training");
                }

                #region Show Belt
                else if (args.ToLower().Equals("belt"))
                {
                    foreach (Item item in chr.beltList)
                    {
                        chr.WriteToDisplay(item.name + " ");
                    }
                    foreach (Item item in chr.wearing)
                    {
                        if (item.wearLocation == Globals.eWearLocation.Head) { dispMsg += "On your head you are wearing " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Neck) { dispMsg += "Around your neck is " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Shoulders) { dispMsg += "Draped over your shoulders is " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Forearms) { dispMsg += "On your arms you are wearing " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Torso) { dispMsg += "Your torso is covered by " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Legs) { dispMsg += "On your legs are " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Back)
                        {
                            if (item.baseType == Globals.eItemBaseType.Armor) { dispMsg += "On your back is " + item.longDesc + ". "; itemcount++; }
                            else { dispMsg += "Strapped to your back is " + item.longDesc + ". "; itemcount++; }
                        }
                        if (item.wearLocation == Globals.eWearLocation.Feet) { dispMsg += "On your feet you are wearing " + item.longDesc + ". "; itemcount++; }
                        if (item.wearLocation == Globals.eWearLocation.Hands) { dispMsg += "On your hands you are wearing " + item.longDesc + ". "; itemcount++; }
                    }
                    if (itemcount > 0)
                    {
                        chr.WriteToDisplay(dispMsg);
                    }
                    else
                    {
                        chr.WriteToDisplay("You are wearing no armor.");
                    }
                    chr.WriteToDisplay("Use \"show inventory\" for a more detailed list of worn items.");

                }
                #endregion

                #region Show Sack
                else if (args.Equals("sack"))
                {
                    //List<Item> tempitemlist = chr.sackList;
                    if (chr.sackList.Count > 0)
                    {
                        ArrayList templist = new ArrayList();
                        Item[] itemList = new Item[chr.sackList.Count];
                        chr.sackList.CopyTo(itemList);
                        foreach (Item item in itemList)
                        {
                            templist.Add(item);
                        }
                        z = templist.Count - 1;
                        dispMsg = "In your sack you see ";
                        while (z >= 0)
                        {

                            Item item = (Item)templist[z];

                            itemcount = 0;
                            for (i = templist.Count - 1; i > -1; i--)
                            {
                                Item tmpitem = (Item)templist[i];
                                if (tmpitem.name == item.name && tmpitem.name.IndexOf("coin") > -1)
                                {
                                    templist.RemoveAt(i);
                                    itemcount = itemcount + (int)item.coinValue;
                                    z = templist.Count;

                                }
                                else if (tmpitem.name == item.name)
                                {
                                    templist.RemoveAt(i);
                                    z = templist.Count;
                                    itemcount += 1;
                                }

                            }
                            if (itemcount > 0)
                            {
                                if (moreThanOne)
                                {
                                    if (z == 0)
                                    {
                                        dispMsg += " and ";
                                    }
                                    else
                                    {
                                        dispMsg += ", ";
                                    }
                                }
                                dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);

                            }
                            moreThanOne = true;
                            z--;
                        }
                        dispMsg += ".";
                        chr.WriteToDisplay(dispMsg);
                    }
                    else
                    {
                        chr.WriteToDisplay("Your sack is empty.");
                    }

                }
                #endregion

                #region Show Locker
                else if (args.Equals("locker"))
                {
                    if (chr.CurrentCell.IsLocker)
                    {
                        if (chr.lockerList.Count > 0)
                        {
                            ArrayList templist = new ArrayList();
                            Item[] itemList = new Item[chr.lockerList.Count];
                            chr.lockerList.CopyTo(itemList);
                            foreach (Item item in itemList)
                            {
                                templist.Add(item);
                            }
                            z = templist.Count - 1;
                            dispMsg = "In your locker you see ";
                            while (z >= 0)
                            {

                                Item item = (Item)templist[z];

                                itemcount = 0;
                                for (i = templist.Count - 1; i > -1; i--)
                                {
                                    Item tmpitem = (Item)templist[i];
                                    if (tmpitem.name == item.name && tmpitem.name == "coins")
                                    {
                                        templist.RemoveAt(i);
                                        itemcount = itemcount + (double)item.coinValue;
                                        z = templist.Count;

                                    }
                                    else if (tmpitem.name == item.name)
                                    {
                                        templist.RemoveAt(i);
                                        z = templist.Count;
                                        itemcount += 1;
                                    }

                                }
                                if (itemcount > 0)
                                {
                                    if (moreThanOne)
                                    {
                                        if (z == 0)
                                        {
                                            dispMsg += " and ";
                                        }
                                        else
                                        {
                                            dispMsg += ", ";
                                        }
                                    }
                                    dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);

                                }
                                moreThanOne = true;
                                z--;
                            }
                            dispMsg += ".";
                            chr.WriteToDisplay(dispMsg);
                        }
                        else
                        { chr.WriteToDisplay("Your locker is empty."); }
                    }
                    else
                    {
                        chr.WriteToDisplay("Your locker is not here.");
                    }
                }
                #endregion

                #region Show Stats
                else if (args.Equals("stats"))
                {
                    if (chr.InUnderworld)
                    {
                        if (chr.UW_hasIntestines) { chr.WriteToDisplay("You have your intestines."); }
                        else { chr.WriteToDisplay("You do not have your intestines."); }

                        if (chr.UW_hasLiver) { chr.WriteToDisplay("You have your liver."); }
                        else { chr.WriteToDisplay("You do not have your liver."); }

                        if (chr.UW_hasLungs) { chr.WriteToDisplay("You have your lungs."); }
                        else { chr.WriteToDisplay("You do not have your lungs."); }

                        if (chr.UW_hasStomach) { chr.WriteToDisplay("You have your stomach."); }
                        else { chr.WriteToDisplay("You do not have your stomach."); }

                        chr.WriteToDisplay("You have " + chr.currentKarma.ToString() + " karma.");
                    }
                    else
                    {
                        string age = chr.getAgeDescription(true);

                        chr.WriteToDisplay("Strength     : " + chr.Strength.ToString().PadRight(7) + "Adds: " + chr.strengthAdd.ToString());
                        chr.WriteToDisplay("Dexterity    : " + chr.Dexterity.ToString().PadRight(7) + "Adds: " + chr.dexterityAdd.ToString());
                        chr.WriteToDisplay("Intelligence : " + chr.Intelligence.ToString().PadRight(7) + "Hits Adj: " + chr.HitsAdjustment);
                        chr.WriteToDisplay("Wisdom       : " + chr.Wisdom.ToString().PadRight(7) + "Stam Adj: " + chr.StaminaAdjustment.ToString());
                        if (chr.IsSpellUser)
                            chr.WriteToDisplay("Charisma     : " + chr.Charisma.ToString().PadRight(7) + "Mana Adj: " + chr.ManaAdjustment.ToString());
                        else chr.WriteToDisplay("Charisma     : " + chr.Charisma.ToString());
                        chr.WriteToDisplay("Constitution : " + chr.Constitution.ToString().PadRight(7) + "Karma / Marks: " + chr.currentKarma.ToString() + " / " + chr.currentMarks.ToString());
                        chr.WriteToDisplay("You are " + chr.getAgeDescription(true) + " level " + chr.Level + " " + chr.Alignment.ToString().ToLower() + " " + chr.classFullName.ToLower() + " from " + Character.RaceToString(chr.race) + ".");
                        chr.WriteToDisplay("You are carrying " + chr.GetEncumbrance() + " m'na and are " + Rules.GetEncumbrance(chr) + " encumbered.");
                        if (chr.IsInvisible) { chr.WriteToDisplay("** You are invisible. **"); }
                    }
                }
                #endregion

                #region Show Skills
                else if (args.Equals("skills"))
                {
                    string spaces = "........................";
                    string skillName = "";
                    string skillName2 = "";
                    chr.WriteToDisplay("Skills:");
                    if (chr.protocol == "old-kesmai")
                    {
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Bow, 0, chr.bow, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Shuriken, 0, chr.shuriken, chr.gender);
                        chr.WriteToDisplay("Bow..........." + skillName + "  Shuriken...." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Dagger, 0, chr.dagger, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Staff, 0, chr.staff, chr.gender);
                        chr.WriteToDisplay("Dagger........" + skillName + "  Staff......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Flail, 0, chr.flail, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Sword, 0, chr.sword, chr.gender);
                        chr.WriteToDisplay("Flail........." + skillName + "  Sword......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Halberd, 0, chr.halberd, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Two_Handed, 0, chr.twoHanded, chr.gender);
                        chr.WriteToDisplay("Halberd......." + skillName + "  Two Handed.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Mace, 0, chr.mace, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Threestaff, 0, chr.threestaff, chr.gender);
                        chr.WriteToDisplay("Mace.........." + skillName + "  Threestaff.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Rapier, 0, chr.rapier, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Unarmed, 0, chr.unarmed, chr.gender);
                        chr.WriteToDisplay("Rapier........" + skillName + "  Unarmed....." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName2 = "";
                        if (chr.BaseProfession == Character.ClassType.Thief || chr.BaseProfession == Character.ClassType.Martial_Artist)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Thievery, 0, chr.thievery, chr.gender);
                            chr.WriteToDisplay("Thievery......" + skillName);
                        }
                        if (chr.BaseProfession == Character.ClassType.Thaumaturge || chr.BaseProfession == Character.ClassType.Wizard ||
                            chr.BaseProfession == Character.ClassType.Thief)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Magic, chr.BaseProfession, chr.magic, chr.gender);
                            chr.WriteToDisplay("Magic........." + skillName);
                        }
                        if (chr.BaseProfession == Character.ClassType.Fighter)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Bash, chr.BaseProfession, chr.bash, chr.gender);
                            chr.WriteToDisplay("Bash.........." + skillName);
                        }
                    }
                    else
                    {
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Bow, 0, chr.bow, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Shuriken, 0, chr.shuriken, chr.gender);
                        chr.WriteToDisplay("Bow..........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Shuriken...." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Dagger, 0, chr.dagger, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Staff, 0, chr.staff, chr.gender);
                        chr.WriteToDisplay("Dagger........" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Staff......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Flail, 0, chr.flail, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Sword, 0, chr.sword, chr.gender);
                        chr.WriteToDisplay("Flail........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Sword......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Halberd, 0, chr.halberd, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Two_Handed, 0, chr.twoHanded, chr.gender);
                        chr.WriteToDisplay("Halberd......." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Two Handed.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Mace, 0, chr.mace, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Threestaff, 0, chr.threestaff, chr.gender);
                        chr.WriteToDisplay("Mace.........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Threestaff.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName = Skills.GetSkillTitle(Globals.eSkillType.Rapier, 0, chr.rapier, chr.gender);
                        skillName2 = Skills.GetSkillTitle(Globals.eSkillType.Unarmed, 0, chr.unarmed, chr.gender);
                        chr.WriteToDisplay("Rapier........" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Unarmed....." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                        skillName2 = "";
                        if (chr.BaseProfession == Character.ClassType.Thief || chr.BaseProfession == Character.ClassType.Martial_Artist)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Thievery, 0, chr.thievery, chr.gender);
                            chr.WriteToDisplay("Thievery......" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                        }
                        if (chr.BaseProfession == Character.ClassType.Thaumaturge || chr.BaseProfession == Character.ClassType.Wizard ||
                            chr.BaseProfession == Character.ClassType.Thief)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Magic, chr.BaseProfession, chr.magic, chr.gender);
                            chr.WriteToDisplay("Magic........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                        }
                        if (chr.BaseProfession == Character.ClassType.Fighter)
                        {
                            skillName = Skills.GetSkillTitle(Globals.eSkillType.Bash, chr.BaseProfession, chr.bash, chr.gender);
                            chr.WriteToDisplay("Bash.........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                        }
                    }
                }
                #endregion

                #region Show Inventory
                else if (args.Equals("inventory") || args.Equals("inv"))
                {
                    Item[] inventory = new Item[chr.wearing.Count];

                    chr.wearing.CopyTo(inventory);

                    if (inventory.Length > 0)
                        chr.WriteToDisplay("Your Worn Inventory:");
                    else chr.WriteToDisplay("You are not wearing anything.");

                    for (int a = 0; a < inventory.Length; a++)
                    {
                        string write = "";
                        if (inventory[a].identifiedList.Contains(chr.PlayerID))
                        {
                            if (inventory[a].wearOrientation != Globals.eWearOrientation.None)
                                write = "[" + inventory[a].wearLocation.ToString() + "] " + inventory[a].identifiedName + " (" + inventory[a].name + ")";
                            else
                                write = "[" + inventory[a].wearOrientation.ToString() + " " + inventory[a].wearLocation.ToString() + "] " + inventory[a].identifiedName + " (" + inventory[a].name + ")";
                        }
                        else
                        {
                            if (inventory[a].wearOrientation == Globals.eWearOrientation.None)
                                write = "[" + inventory[a].wearLocation.ToString() + "] " + inventory[a].unidentifiedName + " (" + inventory[a].name + ")";
                            else
                                write = "[" + inventory[a].wearOrientation.ToString() + " " + inventory[a].wearLocation.ToString() + "] " + inventory[a].identifiedName + " (" + inventory[a].name + ")";
                        }
                        chr.WriteToDisplay(write);
                    }
                } 
                #endregion

                #region Show Rings
                else if (args.Equals("rings") || args.Equals("ring"))
                {
                    string right1 = "empty";
                    string right2 = "empty";
                    string right3 = "empty";
                    string right4 = "empty";
                    string left1 = "empty";
                    string left2 = "empty";
                    string left3 = "empty";
                    string left4 = "empty";

                    if (chr.RightRing1 != null)
                    {
                        right1 = chr.RightRing1.unidentifiedName + " (" + chr.RightRing1.name + ")";
                        if (chr.RightRing1.identifiedList.Contains(chr.PlayerID))
                            right1 = chr.RightRing1.identifiedName + " (" + chr.RightRing1.name + ")";
                        if (chr.RightRing1.isRecall && chr.Map.GetZName(chr.RightRing1.recallZ) != "")
                            right1 += " [" + chr.Map.GetZName(chr.RightRing1.recallZ) + "]";

                    }
                    if (chr.RightRing2 != null)
                    {
                        right2 = chr.RightRing2.unidentifiedName + " (" + chr.RightRing2.name + ")";
                        if (chr.RightRing2.identifiedList.Contains(chr.PlayerID))
                            right2 = chr.RightRing2.identifiedName + " (" + chr.RightRing2.name + ")";
                        if (chr.RightRing2.isRecall && chr.Map.GetZName(chr.RightRing2.recallZ) != "")
                            right2 += " [" + chr.Map.GetZName(chr.RightRing2.recallZ) + "]";
                    }
                    if (chr.RightRing3 != null)
                    {
                        right3 = chr.RightRing3.unidentifiedName + " (" + chr.RightRing3.name + ")";
                        if (chr.RightRing3.identifiedList.Contains(chr.PlayerID))
                            right3 = chr.RightRing3.identifiedName + " (" + chr.RightRing3.name + ")";
                        if (chr.RightRing3.isRecall && chr.Map.GetZName(chr.RightRing3.recallZ) != "")
                            right3 += " [" + chr.Map.GetZName(chr.RightRing3.recallZ) + "]";
                    }
                    if (chr.RightRing4 != null)
                    {
                        right4 = chr.RightRing4.unidentifiedName + " (" + chr.RightRing4.name + ")";
                        if (chr.RightRing4.identifiedList.Contains(chr.PlayerID))
                            right4 = chr.RightRing4.identifiedName + " (" + chr.RightRing4.name + ")";
                        if (chr.RightRing4.isRecall && chr.Map.GetZName(chr.RightRing4.recallZ) != "")
                            right4 += " [" + chr.Map.GetZName(chr.RightRing4.recallZ) + "]";
                    }
                    if (chr.LeftRing1 != null)
                    {
                        left1 = chr.LeftRing1.unidentifiedName + " (" + chr.LeftRing1.name + ")";
                        if (chr.LeftRing1.identifiedList.Contains(chr.PlayerID))
                            left1 = chr.LeftRing1.identifiedName + " (" + chr.LeftRing1.name + ")";
                        if (chr.LeftRing1.isRecall && chr.Map.GetZName(chr.LeftRing1.recallZ) != "")
                            left1 += " [" + chr.Map.GetZName(chr.LeftRing1.recallZ) + "]";
                    }
                    if (chr.LeftRing2 != null)
                    {
                        left2 = chr.LeftRing2.unidentifiedName + " (" + chr.LeftRing2.name + ")";
                        if (chr.LeftRing2.identifiedList.Contains(chr.PlayerID))
                            left2 = chr.LeftRing2.identifiedName + " (" + chr.LeftRing2.name + ")";
                        if (chr.LeftRing2.isRecall && chr.Map.GetZName(chr.LeftRing2.recallZ) != "")
                            left2 += " [" + chr.Map.GetZName(chr.LeftRing2.recallZ) + "]";
                    }
                    if (chr.LeftRing3 != null)
                    {
                        left3 = chr.LeftRing3.unidentifiedName + " (" + chr.LeftRing3.name + ")";
                        if (chr.LeftRing3.identifiedList.Contains(chr.PlayerID))
                            left3 = chr.LeftRing3.identifiedName + " (" + chr.LeftRing3.name + ")";
                        if (chr.LeftRing3.isRecall && chr.Map.GetZName(chr.LeftRing3.recallZ) != "")
                            left3 += " [" + chr.Map.GetZName(chr.LeftRing3.recallZ) + "]";
                    }
                    if (chr.LeftRing4 != null)
                    {
                        left4 = chr.LeftRing4.unidentifiedName + " (" + chr.LeftRing4.name + ")";
                        if (chr.LeftRing4.identifiedList.Contains(chr.PlayerID))
                            left4 = chr.LeftRing4.identifiedName + " (" + chr.LeftRing4.name + ")";
                        if (chr.LeftRing4.isRecall && chr.Map.GetZName(chr.LeftRing4.recallZ) != "")
                            left4 += " [" + chr.Map.GetZName(chr.LeftRing4.recallZ) + "]";
                    }

                    chr.WriteToDisplay("Right Hand:");
                    chr.WriteToDisplay("1. " + right1);
                    chr.WriteToDisplay("2. " + right2);
                    chr.WriteToDisplay("3. " + right3);
                    chr.WriteToDisplay("4. " + right4);
                    chr.WriteToDisplay(" ");
                    chr.WriteToDisplay("Left Hand:");
                    chr.WriteToDisplay("1. " + left1);
                    chr.WriteToDisplay("2. " + left2);
                    chr.WriteToDisplay("3. " + left3);
                    chr.WriteToDisplay("4. " + left4);

                }
                #endregion

                #region Show Resists
                else if (args.ToLower().Equals("resists"))
                {
                    chr.WriteToDisplay("Resists");
                    chr.WriteToDisplay("Fire      : " + chr.FireResistance);
                    chr.WriteToDisplay("Ice       : " + chr.ColdResistance);
                    chr.WriteToDisplay("Death     : " + chr.DeathResistance);
                    chr.WriteToDisplay("Blind     : " + chr.BlindResistance);
                    chr.WriteToDisplay("Fear      : " + chr.FearResistance);
                    chr.WriteToDisplay("Stun      : " + chr.StunResistance);
                    chr.WriteToDisplay("Poison    : " + chr.PoisonResistance);
                    chr.WriteToDisplay("Lightning : " + chr.LightningResistance);
                    chr.WriteToDisplay("Zonk      : " + chr.ZonkResistance);
                }
                #endregion

                #region Show Protection
                else if (args.Equals("protection"))
                {
                    chr.WriteToDisplay("Protection");
                    chr.WriteToDisplay("Fire      : " + chr.FireProtection);
                    chr.WriteToDisplay("Ice       : " + chr.ColdProtection);
                    chr.WriteToDisplay("Death     : " + chr.DeathProtection);
                    chr.WriteToDisplay("Poison    : " + chr.PoisonProtection);
                    chr.WriteToDisplay("Lightning : " + chr.LightningProtection);
                }
                #endregion

                #region Show Effects
                else if (args.Equals("effects"))
                {
                    int effectCount = 1;
                    chr.WriteToDisplay("Spell Effects");
                    foreach (Effect effect in chr.effectList.Values)
                    {
                        chr.WriteToDisplay(effectCount + ". " + Utils.FormatEnumString(effect.effectType.ToString()));
                        effectCount++;
                    }
                    effectCount = effectCount - 1;
                    chr.WriteToDisplay("Total = " + effectCount.ToString());
                    effectCount = 1;
                    chr.WriteToDisplay(" ");
                    chr.WriteToDisplay("Item Effects");
                    foreach (Effect effect in chr.wornEffectList)
                    {
                        chr.WriteToDisplay(effectCount + ". " + Utils.FormatEnumString(effect.effectType.ToString()));
                        effectCount++;
                    }
                    effectCount = effectCount - 1;
                    chr.WriteToDisplay("Total = " + effectCount.ToString());
                    chr.WriteToDisplay(" ");
                    chr.WriteToDisplay("Total Shielding = " + chr.Shielding);
                }
                #endregion

                #region Show Tempstats
                else if (args.Equals("tempstats"))
                {
                    chr.WriteToDisplay("Temporary Stats");
                    chr.WriteToDisplay("Strength     : " + chr.TempStrength);
                    chr.WriteToDisplay("Dexterity    : " + chr.TempDexterity);
                    chr.WriteToDisplay("Intelligence : " + chr.TempIntelligence);
                    chr.WriteToDisplay("Wisdom       : " + chr.TempWisdom);
                    chr.WriteToDisplay("Constitution : " + chr.TempConstitution);
                    chr.WriteToDisplay("Charisma     : " + chr.TempCharisma);
                }
                #endregion

                #region Show AC
                else if (args.Equals("ac"))
                {
                    chr.WriteToDisplay("Armor Class");
                    chr.WriteToDisplay("Base Armor Class           : " + chr.baseArmorClass);
                    chr.WriteToDisplay("Armor Rating               : " + Rules.AC_GetArmorClassRating(chr));
                    chr.WriteToDisplay("Shield Rating vs. Ranged   : " + Rules.AC_GetShieldingArmorClass(chr.Shielding, true));
                    chr.WriteToDisplay("Shield Rating vs. Melee    : " + Rules.AC_GetShieldingArmorClass(chr.Shielding, false));
                    chr.WriteToDisplay("Unarmed Armor Class Adj.   : " + Rules.AC_GetUnarmedArmorClassBonus(chr));
                    chr.WriteToDisplay("Dexterity Armor Class Adj. : " + Rules.AC_GetDexterityArmorClassBonus(chr));
                    chr.WriteToDisplay("Right Hand Armor Class     : " + Rules.AC_GetRightHandArmorClass(chr));
                    chr.WriteToDisplay("Left Hand Armor Class      : " + Rules.AC_GetLeftHandArmorClass(chr));
                    chr.WriteToDisplay("Note: There is some armor class information intentionally omitted.");
                }
                #endregion

                #region Show Training
                else if (args.Equals("training"))
                {
                    string spaces = "...................";
                    string skillName = "";
                    string skillName2 = "";
                    chr.WriteToDisplay("Current Training:");
                    skillName = chr.trainedBow.ToString();
                    skillName2 = chr.trainedShuriken.ToString();
                    chr.WriteToDisplay("Bow..........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Shuriken...." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName = chr.trainedDagger.ToString();
                    skillName2 = chr.trainedStaff.ToString();
                    chr.WriteToDisplay("Dagger........" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Staff......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName = chr.trainedFlail.ToString();
                    skillName2 = chr.trainedSword.ToString();
                    chr.WriteToDisplay("Flail........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Sword......." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName = chr.trainedHalberd.ToString();
                    skillName2 = chr.trainedTwoHanded.ToString();
                    chr.WriteToDisplay("Halberd......." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Two Handed.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName = chr.trainedMace.ToString();
                    skillName2 = chr.trainedThreestaff.ToString();
                    chr.WriteToDisplay("Mace.........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Threestaff.." + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName = chr.trainedRapier.ToString();
                    skillName2 = chr.trainedUnarmed.ToString();
                    chr.WriteToDisplay("Rapier........" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName + "  Martial Arts" + spaces.Substring(0, spaces.Length - skillName2.Length) + skillName2);
                    skillName2 = "";
                    if (chr.BaseProfession == Character.ClassType.Thief || chr.BaseProfession == Character.ClassType.Martial_Artist)
                    {
                        skillName = chr.trainedThievery.ToString();
                        chr.WriteToDisplay("Thievery......" + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                    }
                    if (chr.BaseProfession == Character.ClassType.Thaumaturge || chr.BaseProfession == Character.ClassType.Wizard ||
                        chr.BaseProfession == Character.ClassType.Thief)
                    {
                        skillName = chr.trainedMagic.ToString();
                        chr.WriteToDisplay("Magic........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                    }
                    if (chr.BaseProfession == Character.ClassType.Fighter)
                    {
                        skillName = chr.trainedBash.ToString();
                        chr.WriteToDisplay("Bash.........." + spaces.Substring(0, spaces.Length - skillName.Length) + skillName);
                    }
                } 
                #endregion

                else chr.WriteToDisplay("I don't understand your command.");
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void echo(string args)
        {
            if (args == null)
            {
                if (chr.echo)
                {
                    chr.echo = false;
                    chr.WriteToDisplay("Echo disabled.");
                }
                else
                {
                    chr.echo = true;
                    chr.WriteToDisplay("Echo enabled.");
                }
            }
            else
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs[0].ToLower() == "off") { chr.echo = false; chr.WriteToDisplay("Echo disabled."); }
                else if (sArgs[0].ToLower() == "on") { chr.echo = true; chr.WriteToDisplay("Echo enabled."); }
                else { chr.WriteToDisplay("Format: echo [ on | off ]"); }
            }
        }

        public void impimmortal(string args)
        {
            if (chr.ImpLevel >= Globals.eImpLevel.GM)
            {
                if (chr.IsImmortal)
                {
                    chr.IsImmortal = false;
                    chr.WriteToDisplay("You are no longer immortal.");
                }
                else
                {
                    chr.IsImmortal = true;
                    chr.WriteToDisplay("You are now immortal.");
                }
            }
            else { chr.WriteToDisplay("I don't understand your command."); }
        }

        public void impitemsearch(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args.Equals(null) || args == "")
            {
                chr.WriteToDisplay("Format: impitemsearch <item ID>");
            }

            const int MAX_FINDS = 50;

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs.Length == 1)
            {
                try
                {
                    int itemID = Convert.ToInt32(sArgs[0]);

                    int found = 1;

                    foreach (NPC npc in NPC.NPCList)
                    {
                        if (found >= MAX_FINDS)
                            return;

                        if (npc.lairCellsList.Count > 0)
                        {
                            foreach (Cell cell in npc.lairCellsList)
                            {
                                foreach (Item item in cell.Items)
                                {
                                    if (item.itemID == itemID)
                                    {
                                        chr.WriteToDisplay(found + ". " + npc.GetLogString() + " has " + item.GetLogString() + " in one of it's lair cells.");
                                        found++;
                                    }
                                }
                            }
                        }

                        foreach (Item item in npc.wearing)
                        {
                            if (item.itemID == itemID)
                            {
                                chr.WriteToDisplay(found + ". " + npc.GetLogString() + " is wearing " + item.GetLogString());
                                found++;
                            }
                        }

                        foreach (Item ring in npc.GetRings())
                        {
                            if (ring.itemID == itemID)
                            {
                                chr.WriteToDisplay(found + ". " + npc.GetLogString() + " is wearing the ring " + ring.GetLogString());
                                found++;
                            }
                        }

                        foreach (Item item in npc.beltList)
                        {
                            if (item.itemID == itemID)
                            {
                                chr.WriteToDisplay(found + ". " + npc.GetLogString() + " has " + item.GetLogString() + " on it's belt.");
                                found++;
                            }
                        }

                        foreach (Item item in npc.sackList)
                        {
                            if (item.itemID == itemID)
                            {
                                chr.WriteToDisplay(found + ". " + npc.GetLogString() + " has " + item.GetLogString() + " in it's sack.");
                                found++;
                            }
                        }
                    }
                }
                catch
                {
                    chr.WriteToDisplay("Format: impitemsearch <item ID>");
                }
            }
        }

        public void imptitle(string args)
        {
            if (chr.ImpLevel >= Globals.eImpLevel.GM)
            {
                if (chr.showStaffTitle)
                {
                    chr.showStaffTitle = false;
                    chr.WriteToDisplay("Your title is now OFF.");
                }
                else
                {
                    chr.showStaffTitle = true;
                    chr.WriteToDisplay("Your title is now ON.");
                }
            }
            else
            {
                chr.WriteToDisplay("I don't understand your command.");
            }
        }

        public void anonymous(string args)
        {
            if (chr.IsAnonymous)
            {
                chr.IsAnonymous = false;
                chr.WriteToDisplay("You are no longer anonymous.");
            }
            else
            {
                chr.IsAnonymous = true;
                chr.WriteToDisplay("You are now anonymous.");
            }
        }

        public void rest_x(string args)
        {
            chr.cmdWeight += 2;

            if (chr.cmdWeight > 3)
            {
                return;
            }

            chr.CommandType = CommandType.Rest;

            if (chr.IsWizardEye)
            {
                chr.effectList[Effect.EffectType.Wizard_Eye].StopCharacterEffect();
                return;
            }

            if (chr.IsPeeking)
            {
                chr.effectList[Effect.EffectType.Peek].StopCharacterEffect();
                return;
            }

            if (chr.IsDead && chr.IsPC) // if the character is dead and is a player
            {
                Rules.DeadRest(chr);
                return;
            }

            if (chr.damageRound < DragonsSpineMain.GameRound - 3 && !chr.IsImmortal)
            {
                if (chr.Hits < chr.HitsFull)  // increase stats
                {
                    chr.Hits++;
                    if (chr.hitsRegen > 0 && chr.Hits < chr.HitsFull) // gain additional mana if hpregen item/effect is active
                    {
                        chr.Hits += chr.hitsRegen;
                        if (chr.Hits > chr.HitsFull) { chr.Hits = chr.HitsFull; } // confirm we didn't regen more hits than we have
                    }
                }
                if (chr.Stamina < chr.StaminaFull)
                {
                    chr.Stamina++;
                    if (chr.staminaRegen > 0 && chr.Stamina < chr.StaminaFull)
                    {
                        chr.Stamina += chr.staminaRegen;
                        if (chr.Stamina > chr.StaminaFull) { chr.Stamina = chr.StaminaFull; } // confirm we didn't regen more stamina than we have
                    }
                }
                if (chr.Mana < chr.ManaFull)
                {
                    chr.Mana++;
                    if (chr.manaRegen > 0 && chr.Mana < chr.ManaFull) // gain additional mana if manaRegeneration item or effect is active
                    {
                        chr.Mana += chr.manaRegen;
                        if (chr.Mana > chr.ManaFull) { chr.Mana = chr.ManaFull; } // confirm we didn't regen more mana than we have
                    }
                }
            }

            if (chr.preppedSpell != null) // lose spell
            {
                chr.preppedSpell = null;
                chr.WriteToDisplay("You have lost your warmed spell.");
                chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
            }

            if (!chr.IsPC) { return; } // end the rest now if this is not a player

            //do a level up if hits and stamina are at max
            if (chr.Hits >= chr.HitsFull && chr.Stamina >= chr.StaminaFull)
            {
                if (Rules.GetExpLevel(chr.Experience) > chr.Level)
                {
                    int levelDifference = 1;
                    //int levelDifference = Rules.levelCheck(chr.Experience) - chr.Level; // should be 1, however some players might test this and never rest...
                    for (int a = 0; a < levelDifference; a++)
                    {
                        int hitsGain = Rules.GetHitsGain(chr, 1);
                        int staminaGain = Rules.GetStaminaGain(chr, 1);
                        int manaGain = Rules.GetManaGain(chr, 1);

                        chr.Level += 1; // add a level

                        chr.HitsMax += hitsGain; // add hitsGain to hitsmax
                        chr.StaminaMax += staminaGain; // add staminaGain to stamina

                        if (chr.Strength >= 16) // add a strength add at appropriate level
                        {
                            if (chr.IsPureMelee || chr.IsHybrid) // melee and hybrid
                            {
                                switch (chr.Level)
                                {
                                    case 6:
                                    case 9:
                                    case 12:
                                    case 15:
                                    case 18:
                                    case 21:
                                    case 24:
                                    case 27:
                                    case 30:
                                        chr.strengthAdd += 1;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else if (chr.IsSpellUser) // magic users
                            {
                                switch (chr.Level)
                                {
                                    case 7:
                                    case 11:
                                    case 15:
                                    case 19:
                                    case 23:
                                    case 27:
                                        chr.strengthAdd += 1;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        if (chr.Dexterity >= 16)
                        {
                            if (chr.IsPureMelee || chr.IsHybrid) // melee and hybrid
                            {
                                switch (chr.Level)
                                {
                                    case 6:
                                    case 9:
                                    case 12:
                                    case 15:
                                    case 18:
                                    case 21:
                                    case 24:
                                    case 27:
                                    case 30:
                                        chr.dexterityAdd += 1;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else if (chr.IsSpellUser) // magic users
                            {
                                switch (chr.Level)
                                {
                                    case 7:
                                    case 11:
                                    case 15:
                                    case 19:
                                    case 23:
                                    case 27:
                                        chr.dexterityAdd += 1;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        string pts = "point";
                        if (hitsGain > 1) { pts = "points"; }
                        chr.WriteToDisplay("You have gained " + hitsGain + " hit " + pts + ".");
                        if (staminaGain > 1) { pts = "points"; }
                        else { pts = "point"; }
                        chr.WriteToDisplay("You have gained " + staminaGain + " stamina " + pts + ".");

                        if (chr.IsSpellUser && !chr.IsHybrid)
                        {
                            if (manaGain < 2) { pts = "point"; }
                            else { pts = "points"; }
                            chr.ManaMax += manaGain;
                            chr.WriteToDisplay("You have gained " + manaGain + " mana " + pts + ".");
                        }

                        chr.SendSound(Sound.GetCommonSound(Sound.CommonSound.LevelUp));

                        chr.WriteToDisplay("You are now a level " + chr.Level + " " + chr.classFullName.ToLower() + "!!");

                        // set stats to full
                        chr.Hits = chr.HitsFull;
                        chr.Stamina = chr.StaminaFull;
                        chr.Mana = chr.ManaFull;
                    }
                }
            }
        }

        public void get(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Get what?");
                return;
            }
            take(args);
            return;
        }

        public void scoop(string args)
        {
            chr.cmdWeight += 1;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Scoop what?");
                return;
            }

            try
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs.Length == 1 && sArgs[0].ToLower() != "all")
                {
                    #region scoop <item>
                    if (Item.IsItemOnGround(sArgs[0].ToLower(), chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z))
                    {
                        for (int a = 0; a < chr.CurrentCell.Items.Count; a++)
                        {
                            Item tItem = (Item)chr.CurrentCell.Items[a];
                            if (tItem.name.ToLower() == sArgs[0].ToLower())
                            {
                                if (tItem.size == Globals.eItemSize.Sack_Only || tItem.size == Globals.eItemSize.Belt_Or_Sack)
                                {
                                    if (chr.SackCountMinusGold >= Character.MAX_SACK && tItem.itemType != Globals.eItemType.Coin)
                                    {
                                        chr.WriteToDisplay("Your sack is full.");
                                        return;
                                    }

                                    if (chr.SackItem(tItem))
                                    {
                                        chr.CurrentCell.Remove(tItem);
                                        chr.WriteToDisplay("You scoop the " + tItem.name + " into your sack.");
                                        return;
                                    }
                                    else
                                    {
                                        chr.WriteToDisplay("Your sack is full.");
                                    }
                                }
                                else
                                {
                                    chr.WriteToDisplay("The " + tItem.name + " is too large to fit into your sack.");
                                }
                            }
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                    }
                    #endregion
                }
                else if (sArgs.Length == 2 && sArgs[0].ToLower() == "all")
                {
                    #region scoop all <item>
                    int sackCount = chr.SackCountMinusGold;
                    string itemName = sArgs[1].ToLower();
                    int successCount = 0;
                    if (itemName.EndsWith("s"))
                        itemName = itemName.Substring(0, itemName.Length - 1);

                    if (itemName == "coin")
                    {
                        //ParseCommand(chr, "scoop", "coins");
                        chr.WriteToDisplay("Use 'Scoop coins' instead.", Protocol.TextType.System);
                        return;
                    }

                    while (sackCount < Character.MAX_SACK)
                    {
                        foreach(Item tItem in new List<Item>(chr.CurrentCell.Items))
                        {
                            if (tItem.name.ToLower().StartsWith(itemName) &&
                                (tItem.size == Globals.eItemSize.Sack_Only || tItem.size == Globals.eItemSize.Belt_Or_Sack))
                            {
                                if (chr.SackItem(tItem))
                                {
                                    sackCount++;
                                    successCount++;
                                    chr.CurrentCell.Remove(tItem);
                                }
                            }
                        }
                        break;
                    }
                    
                    if (successCount == 0)
                    {
                        if (sackCount >= Character.MAX_SACK)
                        {
                            chr.WriteToDisplay("Your sack is full.");
                        }
                        else
                        {
                            chr.WriteToDisplay("You don't see any " + itemName + "s here.");
                        }
                        return;
                    }
                    else if (successCount == 1)
                        chr.WriteToDisplay("You scoop 1 " + itemName + " into your sack.");
                    else
                        chr.WriteToDisplay("You scoop " + successCount + " " + itemName + "s into your sack.");
                    #endregion
                }
                else if (sArgs.Length == 3 && (sArgs[2].ToLower() == "counter" || sArgs[2].ToLower() == "altar"))
                {
                    #region scoop <item> from <counter | altar>
                    Cell counterCell = Map.GetNearestCounterOrAltarCell(chr);
                    if (counterCell == null)
                    {
                        if (sArgs[2].ToLower() == "counter")
                            chr.WriteToDisplay("You are not near a counter.");
                        else chr.WriteToDisplay("You are not near an altar.");
                        return;
                    }

                    if (Item.IsItemOnGround(sArgs[0].ToLower(), chr.FacetID, chr.LandID, chr.MapID, counterCell.X, counterCell.Y, counterCell.Z))
                    {
                        for (int a = 0; a < counterCell.Items.Count; a++)
                        {
                            Item tItem = counterCell.Items[a];
                            if (tItem.name.ToLower() == sArgs[0].ToLower())
                            {
                                if (tItem.size == Globals.eItemSize.Sack_Only || tItem.size == Globals.eItemSize.Belt_Or_Sack)
                                {
                                    if (chr.SackCountMinusGold >= Character.MAX_SACK && tItem.itemType != Globals.eItemType.Coin)
                                    {
                                        chr.WriteToDisplay("Your sack is full.");
                                        return;
                                    }

                                    if (chr.SackItem(tItem))
                                    {
                                        counterCell.Remove(tItem);
                                        chr.WriteToDisplay("You scoop the " + tItem.name + " into your sack.");
                                        return;
                                    }
                                }
                                else
                                {
                                    chr.WriteToDisplay("The " + tItem.name + " is too large to fit into your sack.");
                                }
                            }
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
                    }
                    #endregion
                }
                else if (sArgs.Length == 4 && sArgs[0].ToLower() == "all" && (sArgs[3].ToLower() == "counter" || sArgs[3].ToLower() == "altar"))
                {
                    #region scoop all <item> from <counter | altar>
                    Cell counterCell = Map.GetNearestCounterOrAltarCell(chr);
                    if (counterCell == null)
                    {
                        if (sArgs[2].ToLower() == "counter")
                            chr.WriteToDisplay("You are not near a counter.");
                        else chr.WriteToDisplay("You are not near an altar.");
                        return;
                    }

                    int sackCount = chr.SackCountMinusGold;
                    string itemName = sArgs[1].ToLower();
                    int successCount = 0;
                    if (itemName.EndsWith("s"))
                        itemName = itemName.Substring(0, itemName.Length - 1);

                    if (itemName == "coin")
                    {
                        ParseCommand(chr, "scoop", "coins from counter");
                        return;
                    }

                    while (sackCount < Character.MAX_SACK)
                    {
                        for (int a = counterCell.Items.Count - 1; a >= 0; a--)
                        {
                            Item tItem = counterCell.Items[a];
                            if (tItem.name.ToLower().StartsWith(itemName) &&
                                (tItem.size == Globals.eItemSize.Sack_Only || tItem.size == Globals.eItemSize.Belt_Or_Sack))
                            {
                                if (chr.SackItem(tItem))
                                {
                                    sackCount++;
                                    successCount++;
                                    counterCell.Remove(tItem);
                                }
                            }
                        }
                        break;
                    }

                    if (successCount == 0)
                    {
                        chr.WriteToDisplay("You don't see any " + itemName + "s on the " + sArgs[3].ToLower() + ".");
                        return;
                    }
                    else if (successCount == 1)
                        chr.WriteToDisplay("You scoop 1 " + itemName + " into your sack from the " + sArgs[3].ToLower() + ".");
                    else
                        chr.WriteToDisplay("You scoop " + successCount + " " + itemName + "s into your sack from the " + sArgs[3].ToLower() + "."); 
                    #endregion
                }
                else
                {
                    chr.WriteToDisplay("Usage for the scoop command:");
                    chr.WriteToDisplay("\"scoop <item>\"");
                    chr.WriteToDisplay("\"scoop <item> from <counter | altar>\"");
                    chr.WriteToDisplay("\"scoop all <item>\"");
                    chr.WriteToDisplay("\"scoop all <item> from <counter | altar>\"");
                }
            }
            catch (Exception ex)
            {
                Utils.LogException(ex);
                chr.WriteToDisplay("Usage for the scoop command:");
                chr.WriteToDisplay("\"scoop <item>\"");
                chr.WriteToDisplay("\"scoop <item> from <counter | altar>\"");
                chr.WriteToDisplay("\"scoop all <item>\"");
                chr.WriteToDisplay("\"scoop all <item> from <counter | altar>\"");
            }
        }

        public void t(string args)
        {
            take(args);
        }

        public void take(string args)
        {
            int taketype = 0;

            string takeitem = "";

            string takefrom = "";

            int takenum = 0;

            Item item = new Item();

            if (args == null)
            {
                chr.WriteToDisplay("Take what?");
                return;
            }
            else
            {
                String[] sArgs = args.Split(" ".ToCharArray());
                if (sArgs.GetLength(0) < 2)
                {
                    taketype = 1; // take <item>
                    takeitem = sArgs[0].ToLower();
                }
                else if (sArgs.Length == 2)
                {
                    taketype = 2; // Take # <item>
                    try
                    {
                        takenum = Convert.ToInt32(sArgs[0]);
                        takeitem = sArgs[1].ToLower();
                    }
                    catch(Exception)
                    {
                        chr.WriteToDisplay("Useage: Take <#> item");
                        return;
                    }
                }
                else if (sArgs[1].ToLower().Equals("from"))
                {
                    if (sArgs[2] == null)
                    {
                        chr.WriteToDisplay("Take " + sArgs[0] + " from what?");
                        return;
                    }
                    else
                    {
                        taketype = 3; // take <item> from <something>
                        takeitem = sArgs[0].ToLower();
                        takefrom = sArgs[2].ToLower();
                    }
                }
                else if (sArgs[0].ToLower().Equals("ring") && sArgs[1].ToLower().Equals("off"))
                {	//Trap "Take Ring off left/right"
                    remove(args);
                    return;
                }
                else if (sArgs[1].ToLower().Equals("ring") && sArgs[2].ToLower().Equals("off"))
                {	//Trap "Take x Ring off left/right"
                    remove(args);
                    return;
                }
                else if (sArgs[2].ToLower().Equals("from"))
                {
                    if (sArgs[3] == null)
                    {
                        chr.WriteToDisplay("Take " + sArgs[1] + " from what?");
                        return;
                    }
                    else
                    {
                        taketype = 4; // take # <item> from <something>
                        takeitem = sArgs[1].ToLower();
                        takefrom = sArgs[3].ToLower();
                        takenum = Convert.ToInt32(sArgs[0]);
                    }
                }
                else
                {
                    taketype = 0; // Default type - nothing/catchall
                }
            }
            // Now we switch on the taketype
            switch (taketype)
            {
                case 0: // default type
                    chr.WriteToDisplay("Take what?");
                    break;
                case 1: // take <item>

                    if (Item.IsItemOnGround(takeitem.ToLower(), chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z))
                    {
                        for (int a = 0; a < chr.CurrentCell.Items.Count; a++)
                        {
                            Item tItem = (Item)chr.CurrentCell.Items[a];
                            if (tItem.name.ToLower() == takeitem.ToLower())
                            {
                                if (chr.EquipEitherHand(tItem))
                                {
                                    chr.CurrentCell.Remove(tItem);
                                }
                                return;
                            }
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't see a " + takeitem + " here.");
                        return;
                    }
                    break;
                case 2: // take # <item>
                    int z = 0;
                    if (takeitem.IndexOf("coin") > -1 || takeitem == "gold")
                    {
                        item = Item.FindItemOnGround(takeitem, chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                        foreach (Item itm in chr.CurrentCell.Items)
                        {
                            if (itm.name.ToLower().IndexOf("coin") > -1)
                            {
                                item = itm;
                            }
                        }
                        if (item == null)
                        {
                            chr.WriteToDisplay("You do not see any coins here.");
                            return;
                        }
                        else if (takenum <= 0)
                        {
                            chr.WriteToDisplay("You cannot do that.");
                            return;
                        }
                        else if (item.coinValue < takenum)
                        {
                            chr.WriteToDisplay("There aren't that many coins here.");
                            return;
                        }
                        else if (item.coinValue == takenum)
                        {
                            if (chr.EquipEitherHand(item))
                            {
                                chr.CurrentCell.Remove(item);
                            }
                            return;
                        }
                        else
                        {
                            if (chr.RightHand == null)
                            {
                                item.coinValue -= takenum;
                                chr.EquipRightHand(Item.CopyItemFromDictionary(Item.ID_COINS));
                                chr.RightHand.coinValue = takenum;
                                return;
                            }
                            else if (chr.LeftHand == null)
                            {
                                item.coinValue -= takenum;
                                chr.EquipLeftHand(Item.CopyItemFromDictionary(Item.ID_COINS));
                                chr.LeftHand.coinValue = takenum;
                                return;
                            }
                            else
                            {
                                chr.WriteToDisplay("Your hands are full.");
                                return;
                            }
                        }
                    }
                    
                    List<Item> tmpList = chr.CurrentCell.Items;

                    if (takenum > tmpList.Count) { chr.WriteToDisplay("You don't see that here."); return; }

                    z = tmpList.Count - 1;

                    int itemcount = 0;

                    foreach (Item tItem in tmpList)
                    {
                        Item tmpitem = tItem;
                        if (tmpitem.name == takeitem)
                        {
                            itemcount++;
                            if (takenum == itemcount)
                            {
                                if (chr.EquipEitherHand(tItem))
                                {
                                    chr.CurrentCell.Remove(tItem);
                                }
                                return;
                            }
                        }
                    }
                    chr.WriteToDisplay("There aren't that many " + takeitem + "'s here.");
                    break;
                case 3: // take <item> from <something>
                    if (chr.RightHand != null && chr.LeftHand != null)
                    {
                        chr.WriteToDisplay("Your hands are full.");
                        return;
                    }
                    else if (takefrom.ToLower() == "sack" || takefrom.ToLower() == "sac" || takefrom.ToLower() == "sakc")
                    {
                        item = chr.RemoveFromSack(takeitem.ToLower());
                    }
                    else if (takefrom.ToLower() == "locker")
                    {
                        if (chr.CurrentCell.IsLocker)
                        {
                            item = chr.RemoveFromLocker(takeitem.ToLower());
                        }
                        else
                        {
                            chr.WriteToDisplay("Your locker is not here.");
                        }
                    }
                    else if (takefrom.ToLower() == "counter" || takefrom.ToLower() == "altar")
                    {
                        item = Map.GetItemFromCounter(chr, takeitem.ToLower());
                    }
                    else if (takefrom.ToLower() == "ground")
                    {
                        this.take(takeitem);
                        break;
                    }
                    else
                    {
                        chr.WriteToDisplay("I don't understand that command.");
                        break;
                    }
                    if (item != null)
                    {
                        chr.EquipEitherHand(item);
                    }
                    else
                    {
                        chr.WriteToDisplay("The " + takefrom + " does not have a " + takeitem + " in it.");
                    }
                    break;
                case 4: // take # <item> from <something>
                    List<Item> tmplist;
                    switch (takefrom)
                    {
                        case "ground":
                            tmplist = chr.CurrentCell.Items;
                            break;
                        case "sack":
                            tmplist = chr.sackList;
                            break;
                        case "locker":
                            if (!chr.CurrentCell.IsLocker)
                            {
                                chr.WriteToDisplay("There are no lockers here.");
                                return;
                            }
                            tmplist = chr.lockerList;
                            break;
                        case "counter":
                        case "altar":
                            Cell curCell = Map.GetNearestCounterOrAltarCell(chr);
                            tmplist = curCell.Items;
                            //tmplist = null;
                            //for (int ypos = -1; ypos <= 1; ypos += 1)
                            //{
                            //    for (int xpos = -1; xpos <= 1; xpos += 1)
                            //    {
                            //        curCell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y + ypos, chr.X + xpos];
                            //        if (curCell.CellGraphic == "CC" || curCell.CellGraphic == "MM")
                            //        {
                            //            tmplist = curCell.cellItemList;
                            //        }
                            //    }
                            //}
                            if (tmplist == null)
                            {
                                chr.WriteToDisplay("I see no " + takefrom + " here.");
                                return;
                            }
                            break;
                        default:
                            chr.WriteToDisplay("I don't understand that command.");
                            return;
                    }
                    if (takeitem.IndexOf("coin") > -1 || takeitem == "gold")
                    {
                        foreach (Item itm in tmplist)
                        {
                            if (itm.name.ToLower().IndexOf("coin") > -1)
                            {
                                item = itm;
                            }
                        }
                        if (item == null)
                        {
                            chr.WriteToDisplay("You don't see any " + takeitem + " here.");
                            return;
                        }
                        else if (takenum <= 0)
                        {
                            chr.WriteToDisplay("You cannot do that.");
                            return;
                        }
                        else if (item.coinValue < takenum)
                        {
                            chr.WriteToDisplay("There aren't that many coins here.");
                            return;
                        }
                        else if (item.coinValue == takenum)
                        {
                            if (chr.EquipEitherHand(item))
                            {
                                tmplist.Remove(item);
                            }
                            return;
                        }
                        else
                        {
                            if (chr.RightHand == null)
                            {
                                item.coinValue -= takenum;
                                chr.EquipRightHand(Item.CopyItemFromDictionary(Item.ID_COINS));
                                chr.RightHand.coinValue = takenum;
                                return;
                            }
                            else if (chr.LeftHand == null)
                            {
                                item.coinValue -= takenum;
                                chr.EquipLeftHand(Item.CopyItemFromDictionary(Item.ID_COINS));
                                chr.LeftHand.coinValue = takenum;
                                return;
                            }
                            else
                            {
                                chr.WriteToDisplay("Your hands are full.");
                                return;
                            }
                        }
                    }
                    if (takenum > tmplist.Count) { chr.WriteToDisplay("You don't see that here."); return; }
                    int itmcount = 0;
                    foreach (Item tItem in tmplist)
                    {
                        Item tmpitem = tItem;
                        if (tmpitem.name == takeitem)
                        {
                            itmcount++;
                            if (takenum == itmcount)
                            {
                                if (chr.EquipEitherHand(tItem))
                                {
                                    tmplist.Remove(tItem);
                                    return;
                                }
                                else
                                {
                                    chr.WriteToDisplay("Your hands are full.");
                                    return;
                                }
                            }
                        }
                    }
                    chr.WriteToDisplay("There are not that many " + takeitem + "s here.");
                    break;
                default:
                    break;
            }
        }

        public void search_x(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Search what?");
                return;
            }
            else
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                switch (sArgs[0].ToLower())
                {
                    case "0":
                    case "corpse":
                        // Leave the corpse behind
                        Item itm = Item.FindItemOnGround(sArgs[0], chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                        if (itm == null)
                        {
                            chr.WriteToDisplay("You don't see a corpse here.");
                            look("");
                            return;
                        }

                        Item.dumpCorpse(itm, Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z));

                        // coin generated on npc creation
                        //Look
                        look("");
                        break;
                    case "all":
                        foreach (Item corpse in new List<Item>(chr.CurrentCell.Items))
                        {
                            if (corpse != null && corpse.name == "corpse") 
                                Item.dumpCorpse(corpse, chr.CurrentCell);
                        }
                        look("");
                        break;
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "10":
                    case "11":
                    case "12":
                        int countTo = 0;
                        int corpseNum = Convert.ToInt16(sArgs[0]);
                        foreach (Item cItem in chr.CurrentCell.Items)
                        {
                            if (cItem.name == "corpse") { countTo += 1; }
                        }
                        if (countTo < corpseNum)
                        {
                            chr.WriteToDisplay("There aren't that many corpses here.");
                            break;
                        }
                        countTo = 0;
                        foreach (Item cItem in chr.CurrentCell.Items)
                        {
                            if (cItem.name == "corpse")
                            {
                                countTo += 1;
                                if (countTo == corpseNum)
                                {
                                    Item.dumpCorpse(cItem, Cell.GetCell(chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z));
                                    break;
                                }
                            }
                        }
                        look("");
                        break;
                    default:
                        // if secret door cell has a flag cell lock check for flags
                        if (Array.IndexOf(Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0].ToLower(), true).cellLock.lockTypes,
                            Cell.CellLock.LockType.Flag) != -1)
                        {
                            if (!chr.questFlags.Contains(Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0].ToLower(), true).cellLock.key))
                            {
                                if (!chr.contentFlags.Contains(Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0].ToLower(), true).cellLock.key))
                                {
                                    chr.WriteToDisplay("You dont meet the requirements to open the door.");
                                    break;
                                }
                            }
                        }

                        Map.CheckSecretDoor(Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0].ToLower(), true));

                        if (chr.gender == Globals.eGender.Female)
                        {
                            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FemaleHmm));
                        }
                        else if (chr.gender == Globals.eGender.Male)
                        {
                            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MaleHmm));
                        }
                        break;
                }
            }
        }

        public void swap(string args)
        {
            Item tmpItemRight = new Item();
            Item tmpItemLeft = new Item();

            tmpItemRight = null;
            tmpItemLeft = null;

            // Pull stuff out of hands
            tmpItemRight = chr.RightHand;
            tmpItemLeft = chr.LeftHand;

            // Set it to null
            chr.RightHand = null;
            chr.LeftHand = null;

            // put stuff back in hands, reversing the hand it goes into
            chr.RightHand = tmpItemLeft;
            if (chr.RightHand != null && chr.RightHand.nocked && chr.RightHand.name != "crossbow") // un-nock opposite hand if not crossbow
            {
                chr.RightHand.nocked = false;
            } 
            chr.LeftHand = tmpItemRight;
            if (chr.LeftHand != null && chr.LeftHand.nocked && chr.LeftHand.name != "crossbow") // un-nock opposite hand if not crossbow
            {
                chr.LeftHand.nocked = false;
            } 

        }

        public void a(string args)
        {
            again(args);
        }

        public void again(string args)
        {
            chr.inputCommandQueue.Enqueue(chr.LastCommand); //add it to the command queue
        }

        public void sweep(string args)
        {
            chr.cmdWeight += 2;
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (chr.RightHand == null)
            {
                chr.WriteToDisplay("You do not have a broom in your right hand.");
                return;
            }
            if (args == null)
            {
                chr.WriteToDisplay("Sweep where?");
                return;
            }

            Item[] tempItemList = Item.GetAllItemsFromGround(chr.CurrentCell);

            string[] sArgs = args.Split(" ".ToCharArray());

            if(sArgs[0].ToLower() == "u" || sArgs[0].ToLower() == "up") {
                if (chr.CurrentCell.CellGraphic == "up")
                {
                    chr.CommandType = CommandType.Movement;
                    Map.MoveChar(chr, "up", "sweep");
                    foreach (Item item in tempItemList)
                    {
                        chr.CurrentCell.Add(item);
                    }
                    return;
                }
                else
                {
                    chr.WriteToDisplay("You don't see any stairs here.");
                    return;
                }
            }
            if(sArgs[0].ToLower() == "d" || sArgs[0].ToLower() == "down") {
                if (chr.CurrentCell.CellGraphic == "dn")
                {
                    chr.CommandType = CommandType.Movement;
                    Map.MoveChar(chr, "down", "sweep");
                    foreach (Item item in tempItemList)
                    {
                        chr.CurrentCell.Add(item);
                    }
                    return;
                }
                else
                {
                    chr.WriteToDisplay("You don't see any stairs here.");
                    return;
                }
            }

            chr.CommandType = CommandType.Movement;
            Map.MoveChar(chr, sArgs[0], "sweep");
            foreach (Item item in tempItemList)
            {
                chr.CurrentCell.Add(item);
            }

        }

        public void l(string args)
        {
            look(args);
        }

        public void look(string args)
        {
            if (chr.IsBlind) // character is blind
            {
                chr.WriteToDisplay("You are blind and cannot see!");
                return;
            }

            string lookTarget = null;
            string lookin = null;
            string lookNum = null;
            string lookPrep = null;

            // 0 = cell description and item list
            // 1 = look IN something
            // 2 = look AT something
            // 3 = look in a direction
            // 4 = 
            // 5 = look at # item in something
            // 6 =
            // 7 = look ON something

            int lookType = 0;
            int countTo = 0;
            bool lookClosely = false;

            if (args != null && args.ToLower() == "around")
            {
                if (chr.CurrentCell.Description.Length > 0)
                {
                    chr.WriteToDisplay(chr.CurrentCell.Description);
                }
                else
                {
                    chr.WriteToDisplay("You look around the area.");
                }
                return;
            }
            if (args == null || args.Length == 0)
            {
                lookType = 0;
            }
            else
            {
                String[] sArgs = args.Split(" ".ToCharArray());

                sArgs[0] = sArgs[0].ToLower();

                if (sArgs[0].Equals("in"))
                {
                    lookType = 1;
                    lookTarget = sArgs[1].ToLower();
                }
                else if (sArgs[0].Equals("closely"))
                {
                    lookType = 2;
                    lookTarget = sArgs[2].ToLower();
                    lookClosely = true;
                }
                else if (sArgs[0].Equals("at"))
                {
                    if (sArgs.Length < 2)
                    {
                        chr.WriteToDisplay("Look at what?");
                        return;
                    }
                    if (sArgs[1] == "ground") //Trap "look at ground"
                    {
                        lookType = 0;
                    }
                    else if (chr.CurrentCell.HasMirror && sArgs[1] == "mirror")
                    {
                        if (chr.CurrentCell.HasMirror)
                        {
                            chr.TargetName = chr.Name;
                            lookMessage(chr);
                            if (chr.IsHidden)
                                chr.IsHidden = false;
                            return;
                        }
                        else if ((chr.RightHand != null && chr.RightHand.name == "mirror") ||
                            (chr.LeftHand != null && chr.LeftHand.name == "mirror"))
                        {
                            chr.TargetName = chr.Name;
                            lookMessage(chr);
                            if (chr.IsHidden)
                                chr.IsHidden = false;
                            return;
                        }
                        else
                        {
                            chr.WriteToDisplay("You do not see a mirror here.");
                            return;
                        }
                    }
                    else if (sArgs.Length == 4)
                    {
                        lookType = 4;
                        lookTarget = sArgs[1].ToLower();
                        lookin = sArgs[3].ToLower();
                        lookPrep = sArgs[2].ToLower();
                    }
                    else if (sArgs.Length == 5)
                    {
                        lookType = 5;
                        lookTarget = sArgs[2];
                        lookNum = sArgs[1];
                        lookin = sArgs[4];
                    }
                    else if (sArgs.Length == 3)
                    {
                        if (sArgs[1].EndsWith("'s"))
                        {
                            lookType = 8;
                            lookin = sArgs[1].Substring(0, sArgs[1].Length - 2).ToLower();
                            lookTarget = sArgs[2];
                        }
                        else
                        {
                            lookType = 6;
                            lookTarget = sArgs[2];
                            lookNum = sArgs[1];
                        }
                    }
                    else
                    {
                        lookType = 2;
                        lookTarget = sArgs[1].ToLower();
                    }
                }
                else if (sArgs[0] == "n" || sArgs[0] == "north" || sArgs[0] == "e" || sArgs[0] == "east" ||
                    sArgs[0] == "s" || sArgs[0] == "south" || sArgs[0] == "w" || sArgs[0] == "west" ||
                    sArgs[0] == "nw" || sArgs[0] == "northwest" || sArgs[0] == "ne" || sArgs[0] == "northeast" ||
                    sArgs[0] == "se" || sArgs[0] == "southeast" || sArgs[0] == "sw" || sArgs[0] == "southwest")
                {
                    lookType = 3;
                    lookTarget = sArgs[0].ToLower();
                }
                else if (sArgs[0] == "on")
                {
                    if (sArgs[1] == "ground") //Trap "look on ground"
                    {
                        lookType = 0;
                    }
                    else
                    {
                        lookType = 7;
                        lookTarget = sArgs[1].ToLower();
                    }
                }
                else
                {
                    lookType = 2;
                    lookTarget = sArgs[0].ToLower();
                }
            }

            switch (lookType)
            {
                case 0:
                    #region Look at Room
                    string dispMsg = "You see nothing on the ground.";
                    bool moreThanOne = false;
                    int itemcount = 0;
                    int i = 0;
                    int a = 0;
                    int z = 0;

                    List<Item> tempItemList = chr.CurrentCell.Items;
                    if (chr.CurrentCell.Items.Count > 0) // loop through all the items in the Cell the Character is in
                    {
                        ArrayList templist = new ArrayList();
                        Item[] itemList = new Item[chr.CurrentCell.Items.Count];
                        chr.CurrentCell.Items.CopyTo(itemList);
                        foreach(Item item in itemList)
                        {
                            templist.Add(item);
                        }

                        z = templist.Count - 1;
                        dispMsg = "On the ground you see ";
                        while (z >= 0)
                        {
                            Item item = (Item)templist[z];

                            itemcount = 0;
                            for (i = templist.Count - 1; i > -1; i--)
                            {
                                Item tmpitem = (Item)templist[i];
                                if (tmpitem.name == item.name && tmpitem.name == "coins")
                                {
                                    templist.RemoveAt(i);
                                    itemcount = itemcount + (int)item.coinValue;
                                    z = templist.Count;

                                }
                                else if (tmpitem.name == item.name)
                                {
                                    templist.RemoveAt(i);
                                    z = templist.Count;
                                    itemcount += 1;
                                }

                            }
                            if (itemcount > 0)
                            {
                                if (moreThanOne == true)
                                {
                                    if (z == 0) // second to last item
                                    {
                                        dispMsg += " and ";
                                    }
                                    else
                                    {
                                        dispMsg += ", ";
                                    }
                                }
                                dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);

                            }
                            moreThanOne = true;
                            z--;
                        }

                        dispMsg += ".";
                        chr.WriteToDisplay(dispMsg);
                        break;
                    }
                    chr.WriteToDisplay(dispMsg);
                    break;
                    #endregion
                case 1:
                    #region Look in something
                    //Make sure we know what we are looking at
                    Item lookItem = new Item();
                    if (lookTarget == null)
                    {
                        chr.WriteToDisplay("Look in what?");
                        return;
                    }
                    else if (lookTarget == "left")
                    {
                        if (chr.LeftHand == null)
                        {
                            chr.WriteToDisplay("You have nothing in your left hand.");
                        }
                        else
                        {
                            lookItem = chr.LeftHand;
                            chr.WriteToDisplay("You are looking at " + lookItem.GetLookDescription(chr));
                        }
                    }
                    else if (lookTarget == "right")
                    {
                        if (chr.RightHand == null)
                        {
                            chr.WriteToDisplay("You have nothing in your right hand.");
                        }
                        else
                        {
                            lookItem = chr.RightHand;
                            chr.WriteToDisplay("You are looking at " + lookItem.GetLookDescription(chr));
                        }
                    }
                    else if (lookTarget == "sack")
                    {
                        Command.ParseCommand(chr, "show", "sack");
                    }
                    else if (lookTarget == "locker")
                    {
                        Command.ParseCommand(chr, "show", "locker");
                    }
                    return;
                    #endregion
                case 2:
                    #region Look at something
                    if (lookTarget == null || lookTarget == "")
                    {
                        chr.WriteToDisplay("Look at what?");
                        return;
                    }
                    else if (lookTarget == "mirror")
                    {
                        chr.TargetName = chr.Name;
                        lookMessage(chr);
                        if (chr.IsHidden)
                        {
                            chr.IsHidden = false;
                        }
                    }
                    else if (lookTarget == "left")
                    {
                        if (chr.LeftHand == null)
                        {
                            chr.WriteToDisplay("You have nothing in your left hand.");
                        }
                        else
                        {
                            lookItem = chr.LeftHand;
                            chr.WriteToDisplay("You are looking at " + lookItem.GetLookDescription(chr));
                        }
                    }
                    else if (lookTarget == "right")
                    {
                        if (chr.RightHand == null)
                        {
                            chr.WriteToDisplay("You have nothing in your right hand.");
                        }
                        else
                        {
                            lookItem = chr.RightHand;
                            chr.WriteToDisplay("You are looking at " + lookItem.GetLookDescription(chr));
                        }
                    }
                    else if (lookTarget == "sky")
                    {
                        if (!chr.CurrentCell.IsOutdoors)
                        {
                            chr.WriteToDisplay("You cannot see the sky.");
                            return;
                        }

                        string skydesc = "";
                        switch (World.CurrentDailyCycle)
                        {
                            case World.DailyCycle.Morning: skydesc = "The sun is rising."; break;
                            case World.DailyCycle.Afternoon: skydesc = "The sun is high in the afternoon sky."; break;
                            case World.DailyCycle.Evening: skydesc = "The sun is setting."; break;
                            default: skydesc = "It is night time."; break;

                        }
                        if (World.CurrentDailyCycle != World.DailyCycle.Afternoon)
                        {
                            skydesc += " The moon is " + World.CurrentLunarCycle.ToString().ToLower().Replace("_", " ") + ".";
                        }
                        chr.WriteToDisplay(skydesc);
                        return;
                    }
                    else
                    {
                        ////LOOK AT Items
                        //string disMsg = "";
                        Item tmpItem = new Item();
                        if (lookTarget == "" || lookTarget == null)
                        {
                            chr.WriteToDisplay("I dont understand what you want to do.");
                            return;
                        }
                        tmpItem = chr.getAnyItemInSight(lookTarget);  //get a copy of whatever item is in sight
                        if (tmpItem != null)
                        {
                            chr.WriteToDisplay("You are looking at " + tmpItem.GetLookDescription(chr));
                            break;
                        }
                        else
                        {
                            Character tmpNPC = Map.FindTargetInView(chr, lookTarget, false, false);
                            if (tmpNPC == null)
                            {
                                chr.WriteToDisplay("You don't see " + lookTarget + " here.");
                                break;
                            }
                            int perc = (int)(((float)tmpNPC.Hits / (float)tmpNPC.HitsFull) * 100);
                            string DamageDesc = "unhurt";
                            string align = "lawful";
                            string agedesc = "a young";
                            string armordesc = " is wearing no armor.";
                            string examinedesc = "";
                            string torso = "none";
                            string legs = "none";
                            string back = "none";
                            string head = "none";
                            string face = "none";
                            string neck = "none";
                            string shoulders = "none";
                            string gauntlets = "none";
                            string feet = "none";
                            string leftWrist = "none";
                            string rightWrist = "none";
                            string iscarrying = "";
                            string islookingatyou = "";
                            string isfollowingyou = "";
                            string isinatrance = "";
                            if (tmpNPC.Alignment == Globals.eAlignment.Lawful) { align = "a lawful"; }
                            if (tmpNPC.Alignment == Globals.eAlignment.Neutral) { align = "a neutral"; }
                            if (tmpNPC.Alignment == Globals.eAlignment.Chaotic) { align = "a chaotic"; }
                            if (tmpNPC.Alignment == Globals.eAlignment.Evil) { align = "an evil"; }
                            if (tmpNPC.Alignment == Globals.eAlignment.Amoral) { align = "an amoral"; }
                            if (tmpNPC.Age != 0)
                            {
                                agedesc = tmpNPC.getAgeDescription(true);
                            }
                            if (perc >= 0 && perc < 11) DamageDesc = "near death";
                            else if (perc >= 11 && perc < 31) DamageDesc = "badly wounded";
                            else if (perc >= 31 && perc < 71) DamageDesc = "wounded";
                            else if (perc >= 71 && perc < 99) DamageDesc = "barely wounded";
                            else if (perc >= 100) DamageDesc = "unhurt";

                            foreach (Item wear in tmpNPC.wearing)
                            {
                                switch (wear.wearLocation)
                                {
                                    case Globals.eWearLocation.Head: // head examine
                                        if (lookClosely) { head = wear.longDesc; }
                                        break;
                                    case Globals.eWearLocation.Neck: // neck examine
                                        if (lookClosely) { neck = wear.longDesc; }
                                        break;
                                    case Globals.eWearLocation.Face: // face examine
                                        if (lookClosely) { face = wear.longDesc; }
                                        break;
                                    case Globals.eWearLocation.Shoulders: // shoulders examine
                                        if (lookClosely) { shoulders = wear.longDesc; }
                                        break;
                                    case Globals.eWearLocation.Wrist:
                                        if (lookClosely)
                                        {
                                            if (wear.wearOrientation == Globals.eWearOrientation.Right)
                                                rightWrist = wear.longDesc;
                                            else if (wear.wearOrientation == Globals.eWearOrientation.Left)
                                                leftWrist = wear.longDesc;
                                        }
                                        break;
                                    case Globals.eWearLocation.Back: // back
                                        if (wear.baseType == Globals.eItemBaseType.Armor)
                                        {
                                            if (tmpNPC.GetInventoryItem(Globals.eWearLocation.Torso) != null)
                                            {
                                                back = " is wearing " + wear.longDesc + " over ";
                                            }
                                            else
                                            {
                                                back = " is wearing " + wear.longDesc + ".";
                                            }
                                        }
                                        else
                                        {
                                            if (tmpNPC.GetInventoryItem(Globals.eWearLocation.Torso) != null)
                                            {
                                                back = " has " + wear.longDesc + " strapped to " + Character.possessive[(int)tmpNPC.gender].ToLower() + " back. " + Character.pronoun[(int)tmpNPC.gender] + " is wearing ";
                                            }
                                            else
                                            {
                                                back = " has " + wear.longDesc + " strapped to " + Character.possessive[(int)tmpNPC.gender].ToLower() + " back. ";
                                            }
                                        }
                                        break;
                                    case Globals.eWearLocation.Torso: // torso
                                        torso = wear.longDesc;
                                        break;
                                    case Globals.eWearLocation.Legs: // legs
                                        legs = wear.longDesc;
                                        break;
                                    case Globals.eWearLocation.Feet: // feet
                                        if (lookClosely) { feet = wear.longDesc; }
                                        break;
                                    case Globals.eWearLocation.Hands: // gauntlets examine
                                        if (lookClosely) { gauntlets = wear.longDesc; }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (back != "none")
                            {
                                if (torso != "none")
                                {
                                    armordesc = back + torso + ".";
                                }
                                if (legs != "none")
                                {
                                    armordesc = back + torso + " and " + legs + ".";
                                }
                                //else
                                //{
                                //    armordesc = back.Substring(0, back.Length - 6) + ".";
                                //}
                            }
                            if (back == "none")
                            {
                                if (torso != "none")
                                {
                                    if (legs != "none")
                                    {
                                        armordesc = " is wearing " + torso + " and " + legs + ".";
                                    }
                                    else
                                    {
                                        armordesc = " is wearing " + torso + ".";
                                    }
                                }
                                if (torso == "none")
                                {
                                    if (legs != "none")
                                    {
                                        armordesc = " is wearing " + legs + ".";
                                    }
                                }
                            }
                            //get the examine desc if command is LOOK CLOSELY AT
                            if (head != "none")
                            {
                                examinedesc = " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " head is " + head + ".";
                            }
                            if (neck != "none")
                            {
                                examinedesc = examinedesc + " Around " + Character.possessive[(int)tmpNPC.gender].ToLower() + " neck is " + neck + ".";
                            }
                            if (shoulders != "none")
                            {
                                examinedesc = examinedesc + " Draped over " + Character.possessive[(int)tmpNPC.gender].ToLower() + " shoulders is " + shoulders + ".";
                            }
                            if (rightWrist != "none")
                            {
                                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " right wrist is " + rightWrist + ".";
                            }
                            if (leftWrist != "none")
                            {
                                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " left wrist is " + leftWrist + ".";
                            }
                            if (gauntlets != "none")
                            {
                                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " hands are " + gauntlets + ".";
                            }
                            if (feet != "none")
                            {
                                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " feet is " + feet + ".";
                            }
                            //what the npc is carrying
                            if (tmpNPC.RightHand != null && tmpNPC.LeftHand == null)
                            {
                                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.RightHand.shortDesc + ".";
                            }
                            if (tmpNPC.RightHand == null && tmpNPC.LeftHand != null)
                            {
                                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.LeftHand.shortDesc + ".";
                            }
                            if (tmpNPC.RightHand != null && tmpNPC.LeftHand != null)
                            {
                                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.RightHand.shortDesc + " and " + tmpNPC.LeftHand.shortDesc + ".";
                            }
                            //if this is a player
                            if (tmpNPC.IsPC)
                            {
                                //is the LOOKer a knight? do we fool them into thinking we're not a thief?
                                string pcClass = tmpNPC.classFullName.ToLower();
                                switch (tmpNPC.BaseProfession)
                                {
                                    case Character.ClassType.Thief:
                                        if (Rules.DetectThief(tmpNPC, chr) == Character.ClassType.Thief) { pcClass = "thief"; }
                                        else { pcClass = "fighter"; }
                                        break;
                                }
                                if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                                {
                                    islookingatyou = " " + tmpNPC.Name + " is looking at you.";
                                }
                                else if (tmpNPC.TargetName.Length > 0)
                                {
                                    if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                    {
                                        islookingatyou = " " + tmpNPC.Name + " is looking at " + tmpNPC.TargetName + ".";
                                    }
                                }
                                if (tmpNPC.FollowName == chr.Name)
                                {
                                    isfollowingyou = " " + tmpNPC.Name + " is following you.";
                                }
                                else if (tmpNPC.FollowName.Length > 0)
                                {
                                    if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                    {
                                        isfollowingyou = " " + tmpNPC.Name + " is following " + tmpNPC.FollowName + ".";
                                    }
                                }
                                if (tmpNPC.Stunned > 0)
                                {
                                    islookingatyou = " " + tmpNPC.Name + " is stunned.";
                                    isfollowingyou = "";
                                }
                                if (tmpNPC.IsWizardEye || tmpNPC.IsPeeking)
                                { isinatrance = " " + tmpNPC.Name + " is in a trance."; }
                                chr.TargetName = tmpNPC.Name;
                                chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.gender.ToString().ToLower() + " " + pcClass + " from " + tmpNPC.race + ". " + tmpNPC.Name + armordesc + iscarrying + examinedesc + " " + tmpNPC.Name + " is " + DamageDesc + "." + isfollowingyou + islookingatyou + isinatrance);
                            }
                            //get npc class if not a player
                            else
                            {
                                string lookclass = "fighter";
                                switch (tmpNPC.BaseProfession)
                                {
                                    case Character.ClassType.Knight:
                                        lookclass = "knight";
                                        break;
                                    case Character.ClassType.Martial_Artist:
                                        lookclass = "martial artist";
                                        break;
                                    case Character.ClassType.Thaumaturge:
                                        lookclass = "thaumaturge";
                                        break;
                                    case Character.ClassType.Wizard:
                                        lookclass = "wizard";
                                        break;
                                    case Character.ClassType.Thief:
                                        if (Rules.DetectThief(tmpNPC, chr) == Character.ClassType.Thief)
                                        {
                                            lookclass = "thief";
                                            align = tmpNPC.Alignment.ToString().ToLower();
                                        }
                                        else
                                        {
                                            lookclass = "fighter";
                                            align = "lawful";
                                        }
                                        break;
                                    case Character.ClassType.Fighter:
                                    default:
                                        lookclass = "fighter";
                                        break;
                                }
                                // npc is a dragon or drake
                                if (tmpNPC.species == Globals.eSpecies.FireDragon ||
                                    tmpNPC.species == Globals.eSpecies.IceDragon ||
                                    tmpNPC.species == Globals.eSpecies.LightningDrake ||
                                    tmpNPC.species == Globals.eSpecies.TundraYeti)
                                {
                                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                                    }
                                    else if (tmpNPC.TargetName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            islookingatyou = " " + tmpNPC.Name + " is looking at " + tmpNPC.TargetName + ".";
                                        }
                                    }
                                    if (tmpNPC.FollowName == chr.Name)
                                    {
                                        isfollowingyou = " " + tmpNPC.Name + " is following you.";
                                    }
                                    else if (tmpNPC.FollowName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            isfollowingyou = " " + tmpNPC.Name + " is following " + tmpNPC.FollowName + ".";
                                        }
                                    }
                                    if (tmpNPC.Stunned > 0)
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is stunned.";
                                        isfollowingyou = "";
                                    }
                                    if (tmpNPC.IsWizardEye || tmpNPC.IsPeeking)
                                    { isinatrance = " " + tmpNPC.Name + " is in a trance."; }
                                    chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + isfollowingyou + islookingatyou + isinatrance);
                                }
                                //npc has a race
                                else if (!tmpNPC.IsPC && tmpNPC.race != "")
                                {
                                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                                    {
                                        islookingatyou = " " + tmpNPC.Name + " is looking at you.";
                                    }
                                    else if (tmpNPC.TargetName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            islookingatyou = " " + tmpNPC.Name + " is looking at " + tmpNPC.TargetName + ".";
                                        }
                                    }
                                    if (tmpNPC.FollowName == chr.Name)
                                    {
                                        isfollowingyou = " " + tmpNPC.Name + " is following you.";
                                    }
                                    else if (tmpNPC.FollowName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            isfollowingyou = " " + tmpNPC.Name + " is following " + tmpNPC.FollowName + ".";
                                        }
                                    }
                                    if (tmpNPC.Stunned > 0)
                                    {
                                        islookingatyou = " " + tmpNPC.Name + " is stunned.";
                                        isfollowingyou = "";
                                    }
                                    if (tmpNPC.IsWizardEye || tmpNPC.IsPeeking)
                                    { isinatrance = " " + tmpNPC.Name + " is in a trance."; }
                                    chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.gender.ToString().ToLower() + " " + lookclass + " from " + tmpNPC.race + ". " + tmpNPC.Name + armordesc + iscarrying + examinedesc + " " + tmpNPC.Name + " is " + DamageDesc + "." + isfollowingyou + islookingatyou + isinatrance);
                                }
                                //animals
                                else if (tmpNPC.animal)
                                {
                                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                                    }
                                    else if (tmpNPC.TargetName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            islookingatyou = " The " + tmpNPC.Name + " is looking at " + tmpNPC.TargetName + ".";
                                        }
                                    }
                                    if (tmpNPC.FollowName == chr.Name)
                                    {
                                        isfollowingyou = " The " + tmpNPC.Name + " is following you.";
                                    }
                                    else if (tmpNPC.FollowName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            isfollowingyou = " The " + tmpNPC.Name + " is following " + tmpNPC.FollowName + ".";
                                        }
                                    }
                                    if (tmpNPC.Stunned > 0)
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is stunned.";
                                        isfollowingyou = "";
                                    }
                                    if (tmpNPC.IsWizardEye || tmpNPC.IsPeeking)
                                    { isinatrance = " The " + tmpNPC.Name + " is in a trance."; }
                                    chr.WriteToDisplay("You are looking at " + align + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + isfollowingyou + islookingatyou + isinatrance);
                                }
                                //all other npcs
                                else
                                {
                                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                                    }
                                    else if (tmpNPC.TargetName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            islookingatyou = " The " + tmpNPC.Name + " is looking at " + tmpNPC.TargetName + ".";
                                        }
                                    }
                                    if (tmpNPC.FollowName == chr.Name)
                                    {
                                        isfollowingyou = " The " + tmpNPC.Name + " is following you.";
                                    }
                                    else if (tmpNPC.FollowName.Length > 0)
                                    {
                                        if (Map.FindTargetInView(chr, tmpNPC.TargetName, false, false) != null)
                                        {
                                            isfollowingyou = " The " + tmpNPC.Name + " is following " + tmpNPC.FollowName + ".";
                                        }
                                    }
                                    if (tmpNPC.Stunned > 0)
                                    {
                                        islookingatyou = " The " + tmpNPC.Name + " is stunned.";
                                        isfollowingyou = "";
                                    }
                                    if (tmpNPC.IsWizardEye || tmpNPC.IsPeeking)
                                    { isinatrance = " The " + tmpNPC.Name + " is in a trance."; }
                                    chr.WriteToDisplay("You are looking at " + align + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + armordesc + iscarrying + examinedesc + " " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + isfollowingyou + islookingatyou + isinatrance);
                                }
                            }
                        }
                    }
                    break;
                    #endregion
                case 3:
                    # region Look at something in a direction
                    moreThanOne = false;
                    // loop through all the items in the Cell the player is in

                    Cell cll = Map.GetCellRelevantToCell(chr.CurrentCell, lookTarget, false);

                    if (cll.Items.Count > 0)
                    {
                        ArrayList templist = new ArrayList();
                        Item[] itemList = new Item[cll.Items.Count];
                        cll.Items.CopyTo(itemList);
                        foreach (Item item in itemList)
                        {
                            templist.Add(item);
                        }
                        z = templist.Count - 1;
                        if (cll.CellGraphic == "CC")
                        {
                            dispMsg = "On the counter you see ";
                            if (templist.Count == 0) { dispMsg = "You see nothing on the counter"; }
                        }
                        else if (cll.CellGraphic == "MM")
                        {
                            dispMsg = "On the altar you see ";
                            if (templist.Count == 0) { dispMsg = "You see nothing on the altar"; }
                        }
                        else
                        {
                            dispMsg = "On the ground you see ";
                            if (templist.Count == 0) { dispMsg = "You see nothing on the ground"; }
                        }

                        while (z >= 0)
                        {

                            Item item = (Item)templist[z];

                            itemcount = 0;
                            for (i = templist.Count - 1; i > -1; i--)
                            {
                                Item tmpitem = (Item)templist[i];
                                if (tmpitem.name == item.name && tmpitem.itemType == Globals.eItemType.Coin)
                                {
                                    templist.RemoveAt(i);
                                    itemcount = itemcount + (int)item.coinValue;
                                    z = templist.Count;

                                }
                                else if (tmpitem.name == item.name)
                                {
                                    templist.RemoveAt(i);
                                    z = templist.Count;
                                    itemcount += 1;
                                }

                            }
                            if (itemcount > 0)
                            {
                                if (moreThanOne == true)
                                {
                                    if (z == 0) // second to last item
                                    {
                                        dispMsg += " and ";
                                    }
                                    else
                                    {
                                        dispMsg += ", ";
                                    }
                                }
                                dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);

                            }
                            moreThanOne = true;
                            z--;
                        }
                        dispMsg += ".";
                        chr.WriteToDisplay(dispMsg);
                    }
                    else
                    {
                        if (cll.CellGraphic == "CC")
                        {
                            chr.WriteToDisplay("You see nothing on the counter.");
                        }
                        else if (cll.CellGraphic == "MM")
                        {
                            chr.WriteToDisplay("You see nothing on the altar.");
                        }
                        else
                        {
                            chr.WriteToDisplay("You see nothing on the ground.");
                        }
                    }
                    break;
                    #endregion
                case 4:
                    #region Look at something on something
                    dispMsg = "You don't see that here.";
                    if (lookin == "ground")
                    {
                        if (Item.FindItemOnGround(lookTarget, chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z) != null)
                        {
                            Item findItem = Item.FindItemOnGround(lookTarget, chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                            if (lookTarget.IndexOf("coin") != -1)
                            {
                                dispMsg = "You are looking at " + (int)findItem.coinValue + " coins.";
                            }
                            else
                            {
                                dispMsg = "You are looking at " + findItem.GetLookDescription(chr);
                            }
                        }
                        chr.WriteToDisplay(dispMsg);
                        return;
                    }
                    if (lookin == "left" || lookin == "right")
                    {
                        if (lookin == "left")
                        {
                            if (lookTarget == "ring" && lookPrep == "on") //Trap Look at ring on left
                            {
                                int ringnum = chr.findFirstLeftRing();
                                Item ringitem = null;
                                if (ringnum == 0)
                                {
                                    chr.WriteToDisplay("You aren't wearing any rings on that hand.");
                                    return;
                                }
                                switch (ringnum)
                                {
                                    case 1:
                                        ringitem = chr.LeftRing1;
                                        break;
                                    case 2:
                                        ringitem = chr.LeftRing2;
                                        break;
                                    case 3:
                                        ringitem = chr.LeftRing3;
                                        break;
                                    case 4:
                                        ringitem = chr.LeftRing4;
                                        break;
                                }
                                chr.WriteToDisplay("You see a " + ringitem.longDesc + ".");
                                return;
                            }
                            if (chr.LeftHand != null && chr.LeftHand.name == lookTarget)
                            {
                                if (lookTarget.IndexOf("coin") != -1)
                                {
                                    dispMsg = "You are looking at " + (int)chr.LeftHand.coinValue + " coins.";
                                }
                                else
                                {
                                    dispMsg = "You are looking at " + chr.LeftHand.GetLookDescription(chr);
                                }
                            }
                            else
                            {
                                dispMsg = "You don't see a " + lookTarget + " in your left hand.";
                            }
                        }
                        else
                        {
                            if (lookTarget == "ring" && lookPrep == "on") //Trap Look at ring on right
                            {
                                int ringnum = chr.findFirstRightRing();
                                Item ringitem = null;
                                if (ringnum == 0)
                                {
                                    chr.WriteToDisplay("You aren't wearing any rings on that hand.");
                                    return;
                                }
                                switch (ringnum)
                                {
                                    case 1:
                                        ringitem = chr.RightRing1;
                                        break;
                                    case 2:
                                        ringitem = chr.RightRing2;
                                        break;
                                    case 3:
                                        ringitem = chr.RightRing3;
                                        break;
                                    case 4:
                                        ringitem = chr.RightRing4;
                                        break;
                                }
                                chr.WriteToDisplay("You see a " + ringitem.longDesc + ".");
                                return;
                            }
                            if (chr.RightHand != null && chr.RightHand.name == lookTarget)
                            {
                                if (lookTarget.IndexOf("coin") != -1)
                                {
                                    dispMsg = "You are looking at " + (int)chr.RightHand.coinValue + " coins.";
                                }
                                else
                                {
                                    dispMsg = "You are looking at " + chr.RightHand.GetLookDescription(chr);

                                }
                            }
                            else
                            {
                                dispMsg = "You don't see a " + lookTarget + " in your right hand.";
                            }
                        }
                        chr.WriteToDisplay(dispMsg);
                        return;
                    }
                    List<Item> tmpList;
                    if (lookin == "counter" || lookin == "altar")
                    {
                        tmpList = Map.getCopyOfAllItemsFromCounter(chr);
                        if (tmpList == null)
                        {
                            chr.WriteToDisplay("I see no " + lookin + " here.");
                            return;
                        }
                        else
                        {
                            dispMsg = "You don't see that on the " + lookin + ".";
                        }
                    }
                    else if (lookin == "locker")
                    {
                        if (!chr.CurrentCell.IsLocker)
                        {
                            chr.WriteToDisplay("I see no " + lookin + " here.");
                            return;
                        }
                        tmpList = chr.lockerList;
                        dispMsg = "You dont see that in your " + lookin + ".";
                    }
                    else if (lookin == "belt")
                    {
                        tmpList = chr.beltList;
                        dispMsg = "You don't see that on your " + lookin + ".";
                    }
                    else if (lookin == "sack")
                    {
                        tmpList = chr.sackList;
                        dispMsg = "You don't see that in your " + lookin + ".";
                    }
                    else
                    {
                        chr.WriteToDisplay("I don't understand that command.");
                        return;
                    }
                    for (i = tmpList.Count - 1; i > -1; i--)
                    {
                        Item tmpitem = tmpList[i];
                        if (tmpitem.name == "coins" && lookTarget == "coins")
                        {
                            dispMsg = "You are looking at " + (int)tmpitem.coinValue + " coins.";
                            break;
                        }
                        else if (tmpitem.name == lookTarget)
                        {
                            dispMsg = "You are looking at " + tmpitem.GetLookDescription(chr);
                        }
                    }
                    chr.WriteToDisplay(dispMsg);
                    break;
                    #endregion
                case 5:
                    #region Look at # something in something
                    string[] sArgs = args.Split(" ".ToCharArray());
                    sArgs[0] = sArgs[0].ToLower();
                    dispMsg = "You don't see that in your " + lookin + ".";
                    if (sArgs[4] == "right" || sArgs[4] == "left")
                    {
                        int whichring = Convert.ToInt32(lookNum);
                        if (whichring >= 5) { chr.WriteToDisplay("You do not have that many fingers."); return; }
                        switch (sArgs[4])
                        {
                            case "right":
                                if (whichring == 1) { if (chr.RightRing1 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.RightRing1.longDesc + "."; if (chr.RightRing1.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 2) { if (chr.RightRing2 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.RightRing2.longDesc + "."; if (chr.RightRing2.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 3) { if (chr.RightRing3 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.RightRing3.longDesc + "."; if (chr.RightRing3.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 4) { if (chr.RightRing4 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.RightRing4.longDesc + "."; if (chr.RightRing4.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring <= 0) { dispMsg = "Cant find that ring."; }
                                break;
                            case "left":
                                if (whichring == 1) { if (chr.LeftRing1 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.LeftRing1.longDesc + "."; if (chr.LeftRing1.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 2) { if (chr.LeftRing2 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.LeftRing2.longDesc + "."; if (chr.LeftRing2.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 3) { if (chr.LeftRing3 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.LeftRing3.longDesc + "."; if (chr.LeftRing3.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring == 4) { if (chr.LeftRing4 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; } dispMsg = "You are looking at " + chr.LeftRing4.longDesc + "."; if (chr.LeftRing4.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; } }
                                else if (whichring <= 0) { dispMsg = "Cant find that ring."; }
                                break;
                            default:
                                break;
                        }
                        chr.WriteToDisplay(dispMsg);
                        return;
                    }
                    countTo = Convert.ToInt32(lookNum);
                    if (lookin == "counter" || lookin == "altar")
                    {
                        tmpList = Map.getCopyOfAllItemsFromCounter(chr);
                        if (tmpList == null)
                        {
                            chr.WriteToDisplay("I see no " + lookin + " here.");
                            return;
                        }
                        else
                        {
                            dispMsg = "You don't see that on the " + lookin + ".";
                        }
                    }
                    else if (lookin == "locker")
                    {
                        if (!chr.CurrentCell.IsLocker)
                        {
                            chr.WriteToDisplay("I see no " + lookin + " here.");
                            return;
                        }
                        tmpList = chr.lockerList;//.Clone();
                        dispMsg = "You don't see that in your " + lookin + ".";
                    }
                    else if (lookin == "belt")
                    {
                        tmpList = chr.beltList;//.Clone();
                        dispMsg = "You don't see that on your " + lookin + ".";
                    }
                    else if (lookin == "ground")
                    {
                        tmpList = chr.CurrentCell.Items;//.Clone();
                        dispMsg = "You don't see that on the " + lookin + ".";
                    }
                    else //default to sack
                    {
                        tmpList = chr.sackList;
                        dispMsg = "You don't see that in your " + lookin + ".";
                    }

                    //z = tmpList.Count - 1;
                    itemcount = 0;
                    for (a = 0; a < tmpList.Count; a++) // loop through the items in the list
                    {
                        Item item = tmpList[a]; // copy the item
                        if (item.name == lookTarget) // this is the name of the item we're looking for
                        {
                            itemcount++;
                            if (itemcount == countTo)
                            {
                                dispMsg = "You are looking at " + item.GetLookDescription(chr);
                                break;
                            }
                        }
                    }


                    //while (z >= 0)
                    //{

                    //    Item item = (Item)tmpList[z]; // copy the item
                    //    itemcount = 0;

                    //    for (i = tmpList.Count - 1; i > -1; i--)
                    //    {
                    //        Item tmpitem = (Item)tmpList[i];
                    //        if (tmpitem.name == item.name && lookTarget.IndexOf("coin") != -1)
                    //        {
                    //            tmpList.RemoveAt(i);
                    //            dispMsg = "You are looking at " + (int)item.coinValue + " coins.";
                    //            z = tmpList.Count;

                    //        }
                    //        else if (tmpitem.name == lookTarget)
                    //        {
                    //            tmpList.RemoveAt(i);
                    //            z = tmpList.Count;

                    //            if (itemcount == countTo - 1)
                    //            {
                    //                dispMsg = "You are looking at " + tmpitem.longDesc + ".";
                    //                if (tmpitem.name == "bottle" || tmpitem.name == "balm") { dispMsg += " " + Bottle.getFluidDesc(tmpitem.special); }
                    //                if (tmpitem.special.IndexOf("blueglow") > -1) { dispMsg += " It is emitting a faint blue glow."; }
                    //                if (chr.classType == Character.ClassType.Thief)
                    //                {
                    //                    switch (tmpitem.baseType)
                    //                    {
                    //                        case Item.BaseType.Amulet:
                    //                        case Item.BaseType.Ring:
                    //                        case Item.BaseType.Gem:
                    //                        case Item.BaseType.Bracer:
                    //                        case Item.BaseType.Figurine:
                    //                            if (tmpitem.coinValue == 0)
                    //                            { dispMsg += " The " + tmpitem.name + " is worthless."; }
                    //                            else if (tmpitem.coinValue == 1)
                    //                            { dispMsg += " The " + tmpitem.name + " is worth " + tmpitem.coinValue + " coin."; }
                    //                            else { dispMsg += " The " + tmpitem.name + " is worth " + tmpitem.coinValue + " coins."; }
                    //                            break;
                    //                        default:
                    //                            break;
                    //                    }
                    //                }
                    //            }
                    //            itemcount += 1;
                    //        }

                    //    }

                    //    moreThanOne = true;
                    //    z--;
                    //}
                    chr.WriteToDisplay(dispMsg);
                    break;
                    #endregion
                case 6:
                    #region Look at # something
                    try
                    {
                        countTo = Convert.ToInt32(lookNum);
                    }
                    catch
                    {
                        Utils.Log("Command.look(" + args + ")", Utils.LogType.CommandFailure);
                        chr.WriteToDisplay(lookNum + " is not a number.");
                        return;
                    }

                    dispMsg = "You don't see that here.";

                    //ArrayList tmList = (ArrayList)chr.CurrentCell.cellItemList.Clone();
                    itemcount = 0;
                    for (a = 0; a < chr.CurrentCell.Items.Count; a++)
                    {
                        Item item = chr.CurrentCell.Items[a];
                        if (item.name == lookTarget)
                        {
                            itemcount++;
                            if (itemcount == countTo)
                            {
                                dispMsg = "You are looking at " + item.GetLookDescription(chr);
                                break;
                            }
                        }
                    }

                    if (dispMsg == "You don't see that here.")
                    {
                        // Put check for multi critters here.
                        itemcount = 0;
                        ArrayList crSeen = new ArrayList();
                        
                        int sublen = lookTarget.Length;

                        Cell[] cellArray = Cell.GetVisibleCellArray(chr.CurrentCell, 3);

                        for (int j = 0; j < 49; j++)
                        {
                            if (cellArray[j] == null || chr.CurrentCell.visCells[j] == false)
                            {
                                // do nothing
                            }
                            else
                            {
                                // create array of targets
                                if (cellArray[j].Characters.Count > 0)
                                {
                                    foreach (Character ch in cellArray[j].Characters)
                                    {
                                        //
                                        if (ch != chr && !ch.IsDead && Rules.DetectHidden(ch, chr) && Rules.DetectInvisible(ch, chr))
                                        {
                                            crSeen.Add(ch);
                                        }
                                    }
                                }
                            }
                        }

                        foreach (Character tempchar in crSeen)
                        {
                            if (tempchar.Name.ToLower().Substring(0, sublen) == lookTarget.ToLower())
                            {
                                if (countTo - 1 == itemcount)
                                {
                                    lookMessage(tempchar);
                                    return;
                                }
                                itemcount++;
                            }
                        }
                    }
                    chr.WriteToDisplay(dispMsg);

                    break;
                    #endregion
                case 7:
                    #region Look on something

                    moreThanOne = false;

                    List<Item> TmpList = Map.getCopyOfAllItemsFromCounter(chr);

                    if (TmpList == null)
                    {
                        if (lookTarget.ToLower() == "altar")
                        {
                            chr.WriteToDisplay("You do not see an altar here.");
                            return;
                        }
                        else
                        {
                            chr.WriteToDisplay("You do not see a " + lookTarget + " here.");
                            return;
                        }
                    }
                    if (TmpList.Count > 0)
                    {
                        z = TmpList.Count - 1;

                        dispMsg = "On the " + lookTarget + " you see ";

                        while (z >= 0)
                        {
                            Item item = (Item)TmpList[z];

                            itemcount = 0;
                            for (i = TmpList.Count - 1; i > -1; i--)
                            {
                                Item tmpitem = (Item)TmpList[i];
                                if (tmpitem.name == item.name && tmpitem.itemType == Globals.eItemType.Coin)
                                {
                                    TmpList.RemoveAt(i);
                                    itemcount = itemcount + (int)item.coinValue;
                                    z = TmpList.Count;

                                }
                                else if (tmpitem.name == item.name)
                                {
                                    TmpList.RemoveAt(i);
                                    z = TmpList.Count;
                                    itemcount++;
                                }

                            }
                            if (itemcount > 0)
                            {
                                if (moreThanOne)
                                {
                                    if (z == 0) // second to last item
                                    {
                                        dispMsg += " and ";
                                    }
                                    else
                                    {
                                        dispMsg += ", ";
                                    }
                                }
                                dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);
                            }
                            moreThanOne = true;
                            z--;
                        }

                    }
                    else
                    {
                        dispMsg = "You see nothing on the "+lookTarget;
                    }
                    dispMsg += ".";
                    chr.WriteToDisplay(dispMsg);
                    break;
                    #endregion
                case 8:
                    #region Look at someone's item
                    {
                        Character target = Map.FindTargetInView(chr, lookin, false, false);
                        string the = "";
                        if (target.race == "") { the = "The "; }

                        if (target == null)
                        {
                            chr.WriteToDisplay("You don't see " + lookin + " here.");
                            break;
                        }
                        if (lookTarget == null)
                        {
                            chr.WriteToDisplay("Look at " + lookin + "'s what?");
                            return;
                        }
                        else if (lookTarget.ToLower() == "right" || (target.RightHand != null && lookTarget == target.RightHand.name.ToLower()))
                        {
                            if (target.RightHand == null)
                            {
                                chr.WriteToDisplay("You see nothing in " + the.ToLower() + target.Name + "'s right hand.");
                            }
                            else
                            {
                                if (target.RightHand.name.ToLower() == "coins")
                                {
                                    chr.WriteToDisplay("In " + the.ToLower() + target.Name + "'s right hand you see gold coins.");
                                }
                                else
                                {
                                    chr.WriteToDisplay("In " + the.ToLower() + target.Name + "'s right hand you see " + target.RightHand.GetLookDescription(chr));
                                }
                            }
                        }
                        else if (lookTarget.ToLower() == "left" || (target.LeftHand != null && lookTarget.ToLower() == target.LeftHand.name.ToLower()))
                        {
                            if (target.LeftHand == null)
                            {
                                chr.WriteToDisplay("You see nothing in " + the.ToLower() + target.Name + "'s left hand.");
                            }
                            else
                            {
                                if (target.LeftHand.name.ToLower() == "coins")
                                {
                                    chr.WriteToDisplay("In " + the.ToLower() + target.Name + "'s left hand you see gold coins.");
                                }
                                else
                                {
                                    chr.WriteToDisplay("In " + the.ToLower() + target.Name + "'s left hand you see " + target.LeftHand.GetLookDescription(chr));
                                }
                            }
                        }
                        else
                        {
                            bool match = false;
                            foreach (Item wItem in target.wearing)
                            {
                                if (lookTarget.ToLower() == wItem.name.ToLower())
                                {
                                    chr.WriteToDisplay("You are looking at " + wItem.GetLookDescription(chr));
                                    match = true;
                                    break;
                                }
                            }
                            if (!match)
                            {
                                chr.WriteToDisplay(the + target.Name + " is not holding or wearing that.");
                            }
                        }
                    }
                    break;
                    #endregion
                default:
                    break;
            }
            return;
        }

        private void lookMessage(Character tmpNPC)
        {
            if (tmpNPC.IsWizardEye)
            {
                chr.WriteToDisplay("You are looking at a " + tmpNPC.Alignment.ToString().ToLower() + " " + tmpNPC.Name + ". The " + tmpNPC.Name + " is unhurt.");
                return;
            }

            int perc = (int)(((float)tmpNPC.Hits / (float)tmpNPC.HitsFull) * 100);
            bool lookClosely = false;
            string DamageDesc = "unhurt";
            string align = "lawful";
            string agedesc = "a young";
            string armordesc = " is wearing no armor.";
            string examinedesc = "";
            string torso = "none";
            string legs = "none";
            string back = "none";
            string head = "none";
            string face = "none";
            string neck = "none";
            string shoulders = "none";
            string gauntlets = "none";
            string feet = "none";
            string iscarrying = "";
            string islookingatyou = "";
            if (tmpNPC.Alignment == Globals.eAlignment.Lawful) { align = "a lawful"; }
            if (tmpNPC.Alignment == Globals.eAlignment.Neutral) { align = "a neutral"; }
            if (tmpNPC.Alignment == Globals.eAlignment.Chaotic) { align = "a chaotic"; }
            if (tmpNPC.Alignment == Globals.eAlignment.Evil) { align = "an evil"; }
            if (tmpNPC.Alignment == Globals.eAlignment.Amoral) { align = "an amoral"; }
            if (tmpNPC.Age != 0)
            {
                agedesc = tmpNPC.getAgeDescription(true);
            }
            if (perc >= 0 && perc < 11) DamageDesc = "near death";
            else if (perc >= 11 && perc < 31) DamageDesc = "badly wounded";
            else if (perc >= 31 && perc < 71) DamageDesc = "wounded";
            else if (perc >= 71 && perc < 99) DamageDesc = "barely wounded";
            else if (perc >= 100) DamageDesc = "unhurt";

            foreach (Item wear in tmpNPC.wearing)
            {
                switch (wear.wearLocation)
                {
                    case Globals.eWearLocation.Head: // head examine
                        if (lookClosely) { head = wear.longDesc; }
                        break;
                    case Globals.eWearLocation.Neck: // neck examine
                        if (lookClosely) { neck = wear.longDesc; }
                        break;
                    case Globals.eWearLocation.Face: // face examine
                        if (lookClosely) { face = wear.longDesc; }
                        break;
                    case Globals.eWearLocation.Shoulders: // shoulders examine
                        if (lookClosely) { shoulders = wear.longDesc; }
                        break;
                    case Globals.eWearLocation.Back: // back
                        if (wear.baseType == Globals.eItemBaseType.Armor)
                        {
                            if (chr.GetInventoryItem(Globals.eWearLocation.Torso) != null)
                            {
                                back = " is wearing " + wear.longDesc + " over ";
                            }
                            else
                            {
                                back = " is wearing " + wear.longDesc + ".";
                            }

                        }
                        else
                        {
                            if (chr.GetInventoryItem(Globals.eWearLocation.Torso) != null)
                            {
                                back = " has " + wear.longDesc + " strapped to " + Character.possessive[(int)tmpNPC.gender].ToLower() +
                                    " back. " + Character.pronoun[(int)tmpNPC.gender] + " is wearing ";
                            }
                            else
                            {
                                back = " has " + wear.longDesc + " strapped to " + Character.possessive[(int)tmpNPC.gender].ToLower() +
                                    " back. ";
                            }
                        }
                        break;
                    case Globals.eWearLocation.Torso: // torso
                        torso = wear.longDesc;
                        break;
                    case Globals.eWearLocation.Legs: // legs
                        legs = wear.longDesc;
                        break;
                    case Globals.eWearLocation.Feet: // feet
                        if (lookClosely) { feet = wear.longDesc; }
                        break;
                    case Globals.eWearLocation.Hands: // gauntlets examine
                        if (lookClosely) { gauntlets = wear.longDesc; }
                        break;
                    default:
                        break;
                }
            }
            if (back != "none")
            {
                if (torso != "none")
                {
                    armordesc = back + torso + ".";
                }
                if (legs != "none")
                {
                    armordesc = back + torso + " and " + legs + ".";
                }
                if(legs == "none" && torso == "none")
                {
                    armordesc = back + ".";
                }
            }
            if (back == "none")
            {
                if (torso != "none")
                {
                    if (legs != "none")
                    {
                        armordesc = " is wearing " + torso + " and " + legs + ".";
                    }
                    else
                    {
                        armordesc = " is wearing " + torso + ".";
                    }
                }
                if (torso == "none")
                {
                    if (legs != "none")
                    {
                        armordesc = " is wearing " + legs + ".";
                    }
                }
            }
            //get the examine desc if command is LOOK CLOSELY AT
            if (head != "none")
            {
                examinedesc = " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " head is " + head + ".";
            }
            if (neck != "none")
            {
                examinedesc = examinedesc + " Around " + Character.possessive[(int)tmpNPC.gender].ToLower() + " neck is " + neck + ".";
            }
            if (shoulders != "none")
            {
                examinedesc = examinedesc + " Draped over " + Character.possessive[(int)tmpNPC.gender].ToLower() + " shoulders is " + shoulders + ".";
            }
            if (gauntlets != "none")
            {
                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " hands are " + gauntlets + ".";
            }
            if (feet != "none")
            {
                examinedesc = examinedesc + " On " + Character.possessive[(int)tmpNPC.gender].ToLower() + " feet is " + feet + ".";
            }
            //what the npc is carrying
            if (tmpNPC.RightHand != null && tmpNPC.LeftHand == null)
            {
                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.RightHand.shortDesc + ".";
            }
            if (tmpNPC.RightHand == null && tmpNPC.LeftHand != null)
            {
                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.LeftHand.shortDesc + ".";
            }
            if (tmpNPC.RightHand != null && tmpNPC.LeftHand != null)
            {
                iscarrying = " " + Character.pronoun[(int)tmpNPC.gender] + " is carrying " + tmpNPC.RightHand.shortDesc + " and " + tmpNPC.LeftHand.shortDesc + ".";
            }
            //if this is a player
            if (tmpNPC.IsPC)
            {
                //is the LOOKer a knight? do we fool them into thinking we're not a thief?
                string pcClass = tmpNPC.classFullName.ToLower();
                switch (tmpNPC.BaseProfession)
                {
                    case Character.ClassType.Thief:
                        pcClass = Rules.DetectThief(tmpNPC, chr).ToString().ToLower();
                        break;
                }
                if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                {
                    islookingatyou = " " + tmpNPC.Name + " is looking at you.";
                }
                if (tmpNPC.Stunned > 0)
                { islookingatyou = " " + tmpNPC.Name + " is stunned."; }
                chr.TargetName = tmpNPC.Name;
                chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.gender.ToString().ToLower() + " " + pcClass + " from " + Character.RaceToString(tmpNPC.race) + ". " + tmpNPC.Name + armordesc + iscarrying + examinedesc + " " + tmpNPC.Name + " is " + DamageDesc + "." + islookingatyou);
            }
            //get npc class if not a player
            else
            {
                string lookclass = "fighter";
                switch (tmpNPC.BaseProfession)
                {
                    case Character.ClassType.Knight:
                        lookclass = "knight";
                        break;
                    case Character.ClassType.Martial_Artist:
                        lookclass = "martial artist";
                        break;
                    case Character.ClassType.Thaumaturge:
                        lookclass = "thaumaturge";
                        break;
                    case Character.ClassType.Wizard:
                        lookclass = "wizard";
                        break;
                    case Character.ClassType.Thief:
                        if (Rules.DetectThief(tmpNPC, chr) == Character.ClassType.Thief)
                        {
                            lookclass = "thief";
                            align = tmpNPC.Alignment.ToString().ToLower();
                        }
                        else
                        {
                            lookclass = "fighter";
                            align = "lawful";
                        }
                        break;
                    case Character.ClassType.Fighter:
                    default:
                        lookclass = "fighter";
                        break;
                }
                //npc is a dragon or drake
                if (tmpNPC.species == Globals.eSpecies.FireDragon ||
                    tmpNPC.species == Globals.eSpecies.IceDragon ||
                    tmpNPC.species == Globals.eSpecies.LightningDrake ||
                    tmpNPC.species == Globals.eSpecies.TundraYeti)
                {
                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                    {
                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                    }
                    if (tmpNPC.Stunned > 0)
                    { islookingatyou = " The " + tmpNPC.Name + " is stunned."; }
                    chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + islookingatyou);
                    return;
                }
                //npc has a race
                if (!tmpNPC.IsPC && tmpNPC.race != "")
                {
                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                    {
                        islookingatyou = " " + tmpNPC.Name + " is looking at you.";
                    }
                    if (tmpNPC.Stunned > 0)
                    { islookingatyou = " " + tmpNPC.Name + " is stunned."; }
                    chr.WriteToDisplay("You are looking at " + agedesc + " " + tmpNPC.gender.ToString().ToLower() + " " + lookclass + " from " + Character.RaceToString(tmpNPC.race) + ". " + tmpNPC.Name + armordesc + iscarrying + examinedesc + " " + tmpNPC.Name + " is " + DamageDesc + "." + islookingatyou);
                    return;
                }
                //animals
                if (tmpNPC.animal)
                {
                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                    {
                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                    }
                    if (tmpNPC.Stunned > 0)
                    { islookingatyou = " The " + tmpNPC.Name + " is stunned."; }
                    chr.WriteToDisplay("You are looking at " + align + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + islookingatyou);
                }
                //all other npcs
                else
                {
                    if (tmpNPC.TargetName.ToLower().Equals(chr.Name.ToLower()))
                    {
                        islookingatyou = " The " + tmpNPC.Name + " is looking at you.";
                    }
                    if (tmpNPC.Stunned > 0)
                    { islookingatyou = " The " + tmpNPC.Name + " is stunned."; }
                    chr.WriteToDisplay("You are looking at " + align + " " + tmpNPC.shortDesc + ". " + Character.pronoun[(int)tmpNPC.gender] + armordesc + iscarrying + examinedesc + " " + Character.pronoun[(int)tmpNPC.gender] + " is " + DamageDesc + "." + islookingatyou);
                }
            }
        }

        public void remove(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Remove what?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            sArgs[0] = sArgs[0].ToLower();

            // remove left bracelet
            if (sArgs.Length == 2)
            {
                foreach (Item item in chr.wearing)
                {
                    if (item.name == sArgs[1] && item.wearOrientation.ToString().ToLower() == sArgs[0].ToLower())
                    {
                        if (chr.RightHand == null)
                        {
                            chr.RightHand = item;
                            chr.RemoveWornItem(item);
                        }
                        else if (chr.LeftHand == null)
                        {
                            chr.LeftHand = item;
                            chr.RemoveWornItem(item);
                        }
                        else
                        {
                            chr.WriteToDisplay("Your hands are full!");
                        }
                        return;
                    }
                }
                chr.WriteToDisplay("You are not wearing a " + sArgs[1] + " there.");
                return;
            }
            if (sArgs[0] != "ring" && sArgs.Length == 1)
            {
                foreach (Item item in chr.wearing)
                {
                    if (item.name.ToLower() == sArgs[0].ToLower())
                    {
                        if (item.wearOrientation != Globals.eWearOrientation.None)
                        {
                            string[] wearLocs = Enum.GetNames(typeof(Globals.eWearLocation));
                            for (int a = 0; a < wearLocs.Length; a++)
                            {
                                if (wearLocs[a] == item.wearLocation.ToString())
                                {
                                    if (Globals.Max_Wearable.Length >= a + 1)
                                    {
                                        if (Globals.Max_Wearable[a] > 1)
                                        {
                                            chr.WriteToDisplay("Use \"remove <left | right> <item>\".");
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        if (chr.RightHand == null)
                        {
                            chr.RightHand = item;
                            chr.RemoveWornItem(sArgs[0]);
                        }
                        else if (chr.LeftHand == null)
                        {
                            chr.LeftHand = item;
                            chr.RemoveWornItem(sArgs[0]);
                        }
                        else
                        {
                            chr.WriteToDisplay("Your hands are full!");
                            return;
                        }
                        return;
                    }
                }
                chr.WriteToDisplay("You are not wearing a " + sArgs[0] + ".");
                return;
            }
            // Trap "remove ring", "remove ring from L/R" and "remove x ring" here.
            // run findfirstRing functions, reparse sArgs, continue.
            if ((sArgs[0] == "ring" || sArgs[1] == "ring") && sArgs.Length < 4)
            {
                //can't remove a ring if wearing gauntlets
                foreach (Item wItem in chr.wearing)
                {
                    if (wItem.wearLocation == Globals.eWearLocation.Hands)
                    {
                        chr.WriteToDisplay("You need to remove your " + wItem.name + " first.");
                        return;
                    }
                }

                int firstRightRing = chr.findFirstRightRing();
                int firstLeftRing = chr.findFirstLeftRing();
                int firstRing = 0;
                if (firstLeftRing == 0 && firstRightRing == 0)
                {
                    chr.WriteToDisplay("You aren't wearing any rings.");
                    return;
                }
                else
                {
                    if (sArgs.Length == 3)	//"remove ring from left/right"
                    {
                        if (sArgs[2] == "left")
                        {
                            if (firstLeftRing == 0)
                            {
                                chr.WriteToDisplay("You aren't wearing a ring on that hand.");
                                return;
                            }
                            firstRing = firstLeftRing + 4;
                        }
                        else if (sArgs[2] == "right")
                        {
                            if (firstRightRing == 0)
                            {
                                chr.WriteToDisplay("You aren't wearing a ring on that hand.");
                                return;
                            }
                            firstRing = firstRightRing;
                        }
                        else
                        {
                            chr.WriteToDisplay("I don't understand that command.");
                            return;
                        }

                    }
                    else if (sArgs.Length == 2) //"remove x ring" 
                    {
                        switch (sArgs[0])
                        {
                            case "1":
                                if (chr.RightRing1 != null)
                                    firstRing = 1;
                                else
                                    firstRing = 5;
                                break;
                            case "2":
                                if (chr.RightRing2 != null)
                                    firstRing = 2;
                                else
                                    firstRing = 6;
                                break;
                            case "3":
                                if (chr.RightRing3 != null)
                                    firstRing = 3;
                                else
                                    firstRing = 7;
                                break;
                            case "4":
                                if (chr.RightRing4 != null)
                                    firstRing = 4;
                                else
                                    firstRing = 8;
                                break;
                            default:
                                chr.WriteToDisplay("I don't understand that command.");
                                return;
                        }
                    }
                    else if (sArgs.Length == 1) //"remove ring"
                    {
                        if (firstRightRing > 0)
                        {
                            firstRing = firstRightRing;
                        }
                        else
                        {
                            firstRing = firstLeftRing + 4;
                        }
                    }
                    switch (firstRing) //Now reparse
                    {
                        case 1:
                            args = "1 ring from right";
                            break;
                        case 2:
                            args = "2 ring from right";
                            break;
                        case 3:
                            args = "3 ring from right";
                            break;
                        case 4:
                            args = "4 ring from right";
                            break;
                        case 5:
                            args = "1 ring from left";
                            break;
                        case 6:
                            args = "2 ring from left";
                            break;
                        case 7:
                            args = "3 ring from left";
                            break;
                        case 8:
                            args = "4 ring from left";
                            break;
                    }
                    this.remove(args);
                    return;
                }
            }
            if (sArgs.Length == 4) // high chance of ring removal
            {
                //can't remove a ring if wearing gauntlets
                foreach (Item wItem in chr.wearing)
                {
                    if (wItem.wearLocation == Globals.eWearLocation.Hands)
                    {
                        chr.WriteToDisplay("You need to remove your " + wItem.name + " first.");
                        return;
                    }
                }

                Item tItem = null;
                if (sArgs[1] != "ring") { return; } // not removing a ring
                if (sArgs[3] == "right")
                {
                    if (chr.LeftHand != null) { chr.WriteToDisplay("Your left hand must be empty."); return; }
                    switch (sArgs[0])
                    {
                        case "1":
                            if (chr.RightRing1 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.RightRing1;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 0);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.RightRing1 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "2":
                            if (chr.RightRing2 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.RightRing2;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 0);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.RightRing2 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "3":
                            if (chr.RightRing3 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.RightRing3;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 0);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.RightRing3 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "4":
                            if (chr.RightRing4 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.RightRing4;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 0);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.RightRing4 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        default:
                            chr.WriteToDisplay("That is not a valid ring finger.");
                            return;
                    }
                    if (chr.LeftHand == null)
                    {
                        chr.LeftHand = tItem;
                    }
                }
                else if (sArgs[3] == "left")
                {
                    if (chr.RightHand != null) { chr.WriteToDisplay("Your right hand must be empty."); return; }
                    switch (sArgs[0])
                    {
                        case "1":
                            if (chr.LeftRing1 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.LeftRing1;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 1);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.LeftRing1 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "2":
                            if (chr.LeftRing2 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.LeftRing2;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 1);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.LeftRing2 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "3":
                            if (chr.LeftRing3 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.LeftRing3;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 1);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.LeftRing3 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        case "4":
                            if (chr.LeftRing4 == null) { chr.WriteToDisplay("You do not have a ring on that finger."); return; }

                            tItem = chr.LeftRing4;
                            if (tItem.isRecall) // it is a recall ring
                            {
                                if (chr.CurrentCell.IsNoRecall)
                                {
                                    chr.WriteToDisplay("A powerful force cancels your recall magic!");
                                }
                                else
                                {
                                    recall(chr, tItem, 1);
                                }
                            }
                            if (tItem.wasRecall) // was it a recall ring that was reset
                            {
                                tItem.wasRecall = false;
                                tItem.isRecall = true;
                            }
                            chr.LeftRing4 = null;
                            if (tItem.effectType.Length > 0)
                            {
                                Effect.RemoveWornEffectFromCharacter(chr, tItem);
                            }
                            break;
                        default:
                            chr.WriteToDisplay("That is not a valid ring finger.");
                            return;
                    }
                    if (chr.RightHand == null)
                    {
                        chr.RightHand = tItem;
                    }
                }
                else
                {
                    chr.WriteToDisplay("You do not have any rings there.");
                    return;
                }
                return;
            }
        }

        private void recall(Character chr, Item item, int hand)
        {
            chr.SendShout("a thunderclap!");
            chr.WriteToDisplay("You hear a thunderclap!");
            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.ThunderClap));
            chr.CurrentCell = Cell.GetCell(chr.FacetID, item.recallLand, item.recallMap, item.recallX, item.recallY, item.recallZ);
            chr.SendShout("a thunderclap!");
            chr.SendSoundToAllInRange(Sound.GetCommonSound(Sound.CommonSound.ThunderClap));
            item = Item.CopyItemFromDictionary(Item.ID_GOLDRING);
            if (hand == 0)
            {
                chr.LeftHand = item;
            }
            if (hand == 1)
            {
                chr.RightHand = item;
            }
            return;
        }

        public void say(string text)
        {
            foreach (Character ch in Character.pcList)
            {
                if (chr == ch)
                {
                    ch.WriteToDisplay("You say: " + text);
                }
                else
                {
                    ch.WriteToDisplay(chr.Name + ": " + text);
                }
            }
        }

        /// <summary>
        /// This command allows a thief to look into a target's sack.
        /// </summary>
        /// <param name="args">The target.</param>
        public void peek(string args)
        {
            // command weight plus one
            chr.cmdWeight += 1;

            // return void if command weight is greater than 3
            if (chr.cmdWeight > 3)
            {
                return;
            }

            if (chr.BaseProfession != Character.ClassType.Thief)
            {
                chr.WriteToDisplay("You do not know how to do that.");
                return;
            }

            if (!chr.IsHidden)
            {
                chr.WriteToDisplay("You must be hidden to be successful.");
                return;
            }

            // less than "graceful" skill level
            if (Skills.GetSkillLevel(chr.thievery) < 8)
            {
                chr.WriteToDisplay("You are not skilled enough to do that.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Peek what?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            Character target =  Map.FindTargetInCell(chr, sArgs[0]);

            // failed to find the target
            if(target == null)
            {
                chr.WriteToDisplay("You don't see a " + sArgs[1] + " here.");
                return;
            }

            int successChance = Skills.MAX_SKILL_LEVEL - Skills.GetSkillLevel(chr.thievery);

            if (Rules.RollD(1, Skills.MAX_SKILL_LEVEL) > successChance)
            {
                if (chr.sackList.Count > 0)
                {
                    int i = 0;
                    int z = 0;
                    double itemcount = 0;
                    bool moreThanOne = false;

                    ArrayList templist = new ArrayList();

                    Item[] itemList = new Item[chr.sackList.Count];

                    target.sackList.CopyTo(itemList);

                    foreach (Item item in itemList)
                    {
                        templist.Add(item);
                    }

                    z = templist.Count - 1;

                    string dispMsg = "In " + target.shortDesc + "'s sack you see ";

                    while (z >= 0)
                    {

                        Item item = (Item)templist[z];

                        itemcount = 0;

                        for (i = templist.Count - 1; i > -1; i--)
                        {
                            Item tmpitem = (Item)templist[i];

                            if (tmpitem.name == item.name && tmpitem.name.IndexOf("coin") > -1)
                            {
                                templist.RemoveAt(i);
                                itemcount = itemcount + (int)item.coinValue;
                                z = templist.Count;

                            }
                            else if (tmpitem.name == item.name)
                            {
                                templist.RemoveAt(i);
                                z = templist.Count;
                                itemcount += 1;
                            }

                        }
                        if (itemcount > 0)
                        {
                            if (moreThanOne)
                            {
                                if (z == 0)
                                {
                                    dispMsg += " and ";
                                }
                                else
                                {
                                    dispMsg += ", ";
                                }
                            }
                            dispMsg += Character.ConvertNumberToString(itemcount) + Character.GetLookShortDesc(item, itemcount);

                        }
                        moreThanOne = true;
                        z--;
                    }

                    dispMsg += ".";

                    chr.WriteToDisplay(dispMsg);
                }
                else
                {
                    chr.WriteToDisplay("There is nothing in the sack.");
                }
            }
            else
            {
                chr.WriteToDisplay("Your attempt to peek has failed.");

                // if an intelligence check succeeds then the peeking thief has been caught
                if (Rules.FullStatCheck(target, "intelligence"))
                {
                    target.WriteToDisplay(chr.Name + " has attempted to peek into your sack and failed.");

                    chr.WriteToDisplay("Your failure has been noticed!");

                    // take care of flagging
                    Rules.DoFlag(chr, target);
                }
            }
        }

        /// <summary>
        /// STEAL (ITEM) FROM (CREATURE)
        /// STEAL FROM (CREATURE)
        /// </summary>
        /// <param name="args"></param>
        public void steal_x(string args)
        {
            // command weight plus one
            chr.cmdWeight += 1;

            // return void if command weight is greater than 3
            if (chr.cmdWeight > 3)
            {
                return;
            }

            // only martial artists and thieves can steal
            switch (chr.BaseProfession)
            {
                case Character.ClassType.Martial_Artist:
                case Character.ClassType.Thief:
                    break;
                default:
                    chr.WriteToDisplay("You do not know how to do that.");
                    return;
            }

            // if character does not have a hand free they cannot steal
            if (chr.GetFirstFreeHand() == 0)
            {
                chr.WriteToDisplay("You don't have a hand free.");
                return;
            }

            // get rid of the word "from "
            args = args.Replace("from ", "");

            // get the array of arguments -- should be target at [1] or item at [1] and target at [2]
            string[] sArgs = args.Split(" ".ToCharArray());

            // target should be the index 1 or 2, try index 1 argument first
            Character target = Map.FindTargetInCell(chr, sArgs[1]);

            // target not found
            if (target == null)
            {
                // if arguments array is greater than 2 in length then target is at index 2
                if (sArgs.Length > 2)
                {
                    target = Map.FindTargetInCell(chr, sArgs[2]);
                }

                // if target still remains null then the target is invalid
                if (target == null)
                {
                    if (sArgs.Length > 2)
                    {
                        chr.WriteToDisplay("You don't see a " + sArgs[2] + " here.");
                    }
                    else
                    {
                        chr.WriteToDisplay("You don't see a " + sArgs[1] + " here.");
                    }
                    return;
                }
            }

            // give experience for a steal attempt
            Skills.GiveSkillExp(chr, target, Globals.eSkillType.Thievery);

            // 50 percent base chance to steal
            int successChance = 50;

            string itemTarget = "";

            // item target should be argument index 2
            if (sArgs.Length >= 3)
            {
                // chance to successfully steal is reduced by max skill level minus thievery level
                successChance -= Skills.MAX_SKILL_LEVEL - Skills.GetSkillLevel(chr.thievery);

                // item target is index 1
                itemTarget = sArgs[1];
            }

            Item item = null;

            // this is the 100 sided die roll plus the character's thievery skill level
            int roll = Rules.RollD(1, 100);

            // you successfully steal when if the roll plus thievery skill level x 2 is greater than the chance
            if (roll + (Skills.GetSkillLevel(chr.thievery) * 2) > successChance)
            {
                // if there is no specific item being sought after
                if (itemTarget == "")
                {
                    // steal gold if gold is in sack and another d100 roll is equal to or greater than base chance
                    if (target.SackGold > 0 && Rules.RollD(1, 100) >= successChance)
                    {
                        item = Item.CopyItemFromDictionary(Item.ID_COINS);
                        double amount = Rules.RollD(1, (int)target.SackGold);
                        item.coinValue = amount;
                        target.SackGold -= amount;
                    }
                    else
                    {
                        int randomItem = Rules.dice.Next(target.sackList.Count);

                        item = (Item)target.sackList[randomItem];

                        target.sackList.RemoveAt(randomItem);
                    }
                }
                else
                {
                    if (!itemTarget.StartsWith("coin"))
                    {
                        item = target.RemoveFromSack(itemTarget);
                    }
                    else
                    {
                        if (target.SackGold > 0)
                        {
                            item = Item.CopyItemFromDictionary(Item.ID_COINS);
                            double amount = Rules.RollD(1, (int)target.SackGold);
                            item.coinValue = amount;
                            target.SackGold -= amount;                            
                        }
                    }

                    if (item == null)
                    {
                        chr.WriteToDisplay("You could not find a " + sArgs[1] + ".");
                        return;
                    }
                    else
                    {
                        if (roll - (Skills.GetSkillLevel(chr.thievery) * 2) > (successChance / 2))
                        {
                            chr.WriteToDisplay("You have been caught!");

                            target.WriteToDisplay(chr.Name + " has stolen " + item.shortDesc + " from you.");

                            // take care of flagging
                            Rules.DoFlag(chr, target);
                        }
                    }
                }

                // place the item into the character's first free hand
                switch (chr.GetFirstFreeHand())
                {
                    case 1:
                        chr.RightHand = item;
                        break;
                    case 2:
                        chr.LeftHand = item;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                chr.WriteToDisplay("You have been caught!");

                target.WriteToDisplay(chr.Name + " has attempted to steal from you!");

                // take care of flagging
                Rules.DoFlag(chr, target);
            }
        }        

        public void doorunlock(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("What door do you want to unlock?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            Cell cell = Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0], true);

            if (cell == null || (!cell.IsLockedVerticalDoor && !cell.IsLockedHorizontalDoor))
            {
                chr.WriteToDisplay("There is nothing to unlock here.");
                return;
            }

            if (chr.RightHand == null)
            {
                chr.WriteToDisplay("You are not holding the correct key in your right hand.");
                return;
            }

            if (Map.UnlockDoor(cell, chr.RightHand.key))
            {
                chr.WriteToDisplay("You unlock the door.");
                return;
            }

            chr.WriteToDisplay("You do not have the correct key.");

            return;
        }

        public void doorlock(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("What door do you want to lock?");
                return;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            Cell cell = Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0], true);

            if (cell == null || (!cell.IsLockedVerticalDoor && !cell.IsLockedHorizontalDoor))
            {
                chr.WriteToDisplay("There is nothing to lock here.");
                return;
            }

            if (chr.RightHand == null)
            {
                chr.WriteToDisplay("You are not holding the correct key in your right hand.");
                return;
            }

            if (Map.LockDoor(cell, chr.RightHand.key))
            {
                chr.WriteToDisplay("You lock the door.");
            }
            else
            {
                if (cell.cellLock.key != chr.RightHand.key)
                {
                    chr.WriteToDisplay("You do not have the correct key.");
                }
                else
                {
                    chr.WriteToDisplay("The door is already locked.");
                }
            }
        }

        public void group(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Group Commands (Format: group <command> <argument>)");
                chr.WriteToDisplay("group accept - accepts an open invite to join a group");
                chr.WriteToDisplay("group invite <name> - invites <name> to join your group or creates a group");
                chr.WriteToDisplay("group decline - decline an invitation to join a group");
                chr.WriteToDisplay("group status - view current group member information");
                chr.WriteToDisplay("group <message> - send a message to all group members");
                chr.WriteToDisplay("group disband - leave your current group");
                chr.WriteToDisplay("group follow <name> - follow a visible group member");
                return;
            }

            String[] sArgs = args.Split(" ".ToCharArray());

            switch (sArgs[0])
            {
                case "accept":
                case "join":
                    #region accept
                    if (chr.Group == null)
                    {
                        if (chr.GroupInviter >= 0)
                        {
                            if (!Group.AcceptPlayerGroupInvite(chr, chr.GroupInviter))
                            {
                                Group.CreatePlayerGroup(chr.GroupInviter, chr.PlayerID);
                            }
                        }
                        else
                        {
                            chr.WriteToDisplay("You have not been invited to a group.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You are already a member of a group.");
                    }
                    break; 
                    #endregion
                case "invite":
                    #region invite
                    if (chr.Group == null)
                    {
                        if (sArgs.Length < 2)
                        {
                            chr.WriteToDisplay("Format: group invite <player name>");
                            return;
                        }

                        if (Group.IsInviteEligible(sArgs[1], chr))
                        {
                            chr.Group = new Group(chr.PlayerID);
                            chr.WriteToDisplay(chr.Group.InvitePlayer(sArgs[1]));
                        }
                    }
                    else
                    {
                        if (sArgs.Length < 2)
                        {
                            chr.WriteToDisplay("Format: group invite <player name>");
                            return;
                        }

                        if (Group.IsInviteEligible(sArgs[1], chr))
                        {
                            chr.WriteToDisplay(chr.Group.InvitePlayer(sArgs[1]));
                        }
                    }
                    break; 
                    #endregion
                case "decline":
                    #region decline
                    if (chr.Group == null)
                    {
                        if (chr.GroupInviter >= 0)
                        {
                            Group group = Group.GetPlayerGroupByGroupLeaderID(chr.GroupInviter);
                            if (group != null)
                            {
                                PC pc = PC.getOnline(chr.GroupInviter);
                                if (pc != null)
                                {
                                    pc.WriteToDisplay(chr.Name + " declines your group invite.");
                                    chr.WriteToDisplay("You decline the invite into " + pc.Name + "'s group.");
                                    if (pc.Group.GroupMemberIDList == null)
                                    {
                                        pc.Group = null;
                                    }
                                }
                            }
                            chr.GroupInviter = -1;
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("You are already the member of a group. Use \"group disband\" to leave the group.");
                    }
                    break; 
                    #endregion
                case "disband":
                    #region disband
                    if (chr.Group != null)
                    {
                        chr.Group.DisbandPlayerGroupMember(chr.PlayerID, true);
                    }
                    else
                    {
                        if (chr.GroupInviter >= 0)
                        {
                            group("decline");
                        }
                        else
                        {
                            chr.WriteToDisplay("You are not a member of a group.");
                        }
                    }
                    break; 
                    #endregion
                case "follow":
                    #region follow
                    if (chr.Group == null)
                    {
                        chr.WriteToDisplay("You are not in a group.");
                        break;
                    }
                    if (sArgs.Length > 1)
                    {
                        Character leader = Map.FindTargetInView(chr, sArgs[1], false, true);
                        if (leader != null && leader.IsPC && leader.Group == chr.Group)
                        {
                            chr.FollowName = leader.Name;
                            chr.WriteToDisplay("You begin to follow " + leader.Name + ".");
                        }
                        else
                        {
                            chr.WriteToDisplay(sArgs[1] + " is an invalid follow target.");
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Format: group follow <name> - where <name> is a player in your group");
                    }
                    break; 
                    #endregion
                case "status":
                    #region status
                    if (chr.Group == null)
                    {
                        chr.WriteToDisplay("You are not in a group.");
                        break;
                    }
                    if (chr.Group != null && chr.Group.GroupMemberIDList == null)
                    {
                        chr.WriteToDisplay("Your group is not formed yet.");
                        break;
                    }
                    chr.WriteToDisplay("Group Status");
                    string groupMemberInfo = "";
                    foreach (int id in chr.Group.GroupMemberIDList)
                    {
                        PC pc = PC.getOnline(id);
                        if (pc != null)
                        {
                            groupMemberInfo = pc.Name;
                            groupMemberInfo = groupMemberInfo.PadRight(15);
                            groupMemberInfo += "[" + pc.Level + "] " + pc.classFullName;
                            groupMemberInfo = groupMemberInfo.PadRight(32);
                            groupMemberInfo += "H: " + pc.Hits + "/" + pc.HitsFull + " S: " + pc.Stamina + "/" + pc.StaminaFull;
                            if (pc.IsSpellUser)
                            {
                                groupMemberInfo += " M: " + pc.Mana + "/" + pc.ManaFull;
                            }
                            if (pc.Group.GroupLeaderID == pc.PlayerID)
                            {
                                groupMemberInfo += " (Leader)";
                            }
                            chr.WriteToDisplay(groupMemberInfo);
                        }
                    }
                    break; 
                    #endregion
                default:
                    #region group chat
                    if (chr.Group != null)
                    {
                        string groupChat = "";
                        for (int a = 0; a < sArgs.Length; a++)
                        {
                            groupChat += sArgs[a] + " ";
                        }
                        groupChat = groupChat.Substring(0, groupChat.Length - 1);
                        chr.Group.SendGroupMessage(chr.Name + ": " + groupChat);
                    }
                    else
                    {
                        chr.WriteToDisplay("I don't understand your command.");
                    }
                    break; 
                    #endregion
            }
        }

        public void help(string args)
        {
            if (args == null)
            {
                chr.WriteToDisplay("Help is in need of an update if we have any volunteers.");
                chr.WriteToDisplay("Help Topics: ( help <topic> )");
                chr.WriteToDisplay("merchants   talk        magic     training");
                chr.WriteToDisplay("map         movement    combat    formulas");
            }
            else
            {

                String[] sArgs = args.Split(" ".ToCharArray());
                switch (sArgs[0].ToLower())
                {
                    case "map":
                        chr.WriteToDisplay("The map consists of the following tiles:");
                        chr.WriteToDisplay("%% = Air          [_ = Ruins ");
                        chr.WriteToDisplay("~~ = Water        ** = Fire ");
                        chr.WriteToDisplay("@  = Tree         /\\ = Mountain");
                        chr.WriteToDisplay("== = Counter      .~ = Ice");
                        chr.WriteToDisplay("up = Up Stairs    dn = Down Stairs");
                        chr.WriteToDisplay(".  = Empty        [] = Wall ");
                        break;
                    case "movement":
                        chr.WriteToDisplay("N N NE would move you 3 spaces North and one space NorthEast.");
                        chr.WriteToDisplay("You can use up to three moves in one turn. For instance");
                        chr.WriteToDisplay("Portals can be used with a magic chant.");
                        chr.WriteToDisplay("D  = Down        U = Up          Climb Up        Climb Down");
                        chr.WriteToDisplay("NE = NorthEast  NW = NorthWest  SE = SouthEast  SW = SouthWest");
                        chr.WriteToDisplay("N  = North       S = South       E = East        W = West");
                        chr.WriteToDisplay("The possible directions are:");
                        chr.WriteToDisplay("to move using the first letter of the direction.");
                        chr.WriteToDisplay("Movement is done by typing the direction you want");
                        break;
                    case "combat":
                        chr.WriteToDisplay("NAME is the name of the thing you are attacking.");
                        chr.WriteToDisplay("the FIGHT command. The usage is Attack <NAME> where ");
                        chr.WriteToDisplay("Combat is done by using either the Attack command, or");
                        break;
                    case "merchants":
                        chr.WriteToDisplay("Use the command 'show prices' while in front of a counter");
                        chr.WriteToDisplay("to see what items are for sale. To purchase an item place");
                        chr.WriteToDisplay("your gold coins on the counter (put coins on counter) and");
                        chr.WriteToDisplay("then ask the merchant to sell you the item with the command");
                        chr.WriteToDisplay("'<name>, SELL <item name>'. To sell an item use");
                        chr.WriteToDisplay("'<name>, BUY <item name>' or '<name>, BUY ALL'.");
                        chr.WriteToDisplay("Note that you can use the command 'dump <item name> on counter'");
                        chr.WriteToDisplay("to place all <item name> from your sack on the counter in front");
                        chr.WriteToDisplay("of you.");
                        break;
                    case "talk":
                        chr.WriteToDisplay("sight.");
                        chr.WriteToDisplay("using the SAY commandto display your message to anyone in ");
                        chr.WriteToDisplay("instance \"Where is the armor shop? would work the same as");
                        chr.WriteToDisplay("A shortcut is to use the \" instead of the SAY command. For");
                        chr.WriteToDisplay("usage: SAY <MESSAGE> where message is what you want to say.");
                        chr.WriteToDisplay("You can talk to other players by using the SAY command.");
                        break;
                    case "training":
                        chr.WriteToDisplay("Training will greatly increase the amount of skill experience you earn while using the skill.");
                        chr.WriteToDisplay("Training Costs:");
                        for (int a = 0; a <= Skills.MAX_SKILL_LEVEL; a++)
                            chr.WriteToDisplay("Level " + a + ": Full Cost = " + Rules.Formula_TrainingCostForLevel(a) + ", Rank = " +
                                Skills.GetTrainingCostPerRank(a));
                        break;
                    case "formula":
                    case "formulas":
                        chr.WriteToDisplay("Current Stat Formulas (" + chr.Land.Name + "):");
                        string[] classes = Enum.GetNames(typeof(Character.ClassType));                        
                        string hitsFormula = "";
                        string stamFormula = "";
                        string manaFormula = "";
                        string outputString = "";
                        for (int a = 1; a < classes.Length; a++)
                        {
                            hitsFormula = "(" + chr.Land.HitDice[a].ToString() + " * level) + " + chr.Land.StatCapOperand.ToString();
                            stamFormula = "(" + chr.Land.StaminaDice[a].ToString() + " * level)";
                            if(chr.Land.ManaDice[a] > 0)
                            {
                                manaFormula = "(" + chr.Land.ManaDice[a].ToString() + " * level)";
                                outputString = "[" + Utils.FormatEnumString(classes[a]) + "] H: " + hitsFormula + " S: " + stamFormula + " M: " + manaFormula;
                            }
                            else
                            {
                                outputString = "[" + Utils.FormatEnumString(classes[a]) + "] H: " + hitsFormula + " S: " + stamFormula;
                            }
                            chr.WriteToDisplay(outputString);
                        }
                        break;
                    case "experience":
                    case "exp":
                        chr.WriteToDisplay("Experience Levels:");
                        long low = 1600;
                        long high = 3200;

                        for (int a = 3; a <= Globals.MAX_EXP_LEVEL; a++)
                        {
                            chr.WriteToDisplay(low + " - " + high + " = Level " + a); 
                            low = high;
                            high = high * 2;
                        }
                        break;
                    case "magic":
                        break;
                    default:
                        break;
                }
            }
        }

        public bool give(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Format: give <item> to <target>");
                return false;
            }

            string[] sArgs = args.Split(" ".ToCharArray());

            if (sArgs.Length < 3)
            {
                chr.WriteToDisplay("Format: give <item> to <target>");
                return false;
            }

            try
            {

                string giveItem = sArgs[0];

                string giveTarget = sArgs[2];

                Character target = Map.FindTargetInCell(chr, giveTarget);

                if (target == null)
                {
                    chr.WriteToDisplay("You do not see " + giveTarget + " here.");
                    return false;
                }

                if (chr.inHand(giveItem) == 0)
                {
                    chr.WriteToDisplay("You do not appear to have a " + giveItem + " in your hands.");
                    return false;
                }

                if (target.IsPC)
                {
                    if (Array.IndexOf(target.ignoreList, chr.PlayerID) != -1)
                    {
                        chr.WriteToDisplay("You cannot give an item to " + target.Name + ".");
                        return false;
                    }

                    int hand = target.GetFirstFreeHand();

                    if (hand == 0)
                    {
                        chr.WriteToDisplay(target.Name + " does not have an empty hand.");
                        target.WriteToDisplay(chr.Name + " would like to give you an item but you do not have an empty hand.");
                        return false;
                    }

                    switch (hand)
                    {
                        case 1:
                            target.RightHand = chr.getFromHand(giveItem);
                            target.WriteToDisplay(chr.Name + " has given you " + target.RightHand.shortDesc + ".");
                            break;
                        case 2:
                            target.LeftHand = chr.getFromHand(giveItem);
                            target.WriteToDisplay(chr.Name + " has given you " + target.LeftHand.shortDesc + ".");
                            break;
                    }

                    return true;
                }
                else
                {
                    NPC npc = (NPC)target;

                    Item item = chr.getFromHand(giveItem);

                    if (item.attunedID > 0 && item.attunedID != chr.PlayerID)
                    {
                        if (!npc.animal && npc.Alignment == chr.Alignment)
                        {
                            npc.SendToAllInSight(npc.Name + ": The " + item.name + " does not belong to you, " + chr.Name + ". I cannot accept it.");

                        }
                        chr.EquipEitherHandOrDrop(item);
                        return false;
                    }

                    if (npc.receivedItems.ContainsKey(chr.PlayerID))
                    {
                        if (!npc.receivedItems[chr.PlayerID].Contains(item) && (item.attunedID <= 0 || item.attunedID == chr.PlayerID))
                        {
                            npc.receivedItems[chr.PlayerID].Add(item);
                        }
                        else
                        {
                            goto manageItem;
                        }
                    }
                    else
                    {
                        List<Item> itemList = new List<Item>();
                        itemList.Add(item);
                        npc.receivedItems.Add(chr.PlayerID, itemList);
                    }

                    #region Confessor AI
                    if (npc.interactiveType == Merchant.InteractiveType.Confessor)
                    {
                        if (item.baseType == Globals.eItemBaseType.Dagger && item.silver)
                        {
                            if (chr.Alignment == Globals.eAlignment.Neutral && chr.BaseProfession != Character.ClassType.Thief)
                            {
                                chr.Alignment = Globals.eAlignment.Lawful;
                                chr.WriteToDisplay(npc.Name + ": You are forgiven, " + chr.Name + ".");
                                return true;
                            }
                            if (chr.Alignment == Globals.eAlignment.Evil)
                            {
                                chr.Alignment = Globals.eAlignment.Neutral;
                                chr.WriteToDisplay(npc.Name + ": You have taken the first step back to the light.");
                                return true;
                            }
                        }
                        else if (item.baseType == Globals.eItemBaseType.Figurine)
                        {
                            if (chr.currentKarma > 0)
                            {
                                chr.currentKarma--;
                            }
                            return true;
                        }
                    }
                    #endregion

                    #region Quest NPC
                    if (npc.questList.Count > 0)
                    {
                        foreach (Quest quest in npc.questList)
                        {
                            Quest activeQuest = null;

                            foreach (Quest aQuest in chr.questList)
                            {
                                if (aQuest.QuestID == quest.QuestID)
                                {
                                    activeQuest = aQuest;
                                }
                            }

                            short a = 0;

                            if (quest.RequiredItems.ContainsValue(item.itemID) && quest.PlayerMeetsRequirements((PC)chr, true))
                            {
                                if (quest.StepOrder) // this quest has a specified order
                                {
                                    if (activeQuest != null) // chr already has this quest, or has completed it
                                    {
                                        if (quest.IsRepeatable) // quest can be repeated
                                        {
                                            if (activeQuest.Requirements[activeQuest.CurrentStep] == Quest.QuestRequirement.Item &&
                                                activeQuest.RequiredItems[activeQuest.CurrentStep] == item.itemID)
                                            {
                                                activeQuest.FinishStep(npc, (PC)chr, activeQuest.CurrentStep);
                                                return true;
                                            }
                                            else
                                            {
                                                goto manageItem;
                                            }
                                        }
                                        else
                                        {
                                            chr.WriteToDisplay("You have already completed \"" + quest.Name + "\".");
                                            chr.EquipEitherHandOrDrop(item);
                                            return true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (activeQuest != null) // chr already has this quest, or has completed it
                                    {
                                        if (quest.IsRepeatable) // quest can be repeated
                                        {
                                            a = 0;

                                            foreach (Item recvdItem in npc.receivedItems[chr.PlayerID]) // count received items
                                            {
                                                a++;
                                            }

                                            if (a == quest.RequiredItems.Count)
                                            {
                                                if (quest.CoinValues.ContainsKey(a))
                                                {
                                                    if (item.coinValue < quest.CoinValues[a])
                                                    {
                                                        goto manageItem;
                                                    }
                                                }

                                                goto questCompleted;
                                            }
                                            else
                                            {
                                                if (quest.CoinValues.ContainsKey(a))
                                                {
                                                    if (item.coinValue < quest.CoinValues[a])
                                                    {
                                                        goto manageItem;
                                                    }
                                                }

                                                if (a > 0)
                                                {
                                                    quest.FinishStep(target, (PC)chr, a);
                                                }
                                                return true;
                                            }
                                        }
                                        else
                                        {
                                            chr.WriteToDisplay("You have already completed \"" + quest.Name + "\".");
                                            chr.EquipEitherHandOrDrop(item);
                                            return true;
                                        }
                                    }
                                    else // chr does not already have this quest, or has never completed it
                                    {
                                        quest.BeginQuest((PC)chr, true);

                                        a = 0;

                                        foreach (Item recvdItem in npc.receivedItems[chr.PlayerID]) // count received items
                                        {
                                            a++;
                                        }

                                        if (a == quest.RequiredItems.Count)
                                        {
                                            if (quest.CoinValues.ContainsKey(a))
                                            {
                                                if (item.coinValue < quest.CoinValues[a])
                                                {
                                                    goto manageItem;
                                                }
                                            }

                                            goto questCompleted;
                                        }
                                        else
                                        {
                                            if (quest.CoinValues.ContainsKey(a))
                                            {
                                                if (item.coinValue < quest.CoinValues[a])
                                                {
                                                    goto manageItem;
                                                }
                                            }

                                            if (a > 0 && quest.FinishStrings.ContainsKey(a))
                                            {
                                                if (quest.FinishStrings[a] != "")
                                                {
                                                    chr.WriteToDisplay(npc.Name + ": " + quest.FinishStrings[a]);
                                                }
                                            }
                                            return true;
                                        }
                                    }
                                }

                            questCompleted:

                                // break out of this now so the player does not lose an item for a quest that is not available yet
                                if (!quest.PlayerMeetsRequirements((PC)chr, true))
                                {
                                    chr.EquipEitherHandOrDrop(item);
                                    return false;
                                }

                                // verify the quest is started
                                if (activeQuest == null)
                                {
                                    activeQuest = chr.GetQuest(quest.QuestID);
                                    quest.BeginQuest((PC)chr, false);
                                }

                                activeQuest.FinishStep(npc, (PC)chr, a);

                                // clear npc's received items list
                                npc.receivedItems[chr.PlayerID].Clear();

                                return true; // break the foreach loop
                            }
                        }
                    }
                    #endregion

                manageItem:

                    // Give the item back to the player or drop it
                    chr.EquipEitherHandOrDrop(item);

                    //    #region unused
                    //    switch (item.size)
                    //    {
                    //        case Item.Size.Belt_Large_Slot_Only:
                    //            if (npc.IsBeltLargeSlotAvailable)
                    //            {
                    //                npc.beltList.Add(item);
                    //                if (!npc.animal && npc.Alignment == chr.Alignment)
                    //                {
                    //                    npc.SendToAllInSight(npc.Name + ": Thank you for the " + item.name + ", " + chr.Name + ".");
                    //                }
                    //            }
                    //            else
                    //            {
                    //                chr.EquipEitherHandOrDrop(item);
                    //            }
                    //            break;
                    //        case Item.Size.Belt_Only:
                    //            if (npc.beltList.Count < Character.MAX_BELT)
                    //            {
                    //                npc.beltList.Add(item);
                    //                if (!npc.animal && npc.Alignment == chr.Alignment)
                    //                {
                    //                    npc.SendToAllInSight(npc.Name + ": Thank you for the " + item.name + ", " + chr.Name + ".");
                    //                }
                    //            }
                    //            else
                    //            {
                    //                chr.EquipEitherHandOrDrop(item);
                    //            }
                    //            npc.receivedItems[chr.PlayerID].Remove(item);
                    //            break;
                    //        case Item.Size.Belt_Or_Sack:
                    //            if (npc.sackList.Count < Character.MAX_SACK)
                    //            {
                    //                npc.sackList.Add(item);
                    //                if (!npc.animal && npc.Alignment == chr.Alignment)
                    //                {
                    //                    npc.SendToAllInSight(npc.Name + ": Thank you for the " + item.name + ", " + chr.Name + ".");
                    //                }
                    //            }
                    //            else if (npc.beltList.Count < Character.MAX_BELT)
                    //            {
                    //                npc.beltList.Add(item);
                    //                if (!npc.animal && npc.Alignment == chr.Alignment)
                    //                {
                    //                    npc.SendToAllInSight(npc.Name + ": Thank you for the " + item.name + ", " + chr.Name + ".");
                    //                }
                    //            }
                    //            else
                    //            {
                    //                chr.EquipEitherHandOrDrop(item);
                    //            }
                    //            npc.receivedItems[chr.PlayerID].Remove(item);
                    //            break;
                    //        case Item.Size.No_Container:
                    //            npc.CurrentCell.Add(item);
                    //            npc.receivedItems[chr.PlayerID].Remove(item);
                    //            break;
                    //        case Item.Size.Sack_Only:
                    //            if (npc.sackList.Count < Character.MAX_SACK)
                    //            {
                    //                npc.sackList.Add(item);
                    //                if (!npc.animal && npc.Alignment == chr.Alignment)
                    //                {
                    //                    npc.SendToAllInSight(npc.Name + ": Thank you for the " + item.name + ", " + chr.Name + ".");
                    //                }
                    //            }
                    //            else
                    //            {
                    //                chr.EquipEitherHandOrDrop(item);
                    //            }
                    //            npc.receivedItems[chr.PlayerID].Remove(item);
                    //            break;
                    //    }
                    //} 
                    //    #endregion
                    return false;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        #region Implementor Commands

        public void impcommands(string args)
        {
            if (chr.ImpLevel == Globals.eImpLevel.USER)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            if (chr.ImpLevel == Globals.eImpLevel.DEV)
            {
                string rtnStr = "";

                rtnStr = rtnStr + "create item, ";
                rtnStr = rtnStr + "disconnect, ";
                rtnStr = rtnStr + "findpc, ";
                rtnStr = rtnStr + "getitemstats, ";
                rtnStr = rtnStr + "getpceffects, ";
                rtnStr = rtnStr + "getpcstats, ";
                rtnStr = rtnStr + "getnpceffects, ";
                rtnStr = rtnStr + "getnpcstats, ";
                rtnStr = rtnStr + "givexp, ";
                rtnStr = rtnStr + "gotopc, ";
                rtnStr = rtnStr + "immortal, ";
                rtnStr = rtnStr + "impcast, ";
                rtnStr = rtnStr + "impprep, ";
                rtnStr = rtnStr + "impheal, ";
                rtnStr = rtnStr + "impkill, ";
                rtnStr = rtnStr + "impset, ";
                rtnStr = rtnStr + "impunlock, ";
                rtnStr = rtnStr + "mapemote, ";
                rtnStr = rtnStr + "rename, ";
                rtnStr = rtnStr + "setstat, ";
                rtnStr = rtnStr + "shownpccatalog, ";
                rtnStr = rtnStr + "showitemcatalog, ";
                rtnStr = rtnStr + "showallnpc, ";
                rtnStr = rtnStr + "summon, ";
                rtnStr = rtnStr + "takexp, ";
                rtnStr = rtnStr + "title, ";
                rtnStr = rtnStr + "whereami, ";
                rtnStr = rtnStr + "worldemote";

                chr.WriteToDisplay(rtnStr);
            }
        }

        public void impprep(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            if (args == null)
            {
                chr.WriteToDisplay("What spell do you want to cast?");
                return;
            }
            string[] sArgs = args.Split(" ".ToCharArray());
            
            chr.preppedSpell = Spell.GetSpell(sArgs[0].ToLower());
            if (chr.preppedSpell == null)
            {
                chr.WriteToDisplay("Failed to find the spell " + sArgs[0] + ".");
                return;
            }
            chr.preppedSpell.WarmSpell(chr);
            chr.SendToAllInSight(chr.Name + ": " + Spell.GenerateMagicWords());
        }

        public void impban(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Who do you want to ban?");
                return;
            }

            try
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs.Length < 2)
                {
                    chr.WriteToDisplay("Format: impban <name> <# of days>");
                    return;
                }

                int playerID = PC.GetPlayerID(sArgs[0]);

                if (playerID == -1)
                {
                    chr.WriteToDisplay("Unable to find player named " + sArgs[0] + " in the database.");
                    return;
                }

                int days = Convert.ToInt32(sArgs[1]);

                PC pc = PC.GetPC(playerID);

                Account.SaveAccountField(pc.accountID, "banLength", pc.accountID, "Account " + pc.account + " banned for " + days + " days by " + chr.GetLogString());

                chr.WriteToDisplay("You have banned the account of " + pc.account + " for " + days + " days.");                

                PC pc2 = PC.getOnline(playerID);

                if (pc2 != null)
                {
                    impboot(pc2.Name);
                }

            }
            catch(Exception e)
            {
                chr.WriteToDisplay("Format: impban <name> <# of days>");
                Utils.LogException(e);
            }
        }

        public void impboot(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Who do you want to boot?");
                return;
            }

            try
            {

                PC pc = PC.GetOnline(args);

                if (pc == null)
                {
                    chr.WriteToDisplay("Unable to find player named " + args + ".");
                    return;
                }

                Utils.Log(chr.GetLogString() + " booted " + pc.GetLogString(), Utils.LogType.Unknown);

                chr.WriteToDisplay("You have booted the player named " + pc.Name + ".");

                pc.DisconnectSocket();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                chr.WriteToDisplay("Failed to boot the requested player from the game. See a developer for details.");
            }
        }

        public void impcast(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV && chr.IsPC)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            if (args == null)
            {
                chr.WriteToDisplay("What spell do you want to cast?");
                return;
            }
            String[] sArgs = args.Split(" ".ToCharArray());

            chr.preppedSpell = Spell.GetSpell(sArgs[0].ToString());
            if (chr.preppedSpell == null)
            {
                chr.WriteToDisplay("Failed to find the spell " + sArgs[0] + ".");
                return;
            }
            chr.preppedSpell.CastSpell(chr, args);
            chr.preppedSpell = null;
        }

        public void impheal(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            try
            {
                String[] sArgs = args.Split(" ".ToCharArray());
                Character target = Map.FindTargetInView(chr, sArgs[0], true, true);

                if (target != null)
                {
                    if (target.effectList.ContainsKey(Effect.EffectType.Blind))
                    {
                        target.effectList[Effect.EffectType.Blind].StopCharacterEffect();
                    }
                    target.Stunned = 0;
                    target.Poisoned = 0;
                    target.Hits = target.HitsFull;
                    target.Stamina = target.StaminaFull;
                    target.Mana = target.ManaFull;
                    target.WriteToDisplay("You have been healed by the Ghods.");
                    chr.WriteToDisplay("You have healed " + target.Name + " with your Ghodly powers.");
                    target.SendSound(Spell.GetSpell("cure").SoundFile);
                    chr.SendSound(Spell.GetSpell("cure").SoundFile);
                }
                else
                {
                    if (chr.effectList.ContainsKey(Effect.EffectType.Blind))
                    {
                        chr.effectList[Effect.EffectType.Blind].StopCharacterEffect();
                    }
                    chr.Stunned = 0;
                    chr.Poisoned = 0;
                    chr.Hits = chr.HitsFull;
                    chr.Stamina = chr.StaminaFull;
                    chr.Mana = chr.ManaFull;
                    chr.WriteToDisplay("Target not found. You have been healed. (silently)");
                    chr.SendSound(Spell.GetSpell("cure").SoundFile);
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void impunlock(string args)
        {
            if (chr.ImpLevel >= Globals.eImpLevel.GM)
            {
                string[] sArgs = args.Split(" ".ToCharArray());
                //string dir = sArgs[0];

                Cell cell = Map.GetCellRelevantToCell(chr.CurrentCell, sArgs[0], true);

                //switch (dir)
                //{
                //    case "n":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y - 1, chr.X];
                //        break;
                //    case "s":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y + 1, chr.X];
                //        break;
                //    case "w":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y, chr.X - 1];
                //        break;
                //    case "e":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y, chr.X + 1];
                //        break;
                //    case "nw":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y - 1, chr.X - 1];
                //        break;
                //    case "ne":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y - 1, chr.X + 1];
                //        break;
                //    case "se":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y + 1, chr.X + 1];
                //        break;
                //    case "sw":
                //        cell = World.GetFacetByIndex(0).GetLandByID(chr.LandID).GetMapByID(chr.MapID).cells[chr.Y + 1, chr.X - 1];
                //        break;
                //    default:
                //        break;
                //}
                if (cell != null)
                {
                    Map.UnlockDoor(cell, cell.cellLock.key);
                    chr.WriteToDisplay("You unlock the door.");
                }
            }
            else
            {
                chr.WriteToDisplay("I don't understand your command.");
            }
        }

        public void impset(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Format: impset [time | moon] [times: morning, afternoon, evening, night] [moons: new, waxing_crescent, waning_crescent, full]");
                return;
            }

            try
            {
                string[] sArgs = args.Split(" ".ToCharArray());
                string cycleList = "";

                switch (sArgs[0].ToLower())
                {
                    case "time":
                        if(sArgs.Length >= 2)
                        {
                            foreach(World.DailyCycle dailyCycle in Enum.GetValues(typeof(World.DailyCycle)))
                            {
                                cycleList += dailyCycle.ToString() + ", ";
                                if (sArgs[1].ToLower() == dailyCycle.ToString().ToLower())
                                {
                                    chr.WriteToDisplay("Game time set from " + World.CurrentDailyCycle + " to " + sArgs[1].ToUpper() + ".");
                                    World.CurrentDailyCycle = dailyCycle;
                                    return;
                                }
                            }
                            cycleList = cycleList.Substring(0, cycleList.Length - 2);
                            cycleList += ".";
                            chr.WriteToDisplay("Invalid time argument. Choices are " + cycleList);
                        }
                        break;
                    case "moon":
                        if(sArgs.Length >= 2)
                        {
                            foreach (World.LunarCycle lunarCycle in Enum.GetValues(typeof(World.LunarCycle)))
                            {
                                cycleList += lunarCycle.ToString() + ", ";
                                if (sArgs[1].ToLower() == lunarCycle.ToString().ToLower())
                                {
                                    chr.WriteToDisplay("Moon phase set from " + World.CurrentLunarCycle + " to " + sArgs[1].ToUpper() + ".");
                                    World.CurrentLunarCycle = lunarCycle;
                                    return;
                                }
                            }
                            cycleList = cycleList.Substring(0, cycleList.Length - 2);
                            cycleList += ".";
                            chr.WriteToDisplay("Invalid moon phase argument. Choices are " + cycleList);
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void impkill(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV && chr.IsPC)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
                Rules.DoDamage(chr, chr, chr.HitsFull + 1, false);

            string[] sArgs = args.Split(" ".ToCharArray());
            Character target = Map.FindTargetInView(chr, sArgs[0], false, true);

            if (target != null)
            {
                if (!chr.IsInvisible && chr.IsPC)
                {
                    if (target.race != "")
                    {
                        target.SendToAllInSight(target.Name + " has been smitten by the Ghods!");
                    }
                    else
                    {
                        target.SendToAllInSight("The " + target.Name + " has been smitten by the Ghods!");
                    }
                    if (target.deathSound != "") { target.EmitSound(target.deathSound); }
                }
                Rules.DoDamage(target, chr, target.HitsFull + 1, false);
            }
            else
            {
                chr.WriteToDisplay("You don't see a " + sArgs[0] + " here.");
            }
        }

        public void imprename(string args)
        {
            if (chr.ImpLevel >= Globals.eImpLevel.GM)
            {
                if (args == null)
                {
                    chr.WriteToDisplay("Rename <current name> <new name>");
                    return;
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());
                    foreach (Character ch in Character.pcList)
                    {
                        if (ch.IsPC && ch.Name.ToLower() == sArgs[0].ToLower())
                        {
                            chr.WriteToDisplay("Player name changed from " + ch.Name + " to " + sArgs[1] + ".");
                            ch.Name = sArgs[1];
                            ch.WriteToDisplay("Your name has been changed to " + ch.Name + ".");
                        }
                    }
                    return;
                }
            }
            else
            {
                chr.WriteToDisplay("I don't understand your command.");
            }
        }

        public void impgetnpceffects(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("getnpceffects (target)");
                    return;
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());
                    Character target = Map.FindTargetInView(chr, sArgs[0], false, false);
                    if (target != null)
                    {
                        string effectslisting = "";
                        foreach(Effect fx in target.effectList.Values)
                        {
                            effectslisting += " [" + Effect.GetEffectName(fx.effectType) + " Amount: " + fx.effectAmount + " Duration: " + fx.duration + "]";
                        }
                        chr.WriteToDisplay(target.Name + "'s Effects " + effectslisting);
                        chr.WriteToDisplay(target.Name + "'s Protections FIRE: Resist: " + target.FireResistance + " Pro: " + target.FireProtection + " ICE: Resist: " + target.ColdResistance + " Pro: " + target.ColdProtection + " LIGHTNING: Resist: " + target.LightningResistance + " Pro: " + target.LightningProtection +
                            " POISON: Resist: " + target.PoisonResistance + " Pro: " + target.PoisonProtection + " BLIND: Resist: " + target.BlindResistance + " STUN: Resist: " + target.StunResistance + " DEATH: Resist: " + target.DeathResistance + " Pro: " + target.DeathProtection + " BRWATER: " + target.CanBreatheWater.ToString() +
                            " FEATHERFALL: " + target.HasFeatherFall.ToString() + " CANSWIM: " + target.CanBreatheWater.ToString() + " CANFLY: " + target.CanFly.ToString() + " CANCOMMAND: " + target.canCommand.ToString() + " Shield: " + target.Shielding);
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find " + sArgs[0] + ".");
                        return;
                    }
                }
            }
        }

        public void impgetpceffects(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("getpceffects (player)");
                    return;
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());
                    Character target = new Character();
                    foreach (Character chra in Character.pcList)
                    {
                        if (chra.Name.ToLower() == sArgs[0].ToLower())
                        {
                            target = chra;
                        }
                    }
                    if (target != null || target.Name != "Nobody")
                    {
                        string effectslisting = "";
                        foreach (Effect fx in target.effectList.Values)
                        {
                            effectslisting += " [" + Effect.GetEffectName(fx.effectType) + " Amount: " + fx.effectAmount + " Duration: " + fx.duration + "]";
                        }
                        chr.WriteToDisplay(target.Name + "'s Effects " + effectslisting);
                        chr.WriteToDisplay(target.Name + "'s Protections FIRE: Resist: " + target.FireResistance + " Pro: " + target.FireProtection + " ICE: Resist: " + target.ColdResistance + " Pro: " + target.ColdProtection + " LIGHTNING: Resist: " + target.LightningResistance + " Pro: " + target.LightningProtection +
                            " POISON: Resist: " + target.PoisonResistance + " Pro: " + target.PoisonProtection + " BLIND: Resist: " + target.BlindResistance + " STUN: Resist: " + target.StunResistance + " DEATH: Resist: " + target.DeathResistance + " Pro: " + target.DeathProtection + " BRWATER: " + target.CanBreatheWater.ToString() +
                            " FEATHERFALL: " + target.HasFeatherFall.ToString() + " CANSWIM: " + target.CanBreatheWater.ToString() + " CANFLY: " + target.CanFly.ToString() + " CANCOMMAND: " + target.canCommand.ToString() + " Shield: " + target.Shielding);
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find " + sArgs[0] + ".");
                        return;
                    }
                }
            }
        }

        public void impgetpcstats(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("getpcstats (player)");
                    return;
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());
                    Character target = new Character();
                    foreach (Character chra in Character.pcList)
                    {
                        if (chra.Name.ToLower() == sArgs[0].ToLower())
                        {
                            target = chra;
                        }
                    }
                    if (target.Name != "Nobody")
                    {
                        chr.WriteToDisplay(target.Name + "'s stats: CLASS: " + target.BaseProfession + " LEVEL: " + Rules.GetExpLevel(target.Experience) +
                            " STR:" + target.Strength + " DEX:" + target.Dexterity + " INT:" + target.Intelligence + " WIS:" + target.Wisdom +
                            " CON:" + target.Constitution + " CHR:" + target.Charisma + " HP:" + target.Hits + "/" + target.HitsFull +
                            " STAM:" + target.Stamina + "/" + target.StaminaFull + " MANA:" + target.Mana + "/" + target.ManaFull + " XP:" + target.Experience +
                            " BANK:" + target.bankGold + " KILLS:" + target.Kills + " DEATHS:" + target.Deaths + " MANAREGEN: " + target.manaRegen);
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find " + sArgs[0] + ".");
                        return;
                    }
                }
            }
        }

        public void impgetnpcstats(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("getnpcstats <npcName>");
                    return;
                }
                else
                {
                    NPC target = (NPC)Map.FindTargetInView(chr, args, true, true);

                    if (target == null)
                    {
                        for (int a = 0; a < Character.NPCList.Count; a++)
                        {
                            target = (NPC)Character.NPCList[a];
                            if (target.Name.ToLower() == args.ToLower())
                            {
                                if (target.MapID == chr.MapID)
                                {
                                    break;
                                }
                            }
                        }
                        if (target.Name.ToLower() != args.ToLower())
                        {
                            for (int a = 0; a < Character.NPCList.Count; a++)
                            {
                                target = (NPC)Character.NPCList[a];
                                if (target.Name.ToLower() == args.ToLower())
                                {
                                    break;
                                }
                            }
                        }
                    }
                    if (target.Name.ToLower() == args.ToLower())
                    {
                        chr.WriteToDisplay("--- getnpcstats " + args + " ---");
                        chr.WriteToDisplay(target.GetLogString());
                        chr.WriteToDisplay("Hits: " + target.Hits + " / " + target.HitsMax);
                        chr.WriteToDisplay("Stam: " + target.Stamina + " / " + target.StaminaMax);
                        chr.WriteToDisplay("Mana: " + target.Mana + " / " + target.ManaMax);
                        if (target.IsSpellUser)
                        {
                            chr.WriteToDisplay("Abjuration Spells: " + target.abjurationSpells.Count);
                            chr.WriteToDisplay("Alteration Spells: " + target.alterationSpells.Count);
                            chr.WriteToDisplay("Alteration Harmful Spells: " + target.alterationHarmfulSpells.Count);
                            chr.WriteToDisplay("Conjuration Spells: " + target.conjurationSpells.Count);
                            chr.WriteToDisplay("Divination Spells: " + target.divinationSpells.Count);
                            chr.WriteToDisplay("Evocation Spells: " + target.evocationSpells.Count);
                            chr.WriteToDisplay("Evocation AE Spells: " + target.evocationAreaEffectSpells.Count);
                        }
                        if (target.preppedSpell != null)
                        {
                            chr.WriteToDisplay("Prepped Spell: " + Spell.GetLogString(target.preppedSpell));
                        }
                        string spellsKnown = "";
                        for (int a = 0; a < target.spellList.Count; a++)
                        {
                            spellsKnown += Spell.GetSpell((int)target.spellList.ints[a]).Name + ", ";
                        }
                        if (spellsKnown != "")
                        {
                            chr.WriteToDisplay("Spellbook: " + spellsKnown.Substring(0, spellsKnown.Length - 2));
                        }
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find " + args + ".");
                        return;
                    }
                }
            }
        }

        public void imptest(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                Globals.eSkillType[] skills = Skills.GetXHighestSkills(chr, 3);

                if (skills == null)
                    return;

                for(int count = 0; count < skills.Length; count++)
                {
                    chr.WriteToDisplay((count + 1) + ". " + Utils.FormatEnumString(skills[count].ToString()) + " - " + chr.GetSkillExperience(skills[count]));
                }
            }
        }

        public void impworldemote(string text)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                foreach (Character chra in Character.pcList)
                {
                    chra.WriteToDisplay(text);
                }
            }
        }

        public void impmapemote(string text)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                foreach (Character chra in Character.pcList)
                {
                    if (chra.MapID == chr.MapID)
                    {
                        chra.WriteToDisplay(text);
                    }
                }
            }
        }

        public void impgivexp(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("givexp (amount) (target)");
                }
                else
                {
                    string[] sArgs = args.Split(" ".ToCharArray());

                    int amount = Convert.ToInt32(sArgs[0]);

                    Character target = null;

                    foreach (Character chra in Character.pcList)
                    {
                        if (chra.Name.ToLower() == sArgs[1].ToLower())
                        {
                            target = chra;
                        }
                    }

                    if (target.Name != "Nobody")
                    {
                        target.Experience += amount;
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find target named " + sArgs[1] + ".");
                        return;
                    }
                }
            }
        }

        public void imptakexp(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("takexp (amount) (target)");
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());
                    int amount = Convert.ToInt32(sArgs[0]);
                    Character target = new Character();
                    foreach (Character chra in Character.pcList)
                    {
                        if (chra.Name.ToLower() == sArgs[1].ToLower())
                        {
                            target = chra;
                        }
                    }
                    if (target.Name != "Nobody")
                    {
                        target.Experience -= amount;
                    }
                    else
                    {
                        chr.WriteToDisplay("Could not find target.");
                        return;
                    }
                }
            }
        }

        public void impsetstat(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEV)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("SetStat ('str ... cha, implevel') (value) (player)");
                }
                else
                {
                    String[] sArgs = args.Split(" ".ToCharArray());

                    string stat = sArgs[0].ToLower();

                    int num = Convert.ToInt32(sArgs[1]);

                    Character target = new Character();

                    foreach (Character chra in Character.pcList)
                    {
                        if (chra.Name.ToLower() == sArgs[2].ToLower())
                        {
                            target = chra;
                        }
                    }
                    if (target.Name != "Nobody")
                    {
                        switch (stat)
                        {
                            case "con":
                                chr.WriteToDisplay(target.Name + "'s constitution changed from " + target.Constitution + " to " + num + ".");
                                target.Constitution = num;
                                break;
                            case "str":
                                chr.WriteToDisplay(target.Name + "'s strength changed from " + target.Strength + " to " + num + ".");
                                target.Strength = num;
                                break;
                            case "dex":
                                chr.WriteToDisplay(target.Name + "'s dexterity changed from " + target.Dexterity + " to " + num + ".");
                                target.Dexterity = num;
                                break;
                            case "int":
                                chr.WriteToDisplay(target.Name + "'s intelligence changed from " + target.Intelligence + " to " + num + ".");
                                target.Intelligence = num;
                                break;
                            case "wis":
                                chr.WriteToDisplay(target.Name + "'s wisdom changed from " + target.Wisdom + " to " + num + ".");
                                target.Wisdom = num;
                                break;
                            case "cha":
                                chr.WriteToDisplay(target.Name + "'s charisma changed from " + target.Charisma + " to " + num + ".");
                                target.Charisma = num;
                                break;
                            case "implevel":
                                chr.WriteToDisplay(target.Name + "'s implevel changed from " + target.ImpLevel + " to " + num + ".");
                                target.ImpLevel = (Globals.eImpLevel)num;
                                break;
                            case "invis":
                                if (num == 1)
                                {
                                    chr.WriteToDisplay(target.Name + " is now invisible.");
                                    if (target.Name != chr.Name) { target.WriteToDisplay("You are now invisible."); }
                                    target.IsInvisible = true;
                                }
                                else
                                {
                                    chr.WriteToDisplay(target.Name + " is no longer invisible.");
                                    if (target.Name != chr.Name) { target.WriteToDisplay("You are no longer invisible."); }
                                    target.IsInvisible = false;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    return;
                }
            }
        }

        public void impwhereami(string args)
        {
            string info = chr.Name + " F: " + chr.FacetID + " L: " + chr.LandID + " M: " + chr.MapID + " ";
            info += "X: " + chr.X + " Y: " + chr.Y + " Z: " + chr.Z;
            chr.WriteToDisplay(info);
        }

        public void impattune(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            try
            {
                String[] sArgs = args.Split(" ".ToCharArray());
                Item item = Item.FindItemOnGround(sArgs[0], chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                if (item == null)
                {
                    if (chr.RightHand != null && chr.RightHand.name == sArgs[0].ToLower())
                    {
                        item = chr.RightHand;
                    }
                    else if (chr.LeftHand != null && chr.LeftHand.name == sArgs[0].ToLower())
                    {
                        item = chr.LeftHand;
                    }
                }

                if (item != null)
                {
                    PC pc = PC.GetPC(PC.GetPlayerID(sArgs[1]));

                    if (pc != null)
                    {
                        item.AttuneItem(pc.PlayerID, chr.GetLogString() + " used impattune to attune " + item.GetLogString() + " to " + pc.GetLogString() + ".");
                        chr.WriteToDisplay("You have successfully attuned " + sArgs[0] + " to " + pc.Name + ".");
                    }
                    else
                    {
                        chr.WriteToDisplay("Unable to find the player named " + sArgs[1] + " in the database.");
                    }
                }
                else
                {
                    chr.WriteToDisplay("Unable to find an item named " + sArgs[0] + " on the ground or in either of your hands.");
                }
            }
            catch
            {
                chr.WriteToDisplay("Format: impattune <item name> <player name>");
            }

        }

        public void impgohome(string args)
        {
            this.impgotoloc("" + chr.FacetID.ToString() + " 2 10 20 30 0");
        }

        public void impgotoloc(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.AGM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Format: impgotoloc <facet> <land> <map> <ycord> <xcord> <zcord>");
            }
            else
            {
                try
                {
                    string[] sArgs = args.Split(" ".ToCharArray());
                    short facet, land, map;
                    int x, y, z;
                    facet = Convert.ToInt16(sArgs[0]);
                    land = Convert.ToInt16(sArgs[1]);
                    map = Convert.ToInt16(sArgs[2]);
                    x = Convert.ToInt32(sArgs[3]);
                    y = Convert.ToInt32(sArgs[4]);
                    z = Convert.ToInt32(sArgs[5]);

                    if (Cell.GetCell(facet, land, map, x, y, z) == null)
                    {
                        chr.WriteToDisplay("That place is outside of the map bounds.");
                    }
                    else
                    {
                        if (!chr.IsInvisible)
                        {
                            chr.SendToAllInSight(chr.Name + " disappears in a poof of smoke!");
                        }
                        chr.CurrentCell = Cell.GetCell(facet, land, map, x, y, z);
                        if (!chr.IsInvisible)
                        {
                            chr.SendToAllInSight(chr.Name + " appears with a poof of smoke.");
                        }
                    }
                }
                catch
                {
                    chr.WriteToDisplay("Format: impgotoloc <facet> <land> <map> <ycord> <xcord> <zcord>");
                }
            }
            
        }

        public void impshowcell(string args)
        {
            Item tempItem = new Item();
            Character c;
            int x = chr.CurrentCell.Items.Count;
            int i = 0;
            while (x > 0)
            {
                tempItem = (Item)chr.CurrentCell.Items[i];
                Utils.Log("Item " + i + ": " + tempItem.name + " desc: " + tempItem.longDesc, Utils.LogType.Unknown);
                x--;
                i++;
            }
            if (chr.CurrentCell.Characters.Count > 0)
            {
                for (int j = 0; j < chr.CurrentCell.Characters.Count; j++)
                {
                    c = (Character)chr.CurrentCell.Characters[j];
                    Utils.Log("Characters: " + j + ": " + c.Name, Utils.LogType.Unknown);
                }
            }
            return;
        }

        public void impsummon(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.AGM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            else
            {
                if (args == null)
                {
                    chr.WriteToDisplay("Format: impsummon <name>");
                    return;
                }

                foreach (Character ch in Character.pcList)
                {
                    if (ch.Name.ToLower() == args.ToLower())
                    {
                        chr.WriteToDisplay("You have summoned the player named " + ch.Name + " from Facet: " + ch.FacetID + " Land: " + ch.LandID +
                            " Map: " + ch.MapID + " X: " + ch.X + " Y: " + ch.Y + " Z: " + ch.Z);
                        ch.CurrentCell = chr.CurrentCell;
                        ch.WriteToDisplay("You have been summoned by the Ghods.");
                        return;
                    }
                }

                NPC npc = NPC.findNPCinWorld(args);

                if (npc == null)
                {
                    chr.WriteToDisplay("Could not find a player or NPC with the name " + args + ".");
                    return;
                }

                chr.WriteToDisplay("You have summoned the player named " + npc.Name + " from Facet: " + npc.FacetID + " Land: " + npc.LandID +
                            " Map: " + npc.MapID + " X: " + npc.X + " Y: " + npc.Y + " Z: " + npc.Z);
                npc.CurrentCell = chr.CurrentCell;
            }
        }

        public void impnewspawn(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEVJR)
            {
                chr.WriteToDisplay("Security: Request denined!");
                return;
            }
            String[] sArgs = args.Split("=".ToCharArray());
            if (sArgs.Length < 14)
            {
                chr.WriteToDisplay("newspawn <enabled>=<NPCID>=<SpawnTimer>=<MaxAllowedInZone>=<SpawnMessage>=<SinglePoint>=<MinZone>=<MaxZone>=<NPCList>=<spawnLand>=<spawnMap>=<spawnXcord>=<spawnYcord>=<spawnRadius>");
                return;
            }
            // Args:
            // 0 : enabled
            // 1 : NPCID
            // 2 : SpawnTimer
            // 3 : MaxAllowedInZone
            // 4 : SpawnMessage
            // 5 : SinglePoint
            // 6 : MinZone
            // 7 : MaxZone
            // 8 : NPCList
            // 9 : spawnLand
            // 10: spawnMap
            // 11: spawnXcord
            // 12: spawnYcord
            // 13: spawnRadius
            bool enabled = false;
            int NPCID = Convert.ToInt32(sArgs[1].ToString());
            int SpawnTimer = Convert.ToInt32(sArgs[2].ToString());
            int MaxAllowedInZone = Convert.ToInt32(sArgs[3].ToString());
            string SpawnMessage = sArgs[4].ToString();
            bool SinglePoint = false;
            int minZone = Convert.ToInt32(sArgs[6].ToString());
            int maxZone = Convert.ToInt32(sArgs[7].ToString());
            string NPCList = sArgs[8].ToString();
            int spawnLand = Convert.ToInt32(sArgs[9].ToString());
            int spawnMap = Convert.ToInt32(sArgs[10].ToString());
            int spawnXcord = Convert.ToInt32(sArgs[11].ToString());
            int spawnYcord = Convert.ToInt32(sArgs[12].ToString());
            int spawnRadius = Convert.ToInt32(sArgs[13].ToString());

            if (sArgs[0].ToString() == "1" || sArgs[0].ToString() == "true")
            {
                enabled = true;
            }
            if (sArgs[5].ToString() == "1" || sArgs[5].ToString() == "true")
            {
                SinglePoint = true;
            }
            DAL.DBEditor.UpdateSpawnZone(true, enabled, 0, NPCID, SpawnTimer, MaxAllowedInZone, SpawnMessage, SinglePoint, NPCList, minZone, maxZone, spawnLand, spawnMap, spawnXcord, spawnYcord, spawnRadius);
            chr.WriteToDisplay("Spawner created at M: " + spawnMap + " X: " + spawnXcord + " Y: " + spawnYcord);

            return;
        }

        public void impreloadspawnzones(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.DEVJR)
            {
                return;
            }
            SpawnZone.Spawns.Clear();
            foreach (Character critter in Character.NPCList)
            {
                critter.RemoveFromWorld();
                //critter.removeFromLimbo();
                //critter.removeFromServer();
            }
            Character.NPCList.Clear();
            SpawnZone.LoadSpawnZones();
            foreach (Facet facet in World.Facets)
            {
                foreach (SpawnZone szl in facet.Spawns.Values)
                {
                    szl.Timer = 10000;
                }
            }
            NPC.DoSpawn();
            chr.WriteToDisplay("System: Spawnzones reloaded - Total zones: " + SpawnZone.Spawns.Count.ToString());
            chr.WriteToDisplay("System: Spawners have been reset and triggered. - Total Mobs: " + Character.NPCList.Count.ToString());
            return;
        }

        public void impgotonpc(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            if (args == null)
            {
                chr.WriteToDisplay("gotonpc <NPC Name>");
                return;
            }
            else
            {
                string[] sArgs = args.Split(" ".ToCharArray());
                if (sArgs.Length == 1)
                {
                    foreach (Character npc in Character.NPCList)
                    {
                        if (npc.Name.ToLower() == args.ToLower() && npc.MapID == chr.MapID)
                        {
                            chr.CurrentCell = npc.CurrentCell;
                            chr.WriteToDisplay("You have teleported to " + npc.GetLogString() + ".");
                            return;
                        }

                    }
                    foreach (Character npc in Character.NPCList)
                    {
                        if (npc.Name.ToLower() == args.ToLower())
                        {
                            chr.CurrentCell = npc.CurrentCell;
                            chr.WriteToDisplay("You have teleported to " + npc.GetLogString() + ".");
                            return;
                        }
                    }
                }
                else if (sArgs.Length == 2) // gotonpc <npc name> <npc class>
                {
                    foreach (Character npc in Character.NPCList)
                    {
                        if (npc.Name.ToLower() == sArgs[0].ToLower() && npc.BaseProfession.ToString().ToLower() == sArgs[1].ToLower())
                        {
                            chr.CurrentCell = npc.CurrentCell;
                            chr.WriteToDisplay("You have teleported to " + npc.GetLogString() + ".");
                            return;
                        }

                        if (npc.Name.ToLower() == sArgs[0].ToLower() + " " + sArgs[1].ToLower())
                        {
                            chr.CurrentCell = npc.CurrentCell;
                            chr.WriteToDisplay("You have teleported to " + npc.GetLogString() + ".");
                            return;
                        }
                    }
                }
                chr.WriteToDisplay("Could not find NPC matching args: " + args + ".");
            }
        }

        public void impgotopc(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }
            if (args == null)
            {
                chr.WriteToDisplay("gotopc <PlayerName>");
                return;
            }
            else
            {
                foreach (Character ch in Character.pcList)
                {
                    if (ch.Name.ToLower() == args.ToLower())
                    {
                        chr.CurrentCell = ch.CurrentCell;
                        chr.WriteToDisplay("You have teleported to " + ch.GetLogString() + " on map " + World.GetFacetByID(ch.FacetID).GetLandByID(ch.LandID).GetMapByID(ch.MapID).Name + ".");
                        return;
                    }
                }
                chr.WriteToDisplay("Could not find character.");
            }
        }

        public void impcreate(string args)
        {
            if (chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("I don't understand your command.");
                return;
            }

            if (args == null)
            {
                chr.WriteToDisplay("Create [ npc | item | coin ] #");
            }
            else
            {
                String[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs[0].ToLower().Equals("item"))
                {
                    int itemNum = Convert.ToInt32(sArgs[1]);
                    Item item = Item.CopyItemFromDictionary(itemNum);
                    if (item == null)
                    {
                        chr.WriteToDisplay("Item " + sArgs[1] + " not found in item catalog.");
                        return;
                    }

                    if (item.vRandLow > 0) { item.coinValue = Rules.dice.Next(item.vRandLow, item.vRandHigh); }
                    item.whoCreated = chr.GetLogString();
                    chr.CurrentCell.Add(item);
                    chr.WriteToDisplay(item.notes + " created.");

                }
                else if (sArgs[0].ToLower().Equals("npc"))
                {
                    NPC.CreateNPC(Convert.ToInt32(sArgs[1]), chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                    chr.WriteToDisplay("NPC created.");
                }
                else if (sArgs[0].ToLower().IndexOf("coin") != -1)
                {
                    Item coins = Item.CopyItemFromDictionary(Item.ID_COINS);

                    double amount = Convert.ToInt64(sArgs[1]);

                    coins.coinValue = amount;

                    chr.CurrentCell.Add(coins);

                    chr.WriteToDisplay(amount + " coins created.");
                }
                else
                {
                    chr.WriteToDisplay("Format: create [ npc | item | coin ] #");
                }
            }
        }

        public void impgetitemstats(string args)
        {
            if (chr.ImpLevel >= Globals.eImpLevel.DEV)
            {
                Item tmpItem = chr.getItemFromHand(args);
                if (tmpItem == null)
                {
                    tmpItem = Item.FindItemOnGround(args, chr.FacetID, chr.LandID, chr.MapID, chr.X, chr.Y, chr.Z);
                }
                if (tmpItem == null)
                {
                    chr.WriteToDisplay("Unable to find target item \"" + args + "\".");
                    return;
                }
                string effectNames = "none";
                if (tmpItem.effectType != "")
                {
                    effectNames = "";
                    string[] effectsList = tmpItem.effectType.Split(" ".ToCharArray());
                    string[] effectsAmt = tmpItem.effectAmount.Split(" ".ToCharArray());
                    for (int a = 0; a < effectsList.Length; a++)
                    {
                        effectNames += "[" + effectsList[a] + "]" + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(effectsList[a])) + "(" + effectsAmt[a] + ") ";
                    }
                }
                string returnString = tmpItem.GetLogString() + " itemType: " + tmpItem.itemType + " baseType: " +
                    tmpItem.baseType + " shortDesc: " + tmpItem.shortDesc + " weight: " + tmpItem.weight + " effects: " +
                    effectNames + " coinValue: " + tmpItem.coinValue + " vRandLow:" + tmpItem.vRandLow + " vRandHigh: " +
                    tmpItem.vRandHigh + " size: " + tmpItem.size + " special: " + tmpItem.special + " minDamage: " +
                    tmpItem.minDamage + " maxDamage: " + tmpItem.maxDamage + " dropRound: " + tmpItem.dropRound;
                if (tmpItem.spell >= 0)
                    returnString += " spell: " + Spell.GetSpell(tmpItem.spell).Name + " charges: " + tmpItem.charges + " spellPower: " + tmpItem.spellPower;
                chr.WriteToDisplay(returnString);
            }
            else
            {
                chr.WriteToDisplay("I don't understand your command.");
            }
        }
        #endregion

        public void exit(string args)
        {
            quit(args);
        }
        public void forcequit(string args)
        {
            if (DragonsSpineMain.ServerStatus == DragonsSpineMain.ServerState.Locked)
            {
                if (chr.protocol == "old-kesmai") { chr.Write(Map.KP_ENHANCER_DISABLE); }
                chr.RemoveFromWorld();
                chr.PCState = Globals.ePlayerState.CONFERENCE;
                chr.AddToLimbo();
                Conference.Header(chr, true);
                PC pc = (PC)chr;
                System.Threading.Thread saveThread = new System.Threading.Thread(pc.Save);
                saveThread.Start();
                return;
            }
        }
        public void quit(string args)
        {
            chr.cmdWeight += 2;
            if (chr.cmdWeight > 3)
            {
                return;
            }
            chr.CommandType = CommandType.Quit;

            if (chr.IsDead)
            {
                Rules.DeadRest(chr);
                return;
            }

            #region Cannot quit in a no recall zone
            if (chr.CurrentCell.IsNoRecall && chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("You cannot quit here.");
                return;
            }
            #endregion

            #region Cannot quit next to an altar or counter
            if (Map.nearCounter(chr) && chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("You cannot quit in front of a counter or an altar.");
                return;
            }
            #endregion

            #region Cannot quit in front of lockers
            if (chr.CurrentCell.IsLocker && chr.ImpLevel < Globals.eImpLevel.GM)
            {
                chr.WriteToDisplay("You cannot quit in front of your locker.");
                return;
            }
            #endregion

            if (chr.RightHand != null && chr.RightHand.itemType == Globals.eItemType.Corpse)
            {
                chr.WriteToDisplay("You cannot quit while holding a corpse.");
                return;
            }

            if (chr.LeftHand != null && chr.LeftHand.itemType == Globals.eItemType.Corpse)
            {
                chr.WriteToDisplay("You cannot quit while holding a corpse.");
                return;
            }

            if (chr.damageRound <= DragonsSpineMain.GameRound - 3 ||
                DragonsSpineMain.ServerStatus == DragonsSpineMain.ServerState.Locked ||
                chr.ImpLevel > Globals.eImpLevel.USER)
            {
                if (chr.protocol == "old-kesmai") { chr.Write(Map.KP_ENHANCER_DISABLE); }
                chr.RemoveFromWorld();
                chr.PCState = Globals.ePlayerState.CONFERENCE;
                chr.AddToLimbo();
                Conference.Header(chr, true);
                PC pc = (PC)chr;
                System.Threading.Thread saveThread = new System.Threading.Thread(pc.Save);
                saveThread.Start();
                return;
            }

            chr.WriteToDisplay("You must wait three rounds after taking damage to quit.");
        }

        public void tell(string args)
        {
            if(args.Equals(null) || args.Equals(""))
            {
                if (chr.receivePages)
                {
                    chr.receivePages = false;
                    chr.WriteToDisplay("You will no longer receive tells.");
                }
                else
                {
                    chr.receivePages = true;
                    chr.WriteToDisplay("You will now receive tells.");
                }
                PC.saveField(chr.PlayerID, "receiveTells", chr.receiveTells, null);
                return;
            }

            string[] sArgs = args.Split(Protocol.ASPLIT.ToCharArray());

            if (sArgs.Length < 1 || sArgs[0] == "")
            {
                chr.WriteToDisplay("Format: tell <name> <message>");
                return;
            }

            PC pc = PC.GetOnline(sArgs[0]);

            if (pc == null)
            {
                chr.WriteToDisplay(sArgs[0] + " is not online.");
                return;
            }

            string message = "";
            for (int a = 1; a < sArgs.Length; a++)
            {
                message += sArgs[a] + " ";
            }
            message = message.Trim();

            if (pc.PCState == Globals.ePlayerState.PLAYING)
            {
                pc.WriteToDisplay(chr.Name + " tells you, \"" + message + "\"");
                chr.WriteToDisplay("You tell " + pc.Name + ", \"" + message + "\"");
            }
            else
            {
                pc.WriteLine(chr.Name + " tells you, \"" + message + "\"", Protocol.TextType.Private);
                chr.WriteToDisplay("You tell " + pc.Name + ", \"" + message + "\"");
            }

        }

        public void forgive(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Who do you want to forgive?", Protocol.TextType.Status);
                return;
            }

            PC pc = PC.GetPC(PC.GetPlayerID(args));

            if (pc == null)
            {
                chr.WriteToDisplay("There was no player found named " + args + ".", Protocol.TextType.Error);
                return;
            }
            if (pc.PlayersKilled.Contains(chr.PlayerID))
            {
                pc.PlayersKilled.Remove(chr.PlayerID);

                if (pc.currentMarks > 0)
                {
                    pc.currentMarks--;
                }

                chr.WriteToDisplay("You have forgiven " + pc.Name + ".", Protocol.TextType.Status);

                PC online = PC.getOnline(pc.PlayerID);

                if (online != null)
                {
                    online.WriteLine(chr.Name + " has forgiven you.", Protocol.TextType.Status);
                }
            }
            else
            {
                chr.WriteToDisplay(pc.Name + " has not killed you without provocation.", Protocol.TextType.Error);
            }
        }

        public void pet(string args)
        {
            if (args == null || args == "")
            {
                chr.WriteToDisplay("Who do you want to pet?");
                return;
            }

            Character target = Map.FindTargetInCell(chr, args);

            if (target == null)
            {
                chr.WriteToDisplay("You do not see " + args + " here.");
                return;
            }

            if (target.race != "")
            {
                
                chr.WriteToDisplay("You pet " + target.Name + ".");
            }
            else
            {
                if (target.IsSummoned && target.Alignment == chr.Alignment)
                {
                    if (target.special.Contains("figurine"))
                    {
                        chr.WriteToDisplay("You pet the " + target.Name + ".");
                        Rules.DespawnFigurine(target as NPC);
                    }
                    else
                    {
                        chr.WriteToDisplay("You pet the " + target.Name + ".");
                    }
                    return;
                }
                chr.WriteToDisplay("You pet the " + target.Name + ".");
                
                if (target.canCommand)
                {
                    Effect.CreateCharacterEffect(Effect.EffectType.Dog_Follow, 0, target, Rules.RollD(1, 10), chr);
                }
            }

            // good luck dogs
            if (target.Name == "dog" && (target.Alignment == Globals.eAlignment.Lawful || target.Alignment == chr.Alignment))
            {
                if (Rules.RollD(1, 20) == 20)
                {
                    chr.WriteToDisplay("The dog wags it's tail.");
                    target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DogBark));
                    Effect.CreateCharacterEffect(Effect.EffectType.Dog_Luck, 0, chr, Rules.RollD(2, 100) + 10, null);
                }
                else if (Rules.RollD(1, 100) < 10)
                {
                    target.SendToAllInSight(target.Name + ": Woof woof woof!");
                    target.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DogBark));
                }
            }

            if (target.IsPC)
            {
                target.WriteToDisplay(chr.Name + " is petting you.");
                if (chr.PlayersFlagged.Remove(target.PlayerID))
                {
                    chr.WriteToDisplay(target.Name + " is no longer flagged.");
                }
            }
        }

        public void quaff(string args)
        {
            Bottle bottle;

            try
            {
                if (args == null || args == "")
                {
                    if (chr.RightHand != null && chr.RightHand.itemID == Item.ID_BALM)
                    {
                        bottle = (Bottle)chr.RightHand;
                        if (!bottle.IsEmpty())
                        {
                            Bottle.OpenBottle(bottle, chr);
                            Bottle.DrinkBottle(bottle, chr);
                            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                            chr.UnEquipRightHand(chr.RightHand);
                            return;
                        }
                    }
                    if (chr.LeftHand != null && chr.LeftHand.itemID == Item.ID_BALM)
                    {
                        bottle = (Bottle)chr.LeftHand;
                        if (!bottle.IsEmpty())
                        {
                            Bottle.OpenBottle(bottle, chr);
                            Bottle.DrinkBottle(bottle, chr);
                            chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                            chr.UnEquipLeftHand(chr.LeftHand);
                            return;
                        }
                    }
                    foreach (Item item in chr.sackList)
                    {
                        if (item.itemID == Item.ID_BALM)
                        {
                            bottle = (Bottle)item;
                            if (!bottle.IsEmpty())
                            {
                                Bottle.OpenBottle(bottle, chr);
                                Bottle.DrinkBottle(bottle, chr);
                                chr.EmitSound(Sound.GetCommonSound(Sound.CommonSound.BreakingGlass));
                                chr.RemoveFromSack(item);
                                return;
                            }
                        }
                    }
                    chr.WriteToDisplay("You don't have any balms to quaff. Good luck.");
                }
                // TODO: else if supplied a worldItemID
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        #region Emotes
        public void applaud(string args)
        {
            if (args == null || args == "")
            {
                chr.SendToAllInSight(chr.Name + " applauds the performance.");
                chr.WriteToDisplay("You applaud the performance.");
            }
        }
        public void babble(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void bark(string args)
        {
            chr.SendToAllInSight(chr.Name + " barks like a rabid dog.");
            chr.WriteToDisplay("You bark like a rabid dog.");
        }
        public void beam(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void bite(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void blink(string args)
        {
            chr.SendToAllInSight(chr.Name + " blinks.");
            chr.WriteToDisplay("You blinks.");
        }
        public void blush(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void bounce(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void bow(string args)
        {
            chr.SendToAllInSight(chr.Name + " bows.");
            chr.WriteToDisplay("You bow.");
        }
        public void cackle(string args)
        {
            chr.SendToAllInSight(chr.Name + " cackles.");
            chr.WriteToDisplay("You cackle.");
        }
        public void chant(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void chuckle(string args)
        {
            chr.SendToAllInSight(chr.Name + " chuckles to " + Character.possessive[(int)chr.gender].ToLower() + "self.");
            chr.WriteToDisplay("You chuckle to yourself.");
        }
        public void clap(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void clench(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void cough(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void cringe(string args)
        {
            chr.SendToAllInSight(chr.Name + " cringes in fear.");
            chr.WriteToDisplay("You cringe with fear.");
        }
        public void cry(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void curtsy(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void dance(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void drool(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void duck(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void fidget(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void flex(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void flinch(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void frown(string args)
        {
            chr.SendToAllInSight(chr.Name + " frowns deeply.");
            chr.WriteToDisplay("You frown deeply.");
        }
        public void gag(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void gasp(string args)
        {
            chr.SendToAllInSight(chr.Name + " gasps!");
            chr.WriteToDisplay("You gasp!");
        }
        public void giggle(string args)
        {
            chr.SendToAllInSight(chr.Name + " giggles.");
            chr.WriteToDisplay("You giggle.");
        }
        public void glare(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void glower(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void gnash(string args)
        {
            chr.SendToAllInSight(chr.Name + " gnashes " + Character.possessive[(int)chr.gender].ToLower() + " teeth.");
            chr.WriteToDisplay("You gnash your teeth.");
        }
        public void grin(string args)
        {
            chr.SendToAllInSight(chr.Name + " grins.");
            chr.WriteToDisplay("You grin.");
        }
        public void grit(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void grunt(string args)
        {
            chr.SendToAllInSight(chr.Name + " grunts.");
            chr.WriteToDisplay("You grunt.");
        }
        public void groan(string args)
        {
            chr.SendToAllInSight(chr.Name + " groans.");
            chr.WriteToDisplay("You groans.");
        }
        public void grumble(string args)
        {
            chr.SendToAllInSight(chr.Name + " grumble.");
            chr.WriteToDisplay("You grumble.");
        }
        public void gulp(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void hiccup(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void hiss(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void howl(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void hug(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void kiss(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void laugh(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void lick(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void nod(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void pinch(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void roar(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void rub(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void scowl(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void scream(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void shrug(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void shudder(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void sigh(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void smile(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void snarl(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void sneer(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void sneeze(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void snicker(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void sniff(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void squeal(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void stretch(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void strut(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void wail(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void wave(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void whimper(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void whine(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void whistle(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void wince(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void wink(string args) { chr.WriteToDisplay("I don't understand your command."); }
        public void yawn(string args)
        {
            chr.SendToAllInSight(chr.Name + " yawns.");
            chr.WriteToDisplay("You yawn.");
        }
        public void yelp(string args) { chr.WriteToDisplay("I don't understand your command."); }
        #endregion
    }
}
