using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    public class Mail
    {
        long m_mailID;
        int m_senderID;
        int m_receiverID;
        DateTime m_timeSent;
        string m_subject;
        string m_body;
        bool m_attachment;
        bool m_readByReceiver;
        bool m_sent;

        public long MailID
        {
            get { return m_mailID; }
        }

        public int SenderID
        {
            get { return m_senderID; }
        }

        public int ReceiverID
        {
            get { return m_receiverID; }
        }

        public DateTime TimeSent
        {
            get { return m_timeSent; }
        }

        public string Subject
        {
            get { return m_subject; }
        }

        public string Body
        {
            get { return m_body; }
        }

        public bool HasAttachment
        {
            get { return m_attachment; }
        }

        public bool HasBeenReadByReceiver
        {
            get { return m_readByReceiver; }
        }

        public Mail(System.Data.DataRow dr)
        {
            m_mailID = Convert.ToInt64(dr["mailID"]);
            m_senderID = Convert.ToInt32(dr["senderID"]);
            m_receiverID = Convert.ToInt32(dr["receiverID"]);
            m_timeSent = Convert.ToDateTime(dr["timeSent"]);
            m_subject = dr["subject"].ToString();
            m_body = dr["body"].ToString();
            m_attachment = Convert.ToBoolean(dr["attachment"]);
            m_readByReceiver = Convert.ToBoolean(dr["readByReceiver"]);
            // all mail created with this constructor has been sent (comes from the database)
            m_sent = true;
        }

        public Mail(int senderID, int receiverID, string subject, string body, bool attachment)
        {
            m_mailID = -1;
            m_senderID = senderID;
            m_receiverID = receiverID;
            m_subject = subject;
            m_body = body;
            m_attachment = attachment;
            // all mail created with this constructor will not be sent until player chooses to send it
            m_sent = false;
        }

        // returns true if the list contains unread mail
        public static bool HasNewMail(List<Mail> mailList)
        {
            for (int count = 0; count < mailList.Count; count++)
            {
                if (!mailList[count].m_readByReceiver)
                    return true;
            }
            return false;
        }

        // returns true if the mail has been validated
        private bool ValidateOutgoingMail(Mail mail)
        {
            PC receiver = PC.GetPC(mail.ReceiverID);
            PC sender = PC.GetPC(mail.SenderID);

            if (mail.HasAttachment)
            {
                // verify receiver is in same land as sender for attached items
                if (receiver.LandID != sender.LandID)
                {
                    sender.WriteToDisplay("Since the receiver of your mail does not reside in " + sender.Land.ShortDesc +
                        ", and your mail contains attachments, you may not send this mail.");
                    return false;
                }
            }

            // receiver is ignored by sender
            if (Array.IndexOf(receiver.ignoreList, sender.PlayerID) != -1)
            {
                sender.WriteToDisplay("The receiver of your mail currently has you on their ignore list.");
                return false;
            }

            // receiver is on sender's ignore list
            if (Array.IndexOf(sender.ignoreList, receiver.PlayerID) != -1)
            {
                sender.WriteToDisplay("The receiver of your mail is on your ignore list. You must remove them in order to send them mail.");
                return false;
            }

            // filter profanity
            if (receiver.filterProfanity)
            {
                sender.WriteToDisplay("The receiver of your mail has their profanity filter turned on. The mail is being scanned for profanity.");
                string subject = mail.Subject;
                string body = mail.Body;

                mail.m_subject = Conference.FilterProfanity(subject);
                mail.m_body = Conference.FilterProfanity(body);
            }

            return true;
        }

        private void SendMail(Mail mail)
        {
            // if mail is validated, send it (insert into database)
            if (ValidateOutgoingMail(mail))
            {
                DAL.DBMail.InsertMail(mail);
            }

            PC receiver = PC.GetOnline(PC.GetName(mail.ReceiverID));

            if (receiver != null)
            {
                receiver.WriteToDisplay("You have received new mail.");
            }

            PC sender = PC.GetOnline(PC.GetName(mail.SenderID));

            if (sender != null)
            {
                sender.WriteToDisplay("Your mail has been sent.");
            }
        }
    }
}
