using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

using Net;

namespace DragonsSpine
{
    public class ProtoClientIO
    {
        public Server server;
        private bool running = false;
        delegate void OnReceiveDelegate(Client client, Packet packet);
        public void startProtoServer()
        {
            running = true;
            server = Net.Instances.CreateServer(2000, 4000);
            Console.WriteLine("Protocol Server listening to port 4000.");
            server.OnReceive += new ServerDataHandler(server_OnReceive);

        }

        void server_OnReceive(Client client, Packet packet)
        {

            lock (client)
            {
                Console.WriteLine("Client: " + client.Name + " : " + (string)packet.message + "\r\n");
                ProcessMessage(client, packet);
            }

        }

        private void ProcessMessage(Client client, Packet packet)
        {
            if (packet.messageType == 99)
            {
                client.UpdateName((string)packet.message);
            }
            if ((string)packet.message == "test")
            {
                Packet newpack = new Packet();
                newpack.message = "Test test test...";
                newpack.messageType = 1;
                server.SendToName(client.Name, newpack);
            }
            if ((string)packet.message == "status")
            {
                Packet newpack = new Packet();
                newpack.message = "Status: Round: " + DragonsSpineMain.GameRound + " | NPCs: " + Character.NPCList.Count;
                newpack.messageType = 1;
                server.SendToName(client.Name, newpack);
            }
            if ((string)packet.message == "map")
            {
                int rnd = Rules.dice.Next(Character.NPCList.Count);
                Character temp = Character.NPCList[rnd];
                Packet newpack = mapPacket(temp.CurrentCell);
                server.SendToName(client.Name, newpack);
            }
        }
        private Packet mapPacket(Cell centerCell)
        {
            Packet mapPak = new Packet();
            mapPak.messageType = 2;
            string mapstring = "";
            Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, 3);
            for (int x = 0; x < 49; x++)
            {
                if (cellArray[x] == null)
                    mapstring += "?";
                else
                {
                    mapstring += convertMapString(cellArray[x]);
                }
            }
            mapPak.message = mapstring;
            return mapPak;
        }

        private string convertMapString(Cell cell)
        {
            switch (cell.DisplayGraphic)
            {
                case "[]":
                    return "#";
                case ". ":
                    return ".";
                case "@ ":
                case " @":
                case "@@":
                    return "@";

                default:
                    return "?";
            }
        }
    }
}
