//#define TESTSERVER
namespace DragonsSpine
{
    
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// This is the equivalent of our Socks class, but, since it's really a lot more than just socks stuff,
    /// I called it IO
    /// </summary>
    public class IO
    {
        protected TcpListener listener;
        protected int port;

        public static ArrayList addToLogin = new ArrayList();
        public static bool pplToAddToLogin = false;
        public static ArrayList removeFromLogin = new ArrayList();
        public static bool pplToRemoveFromLogin = false;

        public static ArrayList addToCharGen = new ArrayList();
        public static bool pplToAddToCharGen = false;
        public static ArrayList removeFromCharGen = new ArrayList();
        public static bool pplToRemoveFromCharGen = false;

        public static ArrayList addToMenu = new ArrayList();
        public static bool pplToAddToMenu = false;
        public static ArrayList removeFromMenu = new ArrayList();
        public static bool pplToRemoveFromMenu = false;

        public static ArrayList addToLimbo = new ArrayList();
        public static bool pplToAddToLimbo = false;
        public static ArrayList removeFromLimbo = new ArrayList();
        public static bool pplToRemoveFromLimbo = false;

        public static ArrayList addToWorld = new ArrayList();
        public static bool pplToAddToWorld = false;

        public static ArrayList removeFromWorld = new ArrayList();
        public static bool pplToRemoveFromWorld = false;

        public IO(int port)
        {
            this.port = port;
        }

        public IO() : this(3000) { }

        public bool Open()
        {
            try
            {
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                listener = new TcpListener(IPAddress.Any, port);

                Utils.Log("Listening for connections on port " + port + ".", Utils.LogType.SystemGo);
                listener.Start();
                return true;
            }
            catch (Exception e)
            {
                Utils.Log("Listener Error: " + e.Message, Utils.LogType.SystemFatalError);
                return false;
            }
        }

        public void HandleNewConnections()
        {
            if (DragonsSpineMain.ServerStatus == DragonsSpineMain.ServerState.Running ||
                DragonsSpineMain.ServerStatus == DragonsSpineMain.ServerState.Locked)
            {
                while (listener.Pending())
                { //for as long as we have ppl waiting to connect...
                    
                    Socket newSock; //a socket to store their connection in
                    newSock = listener.AcceptSocket(); //accept the connection

                    PC pc = new PC();
                    pc.setSocket(newSock);
                    pc.IsPC = true;
                    pc.PCState = Globals.ePlayerState.LOGIN;
                    string hostName;

                    string clientAddr = "" + IPAddress.Parse(((IPEndPoint)newSock.RemoteEndPoint).Address.ToString());

                    try
                    {
                        hostName = Dns.GetHostEntry(clientAddr).HostName;
                        pc.HostName = hostName;
                    }
                    catch (Exception e)
                    {
                        hostName = "No Hostname: " + e.Message + "";
                        pc.HostName = "Unknown HostName";
                    }
                    try
                    {
                        clientAddr = Dns.GetHostEntry(hostName).AddressList[0].ToString();
                    }
                    catch
                    {
                        clientAddr = "DNS Failed!";
                    }
                    if (World.BannedIPList.Count > 0)
                    {
                        for (int a = 0; a < World.BannedIPList.Count; a++)
                        {
                            if (World.BannedIPList.IndexOf(clientAddr) != -1)
                            {
                                Map.writeAtXY(pc, 5, 3, "Unable to access the login server.");
                                Utils.Log("BANNED LOGIN ATTEMPT: " + clientAddr, Utils.LogType.SystemWarning);
                                newSock.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                                newSock.Close();
                                return;
                            }
                        }
                    }

                    Utils.Log(hostName + " (" + clientAddr + ")", Utils.LogType.Connection);
                    
                    foreach (PC connected in Character.loginList)
                    {
                        if (connected.IPAddress == clientAddr)
                        {
                            connected.RemoveFromLogin();
                            connected.DisconnectSocket();
                        }
                    }
                    

                    pc.AddToLogin();
                    pc.IPAddress = clientAddr;

                    Map.writeAtXY(pc, 5, 3, "\n\rWelcome to " + DragonsSpineMain.APP_NAME + " " + DragonsSpineMain.APP_VERSION + "\n\r");
#if DEBUG
      pc.WriteLine("\n\rThe server is currently running in DEBUG mode.\n\r");              
#endif
                    string[] news = System.Configuration.ConfigurationManager.AppSettings["NEWS"].Split(Protocol.ISPLIT.ToCharArray());
                    foreach (string newsLine in news)
                        pc.WriteLine(newsLine);

                    pc.WriteLine("\n\rEnter your login name. \n\r(If you are new, type in \"new\" to set up an account.)");
                    pc.Write("Login: ");
                }
            }
            return;
        }

        public void GetInput()
        {
            int available;
            byte[] buffer;

            try
            {
                foreach (Character ch in new List<Character>(Character.pcList))
                {
                    available = ch.socketAvailable(); //is there any data in the network buffer?
                    if (available > 0)
                    {
                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                        buffer = new byte[available];
                        available = ch.socketReceive(buffer, available, 0); //read available data
                        if (available <= 0)
                            continue;
                        if (ch.Stunned == 0)
                            ch.AddInput(buffer, available); //add it to the pc's input buffer
                    }
                }

                foreach (Character ch in new List<Character>(Character.confList))
                {
                    available = ch.socketAvailable(); //is there any data in the network buffer?
                    if (available > 0)
                    {
                        buffer = new byte[available];
                        available = ch.socketReceive(buffer, available, 0); //read available data
                        if (available <= 0)
                            continue; //we weren't able to read it for some reason
                        ch.AddInput(buffer, available); //add it to the pc's input buffer
                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                    }

                }
                foreach (Character ch in new List<Character>(Character.menuList))
                {
                    available = ch.socketAvailable(); //is there any data in the network buffer?
                    if (available > 0)
                    {
                        buffer = new byte[available];
                        available = ch.socketReceive(buffer, available, 0); //read available data
                        if (available <= 0)
                            continue; //we weren't able to read it for some reason
                        ch.AddInput(buffer, available); //add it to the pc's input buffer
                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                    }

                }
                foreach (Character ch in new List<Character>(Character.charGenList))
                {
                    available = ch.socketAvailable(); //is there any data in the network buffer?
                    if (available > 0)
                    {
                        buffer = new byte[available];
                        available = ch.socketReceive(buffer, available, 0); //read available data
                        if (available <= 0)
                            continue; //we weren't able to read it for some reason
                        ch.AddInput(buffer, available); //add it to the pc's input buffer
                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                    }

                }
                foreach (Character ch in new List<Character>(Character.loginList))
                {
                    available = ch.socketAvailable(); //is there any data in the network buffer?
                    if (available > 0)
                    {
                        buffer = new byte[available];
                        available = ch.socketReceive(buffer, available, 0); //read available data
                        if (available <= 0)
                            continue; //we weren't able to read it for some reason
                        ch.AddInput(buffer, available); //add it to the pc's input buffer
                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void ProcessCommands(Character ch)
        {
            string all, command, args = null;
            int pos;
            //this is where we will defer to a command interpreter eventually
            while (ch.inputCommandQueueCount() > 0)
            {
                all = ch.inputCommandQueueDequeue();

                //break out the command based on the first comma, or space, send the rest in args
                if (all.IndexOf(",") != -1 && (all.IndexOf(' ') == -1 || all.IndexOf(' ') > all.IndexOf(",")) && ch.PCState == Globals.ePlayerState.PLAYING)
                {
                    if (all.IndexOf("\"") == -1 || all.IndexOf("\"") > all.IndexOf(","))
                    {
                        pos = all.IndexOf(',');
                        pos++;
                    }
                    else if (all.IndexOf("/") == -1 || all.IndexOf("/") > all.IndexOf(","))
                    {
                        pos = all.IndexOf(',');
                        pos++;
                    }
                    else
                    {
                        pos = -1;
                    }
                }
                else
                {
                    pos = all.IndexOf(' ');
                }

                if (pos < 0)
                {
                    command = all;
                }
                else
                {
                    command = all.Substring(0, pos);
                    if (command.IndexOf(",") > -1)
                    {
                        if (pos < (all.Length - 1))
                            args = all.Substring(pos);
                    }
                    else
                    {
                        if (pos < (all.Length - 1))
                            args = all.Substring(pos + 1);
                    }
                }

                //switch(ch.PCState)
                //{
                //    case Globals.ePlayerState.CHECKPASSWORD:
                //    case Globals.ePlayerState.PICKPASSWORD:
                //    case Globals.ePlayerState.VERIFYPASSWORD:
                //    case Globals.ePlayerState.CHANGEPASSWORD:
                //    case Globals.ePlayerState.CHANGEPASSWORD2:
                //        ch.echo = false;
                //        break;
                //    default:
                //        //ch.echo = Convert.ToBoolean(PC.GetField(ch.PlayerID, "echo", ch.echo, ""));
                //        break;
                //}

                switch (ch.PCState)
                {
                    //PLAYING						
                    case Globals.ePlayerState.PLAYING:
                        while (ch.inputCommandQueueCount() > 0)
                        {
                            all = ch.inputCommandQueueDequeue();
                        }
                        Command.ParseCommand(ch, command, args);
                        break;
                    #region Chat Room
                    case Globals.ePlayerState.CONFERENCE:
                        if (Protocol.CheckChatRoomCommand(ch, command, args)) { break; }
                        else
                        {
                            Conference.ChatCommands(ch, command, args);
                        }
                        break;
                    #endregion
                    #region Login
                    case Globals.ePlayerState.LOGIN:
                        // Trap 'new' input
                        if (command.Equals("new"))
                        {
#if TESTSERVER
                            ch.WriteLine("New Accounts are disabled in this build.");
                            ch.PCState = Character.State.LOGIN;
                            ch.Write("Login:");
                            break;
#endif
                            ch.Write("Enter a login name for your account: ");
                            ch.PCState = Globals.ePlayerState.PICKACCOUNT;
                        }
                        else
                        {	// Check to see if the account name exists in the DB
                            if (Account.AccountExists(command))
                            { //found it	
                                ch.account = command.ToLower();
                                ch.Write("Password: ");
                                ch.PCState = Globals.ePlayerState.CHECKPASSWORD;
                            }
                            else
                            {
#if TESTSERVER
                            ch.WriteLine("New Accounts are disabled in this build.");
                            ch.PCState = Character.State.LOGIN;
                            ch.Write("Login:");
                            break;
#endif
                                ch.Write("That account name was not found. Would you like to create a new account?(y/n): ");
                                ch.PCState = Globals.ePlayerState.NEWCHAR;
                            }
                        }
                        break;
                    #endregion
                    #region Login - Check Password
                    case Globals.ePlayerState.CHECKPASSWORD:
                        bool accountLoggedIn = false;
                        ch.accountID = Account.GetAccountID(ch.account); // plug the account ID into this character
                        if (command == Account.GetPassword(ch.account)) // Then check against the account password.
                        {
                            ch.Password = command;
                            foreach (Character chr in Character.pcList)
                            {
                                if (chr.accountID == ch.accountID)
                                {
                                    accountLoggedIn = true;
                                    break;
                                }
                            }
                            if (!accountLoggedIn)
                            {
                                foreach (Character chr in Character.confList)
                                {
                                    if (chr.accountID == ch.accountID)
                                    {
                                        accountLoggedIn = true;
                                        break;
                                    }
                                }
                            }
                            if (!accountLoggedIn)
                            {
                                foreach (Character chr in Character.menuList)
                                {
                                    if (chr.accountID == ch.accountID)
                                    {
                                        accountLoggedIn = true;
                                        break;
                                    }
                                }
                            }
                            if (!accountLoggedIn)
                            {
                                foreach (Character chr in Character.charGenList)
                                {
                                    if (chr.accountID == ch.accountID)
                                    {
                                        accountLoggedIn = true;
                                        break;
                                    }
                                }
                            }
                            if (accountLoggedIn)
                            {
                                ch.WriteLine("That account is already logged in. Try again in a few minutes.");
                                ch.PCState = Globals.ePlayerState.LOGIN;
                                ch.Write("login: ");
                            }
                            else
                            {
                                // Find the last character played on this account.
                                Map.clearMap(ch);
                                ch.RemoveFromLogin();
                                int lastPlayed = Account.GetLastPlayed(ch.accountID);
                                if (lastPlayed <= 0)
                                {
                                    ch.AddToCharGen(); // add the character to the character generator list
                                    ch.WriteLine("There are no characters presently on this account.  Please create one.");
                                    ch.WriteLine(CharGen.newChar());
                                    ch.Write(CharGen.pickRace());
                                    ch.PCState = Globals.ePlayerState.PICKRACE;
                                }
                                else
                                {
                                    Account.SetIPAddress(ch.accountID, ch.IPAddress);  // save the IPAddress
                                    PC pc1 = PC.GetPC(lastPlayed);  // start to copy the last played character
                                    ch.PlayerID = lastPlayed;
                                    pc1.PlayerID = lastPlayed;
                                    PC.LoadCharacter(ch, pc1); // fill in our ch with all the properties of lastplayed char.

                                    if (ch.currentMarks >= Character.MAX_MARKS)
                                    {
                                        Map.writeAtXY(ch, 5, 3, "Your account has " + ch.currentMarks.ToString() + " Marks due to unwarranted or unexcused player killing. Your account is frozen until you contact a game developer to discuss the situation.");
                                        Utils.Log(ch.GetLogString() + " has " + ch.currentMarks + " current Marks and " + ch.lifetimeMarks + " lifetime Marks.", Utils.LogType.SystemWarning);
                                        ch.RemoveFromServer();
                                    }
                                    else
                                    {
                                        if (Account.GetEmail(ch.accountID).Length <= 0)
                                        {
                                            ch.AddToLogin();
                                            ch.Write("Please enter your email address: ");
                                            ch.PCState = Globals.ePlayerState.PICKEMAIL;
                                        }
                                        else
                                        {
                                            Map.clearMap(ch);
                                            ch.AddToMenu(); // add the character to the menu list
                                            Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                                            ch.lastOnline = DateTime.Now;//.AddSeconds(DateTime.Now.Second); // set last online
                                            PC.saveField(ch.PlayerID, "lastOnline", ch.lastOnline, null); // save last online
                                            Conference.FriendNotify(ch, true); // notify friends
                                            ch.PCState = Globals.ePlayerState.MENU; // set state to menu
                                            Menu.PrintMainMenu(ch); // print main menu
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ch.WriteLine("Invalid password.");
                            ch.PCState = Globals.ePlayerState.LOGIN;
                            ch.Write("login:");
                        }
                        break;
                    #endregion
                    #region New Account
                    case Globals.ePlayerState.NEWCHAR:
                        Map.clearMap(ch);
                        if (command.ToLower().Equals("y"))
                        {
                            ch.PCState = Globals.ePlayerState.PICKACCOUNT;
                            ch.WriteLine("Account names must be alphanumeric, atleast " + Account.ACCOUNT_MIN_LENGTH + " characters in length, and no more than " + 
                                Account.ACCOUNT_MAX_LENGTH + " characters in length.");
                            ch.Write("\n\rPlease enter a name for the account: ");
                        }
                        else if (command.ToLower().Equals("n"))
                        {
                            ch.Write("\n\rPlease enter your login account name again: ");
                            ch.PCState = Globals.ePlayerState.LOGIN;
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.\n\r");
                            ch.Write("Create a new character now?(Y/N): ");
                        }
                        break;
                    #endregion
                    #region Pick Account
                    case Globals.ePlayerState.PICKACCOUNT:
                        Map.clearMap(ch);
                        if (Account.AccountNameDenied(command.ToLower()))  // check to see if account name exists or is denied
                        {
                            ch.WriteLine("\n\rThat name is invalid.");
                            ch.Write("Please enter another name for the account: ");
                        }
                        else
                        {
                            ch.account = command.ToLower(); // all accounts are lower case
                            ch.PCState = Globals.ePlayerState.PICKEMAIL;
                            ch.Write("\n\rPlease enter your email address: ");
                            
                        }
                        break;
                    #endregion
                    #region Pick Email Address
                    case Globals.ePlayerState.PICKEMAIL:
                        ch.Email = command;
                        ch.Write("Please verify your email address: ");
                        ch.PCState = Globals.ePlayerState.VERIFYEMAIL;
                        break;
                    #endregion
                    #region Verify Email
                    case Globals.ePlayerState.VERIFYEMAIL:
                        if (command == ch.Email)
                        {
                            if (!Account.AccountExists(ch.account))
                            {
                                ch.PCState = Globals.ePlayerState.PICKPASSWORD;
                                ch.Write("\n\rPlease enter a password for your account (minimum 4 chars, maximum 12): ");
                            }
                            else
                            {
                                Map.clearMap(ch);
                                ch.RemoveFromLogin();
                                Account.SetEmail(ch.accountID, ch.Email); // save email address
                                // temporary to get everyones email information
                                ch.AddToMenu(); // add the character to the menu list
                                Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                                ch.lastOnline = DateTime.Now; // set last online
                                PC.saveField(ch.PlayerID, "lastOnline", ch.lastOnline, null); // save last online
                                Conference.FriendNotify(ch, true); // notify friends
                                ch.PCState = Globals.ePlayerState.MENU; // set state to menu
                                Menu.PrintMainMenu(ch); // print main menu
                            }
                        }
                        else
                        {
                            ch.WriteLine("Email addresses did not match.");
                            ch.Write("Please enter your email address: ");
                            ch.PCState = Globals.ePlayerState.PICKEMAIL;
                        }
                        break;
                    #endregion
                    #region Pick Password
                    case Globals.ePlayerState.PICKPASSWORD:
                        Map.clearMap(ch);
                        if (!CharGen.PasswordDenied(command))
                        {
                            ch.Password = command;
                            ch.PCState = Globals.ePlayerState.VERIFYPASSWORD;	//Set next state
                            ch.Write("\n\rPlease re-type your password: ");
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat password is invalid.");
                            ch.Write("Please enter your password: ");
                        }
                        break;
                    #endregion
                    #region Verify Password
                    case Globals.ePlayerState.VERIFYPASSWORD:
                        Map.clearMap(ch);
                        if (CharGen.PasswordVerified(ch, command))
                        {
                            // write the new account info to account table
                            if (Account.InsertAccount(ch.account, ch.Password, ch.IPAddress, ch.Email) != -1)  // a failed insert returns -1
                            {
                                ch.accountID = Account.GetAccountID(ch.account); // get the accountID the DB just created and put it in the player record
                                ch.WriteLine(CharGen.newChar()); // start the character generator
                                ch.Write(CharGen.pickGender());
                                ch.PCState = Globals.ePlayerState.PICKGENDER;
                            }
                            else
                            {
                                ch.Write("Error in writing Account to Database. Try Login again: ");
                                ch.PCState = Globals.ePlayerState.LOGIN;
                            }

                        }
                        else
                        {
                            ch.WriteLine("That password did not match.");
                            ch.Write("Please re-type your password");
                        }
                        break;
                    #endregion
                    #region New Character
                    case Globals.ePlayerState.PROTO_CHARGEN:
                        Protocol.CheckCharGenCommand(ch, command, args);
                        break;
                    #region Pick Gender
                    case Globals.ePlayerState.PICKGENDER:
                        Map.clearMap(ch);
                        if (CharGen.genderVerify(command))
                        {
                            ch.gender = (Globals.eGender)Convert.ToInt16(command); // set gender
                            ch.Write(CharGen.pickRace()); // show next state text
                            ch.PCState = Globals.ePlayerState.PICKRACE;
                            //ch.pcState = Character.State.ROLLSTATS; // set next state
                            //ch.Write(charGen.rollStats(ch));
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.");
                            ch.Write(CharGen.pickGender());
                        }
                        break;
                         #endregion
                    #region Pick Race
                    case Globals.ePlayerState.PICKRACE:
                        Map.clearMap(ch);
                        if (CharGen.raceVerify(command))
                        {
                            switch (command.ToLower())
                            {
                                case "i":
                                    ch.race = "Illyria";
                                    break;
                                case "m":
                                    ch.race = "Mu";
                                    break;
                                case "l":
                                    ch.race = "Lemuria";
                                    break;
                                case "lg":
                                    ch.race = "Leng";
                                    break;
                                case "d":
                                    ch.race = "Draznia";
                                    break;
                                case "h":
                                    ch.race = "Hovath";
                                    break;
                                case "mn":
                                    ch.race = "Mnar";
                                    break;
                                case "b":
                                    ch.race = "Barbarian";
                                    break;
                                default:
                                    ch.race = "somewhere";
                                    break;
                            }
                            ch.Write(CharGen.pickClass());
                            ch.PCState = Globals.ePlayerState.PICKCLASS;
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.");
                            ch.Write(CharGen.pickRace());
                        }
                        break;
                    #endregion
                    #region Pick Class
                    case Globals.ePlayerState.PICKCLASS:
                        Map.clearMap(ch);
                        Character.ClassType classVerified = CharGen.classVerify(command);
                        if (classVerified != Character.ClassType.None)
                        {
                            ch.BaseProfession = classVerified;
                            ch.classFullName = Utils.FormatEnumString(ch.BaseProfession.ToString());
                            ch.Write(CharGen.RollStats(ch));
                            ch.PCState = Globals.ePlayerState.ROLLSTATS;
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.");
                            ch.Write(CharGen.pickClass());
                        }
                        break;
                    #endregion
                    #region Roll Stats
                    case Globals.ePlayerState.ROLLSTATS:
                        Map.clearMap(ch);
                        if (Protocol.CheckCharGenCommand(ch, command, args)) { break; }
                        if (CharGen.statVerify(command))
                        {
                            if (command.ToLower().Equals("n"))
                            {
                                //set all our stats
                                if (!ch.usingClient)
                                {
                                    ch.PCState = Globals.ePlayerState.PICKFIRSTNAME;	//Set next state
                                    ch.Write("\n\rPlease enter a name for your character: ");
                                }
                                else
                                {
                                    ch.RemoveFromCharGen(); // remove the character from the character generator list
                                    ch.AddToMenu(); // add the character to the menu list
                                    ch.PCState = Globals.ePlayerState.MENU;	// set state to menu
                                    ch.IsNewPC = true;  // This char hasn't been saved to DB yet. This tells Save to insert, rather than update.
                                    CharGen.SetupNewCharacter(ch);  // This routine saves char before return.
                                    ch.IsNewPC = false; // So now we set newchar to false - future saves will update, not insert.
                                    ch.PlayerID = DAL.DBPlayer.getPlayerID(ch.Name); // Plug the new PlayerID (generated by the db) into character.
                                    Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                                    Protocol.SendCharacterList(ch); // added a new character so send an update character list
                                }
                            }
                            else
                            {
                                ch.Write(CharGen.RollStats(ch));
                            }
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.");
                        }
                        break;
                    #endregion
                    #region Pick First Name
                    case Globals.ePlayerState.PICKFIRSTNAME:
                        Map.clearMap(ch);
                        if (!CharGen.CharacterNameDenied(ch, command))
                        {
                            command = command.Substring(0, 1).ToUpper() + command.Substring(1, command.Length - 1); // capitalize the first letter
                            ch.Name = command;
                            ch.WriteLine("Character creation successful!");
                            Map.clearMap(ch);
                            ch.RemoveFromCharGen(); // remove the character from the character generator list
                            ch.AddToMenu(); // add the character to the menu list
                            ch.PCState = Globals.ePlayerState.MENU;	// set state to menu
                            ch.IsNewPC = true;  // This char hasn't been saved to DB yet. This tells Save to insert, rather than update.
                            CharGen.SetupNewCharacter(ch);  // This routine saves char before return.
                            ch.IsNewPC = false; // So now we set newchar to false - future saves will update, not insert.
                            ch.PlayerID = DAL.DBPlayer.getPlayerID(ch.Name); // Plug the new PlayerID (generated by the db) into character.
                            #region New Spellbook
                            switch (ch.BaseProfession) // spells and spellbook
                            {
                                case Character.ClassType.Thaumaturge:
                                case Character.ClassType.Wizard:
                                case Character.ClassType.Thief:
                                    Item spellbook = Item.CopyItemFromDictionary(Item.ID_SPELLBOOK); // the book is attuned in the new character's first save
                                    spellbook.AttuneItem(ch.PlayerID, "New character spellbook.");
                                    ch.SackItem(spellbook);
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                            Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat name is invalid.");
                            ch.Write("Please enter your character's first name: ");
                        }
                        break;
                    #endregion
                    #endregion
                    #region Main Menu
                    case Globals.ePlayerState.MENU:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else
                        {
                            switch (command.ToLower())
                            {
                                case "1":
                                    if (DragonsSpineMain.ServerStatus < DragonsSpineMain.ServerState.Locked || ch.ImpLevel >= Globals.eImpLevel.GM)
                                    {
                                        ch.PCState = Globals.ePlayerState.PLAYING; // change character state to playing
                                        ch.RemoveFromMenu(); // remove the character from the menu list
                                        ch.AddToWorld(); // add the character to the world list
                                        Map.clearMap(ch);
                                        if (ch.protocol == "old-kesmai") { ch.updateAll = true; ch.Write(Map.KP_ENHANCER_ENABLE + Map.KP_ERASE_TO_END); }
                                    }
                                    else
                                    {
                                        ch.RemoveFromMenu();
                                        ch.AddToLimbo();
                                        Map.clearMap(ch);
                                        ch.PCState = Globals.ePlayerState.CONFERENCE;
                                        Conference.Header(ch, true);
                                        if (ch.usingClient)
                                        {
//                                            Protocol.sendMessageBox(ch, "The game world is currently locked.", "World Locked", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            ch.WriteLine("The game world is currently offline, please try again later.", Protocol.TextType.System);
                                        }
                                    }
                                    break;
                                case "2":
                                    ch.RemoveFromMenu();
                                    ch.AddToLimbo();
                                    ch.PCState = Globals.ePlayerState.CONFERENCE;
                                    Conference.Header(ch, true);
                                    break;
                                case "disconnect":
                                case "logout":
                                case "quit":
                                case "3":
                                    Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                                    ch.RemoveFromMenu();
                                    ch.RemoveFromServer();
                                    break;
                                case "4":
                                    ch.PCState = Globals.ePlayerState.ACCOUNTMAINT;
                                    Menu.PrintAccountMenu(ch, "");
                                    break;
                                case "5":
                                    if (ch.protocol == "normal")
                                    {
                                        ch.protocol = "old-kesmai";
                                    }
                                    else
                                    {
                                        ch.protocol = "normal";
                                    }
                                    Menu.PrintMainMenu(ch);
                                    break;
                                case "6":
                                    Map.clearMap(ch);
                                    ch.PCState = Globals.ePlayerState.CHANGECHAR;
                                    Menu.PrintCharMenu(ch);
                                    if (!ch.usingClient) { ch.Write("Command: "); }
                                    break;
                                default:
                                    Menu.PrintMainMenu(ch);
                                    break;
                            }
                        }
                        break;
                    #endregion
                    #region Character Menu
                    case Globals.ePlayerState.CHANGECHAR:
                        Menu.PrintCharMenu(ch);
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else
                        {
                            switch (command.ToLower())
                            {
                                case "1":
                                    if (DAL.DBPlayer.countCharacters(ch.accountID) >= Character.MAX_CHARS)
                                    {
                                        ch.WriteLine("\n\rYou have reached the maximum amount of characters allowed.  Try deleting an existing character.");
                                        break;
                                    }
                                    Map.clearMap(ch);
                                    PC newchar = new PC();
                                    newchar.account = ch.account;
                                    newchar.accountID = ch.accountID;
                                    newchar.protocol = ch.protocol;
                                    newchar.usingClient = ch.usingClient;
                                    newchar.echo = ch.echo;
                                    newchar.confRoom = ch.confRoom;
                                    ch.RemoveFromMenu(); // remove the character from the menu list
                                    PC.LoadCharacter(ch, newchar); // load the new character
                                    ch.AddToCharGen(); // add the character to the character generation list
                                    ch.WriteLine(CharGen.newChar());
                                    ch.Write(CharGen.pickGender());
                                    ch.PCState = Globals.ePlayerState.PICKGENDER;
                                    break;
                                case "2":
                                    Map.clearMap(ch);
                                    ch.WriteLine(PC.GetCharacterList(ch.accountID, false, ch));
                                    ch.WriteLine("");
                                    ch.Write("Select Character: ");
                                    ch.PCState = Globals.ePlayerState.CHANGECHAR2;
                                    break;
                                case "3":
                                    Map.clearMap(ch);
                                    ch.WriteLine("Delete Character Menu");
                                    ch.WriteLine("Current Character: " + ch.Name + " Level: " + ch.Level + " Class: " + ch.classFullName + " Map: " + ch.Land.Name);
                                    ch.WriteLine("");
                                    ch.WriteLine(PC.GetCharacterList(ch.accountID, false, ch));
                                    ch.WriteLine("");
                                    ch.Write("Select character to delete (0 to abort): ");
                                    ch.PCState = Globals.ePlayerState.DELETECHAR;
                                    break;
                                case "4":
                                    ch.PCState = Globals.ePlayerState.MENU;
                                    Menu.PrintMainMenu(ch);
                                    break;
                                case "disconnect":
                                case "logout":
                                case "quit":
                                    Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                                    ch.RemoveFromMenu();
                                    ch.RemoveFromServer();
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    #endregion
                    #region Change Character Menu
                    case Globals.ePlayerState.CHANGECHAR2:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else
                        {
                            string oldLogString = ch.GetLogString();
                            if (PC.SelectNewCharacter(ch, Convert.ToInt32(command)))
                            {
                                Utils.Log(oldLogString, Utils.LogType.Logout);
                                Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                                ch.PCState = Globals.ePlayerState.MENU;
                                Menu.PrintMainMenu(ch);
                            }
                            else
                            {
                                ch.WriteLine("Invalid Character.");
                                ch.Write("Select Character: ");
                            }
                        }
                        break;
                    #endregion
                    #region Delete Character Menu
                    case Globals.ePlayerState.DELETECHAR:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        int x = Convert.ToInt32(command);
                        if (x < 0 || x > 8)
                        {
                            ch.WriteLine("Invalid Character.");
                            ch.Write("Select Character: ");
                            break;
                        }
                        if (x == 0)
                        {
                            ch.PCState = Globals.ePlayerState.CHANGECHAR;
                            Menu.PrintCharMenu(ch);
                            if (!ch.usingClient) { ch.Write("Command: "); }
                            break;
                        }

                        string field = "Name"; // we build a list of all the char names on this account
                        string[] playerlist = DAL.DBPlayer.GetCharacterList(field, ch.accountID);
                        string name = playerlist[Convert.ToInt32(command) - 1]; // then pull the one matching player's input
                        int id = DAL.DBPlayer.getPlayerID(name);
                        ch.npcID = id; // npcID makes a handy place to store the to-be-deleted PlayerID
                        Map.clearMap(ch);
                        if (ch.PlayerID == ch.npcID)  // the active character is trying to be deleted.
                        {
                            Menu.PrintCharMenu(ch);
                            ch.WriteLine("You are attempting to delete your currently active character.");
                            ch.WriteLine("Please change to another character before deleting this one.");
                            ch.PCState = Globals.ePlayerState.CHANGECHAR;
                            if (!ch.usingClient) { ch.Write("Command: "); }
                            break;
                        }
                        ch.WriteLine(Map.BRED + "WARNING:" + Map.CEND + " Once deleted, this character cannot be recovered!");
                        ch.Write("Are you sure you want to delete " + Map.BWHT + name + Map.CEND + "? (y/n):");

                        ch.PCState = Globals.ePlayerState.DELETECHAR2;
                        break;
                    #endregion
                    #region Confirm Character Delete
                    case Globals.ePlayerState.DELETECHAR2:
                        Map.clearMap(ch);
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else if (command.Equals("y"))
                        {
                            if (DAL.DBPlayer.deleteCharacter(ch.npcID))
                            {
                                ch.PCState = Globals.ePlayerState.MENU;
                                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                                {
                                    Protocol.SendCharacterList(ch); // deleted a character so send an updated character list
                                }
                                Menu.PrintMainMenu(ch);
                                ch.npcID = 0;  // Reset the field we borrowed to hold this variable.
                                break;
                            }
                            else
                            {
                                Menu.PrintCharMenu(ch);
                                ch.WriteLine("Character Deletion FAILED.");
                                ch.npcID = 0;  // Reset the field we borrowed to hold this variable.
                                ch.PCState = Globals.ePlayerState.CHANGECHAR;
                                ch.Write("Command: ");
                                break;
                            }
                        }
                        else if (command.Equals("n"))
                        {
                            Map.clearMap(ch);
                            ch.WriteLine("Character deletion aborted.");
                            ch.PCState = Globals.ePlayerState.CHANGECHAR;
                            Menu.PrintCharMenu(ch);
                            ch.Write("Command: ");
                            break;
                        }
                        else
                        {
                            ch.WriteLine("\n\rThat was not an option.\n\r");
                            ch.Write("Create a new character now?(Y/N): ");
                        }
                        break;
                    #endregion
                    #region Account Menu
                    case Globals.ePlayerState.ACCOUNTMAINT:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else
                        {
                            switch (command.ToLower())
                            {
                                case "1":
                                    ch.PCState = Globals.ePlayerState.CHANGEPASSWORD;
                                    Map.clearMap(ch);
                                    ch.WriteLine("");
                                    ch.Write("Current Password: ");
                                    break;
                                case "2":
                                    ch.PCState = Globals.ePlayerState.MENU;
                                    Menu.PrintMainMenu(ch);
                                    break;
                                default:
                                    Menu.PrintAccountMenu(ch, "");
                                    break;
                            }
                        }
                        break;
                    #endregion
                    #region Change Password
                    case Globals.ePlayerState.CHANGEPASSWORD:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else if (command == Account.GetPassword(ch.account))
                        {
                            ch.PCState = Globals.ePlayerState.CHANGEPASSWORD2;
                            ch.WriteLine("");
                            ch.Write("New Password: ");
                            break;
                        }
                        else
                        {
                            ch.PCState = Globals.ePlayerState.ACCOUNTMAINT;
                            Menu.PrintAccountMenu(ch, "INCORRECT PASSWORD.");
                            break;
                        }
                    case Globals.ePlayerState.CHANGEPASSWORD2:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        if (command.Length >= 6 && command.Length <= 20)
                        {
                            ch.PCState = Globals.ePlayerState.CHANGEPASSWORD3;
                            ch.Password = command;
                            ch.WriteLine("");
                            ch.Write("Verify New Password: ");
                            break;
                        }
                        else
                        {
                            ch.PCState = Globals.ePlayerState.ACCOUNTMAINT;
                            Menu.PrintAccountMenu(ch, "PASSWORD NOT CHANGED. PASSWORDS MUST BE BETWEEN 6 AND 20 CHARACTERS IN LENGTH.");
                            break;
                        }
                    case Globals.ePlayerState.CHANGEPASSWORD3:
                        if (Protocol.CheckMenuCommand(ch, command, args)) { break; }
                        else if (command == ch.Password)
                        {
                            Account.SetPassword(ch.accountID, ch.Password); // save new password
                            ch.PCState = Globals.ePlayerState.ACCOUNTMAINT;
                            Menu.PrintAccountMenu(ch, "PASSWORD CHANGED.");
                            break;
                        }
                        else
                        {
                            ch.Password = Account.GetPassword(ch.account); // get old password
                            ch.PCState = Globals.ePlayerState.ACCOUNTMAINT;
                            Menu.PrintAccountMenu(ch, "PASSWORD NOT CHANGED. VERIFIED PASSWORD DID NOT MATCH.");
                            break;
                        }
                    #endregion
                    default:
                        Command.ParseCommand(ch, command, args);
                        break;
                }
            }
        }

        public void SendOutput() // send data in each pc's output queue
        {
            
            try
            {
                #region World
                foreach(Character ch in new List<Character>(Character.pcList))
                {
                    while (ch.outputQueueCount() > 0)
                    {
                        try
                        {
                            if (ch.socketSend(System.Text.Encoding.ASCII.GetBytes(ch.outputQueueDequeue())) == -1)
                            {
                                Utils.Log(ch.GetLogString() + " lost connection, removing from world.", Utils.LogType.Disconnect);
                                ch.RemoveFromWorld();
                                ch.RemoveFromServer();
                                break; // break the while loop
                            }
                        }
                        catch
                        {
                            break;
                        }
                    }
                }
                #endregion
                #region Limbo
                foreach(Character ch in new List<Character>(Character.confList))
                {
                    while (ch.outputQueueCount() > 0)
                    {
                        if (ch.socketSend(System.Text.Encoding.ASCII.GetBytes(ch.outputQueueDequeue())) == -1)
                        {
                            Utils.Log(ch.GetLogString() + " lost connection, removing from limbo.", Utils.LogType.Disconnect);
                            ch.RemoveFromLimbo();
                            ch.RemoveFromServer();
                            break;
                        }
                    }
                }
                #endregion
                #region Menu
                    foreach(Character ch in new List<Character>(Character.menuList))
                    {
                        while (ch.outputQueueCount() > 0)
                        {
                            if (ch.socketSend(System.Text.Encoding.ASCII.GetBytes(ch.outputQueueDequeue())) == -1)
                            {
                                Utils.Log(ch.GetLogString() + " lost connection, removing from menu.", Utils.LogType.Disconnect);
                                ch.RemoveFromMenu();
                                ch.RemoveFromServer();
                                break;
                            }
                        }
                    }
                #endregion
                #region CharGen
                    foreach(Character ch in new List<Character>(Character.charGenList))
                    {
                        while (ch.outputQueueCount() > 0)
                        {
                            if (ch.socketSend(System.Text.Encoding.ASCII.GetBytes(ch.outputQueueDequeue())) == -1)
                            {
                                Utils.Log(ch.GetLogString() + " lost connection, removing from chargen.", Utils.LogType.Disconnect);
                                ch.RemoveFromCharGen();
                                ch.RemoveFromServer();
                                break;
                            }
                        }
                    }
                #endregion
                #region Login
                foreach(Character ch in new List<Character>(Character.loginList))
                {
                    while (ch.outputQueueCount() > 0)
                    {
                        if (ch.socketSend(System.Text.Encoding.ASCII.GetBytes(ch.outputQueueDequeue())) == -1)
                        {
                            Utils.Log(ch.HostName + " (" + ch.IPAddress + ") lost connection, removing from login.", Utils.LogType.Disconnect);
                            ch.RemoveFromLogin();
                            ch.RemoveFromServer();
                            break;
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void Close() // this will shut down everything
        {
            for (int a = Character.pcList.Count - 1; a >= 0; a--)
            {
                PC pc = (PC)Character.pcList[a];
                pc.WriteToDisplay("Your character is being saved before shutdown commences.");
                pc.Save();
                pc.RemoveFromServer();
            }
            System.Environment.Exit(0);
        }

        public void ProcessRealTimeCommands() // process commands for players at login, menu, chargen, and conference
        {
            foreach (Character ch in new List<Character>(Character.pcList))
            {
                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    Character.ValidatePlayer(ch);

                    if (ch.Stunned <= 0 && !ch.IsFeared)
                    {
                        IO.ProcessCommands(ch);
                        ch.cmdWeight = 0;
                    }
                    else if (ch.Stunned > 0 || ch.IsFeared || ch.IsBlind)
                    {
                        if (ch.FollowName.Length > 0)
                            ch.BreakFollowMode();
                    }
                }
                IO.ProcessCommands(ch);
            }

            foreach (Character ch in new List<Character>(Character.confList))
                IO.ProcessCommands(ch);

            foreach (Character ch in new List<Character>(Character.charGenList))
                IO.ProcessCommands(ch);

            foreach (Character ch in new List<Character>(Character.menuList))
                IO.ProcessCommands(ch);

            foreach (Character ch in new List<Character>(Character.loginList))
                IO.ProcessCommands(ch);
        }
    }
}
