using System;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine
{
	public class Quest
	{
        static Dictionary<int, Quest> questDictionary = new Dictionary<int, Quest>();

        public const string ASPLIT = " "; // attribute delimiter
        public const string ISPLIT = "^"; // item delimiter
        public const string VSPLIT = "~"; // variable delimiter

        public enum QuestRequirement
        {
            /// <summary>
            /// No quest requirement.
            /// </summary>
            None,
            /// <summary>
            /// Specific amount of coin required to complete quest.
            /// </summary>
            Coin,
            /// <summary>
            /// Flag required to complete quest.
            /// </summary>
            Flag,
            /// <summary>
            /// Item required to complete quest.
            /// </summary>
            Item,
            /// <summary>
            /// NPC required to complete quest.
            /// </summary>
            NPC
        }

        #region Private Data
        int questID = -1; // the database catalog ID of this quest
        string notes = ""; // notes about this quest (meant for DEVs only)
        string name = ""; // the name of this quest
        string description = ""; // the description of this quest (to be used for quest journals later)
        string completedDescription = ""; // the description of the quest after it has been completed (to be used for quest journals later)
        Dictionary<short, QuestRequirement> requirements = new Dictionary<short, QuestRequirement>(); //
        List<string> requiredFlags = new List<string>(); // the flags required to initiate this quest, if any
        string rewardTitle = ""; // #~classFullName, where # is the step at which the classFullName is received
        string rewardClass = ""; // #~classType, where # is the step at which the classType is received
        Dictionary<short, string> rewardFlags = new Dictionary<short, string>(); // the flags that are rewarded and at which step, "" is nullifier
        Dictionary<short, int> requiredItems = new Dictionary<short, int>(); // step #~item ID
        Dictionary<short, double> coinValues = new Dictionary<short, double>(); // step~required or reward coin value
        Dictionary<short, int> rewardItems = new Dictionary<short, int>(); // step #~item ID
        Dictionary<short, int> rewardExperience = new Dictionary<short, int>(); // step #~experience amount
        Dictionary<short, string> rewardStats = new Dictionary<short, string>(); // step #~stat " " stat increase
        // stat abbreviations: (h)its, (s)tamina, (m)ana, str, con, wis, int, dex, chr
        Dictionary<short, string> rewardTeleports = new Dictionary<short, string>(); // step #~land,map,x,y,z (cell locks can be used in conjunction with this, target cell gets the celllock!)
        Dictionary<string, string> responseStrings = new Dictionary<string, string>(); // initiator string = key, response string = value
        List<string> hintStrings = new List<string>(); // the strings displayed to tip off the player that this is a quest NPC
        Dictionary<string, short> flagStrings = new Dictionary<string, short>(); // string detected, flag given
        Dictionary<string, short> stepStrings = new Dictionary<string, short>(); // string detected, at which step (step is then incremented)
        Dictionary<short, string> finishStrings = new Dictionary<short, string>(); // the string displayed when this quest is completed at finishString[x]
        Dictionary<short, string> failStrings = new Dictionary<short, string>(); // the string displayed when this quest is failed at failString[x]
        List<Character.ClassType> classTypes = new List<Character.ClassType>(); // classType restrictions
        List<Globals.eAlignment> alignments = new List<Globals.eAlignment>(); // alignment restrictions
        bool repeatable = false; // true if this quest can be repeated
        bool stepOrder = false;
        short maximumLevel = 0;
        short minimumLevel = 0;
        Dictionary<short, string> soundFiles = new Dictionary<short, string>();
        short totalSteps = 1;
        bool despawnsNPC = false;
        int masterQuestID = 0;
        bool teleportGroup = false;

        int timesCompleted = 0; // how many times this quest has been completed
        List<short> completedSteps = new List<short>();
        short currentStep = 0; // the player's current step in this quest
        string startDate = "";
        string finishDate = "";
        #endregion

        #region Public Properties

        public int QuestID
        {
            get { return this.questID; }
        }
        public string Notes
        {
            get { return this.notes; }
        }
        public string Name
        {
            get { return this.name; }
        }
        public string Description
        {
            get { return this.description; }
        }
        public string CompletedDescription
        {
            get { return this.completedDescription; }
        }
        public List<String> RequiredFlags
        {
            get { return this.requiredFlags; }
        }
        public Dictionary<short, string> RewardFlags
        {
            get { return this.rewardFlags; }
        }
        public string RewardTitle
        {
            get { return this.rewardTitle; }
        }
        public string RewardClass
        {
            get { return this.rewardClass; }
        }
        public Dictionary<short, int> RequiredItems
        {
            get { return this.requiredItems; }
        }
        public Dictionary<short, double> CoinValues
        {
            get { return this.coinValues; }
        }
        public Dictionary<short, int> RewardItems
        {
            get { return this.rewardItems; }
        }
        public Dictionary<short, int> RewardExperience
        {
            get { return this.rewardExperience; }
        }
        public Dictionary<short, string> RewardStats
        {
            get { return this.rewardStats; }
        }
        public Dictionary<short, string> RewardTeleports
        {
            get { return this.rewardTeleports; }
        }
        public Dictionary<string, string> ResponseStrings
        {
            get { return this.responseStrings; }
        }
        public List<string> HintStrings
        {
            get { return this.hintStrings; }
        }
        public Dictionary<string, short> FlagStrings
        {
            get { return this.flagStrings; }
        }
        public Dictionary<string, short> StepStrings
        {
            get { return this.stepStrings; }
        }
        public Dictionary<short, string> FinishStrings
        {
            get { return this.finishStrings; }
        }
        public Dictionary<short, string> FailStrings
        {
            get { return this.failStrings; }
        }
        public List<Character.ClassType> ClassTypes
        {
            get { return this.classTypes; }
        }
        public bool IsClassRestricted
        {
            get
            {
                if (this.classTypes.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public Dictionary<short, QuestRequirement> Requirements
        {
            get { return this.requirements; }
        }
        public List<Globals.eAlignment> Alignments
        {
            get { return this.alignments; }
        }
        public bool IsAlignmentRestricted
        {
            get
            {
                if (this.alignments.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public short MaximumLevel
        {
            get
            {
                return this.maximumLevel;
            }
        }
        public short MinimumLevel
        {
            get
            {
                return this.minimumLevel;
            }
        }
        public bool IsLevelRestricted
        {
            get
            {
                if (this.minimumLevel > 0 || this.maximumLevel > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsRepeatable
        {
            get { return this.repeatable; }
        }
        public bool StepOrder
        {
            get { return this.stepOrder; }
        }
        public Dictionary<short, string> SoundFiles
        {
            get
            {
                return this.soundFiles;
            }
        }
        public short TotalSteps
        {
            get
            {
                return this.totalSteps;
            }
        }
        public bool DespawnsNPC
        {
            get { return this.despawnsNPC; }
        }
        public int MasterQuestID
        {
            get { return this.masterQuestID; }
        }
        public bool TeleportGroup
        {
            get { return this.teleportGroup; }
        }

        public bool IsSubquest
        {
            get
            {
                if (this.MasterQuestID > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public int TimesCompleted
        {
            get { return this.timesCompleted; }
            set { this.timesCompleted = value; }
        }
        public List<short> CompletedSteps
        {
            get { return this.completedSteps; }
        }
        public short CurrentStep
        {
            get { return this.currentStep; }
            set { this.currentStep = value; }
        }
        public string StartDate
        {
            get { return this.startDate; }
            set { this.startDate = value; }
        }
        public string FinishDate
        {
            get { return this.finishDate; }
            set { this.finishDate = value; }
        } 
        #endregion

        public Quest()
        {
        }

        public Quest(System.Data.DataRow dr)
        {
            string[] s = null;
            string[] t = null;
            int a = 0;

            this.questID = Convert.ToInt32(dr["questID"]);
            this.notes = dr["notes"].ToString();
            this.name = dr["name"].ToString();
            this.description = dr["description"].ToString();
            this.completedDescription = dr["completedDescription"].ToString();
            s = dr["requirements"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.requirements.Add(Convert.ToInt16(t[0]), (QuestRequirement)(Globals.eItemBaseType)Enum.Parse(typeof(Quest.QuestRequirement), t[1]));
                }
            }

            this.rewardTitle = dr["rewardTitle"].ToString();

            this.rewardClass = dr["rewardClass"].ToString();
            
            s = dr["requiredFlags"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    this.requiredFlags.Add(s[a]);
                }
            }
            s = dr["rewardFlags"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.rewardFlags.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            s = dr["requiredItems"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.requiredItems.Add(Convert.ToInt16(t[0]), Convert.ToInt32(t[1]));
                }
            }
            s = dr["coinValues"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.coinValues.Add(Convert.ToInt16(t[0]), Convert.ToDouble(t[1]));
                }
            }
            s = dr["rewardItems"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.rewardItems.Add(Convert.ToInt16(t[0]), Convert.ToInt32(t[1]));
                }
            }
            s = dr["rewardExperience"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.rewardExperience.Add(Convert.ToInt16(t[0]), Convert.ToInt32(t[1]));
                }
            }
            s = dr["rewardStats"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.rewardStats.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            s = dr["responseStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.responseStrings.Add(t[0], t[1]);
                }
            }
            s = dr["rewardTeleports"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.rewardTeleports.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            s = dr["hintStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    this.hintStrings.Add(s[a]);
                }
            }
            s = dr["flagStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.flagStrings.Add(t[0], Convert.ToInt16(t[1]));
                }
            }
            s = dr["stepStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.stepStrings.Add(t[0], Convert.ToInt16(t[1]));
                }
            }
            s = dr["finishStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.finishStrings.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            s = dr["failStrings"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.failStrings.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            s = dr["classTypes"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    this.classTypes.Add((Character.ClassType)Enum.Parse(typeof(Character.ClassType), s[a]));
                }
            }
            s = dr["alignments"].ToString().Split(ISPLIT.ToCharArray());
            for(a = 0; a < s.Length; a++)
            {
                if(s[a].Length > 0)
                {
                    this.alignments.Add((Globals.eAlignment)Enum.Parse(typeof(Globals.eAlignment), s[a]));
                }
            }
            s = dr["soundFiles"].ToString().Split(ISPLIT.ToCharArray());
            for (a = 0; a < s.Length; a++)
            {
                if (s[a].Length > 0)
                {
                    t = s[a].Split(VSPLIT.ToCharArray());
                    this.soundFiles.Add(Convert.ToInt16(t[0]), t[1]);
                }
            }
            this.maximumLevel = Convert.ToInt16(dr["maximumLevel"]);
            this.minimumLevel = Convert.ToInt16(dr["minimumLevel"]);
            this.repeatable = Convert.ToBoolean(dr["repeatable"]);
            this.stepOrder = Convert.ToBoolean(dr["stepOrder"]);
            this.totalSteps = Convert.ToInt16(dr["totalSteps"]);
            this.despawnsNPC = Convert.ToBoolean(dr["despawnsNPC"]);
            this.masterQuestID = Convert.ToInt32(dr["masterQuestID"]);
            this.teleportGroup = Convert.ToBoolean(dr["teleportGroup"]);
        }

        public static bool LoadQuests()
        {
            return DAL.DBWorld.LoadQuests();
        }

        public static void Add(Quest quest)
        {
            questDictionary.Add(quest.QuestID, quest);
        }

        public static Quest GetQuest(int questID)
        {
            if (questDictionary.ContainsKey(questID))
            {
                return questDictionary[questID];
            }
            return null;
        }

        public static Quest CopyQuest(int questID)
        {
            if(questDictionary.ContainsKey(questID))
            {
                return (Quest)questDictionary[questID].MemberwiseClone();
            }
            return null;
        }

        public string GetLogString()
        {
            return "[" + this.QuestID + "] " + this.Name;
        }

        public bool PlayerMeetsRequirements(PC player, bool inform)
        {
            if (!this.IsRepeatable)
            {
                Quest qCheck = player.GetQuest(this.QuestID);
                if (qCheck != null && qCheck.CompletedSteps.Count >= qCheck.TotalSteps)
                {
                    if (inform)
                    {
                        player.WriteToDisplay("You have already completed the quest \"" + this.Name + "\".");
                    }
                    return false;
                }
            }

            foreach (string reqFlag in this.RequiredFlags)
            {
                if (player.questFlags.IndexOf(reqFlag) == -1)
                {
                    if (inform)
                    {
                        player.WriteToDisplay("You do not have the appropriate flags for the quest \"" + this.Name + "\".");
                    }
                    return false;
                }
            }

            if (this.IsAlignmentRestricted)
            {
                if (!this.Alignments.Contains(player.Alignment))
                {
                    if (inform)
                    {
                        player.WriteToDisplay("You do not meet the alignment requirements for the quest \"" + this.Name + "\".");
                    }
                    return false;
                }
            }
            if (this.IsClassRestricted)
            {
                // TODO: search sub classes
                if (!this.ClassTypes.Contains(player.BaseProfession))
                {
                    if (inform)
                    {
                        player.WriteToDisplay("You do not meet the class requirements for the quest \"" + this.Name + "\".");
                    }
                    return false;
                }
            }
            if (this.IsLevelRestricted)
            {
                if (this.minimumLevel > 0)
                {
                    if (player.Level < this.minimumLevel)
                    {
                        if (inform)
                        {
                            player.WriteToDisplay("You do not meet the level requirements for the quest \"" + this.Name + "\".");
                        }
                        return false;
                    }
                }
                if (this.maximumLevel > 0)
                {
                    if (player.Level > this.maximumLevel)
                    {
                        if (inform)
                        {
                            player.WriteToDisplay("You do not meet the level requirements for the quest \"" + this.Name + "\".");
                        }
                        return false;
                    }
                }
            }
            return true;
        }

        public bool BeginQuest(PC questor, bool inform)
        {
            Quest newQuest = questor.GetQuest(this.QuestID);

            if (newQuest != null)
            {
                DateTime start;
                DateTime.TryParse(newQuest.StartDate, out start);
                if (start < DateTime.Now && !newQuest.CompletedSteps.Contains(newQuest.TotalSteps))
                {
                    if (inform)
                    {
                        questor.WriteToDisplay("You have already started this quest \"" + this.Name + "\".");
                        return false;
                    }
                }
                else
                {
                    if (newQuest.IsRepeatable)
                    {
                        goto verifyQuestRequirements;
                    }

                    if (inform)
                    {
                        questor.WriteToDisplay("You have already completed the quest " + this.Name + ".");
                    }
                }
                return false;
            }

        verifyQuestRequirements:

            if (!PlayerMeetsRequirements(questor, true))
            {
                return false;
            }

            newQuest = (Quest)this.MemberwiseClone();
            newQuest.StartDate = DateTime.Now.ToString();
            newQuest.CurrentStep = 1;
            questor.questList.Add(newQuest);
            return true;
        }

        public void FinishStep(Character questGiver, PC questor, short step)
        {
            try
            {
                // precautionary
                if (this.CurrentStep <= 0) { this.CurrentStep = 1; }

                // flag check
                if (!PlayerMeetsRequirements(questor, true))
                {
                    return;
                }

                // make the quest giver stand still for a period of time to conclude interaction
                if (questGiver != null)
                {
                    Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, questGiver, Rules.dice.Next(28, 40), null);
                }

                // the quest giver tells the player that the quest is complete
                if (this.FinishStrings.ContainsKey(step))
                {
                    if (questGiver != null)
                    {
                        string emote = Utils.ParseEmote(this.FinishStrings[step]);
                        string finish = this.FinishStrings[step];
                        if (emote != "")
                        {
                            finish = finish.Replace("{" + emote + "}", "");
                            questor.WriteToDisplay(questGiver.Name + " " + emote);
                        }
                        if (finish.Length > 0)
                        {
                            questor.WriteToDisplay(questGiver.Name + ": " + finish);
                        }
                    }
                    else
                    {
                        questor.WriteToDisplay(this.FinishStrings[step]);
                    }
                }

                // play quest sound file if applicable
                if (this.SoundFiles.ContainsKey(step))
                {
                    if (questGiver != null)
                    {
                        questGiver.EmitSound(this.SoundFiles[step]);
                    }
                    else
                    {
                        questor.CurrentCell.EmitSound(this.SoundFiles[step]);
                    }
                }

                // give reward title
                if (this.RewardTitle != "")
                {
                    string[] s = this.RewardTitle.Split(VSPLIT.ToCharArray());
                    if (s.Length > 0 && Convert.ToInt16(s[0]) == this.CurrentStep)
                    {
                        string oldTitle = questor.classFullName; // store old title
                        questor.classFullName = s[1];
                        questor.WriteToDisplay("Your title has been changed from " + oldTitle + " to " + questor.classFullName + ".");
                    }
                }

                // give reward class
                if (this.RewardClass != "")
                {
                    string[] s = this.RewardClass.Split(VSPLIT.ToCharArray());

                    if (s.Length > 0 && Convert.ToInt16(s[0]) == this.CurrentStep)
                    {
                        string oldClass = "";
                        bool classMatch = false;
                        foreach (Character.ClassType classType in Enum.GetValues(typeof(Character.ClassType)))
                        {
                            if (classType.ToString().ToLower() == s[1].ToLower())
                            {
                                oldClass = questor.BaseProfession.ToString(); // store old class
                                questor.BaseProfession = (Character.ClassType)Enum.Parse(typeof(Character.ClassType), s[1], true);
                                questor.WriteToDisplay("Your character class has been changed from " + oldClass + " to " + questor.BaseProfession.ToString() + ".");
                                classMatch = true;
                                break;
                            }
                        }

                        if (!classMatch)
                        {
                            // search subclasses here for a match then change subclass... TODO
                        }
                    }
                }

                // give reward item
                if (this.RewardItems.ContainsKey(step))
                {
                    Item reward = Item.CopyItemFromDictionary(this.RewardItems[step]);

                    if (reward != null)
                    {
                        if (this.CoinValues.ContainsKey(step)) // set coin value of reward if necessary
                        {
                            reward.coinValue = this.CoinValues[step];
                        }

                        if (reward.attuneType == Globals.eAttuneType.Quest)
                        {
                            reward.AttuneItem(questor);
                        }

                        questor.EquipEitherHandOrDrop(reward);
                    }

                    if (this.TotalSteps == 1 && this.RewardItems.Count > 1) // used for simple escort quests with multiple rewards
                    {
                        foreach (short s in this.RewardItems.Keys)
                        {
                            if (s > 1) // already rewarded the first item
                            {
                                reward = Item.CopyItemFromDictionary(this.RewardItems[s]);
                                if (reward != null)
                                {
                                    if (this.CoinValues.ContainsKey(s)) // set coin value of reward if necessary
                                    {
                                        reward.coinValue = this.CoinValues[s];
                                    }

                                    if (reward.attuneType == Globals.eAttuneType.Quest)
                                    {
                                        reward.AttuneItem(questor);
                                    }

                                    questor.EquipEitherHandOrDrop(reward);
                                }
                            }
                        }
                    }
                }

                // give quest flag
                if (this.RewardFlags.ContainsKey(step))
                {
                    if (!questor.questFlags.Contains(this.RewardFlags[step]))
                    {
                        questor.questFlags.Add(this.RewardFlags[step]);
                        if (this.RewardFlags[step].IndexOf("_C") != -1) // add a permanent content flag
                        {
                            // remove the _C at the end of the flag
                            questor.contentFlags.Add(this.RewardFlags[step].Substring(0, this.RewardFlags[step].IndexOf("_C")));
                        }
                        //questor.WriteToDisplay("You have received a quest flag!");
                    }
                }

                // give quest experience
                if (this.RewardExperience.ContainsKey(step))
                {
                    string sub = "";

                    if (this.IsSubquest)
                    {
                        sub = "sub";
                    }

                    if (this.CurrentStep >= this.TotalSteps)
                    {
                        questor.WriteToDisplay("You earn " + this.RewardExperience[step] + " experience for completing the " + sub + "quest \"" + this.Name + "\".");
                    }
                    else
                    {
                        questor.WriteToDisplay("You earn " + this.RewardExperience[step] + " experience for completing a step of the " + sub +"quest \"" + this.Name + "\".");
                    }
                    questor.Experience += this.RewardExperience[step];
                }

                // give reward stats
                if (this.RewardStats.ContainsKey(step))
                {
                    // Stats: stat #
                    // TODO: Faction: faction # #
                    
                    string[] stat = this.RewardStats[step].Split(ASPLIT.ToCharArray());

                    switch (stat[0].ToLower())
                    {
                        case "h":
                            questor.HitsAdjustment += Convert.ToInt32(stat[1]);
                            questor.WriteToDisplay("Your maximum hits have increased by " + Convert.ToInt32(stat[1]) + ".");
                            break;
                        case "s":
                            questor.StaminaAdjustment += Convert.ToInt32(stat[1]);
                            questor.WriteToDisplay("Your maximum stamina has increased by " + Convert.ToInt32(stat[1]) + ".");
                            break;
                        case "m":
                            questor.ManaAdjustment += Convert.ToInt32(stat[1]);
                            questor.WriteToDisplay("Your maximum mana has increased by " + Convert.ToInt32(stat[1]) + ".");
                            break;
                        default:
                            // TODO: add faction here
                            break;
                    }
                }

                // reward teleport
                if (this.RewardTeleports.ContainsKey(step))
                {
                    string[] coords = this.RewardTeleports[step].Split(",".ToCharArray());
                    questor.CurrentCell = Cell.GetCell(questor.FacetID, Convert.ToInt16(coords[0]), Convert.ToInt16(coords[1]),
                        Convert.ToInt32(coords[2]), Convert.ToInt32(coords[3]), Convert.ToInt32(coords[4]));

                    if (coords.Length >= 6 && coords[5].Length > 0)
                    {
                            questor.WriteToDisplay(coords[5]);
                    }

                    // teleport the group with the questor
                    if (this.TeleportGroup && questor.Group != null)
                    {
                        string reason = "";
                        if (coords.Length >= 6 && coords[5].Length > 1)
                        {
                            reason = coords[5];
                        }
                        questor.Group.TeleportGroup(questor, questor.CurrentCell, reason);
                    }
                }

                if (this.TotalSteps < this.CurrentStep)
                {
                    this.CurrentStep++;
                    // log the quest step completion
                    Utils.Log(this.GetLogString() + ", quest step " + step + ", was completed by " + questor.GetLogString(), Utils.LogType.QuestCompletion);
                }

                if (!this.CompletedSteps.Contains(this.CurrentStep))
                {
                    this.CompletedSteps.Add(this.CurrentStep);
                }

                if (this.CurrentStep >= this.TotalSteps)
                {
                    this.CompleteQuest(questor);
                }

                if (this.DespawnsNPC)
                {
                    NPC npc = (NPC)questGiver;
                    npc.RoundsRemaining = 0;
                    npc.special += " despawn";
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void CompleteQuest(PC questor)
        {
            try
            {
                // increment times completed
                this.TimesCompleted++;
                // stamp the finish date
                this.FinishDate = DateTime.Now.ToString();
                // clear out the completed steps if repeatable
                if (this.IsRepeatable)
                {
                    this.completedSteps = new List<short>();
                }

                // clear out old flags that were needed for this quest
                foreach (string flag in this.RequiredFlags)
                {
                    if (questor.questFlags.Contains(flag))
                    {
                        questor.questFlags.Remove(flag);
                    }
                }

                // log the quest completion
                Utils.Log(this.GetLogString() + " was completed by " + questor.GetLogString(), Utils.LogType.QuestCompletion);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

    }
}
