using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    public class Point
    {
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return "{" + this.x.ToString() + "," + this.y.ToString() + "}";
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point)) return false;

            return this == (Point)obj;
        }

        public static bool operator ==(Point lhs, Point rhs)
        {
            if ((object)lhs == null && (object)rhs == null)
                return true;

            if ((object)lhs == null || (object)rhs == null)
                return false;

            if (lhs.x == rhs.x && lhs.y == rhs.y)
                return true;
            else
                return false;
        }

        public static bool operator !=(Point lhs, Point rhs)
        {
            if ((object)lhs == null && (object)rhs == null)
                return false;

            if ((object)lhs == null || (object)rhs == null)
                return true;

            if (lhs.x == rhs.x && lhs.y == rhs.y)
                return false;
            else
                return true;
        }

        public static Point operator -(Point lhs, Point rhs)
        {
            return new Point(lhs.x - rhs.x, lhs.y - rhs.y);
        }

    }
}
