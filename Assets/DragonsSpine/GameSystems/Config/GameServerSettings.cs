using System;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;


namespace DragonsSpine.Config
{
    /// <summary>
    /// The game server settings class. Note that if there is an xml file then values need to be changed there.
    /// The initialized values here are default.
    /// </summary>
    public class GameServerSettings
    {
        public static bool LoadSettings = false;

        public int Port = 3000;
        public short UDPPort = 3010;

        public string LogConfigFile = "." + Path.DirectorySeparatorChar + "Config" + Path.DirectorySeparatorChar + "logconfig.xml";
        public string ServerVersion = ConfigurationManager.AppSettings["APP_VERSION"];
        public string ClientVersion = ConfigurationManager.AppSettings["CLIENT_VERSION"];
        public string GameName = ConfigurationManager.AppSettings["APP_NAME"];
        public string ServerNews = "Visit www.dragonsspine.com for news and information.";
        public string ServerProtocol = ConfigurationManager.AppSettings["APP_PROTOCOL"];
        public bool ClearStoresOnStartup = false;
        public bool RestockStoresOnStartup = false;
        public bool RequireMakeRecallReagent = false;
        public bool AllowDisplayCombatDamage = true;
        public bool UnderworldEnabled = false;
        public bool NPCSkillGain = false;

        //public string ScriptAssemblies = "DragonsSpine.dll,DragonsSpineScripts.dll";
        public string ScriptAssemblies = "DragonsSpineScripts.dll";
        //public string ScriptCompilationTarget = Utils.GetStartupPath() + Path.DirectorySeparatorChar + "Lib" + Path.DirectorySeparatorChar + "GameScripts.dll";
        public string ScriptCompilationTarget = Utils.GetStartupPath() +"Lib" + Path.DirectorySeparatorChar + "GameScripts.dll";

        /// <summary>
        /// Saves the current settings.
        /// </summary>
        public void Save()
        {
            try
            {
                string dirName = Utils.GetStartupPath() + "Config" + Path.DirectorySeparatorChar;
                string fileName = "serverconfig.xml";

                Stream stream = File.Create(dirName + fileName);

                XmlSerializer serializer = new XmlSerializer(typeof(GameServerSettings));
                serializer.Serialize(stream, this);
                stream.Close();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        /// <summary>
        /// Loads settings from a file.
        /// </summary>
        public static GameServerSettings Load()
        {
            try
            {
                if (!LoadSettings)
                    return new GameServerSettings();

                string dirName = Utils.GetStartupPath() + "Config" + Path.DirectorySeparatorChar;
                string fileName = "serverconfig.xml";

                if (!File.Exists(dirName + fileName))
                {
                    DragonsSpineMain.Instance.Settings.Save();
                    return new GameServerSettings();
                }

                Stream stream = File.OpenRead(dirName + fileName);
                XmlSerializer serializer = new XmlSerializer(typeof(GameServerSettings));
                GameServerSettings settings = (GameServerSettings)serializer.Deserialize(stream);
                stream.Close();
                return settings;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return new GameServerSettings();
            }
        }
    }
}
