using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    public static class Skills
    {
        public const int MAX_SKILL_LEVEL = 19;

        #region Skill Titles
        public static string[] unarmedLvlName = new string[20]
			{
				"Untrained","White Belt","Yellow Belt","Green Belt","Blue Belt","Red Belt","Black Belt","1st Dan","2nd Dan",
				"3rd Dan","4th Dan","5th Dan","6th Dan","7th Dan","8th Dan","9th Dan","White Sash","Red Sash","Gold Sash","Master"
			};

        public static string[] weaponLvlName = new string[20]
			{
				"Untrained","Awkward","Mediocre","Capable","Familiar","Practiced","Competent","Experienced","Skillful","Proficient",
				"Exceptional","Brilliant","Expert","Astonishing","Amazing","Incredible","Master","Genius","Unearthly","Immortal"
			};

        public static string[] thievingLvlName = new string[20]
			{
				"Untrained","Clumsy","Mediocre","Average","Talented","Practiced","Deft","Efficient","Graceful","Professional",
				"Dexterous","Adroit","Expert","Astonishing","Amazing","Incredible","Magician","Peerless","Incomprehendable","Master"
			};

        public static string[] thaumLvlNameFemale = new string[20]
			{
				"Untrained","Shaman","Apprentice","Initiate","Acolyte","Healer","Canoness","Exorcist","Priestess","Seer","Summoner of Snakes",
				"Summoner of Spirits","Summoner of Demons","Prophetess","Matriarch","High Priestess","Master of Demons","Master of the Dead",
				"Master of Earth and Sky","Hierophant"
			};

        public static string[] thaumLvlNameMale = new string[20]
            {
				"Untrained","Shaman","Apprentice","Initiate","Acolyte","Healer","Canon","Exorcist","Priest","Seer","Summoner of Snakes",
				"Summoner of Spirits","Summoner of Demons","Prophet","Patriarch","High Priest","Master of Demons","Master of the Dead",
				"Master of Earth and Sky","Hierophant"
			};
        public static string[] wizardLvlName = new string[20]
			{
				"Untrained","Aspirant","Apprentice","Apprentice to Fire","Apprentice to Ice","Apprentice to Illusions","Shaper of Fire",
				"Shaper of Ice","Wizard","Shaper of Illusions","Illusionist","Master of Earth",	"Master of Illusions","Master of Air",
				"Mage","Lord of Fire","Lord of Illusions","Lord of Air","Archmage","Magus"
			};

        public static string[] thiefLvlName = new string[20]
			{
				"Untrained","Skulker in Shadows","Master of Mischief","Diviner of Magics","Knight of Darkness","Opener of Ways",
				"Lurker in Darkness","Obscurer of Ways","Master of Water","Master of Air","Master of Secrets","Master Thief",
				"Shadow Thief","Shadow Mage","Shadow Stalker","Shadow Lord","Thief of Wands","Thief of Cups","Thief of Pentacles",
				"Thief of Souls"
			};
        #endregion

        public static long GetSkillToMax(int level)
        {
            long m = 1600;

            for (int a = 1; a < level; a++)
                m = m * 2;

            return m;
        }

        public static long GetSkillToNext(int level)
        {
            if (level == 1)
                return 1569;

            long m = 1600;

            for (int a = 2; a < level; a++)
                m = m * 2;

            return m;
        }

        public static int GetSkillLevel(long skillExp)
        {
            if (skillExp < 31)
                return 0;

            long low = 31;

            long high = 1600;

            for (int a = 1; a <= Skills.MAX_SKILL_LEVEL; a++)
            {
                if (skillExp >= low && skillExp < high)
                    return a;

                low = high;

                high = high * 2;
            }

            return 1;
            //if (skillExp < 1)									    //Level 0
            //    return 0;
            //else if (skillExp >= 1 && skillExp < 1600)				//Level 1
            //    return 1;
            //else if (skillExp >= 1600 && skillExp < 3200)			//Level 2
            //    return 2;
            //else if (skillExp >= 3200 && skillExp < 6400)			//Level 3
            //    return 3;
            //else if (skillExp >= 6400 && skillExp < 12800)			//Level 4
            //    return 4;
            //else if (skillExp >= 12800 && skillExp < 25600)			//Level 5
            //    return 5;
            //else if (skillExp >= 25600 && skillExp < 51200)			//Level 6
            //    return 6;
            //else if (skillExp >= 51200 && skillExp < 102400)			//Level 7
            //    return 7;
            //else if (skillExp >= 102400 && skillExp < 204800)		//Level 8
            //    return 8;
            //else if (skillExp >= 204800 && skillExp < 409600)		//Level 9
            //    return 9;
            //else if (skillExp >= 409600 && skillExp < 819200)		//Level 10
            //    return 10;
            //else if (skillExp >= 819200 && skillExp < 1638400)		//Level 11
            //    return 11;
            //else if (skillExp >= 1638400 && skillExp < 3276800)		//Level 12
            //    return 12;
            //else if (skillExp >= 3276800 && skillExp < 6553600)		//Level 13
            //    return 13;
            //else if (skillExp >= 6553600 && skillExp < 13107200)		//Level 14
            //    return 14;
            //else if (skillExp >= 13107200 && skillExp < 26214400)	//Level 15
            //    return 15;
            //else if (skillExp >= 26214400 && skillExp < 52428800)	//Level 16
            //    return 16;
            //else if (skillExp >= 52428800 && skillExp < 104857600)	//Level 17
            //    return 17;
            //else if (skillExp > 104857600 && skillExp < 209715200)	//Level 18
            //    return 18;
            //else if (skillExp > 209715200 && skillExp < 419430400)	//Level 19
            //    return 19;
            //else if (skillExp > 419430400) // Level 20
            //    return 20;
            //else
            //    return 1;
        }

        public static string GetSkillTitle(Globals.eSkillType skillType, Character.ClassType classType, long skillExp, Globals.eGender gender)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Thievery:
                    return thievingLvlName[Skills.GetSkillLevel(skillExp)];
                case Globals.eSkillType.Magic:
                    if (classType == Character.ClassType.Thaumaturge)
                    {
                        if (gender == Globals.eGender.Female)
                        {
                            return thaumLvlNameFemale[Skills.GetSkillLevel(skillExp)];
                        }
                        else
                        {
                            return thaumLvlNameMale[Skills.GetSkillLevel(skillExp)];
                        }
                    }
                    else if (classType == Character.ClassType.Wizard)
                    {
                        return wizardLvlName[Skills.GetSkillLevel(skillExp)];
                    }
                    else if (classType == Character.ClassType.Thief)
                    {
                        return thiefLvlName[Skills.GetSkillLevel(skillExp)];
                    }
                    else
                    {
                        return "Untrained";
                    }

                case Globals.eSkillType.Unarmed:
                    return unarmedLvlName[Skills.GetSkillLevel(skillExp)];
                default:
                    return weaponLvlName[Skills.GetSkillLevel(skillExp)];
            }
        }

        /// <summary>
        /// Give skill for stealing and casting, and as a result of special block in combat
        /// </summary>
        /// <param name="ch"></param>
        /// <param name="skillType"></param>
        /// <param name="amount"></param>
        public static void GiveSkillExp(Character ch, Globals.eSkillType skillType, int amount)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["NPCSkillGain"].ToLower() == "false" && !ch.IsPC)
                return;

            string currentSkillTitle = Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender);

            double bonus = 1;

            int skillExpGained = amount;

            int skillGainHardCap = Skills.GetSkillLevel(ch.GetSkillExperience(skillType)) * 100;

            if (skillGainHardCap <= 0) skillGainHardCap = 100;

            #region Verify hard cap for amount of skill gained not exceeded (skill level * 100)
            if (skillExpGained > skillGainHardCap)
                skillExpGained = skillGainHardCap;
            #endregion

            #region Set skill gain bonus for spell casters with high wisdom or intelligence
            if (skillType == Globals.eSkillType.Magic)
            {
                if (ch.IsWisdomCaster)
                {
                    if (ch.Wisdom + ch.TempWisdom == 16) { bonus = 1.5; }
                    else if (ch.Wisdom + ch.TempWisdom == 17) { bonus = 2; }
                    else if (ch.Wisdom + ch.TempWisdom >= 18) { bonus = 2.5; }
                }
                if (ch.IsIntelligenceCaster)
                {
                    if (ch.Intelligence + ch.TempIntelligence == 16) { bonus = 1.5; }
                    else if (ch.Intelligence + ch.TempIntelligence == 17) { bonus = 2; }
                    else if (ch.Intelligence + ch.TempIntelligence >= 18) { bonus = 2.5; }
                }
            }
            #endregion

            #region Set skill gain bonus for thieves with high dexterity using thievery skill
            if (ch.BaseProfession == Character.ClassType.Thief && skillType == Globals.eSkillType.Thievery)
            {
                if (ch.Dexterity + ch.TempDexterity == 16) { bonus = 1.5; }
                else if (ch.Dexterity + ch.TempDexterity == 17) { bonus = 2; }
                else if (ch.Dexterity + ch.TempDexterity >= 18) { bonus = 2.5; }
            }
            #endregion

            #region Set skill gain bonus for specialized fighters
            if (ch.fighterSpecialization == skillType)
            {
                bonus += 1;
            }
            #endregion

            #region Set skill gain bonus for martial artists using unarmed skill
            if (ch.BaseProfession == Character.ClassType.Martial_Artist && skillType == Globals.eSkillType.Unarmed)
            {
                bonus += 1;
            }
            #endregion

            if (ch.GetTrainedSkillExperience(skillType) > 11)
            {
                int skillTrainingBonus = (int)((float)ch.GetTrainedSkillExperience(skillType) * (.02 * bonus));

                skillExpGained += skillTrainingBonus;

                //ch.SetTrainedSkillExperience(skillType, ch.GetTrainedSkillExperience(skillType) - skillTrainingBonus);

                ch.SetTrainedSkillExperience(skillType, ch.GetTrainedSkillExperience(skillType) - (int)((float)ch.GetTrainedSkillExperience(skillType) * (.02 * bonus)));

                if (ch.GetTrainedSkillExperience(skillType) < 0)
                {
                    ch.SetTrainedSkillExperience(skillType, 0);
                }
            }
            else ch.SetTrainedSkillExperience(skillType, 0);

            #region Verify hard cap for amount of skill gained not exceeded (skill level * 100)
            if (skillExpGained > skillGainHardCap)
            {
                skillExpGained = skillGainHardCap;
            }
            #endregion

            ch.SetSkillExperience(skillType, ch.GetSkillExperience(skillType) + skillExpGained);

            #region If character has risen a skill level send skill up sound and message
            if (Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender) != currentSkillTitle)
            {
                ch.SendSound(Sound.GetCommonSound(Sound.CommonSound.SkillUp));
                ch.WriteToDisplay("You have risen from " + currentSkillTitle + " to " + Utils.FormatEnumString(Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender) + " in your " + Utils.FormatEnumString(skillType.ToString()).ToLower()) + " skill.");
            }
            #endregion

            #region Log as non combat skill gain if applicable
            Utils.Log(ch.GetLogString() + " gained " + skillExpGained.ToString() + " " + skillType.ToString() + " skill.", Utils.LogType.SkillGainNonCombat);
            #endregion
        }

        /// <summary>
        /// Called as a result of combat damage or magic spell success
        /// </summary>
        /// <param name="ch"></param>
        /// <param name="target"></param>
        /// <param name="skillType"></param>
        public static void GiveSkillExp(Character ch, Character target, Globals.eSkillType skillType)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["NPCSkillGain"].ToLower() == "false" && !ch.IsPC)
                return;

            string currentSkillLevelName = Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender);

            int skillExpGained = (int)((float)target.Experience * Skills.CalculateSkillRisk(ch, target));

            double bonus = 1;

            int skillGainHardCap = Skills.GetSkillLevel(ch.GetSkillExperience(skillType)) * 100;

            if (skillGainHardCap <= 0) skillGainHardCap = 100;

            #region Verify hard cap for amount of skill gained not exceeded (skill level * 100)
            if (skillExpGained > skillGainHardCap)
                skillExpGained = skillGainHardCap;
            #endregion

            #region Set skill gain bonus for spell casters with high wisdom or intelligence
            if (skillType == Globals.eSkillType.Magic)
            {
                if (ch.IsWisdomCaster)
                {
                    if (ch.Wisdom + ch.TempWisdom == 16) { bonus = 1.5; }
                    else if (ch.Wisdom + ch.TempWisdom == 17) { bonus = 2; }
                    else if (ch.Wisdom + ch.TempWisdom >= 18) { bonus = 2.5; }
                }
                if (ch.IsIntelligenceCaster)
                {
                    if (ch.Intelligence + ch.TempIntelligence == 16) { bonus = 1.5; }
                    else if (ch.Intelligence + ch.TempIntelligence == 17) { bonus = 2; }
                    else if (ch.Intelligence + ch.TempIntelligence >= 18) { bonus = 2.5; }
                }
            }
            #endregion

            #region Set skill gain bonus for thieves with high dexterity using thievery skill
            if (ch.BaseProfession == Character.ClassType.Thief && skillType == Globals.eSkillType.Thievery)
            {
                if (ch.Dexterity + ch.TempDexterity == 16) { bonus = 1.5; }
                else if (ch.Dexterity + ch.TempDexterity == 17) { bonus = 2; }
                else if (ch.Dexterity + ch.TempDexterity >= 18) { bonus = 2.5; }
            }
            #endregion

            #region Set skill gain bonus for specialized fighters
            if (ch.fighterSpecialization == skillType)
            {
                bonus += 1;
            }
            #endregion

            #region Set skill gain bonus for martial artists using unarmed skill
            if (ch.BaseProfession == Character.ClassType.Martial_Artist && skillType == Globals.eSkillType.Unarmed)
            {
                bonus += 1;
            }
            #endregion

            if (ch.GetTrainedSkillExperience(skillType) > 11)
            {
                int skillTrainingBonus = (int)((float)ch.GetTrainedSkillExperience(skillType) * (.02 * bonus));

                skillExpGained += skillTrainingBonus;

                //ch.SetTrainedSkillExperience(skillType, ch.GetTrainedSkillExperience(skillType) - skillTrainingBonus);

                ch.SetTrainedSkillExperience(skillType, ch.GetTrainedSkillExperience(skillType) - (int)((float)ch.GetTrainedSkillExperience(skillType) * (.02 * bonus)));

                if (ch.GetTrainedSkillExperience(skillType) < 0)
                {
                    ch.SetTrainedSkillExperience(skillType, 0);
                }
            }
            else ch.SetTrainedSkillExperience(skillType, 0);

            #region Verify hard cap for amount of skill gained not exceeded (skill level * 100)
            if (skillExpGained > skillGainHardCap)
            {
                skillExpGained = skillGainHardCap;
            }
            #endregion

            ch.SetSkillExperience(skillType, ch.GetSkillExperience(skillType) + skillExpGained);

            #region If character has risen a skill level send skill up sound and message
            if (Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender) != currentSkillLevelName)
            {
                ch.SendSound(Sound.GetCommonSound(Sound.CommonSound.SkillUp));
                ch.WriteToDisplay("You have risen from " + currentSkillLevelName + " to " + Skills.GetSkillTitle(skillType, ch.BaseProfession, ch.GetSkillExperience(skillType), ch.gender) + " in your " + Utils.FormatEnumString(skillType.ToString()).ToLower() + " skill.");
            }
            #endregion

            Utils.Log(ch.GetLogString() + " +" + skillExpGained + " " + skillType.ToString().ToLower() + " vs. " + target.GetLogString() + ".", Utils.LogType.SkillGainCombat);

        }

        public static bool SkillTrain(Character chr, Character trainer, Item gold)
        {
            string skillName = "";

            Globals.eSkillType skillType = Globals.eSkillType.None;

            if (chr.RightHand == null)
            {
                skillType = Globals.eSkillType.Unarmed;
                skillName = "in the martial arts";
            }
            else
            {
                skillType = chr.RightHand.skillType;
                if (skillType == Globals.eSkillType.Magic)
                {
                    switch (chr.BaseProfession)
                    {
                        case Character.ClassType.Thaumaturge:
                            skillName = "in the art of thaumaturgy";
                            break;
                        case Character.ClassType.Thief:
                            skillName = "in the art of shadow magic";
                            break;
                        case Character.ClassType.Wizard:
                            skillName = "in the art of wizardry";
                            break;
                        default:
                            skillName = "in magic";
                            break;
                    }
                }
                else if (skillType == Globals.eSkillType.Thievery)
                {
                    skillName = "in the ways of thievery";
                }
                else
                {
                    skillName = "with " + chr.RightHand.shortDesc;
                }
            }

            // trainer's current skill experience
            long trainerSkill = trainer.GetSkillExperience(skillType);

            #region Return false if the trainer is not skilled
            if (trainerSkill <= 0) // if the trainer is not skilled
            {
                chr.WriteToDisplay(trainer.Name + ": I am not skilled " + skillName + ".");
                return false;
            }
            #endregion

            // player's current skill experience
            long skillExp = chr.GetSkillExperience(skillType);

            #region Return false if the player is more skilled than the trainer
            if (skillExp > trainerSkill)
            {
                chr.WriteToDisplay(trainer.Name + ": You are more skilled " + skillName.ToLower() + " than I am.");
                return false;
            }
            #endregion

            // player's current trained amount with the skill
            long currentTrained = chr.GetTrainedSkillExperience(skillType);

            // total skill experience needed for this skill level
            long skillMax = Skills.GetSkillToMax(Skills.GetSkillLevel(skillExp));

            // how far the player is into the skill level
            long skillIntoLevel = skillMax - skillExp;

            // how much skill the player has remaining in current skill level
            long skillRemain = skillMax - skillIntoLevel; // Skills.GetSkillToNext(Skills.GetSkillLevel(skillExp)) - skillIntoLevel;

            // current skill rank
            int skillRank = Skills.GetSkillRank(skillExp);

            // cost per rank for current skill level
            int skillRankCost = Skills.GetTrainingCostPerRank(Skills.GetSkillLevel(skillExp));

            int nextLevelRankCost = Skills.GetTrainingCostPerRank(Skills.GetSkillLevel(skillExp) + 1);

            long maxTrainAmount = 0;

            if (skillRank <= 5)
            {
                maxTrainAmount = 5 * skillRankCost;
            }
            else
            {
                int difference = 10 - skillRank;
                maxTrainAmount = (difference * skillRankCost) + (nextLevelRankCost * (5 - difference));
            }

            if (currentTrained >= maxTrainAmount) // if the player's already trained to the maximum then send them off to practice
            {
                chr.WriteToDisplay(trainer.Name + ": You must practice more before I can train you again.");
                return false;
            }

            // subtract the amount this character is already trained from the maximum to set new maximum
            if (currentTrained > 0)
                maxTrainAmount = maxTrainAmount - currentTrained;

            long trainAmount = 0;

            // figure the amount of gold we'll take and set the training amount
            if (gold.coinValue > maxTrainAmount)
            {
                trainAmount = maxTrainAmount;
                gold.coinValue = gold.coinValue - trainAmount;
                if (gold.land == -1)
                    Map.PutItemOnCounter(chr, gold);
                else trainer.CurrentCell.Add(gold);
            }
            else trainAmount = Convert.ToInt64(gold.coinValue);

            chr.SetTrainedSkillExperience(skillType, chr.GetTrainedSkillExperience(skillType) + trainAmount);

            if (skillType == Globals.eSkillType.Magic)
                skillName = "in magic";

            Utils.Log(chr.GetLogString() + " trains " + skillType + ". current: " + skillExp + " trainAmount: " + trainAmount + " maxTrainAmount: " + maxTrainAmount +
                " skillMaxXp: " + skillMax + " skillRemain: " + skillRemain + " skillRank: " + skillRank + " skillRankCost: " + skillRankCost +
                " nextLevelRankCost: " + nextLevelRankCost + " trainedSkillExp: " + currentTrained, Utils.LogType.SkillTraining);

            chr.WriteToDisplay(trainer.Name + ": You have been trained " + skillName + ", go now and practice.");

            chr.Experience += trainAmount; // give training experience

            Utils.Log(chr.GetLogString() + " earns " + trainAmount + " experience from training.", Utils.LogType.ExperienceTraining);

            return true;
        }

        public static double CalculateSkillRisk(Character ch, Character target)
        {
            int levelDifference = target.Level - ch.Level;

            double baseM = .04;

            double riskM = .01;

            double finalM = .04;

            int pctHits = (int)(((float)ch.Hits / (float)ch.HitsFull) * 100);

            if (levelDifference < 0) // the character is engaging a lower level critter
            {
                finalM = ((ch.numAttackers - 1) * riskM) + baseM;
            }
            else if (levelDifference == 0) // the character is engaging a critter of equal level
            {
                if (pctHits >= 50) // character's health is at or above 50%
                {
                    finalM = ((ch.numAttackers - 1) * riskM) + baseM;
                }
                else if (pctHits < 50 && pctHits > 25) // character's health is below 50% and above 25%
                {
                    finalM = ((ch.numAttackers - 1) * (riskM * 2)) + baseM;
                }
                else // character's health is at or below 25%
                {
                    finalM = ((ch.numAttackers - 1) * (riskM * 3)) + baseM;
                }
            }
            else // the character is engaging a higher level critter
            {
                if (pctHits >= 75) // character's health is at or above 75%
                {
                    finalM = ((ch.numAttackers - 1 + levelDifference) * (riskM)) + baseM;
                }
                else if (pctHits < 75 && pctHits > 30) // character's health is below 75% and above 30%
                {
                    finalM = ((ch.numAttackers - 1 + levelDifference) * (riskM * 2)) + baseM;
                }
                else // character's health is at or below 30%
                {
                    finalM = ((ch.numAttackers - 1 + levelDifference) * (riskM * 3)) + baseM;
                }
            }
            Utils.Log("Final Multiplier = " + finalM + " " + ch.GetLogString() + " H: " + pctHits + "% #Atkrs: " + ch.numAttackers + " vs. " + target.GetLogString() + ".", Utils.LogType.SkillGainRisk);
            return finalM;
        }

        public static int GetTrainingCostPerRank(int skillLevel)
        {
            // get cost to train the entire skill level, divided by 10 ranks
            return (int)(Rules.Formula_TrainingCostForLevel(skillLevel) / 10);
        }

        public static int GetSkillRank(long currentSkill)
        {
            long max = Skills.GetSkillToMax(Skills.GetSkillLevel(currentSkill)); // get skill to max
            long into = max - currentSkill; // get skill into current skill level
            long remaining = Skills.GetSkillToNext(Skills.GetSkillLevel(currentSkill)) - into; // get skill remaining in this skill level

            int rank = (int)(((float)remaining / (float)Skills.GetSkillToNext(Skills.GetSkillLevel(currentSkill))) * 10);

            return rank;
        }

        public static string SkillRankToString(int rank)
        {
            string skillrank = "rank";
            switch (rank)
            {
                case 1:
                    skillrank = "first rank";
                    break;
                case 2:
                    skillrank = "second rank";
                    break;
                case 3:
                    skillrank = "third rank";
                    break;
                case 4:
                    skillrank = "fourth rank";
                    break;
                case 5:
                    skillrank = "fifth rank";
                    break;
                case 6:
                    skillrank = "sixth rank";
                    break;
                case 7:
                    skillrank = "seventh rank";
                    break;
                case 8:
                    skillrank = "eighth rank";
                    break;
                case 9:
                    skillrank = "ninth rank";
                    break;
                case 10:
                    skillrank = "ninth rank";
                    break;
                default:
                    skillrank = "rank";
                    break;
            }
            return skillrank;
        }

        public static double GetSkillRisk(Character ch, Character target)
        {
            int levelDifference = target.Level - ch.Level;
            double baseMultiplier = .04;
            double riskMultiplier = .01;
            double finalMultiplier = .04;
            int pctHits = (int)(((float)ch.Hits / (float)ch.HitsMax) * 100);

            if (levelDifference < 0) // the character is engaging a lower level critter
            {
                finalMultiplier = ((ch.numAttackers - 1) * riskMultiplier) + baseMultiplier;
            }
            else if (levelDifference == 0) // the character is engaging a critter of equal level
            {
                if (pctHits >= 50) // character's health is at or above 50%
                {
                    finalMultiplier = ((ch.numAttackers - 1) * riskMultiplier) + baseMultiplier;
                }
                else if (pctHits < 50 && pctHits > 25) // character's health is below 50% and above 25%
                {
                    finalMultiplier = ((ch.numAttackers - 1) * (riskMultiplier * 2)) + baseMultiplier;
                }
                else // character's health is at or below 25%
                {
                    finalMultiplier = ((ch.numAttackers - 1) * (riskMultiplier * 3)) + baseMultiplier;
                }
            }
            else // the character is engaging a higher level critter
            {
                if (pctHits >= 75) // character's health is at or above 75%
                {
                    finalMultiplier = ((ch.numAttackers - 1 + levelDifference) * (riskMultiplier)) + baseMultiplier;
                }
                else if (pctHits < 75 && pctHits > 30) // character's health is below 75% and above 30%
                {
                    finalMultiplier = ((ch.numAttackers - 1 + levelDifference) * (riskMultiplier * 2)) + baseMultiplier;
                }
                else // character's health is at or below 30%
                {
                    finalMultiplier = ((ch.numAttackers - 1 + levelDifference) * (riskMultiplier * 3)) + baseMultiplier;
                }
            }
            Utils.Log("Final Multiplier = " + finalMultiplier + " " + ch.GetLogString() + " H: " + pctHits + "% #Atkrs: " + ch.numAttackers + " vs. " + target.GetLogString() + ".", Utils.LogType.SkillGainRisk);
            return finalMultiplier;
        }

        public static void SkillLoss(Character ch, Globals.eSkillType skillType, int amount)
        {
            if (skillType == Globals.eSkillType.None) return;

            long currSkillExp = ch.GetSkillExperience(skillType);
            string currSkillTitle = Skills.GetSkillTitle(skillType, ch.BaseProfession, currSkillExp, ch.gender);
            int currSkillLevel = Skills.GetSkillLevel(currSkillExp);

            if (currSkillExp > 31 + amount) { currSkillExp -= amount; }

            ch.SetSkillExperience(skillType, currSkillExp);

            if (Skills.GetSkillLevel(currSkillExp) != currSkillLevel)
            {
                ch.WriteToDisplay("You have fallen from " + currSkillTitle + " to " + Skills.GetSkillTitle(skillType, ch.BaseProfession, currSkillExp, ch.gender) + " in your " + Utils.FormatEnumString(skillType.ToString()) + " skill.");
            }
        }

        public static void SkillLossOverTime(Character ch)
        {
            foreach (Globals.eSkillType skillType in Enum.GetValues(typeof(Globals.eSkillType)))
            {
                if (skillType != Globals.eSkillType.None)
                {
                    Skills.SkillLoss(ch, skillType, Skills.GetSkillLevel(ch.GetSkillExperience(skillType)));
                }
            }
        }

        public static Globals.eSkillType[] GetXHighestSkills(Character ch, int x)
        {

            try
            {
                Globals.eSkillType[] skills = (Globals.eSkillType[])Enum.GetValues(typeof(Globals.eSkillType));
                long[] values = new long[skills.Length];

                for (int count = 0; count < skills.Length; count++)
                {
                    values[count] = ch.GetSkillExperience(skills[count]);
                }

                // sorted in ascending order
                Array.Sort(values, skills);

                Globals.eSkillType[] xHighest = new Globals.eSkillType[x];

                for (int count = 0, at = skills.Length - 1; count < x; count++, at--)
                {
                    xHighest[count] = skills[at];
                }

                return xHighest;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }
    }
}
