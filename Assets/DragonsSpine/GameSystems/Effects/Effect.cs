using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;

namespace DragonsSpine
{
	public class Effect
	{
        public enum EffectType
        {
            #region Effect Types
            None, // 0
            Fire,
            Ice,
            Darkness,
            Poison_Cloud,
            Web, // 5
            Light,
            Concussion,
            Find_Secret_Door,
            Illusion,
            Dragon__s_Breath_Fire, // 10
            Dragon__s_Breath_Ice,
            Turn_Undead,
            Whirlwind, 
            Breathe_Water,
            Feather_Fall, // 15
            Night_Vision,
            Wizard_Eye,
            Peek,
            HitsMax,
            Hits, // 20
            Exp,
            Stamina,
            StamLeft,
            Permanent_Strength,
            Permanent_Dexterity, // 25
            Permanent_Intelligence,
            Permanent_Wisdom,
            Permanent_Constitution,
            Permanent_Charisma,
            Temporary_Strength, // 30
            Temporary_Dexterity,
            Temporary_Intelligence,
            Temporary_Wisdom,
            Temporary_Constitution,
            Temporary_Charisma, // 35
            Shield,
            Regenerate_Hits,
            Regenerate_Mana,
            Regenerate_Stamina,
            Protection_from_Fire, // 40
            Protection_from_Cold,
            Protection_from_Poison,
            Protection_from_Fire_and_Ice,
            Protection_from_Blind_and_Fear,
            Protection_from_Stun_and_Death, // 45
            Resist_Fear,
            Resist_Blind,
            Resist_Stun,
            Resist_Lightning,
            Resist_Death, // 50
            Resist_Zonk,
            Knight_Ring,
            Bless,
            Blind,
            Fear, // 55
            Poison,
            Balm,
            Naphtha,
            Ale,
            Wine, // 60
            Beer,
            Coffee,
            Water,
            Youth_Potion,
            Drake_Potion, // 65
            Blindness_Cure,
            Mana_Restore,
            Stamina_Restore,
            Nitro,
            OrcBalm, // 70
            Permanent_Mana,
            Whiskey,
            Rum,
            Unlocked_Horizontal_Door,
            Unlocked_Vertical_Door, // 75
            Hide_In_Shadows,
            Flight,
            Find_Secret_Rockwall,
            Hide_Door, // 79
            MindControl,
            Black_Fog,
            Lightning_Storm,
            Fire_Storm,
            Lava, // 84
            Lightning,
            Hello_Immobility, // used to keep an NPC still for a random amount of time during PC-NPC interaction
            Dog_Luck, // when you pet a dog - if player has this effect the player IsLucky
            Dog_Follow
            #endregion
        }
        
		public EffectType effectType; // Constant that shows the effect type of this object
		public int effectAmount; // power of the effect
		public string effectGraphic; // graphic shown on the map, if blank, then no graphic or this is an effect attached to a character
		public int duration; // in rounds
        public Character target;
		public Character caster; // who cast this spell if the effect came from a spell, if not leave blank
		//public ArrayList cellList = new ArrayList();
        public List<Cell> cellList = new List<Cell>();
        public bool IsPermanent
        {
            get
            {
                if (this.duration == -1)
                {
                    return true;
                }
                return false;
            }
        }
        System.Timers.Timer effectTimer;
        // area effect to a single cell
        public Effect(EffectType effectType, string effectGraphic, int effectAmount, int duration, Character caster, Cell cell)
        {
            
            this.effectType = effectType;
            if (effectGraphic == "")
            {
                this.effectGraphic = cell.CellGraphic;
            }
            else
            {
                this.effectGraphic = effectGraphic;
                cell.DisplayGraphic = effectGraphic;
            }
            this.effectAmount = effectAmount;
            this.duration = duration;
            this.caster = caster;
            this.cellList = new List<Cell>();
            this.cellList.Add(cell);
            if (cell.Effects.ContainsKey(effectType))
            {
                cell.Effects[effectType].effectGraphic = effectGraphic;
                cell.Effects[effectType].effectAmount = effectAmount;
                cell.Effects[effectType].duration = duration;
                cell.Effects[effectType].caster = caster;
            }
            else
            {
                cell.Add(this);
            }
            if (this.effectGraphic != "")
            {
                cell.DisplayGraphic = this.effectGraphic;
            }
            this.effectTimer = new System.Timers.Timer(DragonsSpineMain.MasterRoundInterval);
            this.effectTimer.Elapsed += new System.Timers.ElapsedEventHandler(AreaEffectEvent);
            this.effectTimer.Start();
            if (this.effectType == EffectType.Whirlwind || this.effectType == EffectType.Fire_Storm)
            {
                return;
            }
            else
            {
                Effect.DoAreaEffect(cell, this);
            }

        }

        // area effect to an array of cells
		public Effect(EffectType effectType, string effectGraphic, int effectAmount, int duration, Character caster, ArrayList cellList)
		{
            foreach (Cell cell in cellList)
            {
                Effect areaEffect = new Effect(effectType, effectGraphic, effectAmount, duration, caster, cell);
            }
            #region Effect code
            /*
            this.effectType = effectType;
            this.effectGraphic = effectGraphic;
			this.effectAmount = effectAmount;
			this.duration = duration;
			this.caster = caster;
			this.cellList = cellList;
            foreach (Cell cell in cellList)
            {
                if (cell.effectList.ContainsKey(effectType))
                {
                    cell.effectList[effectType].effectGraphic = effectGraphic;
                    cell.effectList[effectType].effectAmount = effectAmount;
                    cell.effectList[effectType].duration = duration;
                    cell.effectList[effectType].caster = caster;
                }
                else
                {
                    cell.Add(this);
                }
                
                if (this.effectGraphic != "")
                {
                    cell.DisplayGraphic = this.effectGraphic;
                }
                //World.GetFacetByIndex(0).GetLandByID(cell.land).GetMapByID(cell.map).updateVisionCellVisible(cell);
            }
            DragonsSpineMain.effectList.Add(this);
            DragonsSpineMain.serverWindow.updateEffectsCount();
            //if (this.duration > 0)
            //{
                this.effectTimer = new System.Timers.Timer(DragonsSpineMain.MasterRoundInterval);
                this.effectTimer.Elapsed += new System.Timers.ElapsedEventHandler(AreaEffectEvent);
                this.effectTimer.Start();
                //}
             */
            #endregion
		}

        // character effect
        public Effect(EffectType effectType, int amount, int duration, Character target, Character caster)
        {
            this.effectType = effectType;
            this.effectAmount = amount;
            this.duration = duration;
            this.target = target;
            this.caster = caster;
            this.target.effectList.Add(this.effectType, this);
            this.effectTimer = new System.Timers.Timer(DragonsSpineMain.MasterRoundInterval);
            this.effectTimer.Elapsed += new System.Timers.ElapsedEventHandler(CharacterEffectEvent);
            this.effectTimer.Start();
        }

        // worn effect
        public Effect(EffectType effectType, int amount, Character target)
        {
            this.effectType = effectType;
            this.effectAmount = amount;
            this.target = target;
            this.caster = target;
        }

        void AreaEffectEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (this.duration > 0)
                    this.duration--;

                if (this.duration == 0)
                {
                    this.effectTimer.Stop();
                    this.StopAreaEffect();
                }
                else
                {
                    foreach (Cell cell in new List<Cell>(this.cellList))
                        Effect.DoAreaEffect(cell, this);
                }
            }
            catch (Exception ex)
            {
                Utils.LogException(ex);
            }
        }

        void CharacterEffectEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!this.target.IsPC || this.target.PCState == Globals.ePlayerState.PLAYING)
            {
                if (this.duration > 0)
                {
                    this.duration--;
                }

                try
                {
                    if (this.duration == 0)
                    {
                        if (this.effectType == EffectType.Hide_In_Shadows)
                        {
                            if (this.caster != null)
                            {
                                if (this.caster.IsPC)
                                {
                                    if (this.caster.IsHidden)
                                    {
                                        Map.checkHiddenStatus(this.caster);
                                    }
                                    if (!this.caster.IsHidden)
                                    {
                                        this.effectTimer.Stop();
                                        this.StopCharacterEffect();
                                    }
                                }
                            }
                        }
                        else
                        {
                            this.effectTimer.Stop();
                            this.StopCharacterEffect();
                        }
                    }
                    else
                    {
                        switch (this.effectType)
                        {
                            case EffectType.Balm:
                                Effect.DoBalmEffect(this.target, this);
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.duration = 1;
                    Utils.LogException(ex);
                }
            }
        }

        public void StopAreaEffect()
        {           
            try
            {
                Effect effect;

                foreach (Cell cell in this.cellList)
                {
                    cell.Remove(this.effectType);
                    if (cell.Effects.Count <= 0)
                    {
                        if (cell.DisplayGraphic != cell.CellGraphic)
                            cell.DisplayGraphic = cell.CellGraphic;

                        switch (this.effectType)
                        {
                            case EffectType.Hide_Door:
                                cell.IsSecretDoor = false;
                                break;
                            case EffectType.Find_Secret_Door:
                                if (cell.Characters.Count > 0)
                                {
                                    effect = new Effect(EffectType.Concussion, "", Rules.RollD(1, 20), 0, null, cell);
                                    cell.IsSecretDoor = false;
                                    if (Rules.RollD(1, 2) == 1)
                                    {
                                        cell.CellGraphic = "[_";
                                        cell.DisplayGraphic = cell.CellGraphic;
                                    }
                                    else
                                    {
                                        cell.CellGraphic = "_]";
                                        cell.DisplayGraphic = cell.CellGraphic;
                                    }
                                }
                                else
                                {
                                    cell.DisplayGraphic = "[]";
                                    cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.CloseDoor));
                                }
                                break;
                            case EffectType.Find_Secret_Rockwall:
                                if (cell.Characters.Count > 0)
                                {
                                    effect = new Effect(EffectType.Concussion, "", Rules.RollD(1, 20), 0, null, cell);
                                    effect = new Effect(EffectType.Find_Secret_Rockwall, ". ", 0, 8, null, cell);
                                }
                                else
                                {
                                    cell.DisplayGraphic = "/\\";
                                    cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.CloseDoor));
                                }
                                break;
                            case EffectType.Fire:
                                if (cell.CellGraphic == "@ ")
                                    effect = new Effect(Effect.EffectType.Illusion, "t ", 0, 180, null, cell);
                                else if (cell.CellGraphic == " @")
                                    effect = new Effect(Effect.EffectType.Illusion, " t", 0, 180, null, cell);
                                else if (cell.CellGraphic == "@@")
                                    effect = new Effect(Effect.EffectType.Illusion, "tt", 0, 180, null, cell);
                                else if (cell.CellGraphic == "ww" && cell.DisplayGraphic == "ww")
                                    effect = new Effect(Effect.EffectType.Illusion, ". ", 0, 180, null, cell);
                                else if (cell.CellGraphic == "\"\"")
                                    effect = new Effect(Effect.EffectType.Illusion, "''", 0, 180, null, cell);
                                //else if (cell.CellGraphic == "~.")
                                //    effect = new Effect(Effect.EffectType.Illusion, "~~", 0, 180, null, cell);
                                break;
                            case EffectType.Ice:
                                // ice covers the trees in frost
                                if (cell.CellGraphic == "@ ")
                                    effect = new Effect(Effect.EffectType.Illusion, "t ", 0, 180, null, cell);
                                else if (cell.CellGraphic == " @")
                                    effect = new Effect(Effect.EffectType.Illusion, " t", 0, 180, null, cell);
                                else if (cell.CellGraphic == "@@")
                                    effect = new Effect(Effect.EffectType.Illusion, "tt", 0, 180, null, cell);
                                else if (cell.CellGraphic == "ww" && cell.DisplayGraphic == "ww")
                                    effect = new Effect(Effect.EffectType.Illusion, ". ", 0, 180, null, cell);
                                else if (cell.CellGraphic == "\"\"")
                                    effect = new Effect(Effect.EffectType.Illusion, "''", 0, 180, null, cell);
                                break;
                            case EffectType.Dragon__s_Breath_Fire:
                                if (cell.CellGraphic == "@ " || cell.CellGraphic == "@@")
                                    effect = new Effect(Effect.EffectType.Illusion, "t*", 1, 180, null, cell);
                                else if (cell.CellGraphic == " @")
                                    effect = new Effect(Effect.EffectType.Illusion, "*t", 1, 180, null, cell);
                                else if (cell.CellGraphic == "ww" && cell.DisplayGraphic == "ww")
                                    effect = new Effect(Effect.EffectType.Illusion, ". ", 0, 180, null, cell);
                                else if (cell.CellGraphic == "\"\"")
                                    effect = new Effect(Effect.EffectType.Illusion, "''", 0, 180, null, cell);
                                break;
                            default:
                                break;

                        }
                    }
                    else
                    {
                        cell.DisplayGraphic = cell.CellGraphic;

                        foreach (Effect cellEffect in cell.Effects.Values)
                            if (cellEffect.effectGraphic != cell.CellGraphic && cellEffect.effectGraphic != "")
                                cell.DisplayGraphic = cellEffect.effectGraphic;
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void StopCharacterEffect()
        {
            try
            {
                switch (this.effectType)
                {
                    case EffectType.Dog_Follow:
                        this.target.FollowName = "";
                        break;
                    case EffectType.Peek:
                        this.target.CurrentCell = this.target.MyClone.CurrentCell;
                        this.target.MyClone.CurrentCell = null;
                        this.target.IsInvisible = this.target.WasInvisible;
                        this.target.WasInvisible = false;
                        this.target.MyClone.RemoveFromWorld();
                        this.target.MyClone.RemoveFromLimbo();
                        this.target.MyClone = null;
                        this.target.PeekTarget = null;
                        break;
                    case EffectType.Wizard_Eye:
                        Cell oldCell = this.target.CurrentCell;
                        this.target.CurrentCell = this.target.MyClone.CurrentCell;
                        this.target.MyClone.CurrentCell.Remove(this.target.MyClone);
                        this.target.Name = this.target.MyClone.Name;
                        this.target.RightHand = this.target.MyClone.RightHand;
                        this.target.LeftHand = this.target.MyClone.LeftHand;
                        this.target.Hits = this.target.MyClone.Hits;
                        this.target.HitsMax = this.target.MyClone.HitsMax;
                        this.target.HitsAdjustment = this.target.MyClone.HitsAdjustment;
                        this.target.Stamina = this.target.MyClone.Stamina;
                        this.target.StaminaMax = this.target.MyClone.StaminaMax;
                        this.target.StaminaAdjustment = this.target.MyClone.StaminaAdjustment;
                        this.target.Mana = this.target.MyClone.Mana;
                        this.target.ManaMax = this.target.MyClone.ManaMax;
                        this.target.ManaAdjustment = this.target.MyClone.ManaAdjustment;
                        this.target.animal = false;
                        Character.SetCharacterVisualKey(this.target);
                        this.target.effectList[EffectType.Hide_In_Shadows].StopCharacterEffect();
                        this.target.MyClone.RemoveFromWorld();
                        this.target.MyClone = null;
                        break;
                    case EffectType.Protection_from_Fire:
                        this.target.FireResistance--;
                        this.target.FireProtection -= effectAmount;
                        break;
                    case EffectType.Protection_from_Cold:
                        this.target.ColdResistance--;
                        this.target.ColdProtection -= effectAmount;
                        break;
                    case EffectType.HitsMax:
                        this.target.HitsMax -= effectAmount;
                        break;
                    case EffectType.Hits:
                        this.target.Hits -= effectAmount;
                        break;
                    case EffectType.Stamina:
                        this.target.StaminaMax -= effectAmount;
                        break;
                    case EffectType.Temporary_Strength:
                        this.target.TempStrengthRaw -= effectAmount;
                        break;
                    case EffectType.Temporary_Dexterity:
                        this.target.TempDexterityRaw -= effectAmount;
                        break;
                    case EffectType.Temporary_Intelligence:
                        this.target.TempIntelligenceRaw -= effectAmount;
                        break;
                    case EffectType.Temporary_Wisdom:
                        this.target.TempWisdomRaw -= effectAmount;
                        break;
                    case EffectType.Temporary_Constitution:
                        this.target.TempConstitutionRaw -= effectAmount;
                        break;
                    case EffectType.Temporary_Charisma:
                        this.target.TempCharismaRaw -= effectAmount;
                        break;
                    case EffectType.Protection_from_Fire_and_Ice:
                        this.target.FireResistance--;
                        this.target.FireProtection -= effectAmount;
                        this.target.ColdResistance--;
                        this.target.ColdProtection -= effectAmount;
                        break;
                    case EffectType.Shield:
                        this.target.Shielding -= effectAmount;
                        break;
                    case EffectType.Resist_Fear:
                        this.target.FearResistance--;
                        break;
                    case EffectType.Resist_Blind:
                        this.target.BlindResistance--;
                        break;
                    case EffectType.Resist_Lightning:
                        this.target.LightningResistance--;
                        break;
                    case EffectType.Protection_from_Poison:
                        this.target.PoisonResistance--;
                        this.target.PoisonProtection -= effectAmount;
                        break;
                    case EffectType.Resist_Stun:
                        this.target.StunResistance--;
                        break;
                    case EffectType.Resist_Death:
                        this.target.DeathProtection -= effectAmount;
                        break;
                    case EffectType.Protection_from_Blind_and_Fear:
                        this.target.BlindResistance--;
                        this.target.FearResistance--;
                        break;
                    case EffectType.Protection_from_Stun_and_Death:
                        this.target.StunResistance--;
                        this.target.DeathResistance--;
                        this.target.DeathProtection -= this.effectAmount;
                        break;
                    case EffectType.Bless:
                        this.target.Shielding -= 1;
                        this.target.TempDexterityRaw -= 1;
                        this.target.TempConstitutionRaw -= 1;
                        break;
                    case EffectType.Balm: // healing over time
                        if (this.effectAmount > 0)
                        {
                            this.target.Hits += this.effectAmount;
                            if (this.target.Hits > this.target.HitsFull)
                                this.target.Hits = this.target.HitsFull;
                        }
                        break;
                    default:
                        break;
                }

                this.target.effectList.Remove(this.effectType);
                if (this.target.PCState == Globals.ePlayerState.PLAYING || !this.target.IsPC)
                {
                    if (Effect.GetEffectName(this.effectType) != "Unknown")
                    {
                        this.target.WriteToDisplay("The " + Effect.GetEffectName(this.effectType) + " spell has worn off.");

                        if (this.caster != null && this.caster.IsPC)
                        {
                            PC pc = PC.getOnline(this.caster.PlayerID);
                            if (pc != null && pc.PCState == Globals.ePlayerState.PLAYING && pc.PlayerID != this.target.PlayerID)
                            {
                                pc.WriteToDisplay("Your " + GetEffectName(this.effectType) + " spell has worn off of " + this.target.Name + ".");
                            }
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                Utils.Log("Failure at StopCharacterEffect Effect: " + this.effectType.ToString(), Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
        }

        public static void CreateCharacterEffect(EffectType effectType, int effectAmount, Character target, int duration, Character caster)
        {
            try
            {
                if (target.effectList.ContainsKey(effectType)) // the target already has this effect type
                {
                    if (target.effectList[effectType].effectAmount <= effectAmount)
                    {
                        target.effectList[effectType].effectAmount = effectAmount;
                        target.effectList[effectType].duration = duration;
                        target.effectList[effectType].caster = caster;
                    }
                    return;
                }

                Effect effect = new Effect(effectType, effectAmount, duration, target, caster);

                switch (effectType)
                {
                    case EffectType.Dog_Follow:
                        target.FollowName = caster.Name;
                        effect.caster = null;
                        break;
                    #region Strictly Bottle Effects
                    case EffectType.Ale:
                        target.SendToAllInSight(target.Name + " burps loudly.");
                        break;
                    case EffectType.Balm: // healing over time
                        Effect.DoBalmEffect(target, target.effectList[effectType]);
                        break;
                    case EffectType.Beer:
                        target.SendToAllInSight(target.Name + " burps.");
                        break;
                    case EffectType.Blindness_Cure:
                        if (target.IsBlind)
                        {
                            if (target.effectList.ContainsKey(EffectType.Blind))
                            {
                                target.effectList[EffectType.Blind].StopCharacterEffect();
                                target.WriteToDisplay("Your blindness has been cured!");
                            }
                        }
                        break;
                    case EffectType.Coffee:
                        target.Stamina += effectAmount;
                        break;
                    case EffectType.Drake_Potion:
                        if (target.Constitution < target.Land.MaxAbilityScore)
                            target.Constitution++;
                        target.HitsMax += effectAmount;
                        target.Hits = target.HitsFull;
                        target.StaminaMax += effectAmount;
                        target.Stamina = target.StaminaFull;
                        target.Mana = target.ManaFull;
                        break;
                    case EffectType.Mana_Restore:
                        target.Mana = target.ManaFull;
                        break;
                    case EffectType.Naphtha:
                        target.Poisoned = effectAmount;
                        break;
                    case EffectType.Nitro:
                        // TODO
                        break;
                    case EffectType.OrcBalm:
                        target.Stunned = (short)Rules.RollD(1, effectAmount);
                        break;
                    case EffectType.Permanent_Mana:
                        target.ManaMax += effectAmount;
                        target.Mana = target.ManaFull;
                        break;
                    case EffectType.Stamina_Restore:
                        target.Stamina = target.StaminaFull;
                        break;
                    case EffectType.Water:
                        break;
                    case EffectType.Wine:
                        break;
                    case EffectType.Youth_Potion:
                        if (target.Age > World.AgeCycles[0])
                        {
                            target.Age = World.AgeCycles[0];
                            target.WriteToDisplay("You feel young again!");
                        }
                        else
                        {
                            target.WriteToDisplay("The fluid is extremely bitter.");
                        }
                        break;
                    #endregion
                    case EffectType.HitsMax:
                        target.HitsMax += effectAmount;
                        break;
                    case EffectType.Hits:
                        target.Hits += effectAmount;
                        break;
                    case EffectType.Stamina:
                        target.StaminaMax += effectAmount;
                        break;
                    #region Permanent Stats
                    case EffectType.Permanent_Strength:
                        target.Strength += effectAmount;
                        if (target.Strength > target.Land.MaxAbilityScore)
                        {
                            target.Strength = target.Land.MaxAbilityScore;
                        }
                        target.Stunned = 3;
                        break;
                    case EffectType.Permanent_Dexterity:
                        target.Dexterity += effectAmount;
                        Effect.CreateCharacterEffect(EffectType.Blind, 0, target, 3, null);
                        if (target.Dexterity > target.Land.MaxAbilityScore)
                        {
                            target.Dexterity = target.Land.MaxAbilityScore;
                        }
                        break;
                    case EffectType.Permanent_Intelligence:
                        target.Intelligence += effectAmount;
                        if (target.Intelligence > target.Land.MaxAbilityScore)
                        {
                            target.Intelligence = target.Land.MaxAbilityScore;
                        }
                        break;
                    case EffectType.Permanent_Wisdom:
                        target.Wisdom += effectAmount;
                        if (target.Wisdom > target.Land.MaxAbilityScore)
                        {
                            target.Wisdom = target.Land.MaxAbilityScore;
                        }
                        break;
                    case EffectType.Permanent_Constitution:
                        target.Constitution += effectAmount;
                        if (target.Constitution > target.Land.MaxAbilityScore)
                        {
                            target.Constitution = target.Land.MaxAbilityScore;
                        }
                        break;
                    case EffectType.Permanent_Charisma:
                        target.Charisma += effectAmount;
                        if (target.Charisma > target.Land.MaxAbilityScore)
                        {
                            target.Charisma = target.Land.MaxAbilityScore;
                        }
                        break;
                    #endregion
                    #region Temporary Stats
                    case EffectType.Temporary_Strength:
                        target.TempStrengthRaw += effectAmount;
                        target.EmitSound(Spell.GetSpell("strength").SoundFile);
                        break;
                    case EffectType.Temporary_Dexterity:
                        target.TempDexterityRaw += effectAmount;
                        break;
                    case EffectType.Temporary_Intelligence:
                        target.TempIntelligenceRaw += effectAmount;
                        break;
                    case EffectType.Temporary_Wisdom:
                        target.TempWisdomRaw += effectAmount;
                        break;
                    case EffectType.Temporary_Constitution:
                        target.TempConstitutionRaw += effectAmount;
                        break;
                    case EffectType.Temporary_Charisma:
                        target.TempCharismaRaw += effectAmount;
                        break;
                    #endregion
                    case EffectType.Shield:
                        target.Shielding += effectAmount;
                        break;
                    #region Increase Protection
                    case EffectType.Protection_from_Fire:
                        target.FireResistance++;
                        target.FireProtection += effectAmount;
                        break;
                    case EffectType.Protection_from_Cold:
                        target.ColdResistance++;
                        target.ColdProtection += effectAmount;
                        break;
                    case EffectType.Protection_from_Fire_and_Ice:
                        target.FireResistance++;
                        target.FireProtection += effectAmount;
                        target.ColdResistance++;
                        target.ColdProtection += effectAmount;
                        break;
                    case EffectType.Protection_from_Poison:
                        target.PoisonResistance++;
                        target.PoisonProtection += effectAmount;
                        break;
                    case EffectType.Protection_from_Blind_and_Fear:
                        target.BlindResistance++;
                        target.FearResistance++;
                        break;
                    case EffectType.Protection_from_Stun_and_Death:
                        target.StunResistance++;
                        target.DeathResistance++;
                        target.DeathProtection += effectAmount;
                        break;
                    #endregion
                    #region Increase Resist
                    case EffectType.Resist_Fear:
                        target.FearResistance++;
                        break;
                    case EffectType.Resist_Blind:
                        target.BlindResistance++;
                        break;
                    case EffectType.Resist_Stun:
                        target.StunResistance++;
                        break;
                    case EffectType.Resist_Lightning:
                        target.LightningResistance++;
                        break;
                    case EffectType.Resist_Death:
                        target.DeathResistance++;
                        break;
                    #endregion
                    case EffectType.Bless:
                        target.Shielding += 1;
                        target.TempDexterityRaw += 1;
                        target.TempConstitutionRaw += 1;
                        break;
                    case EffectType.Hide_In_Shadows:
                        //caster.IsHidden = true;
                        break;
                    case EffectType.Peek:
                        caster.MyClone = caster.CloneCharacter();
                        caster.MyClone.AddToWorld();
                        caster.MyClone.IsPC = false;
                        if (caster.IsInvisible)
                        {
                            caster.WasInvisible = true;
                        }
                        caster.IsInvisible = true;
                        caster.PeekTarget = target;
                        break;
                    #region WizardEye
                    case EffectType.Wizard_Eye:
                        target.MyClone = target.CloneCharacter();
                        target.MyClone.IsPC = false;
                        Effect.CreateCharacterEffect(EffectType.Wizard_Eye, 0, target.MyClone, duration, target);
                        Effect.CreateCharacterEffect(EffectType.Hide_In_Shadows, 0, target, -1, target);
                        target.MyClone.CurrentCell = target.CurrentCell;
                        target.MyClone.AddToWorld();
                        if (target.BaseProfession == Character.ClassType.Thief)
                            target.Name = "rat";
                        else
                            target.Name = "toad";
                        Character.SetCharacterVisualKey(target);
                        target.RightHand = null;
                        target.LeftHand = null;
                        target.Hits = 3;
                        target.HitsMax = 3;
                        target.HitsAdjustment = 0;
                        target.Stamina = 3;
                        target.StaminaMax = 3;
                        target.StaminaAdjustment = 0;
                        target.Mana = 0;
                        target.ManaMax = 0;
                        target.ManaAdjustment = 0;
                        target.animal = true;
                        break;
                    #endregion
                    case EffectType.Fear:
                        if(!target.IsPC)
                        {
                            if(target.Group != null)
                            {
                                target.Group.Remove((NPC)target);
                            }
                        }
                        break;
                    case EffectType.Blind:
                        if (!target.IsPC)
                        {
                            if (target.Group != null)
                            {
                                target.Group.Remove((NPC)target);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                if (target != null && caster != null)
                {
                    Utils.Log("Effect.CreateCharacterEffect(): [Character: " + target.Name + "] [Caster: " + caster.Name + "] [Effect Name: " + effectType.ToString() + "]", Utils.LogType.SystemWarning);
                }
                Utils.LogException(e);
            }
        }

		public static void AddWornEffectToCharacter(Character ch, Item item)
		{
			if(item.effectType.Length > 0 && item.effectType != "" && (item.charges == -1 || item.charges > 0))
			{
				try
				{
					string[] itemEffectType = item.effectType.Split(" ".ToCharArray());
					string[] itemEffectAmount = item.effectAmount.Split(" ".ToCharArray());

                    for (int a = 0; a < itemEffectType.Length; a++)
                    {
                        Effect effect = new Effect((EffectType)Convert.ToInt32(itemEffectType[a]), Convert.ToInt32(itemEffectAmount[a]), ch);

                        switch (effect.effectType)
                        {
                            case EffectType.Regenerate_Mana:
                                ch.manaRegen += effect.effectAmount;
                                break;
                            case EffectType.HitsMax:
                                ch.HitsMax += effect.effectAmount;
                                break;
                            case EffectType.Hits:
                                ch.Hits += effect.effectAmount;
                                break;
                            case EffectType.Stamina:
                                ch.StaminaMax += effect.effectAmount;
                                break;
                            #region Temporary Stats
                            case EffectType.Temporary_Strength:
                                ch.TempStrengthRaw += effect.effectAmount;
                                break;
                            case EffectType.Temporary_Dexterity:
                                ch.TempDexterityRaw += effect.effectAmount;
                                break;
                            case EffectType.Temporary_Intelligence:
                                ch.TempIntelligenceRaw += effect.effectAmount;
                                break;
                            case EffectType.Temporary_Wisdom:
                                ch.TempWisdomRaw += effect.effectAmount;
                                break;
                            case EffectType.Temporary_Constitution:
                                ch.TempConstitutionRaw += effect.effectAmount;
                                break;
                            case EffectType.Temporary_Charisma:
                                ch.TempCharismaRaw += effect.effectAmount;
                                break;
                            #endregion
                            #region Increase Protection
                            case EffectType.Protection_from_Fire:
                                ch.FireResistance++;
                                ch.FireProtection += effect.effectAmount;
                                break;
                            case EffectType.Protection_from_Cold:
                                ch.ColdResistance++;
                                ch.ColdProtection += effect.effectAmount;
                                break;
                            case EffectType.Protection_from_Fire_and_Ice:
                                ch.FireResistance++;
                                ch.FireProtection += effect.effectAmount;
                                ch.ColdResistance++;
                                ch.ColdProtection += effect.effectAmount;
                                break;
                            case EffectType.Protection_from_Poison:
                                ch.PoisonResistance++;
                                ch.PoisonProtection += effect.effectAmount;
                                break;
                            case EffectType.Protection_from_Blind_and_Fear:
                                ch.BlindResistance += effect.effectAmount;
                                ch.FearResistance += effect.effectAmount;
                                break;
                            case EffectType.Protection_from_Stun_and_Death:
                                ch.StunResistance += effect.effectAmount;
                                ch.DeathResistance++;
                                ch.DeathProtection += effect.effectAmount;
                                break;
                            #endregion
                            #region Increase Resist
                            case EffectType.Resist_Fear:
                                ch.FearResistance += effect.effectAmount;
                                break;
                            case EffectType.Resist_Blind:
                                ch.BlindResistance += effect.effectAmount;
                                break;
                            case EffectType.Resist_Stun:
                                ch.StunResistance += effect.effectAmount;
                                break;
                            case EffectType.Resist_Lightning:
                                ch.LightningResistance += effect.effectAmount;
                                break;
                            case EffectType.Resist_Death:
                                ch.DeathResistance += effect.effectAmount;
                                break;
                            case EffectType.Resist_Zonk:
                                ch.ZonkResistance++;
                                break;
                            #endregion
                            case EffectType.Shield:
                                ch.Shielding += effect.effectAmount;
                                break;
                            case EffectType.Knight_Ring:
                                if (ch.IsPC && item.attunedID == ch.PlayerID)
                                {
                                    ch.ManaMax = 3;
                                    ch.Mana = 0;
                                    ch.knightRing = true;
                                }
                                else if (!ch.IsPC)
                                {
                                    ch.ManaMax = 3;
                                    ch.Mana = 0;
                                    ch.knightRing = true;
                                }
                                break;
                            default:
                                //Utils.Log("Effect.AddWornEffectToCharacter does not add for the effect " + Utils.FormatEnumString(effect.effectType.ToString()), Utils.LogType.SystemWarning);
                                break;
                        }
                        ch.wornEffectList.Add(effect);
                    }

					if(item.charges > 0)
                    {
                        item.charges -= 1;
                    }
				}
				catch(Exception e)
				{
                    Utils.Log("Failure at Effect.AddWornEffectToCharacter(" + ch.GetLogString() + ", " + item.GetLogString() + ")", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
				}
			}
		}

		public static void RemoveWornEffectFromCharacter(Character ch, Item item)
		{
			if(item.effectType.Length > 0 && item.effectType != "0")
			{
				try
				{
					string[] itemEffectType = item.effectType.Split(" ".ToCharArray());
					string[] itemEffectAmount = item.effectAmount.Split(" ".ToCharArray());

					for(int a = 0; a < itemEffectType.Length; a++)
                    {
                        foreach (Effect effect in new List<Effect>(ch.wornEffectList))
                        {
                            if (effect.effectType == (EffectType)Convert.ToInt32(itemEffectType[a]) &&
                                effect.effectAmount == Convert.ToInt32(itemEffectAmount[a]))
                            {
                                ch.wornEffectList.Remove(effect);
                                //ch.WriteToDisplay("Removed: " + effect.effectType.ToString(), Protocol.TextType.PlayerChat);
                                break;
                            }
                        }

						switch((EffectType)Convert.ToInt32(itemEffectType[a]))
						{
                            case EffectType.Resist_Zonk:
                                ch.ZonkResistance -= Convert.ToInt32(itemEffectAmount[a]);
                                break;
                            case EffectType.Regenerate_Mana:
                                ch.manaRegen -= Convert.ToInt32(itemEffectAmount[a]);
                                break;
							case EffectType.Protection_from_Fire:
								ch.FireResistance--;
								ch.FireProtection -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Protection_from_Cold:
								ch.ColdResistance--;
								ch.ColdProtection -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.HitsMax:
								ch.HitsMax -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Hits:
								ch.Hits -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Stamina:
								ch.StaminaMax -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Strength:	
								ch.TempStrengthRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Dexterity:		
								ch.TempDexterityRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Intelligence:		
								ch.TempIntelligenceRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Wisdom:			
								ch.TempWisdomRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Constitution:		
								ch.TempConstitutionRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Temporary_Charisma:		
								ch.TempCharismaRaw -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Protection_from_Fire_and_Ice:
								ch.FireResistance--;
								ch.FireProtection -= Convert.ToInt32(itemEffectAmount[a]);
								ch.ColdResistance--;
								ch.ColdProtection -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Shield:
								ch.Shielding -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Protection_from_Poison:
								ch.PoisonResistance--;
								ch.PoisonProtection -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Protection_from_Blind_and_Fear:
								ch.BlindResistance -= Convert.ToInt32(itemEffectAmount[a]);
								ch.FearResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Protection_from_Stun_and_Death:
								ch.StunResistance -= Convert.ToInt32(itemEffectAmount[a]);
								ch.DeathResistance--;
								ch.DeathProtection -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Resist_Fear:
								ch.FearResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Resist_Blind:
								ch.BlindResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Resist_Stun:
								ch.StunResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Resist_Lightning:
								ch.LightningResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Resist_Death:
								ch.DeathResistance -= Convert.ToInt32(itemEffectAmount[a]);
								break;
							case EffectType.Knight_Ring:
								ch.ManaMax = 0;
								ch.Mana = 0;
								ch.knightRing = false;
								break;
							default:
                                //Utils.Log("Effect.RemoveWornEffectToCharacter does not remove for the effect " + Utils.FormatEnumString(((EffectType)Convert.ToInt32(itemEffectType[a])).ToString()), Utils.LogType.SystemWarning);
								break;
						}
					}
				}
				catch(Exception e)
				{
                    Utils.Log("Failure at Effect.RemoveWornEffectToCharacter(" + ch.GetLogString() + ", " + item.GetLogString() + ")", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
				}
			}
		}

        public static string GetEffectAdjective(Effect.EffectType effectType, int effectAmount)
        {
            string effectAdjective = "";

            ////adjective for protection
            //if(effectType >= 104 && effectType <= 114)
            //{
            //    if(effectAmount <= 10){effectAdjective = "a weak";}
            //    if(effectAmount > 10 && effectAmount <= 49){effectAdjective = "a strong";}
            //    if(effectAmount > 49 && effectAmount < 80){effectAdjective = "a powerful";}
            //    if(effectAmount >= 80){effectAdjective = "a very powerful";}
            //}
            ////adjective for stats
            //else if(effectType >= 60 && effectType < 104)
            //{
            //    if(effectAmount < 3){effectAdjective = "a weak";}
            //    if(effectAmount >= 3 && effectAmount < 6){effectAdjective = "a strong";}
            //    if(effectAmount >= 6 && effectAmount < 9){effectAdjective = "a powerful";}
            //    if(effectAmount >= 9){effectAdjective = "a very powerful";}
            //}
            //else{effectAdjective = "the";}
            return effectAdjective;
        }

        public static string GetEffectName(EffectType effectType) // returns the effect name, or an empty string if none
        {
            switch (effectType)
            {
                case EffectType.Fire:
                    return "Fire";
                case EffectType.Ice:
                    return "Ice";
                case EffectType.Darkness:
                    return "Darkness";
                case EffectType.MindControl:
                    return "Mind Control";
                case EffectType.Poison_Cloud:
                    return "Poison Cloud";
                case EffectType.Web:
                    return "Web";
                case EffectType.Light:
                    return "Light";
                case EffectType.Concussion:
                    return "Concussion";
                case EffectType.Find_Secret_Door:
                    return "Open Secret Door";
                case EffectType.Illusion:
                    return "Illusion";
                case EffectType.Dragon__s_Breath_Fire:
                    return "Dragon's Breath Fire";
                case EffectType.Dragon__s_Breath_Ice:
                    return "Dragon's Breath Ice";
                case EffectType.Turn_Undead:
                    return "Turn Undead";
                case EffectType.Whirlwind:
                    return "Whirlwind";
                case EffectType.Wizard_Eye:
                    return "Wizard Eye";
                case EffectType.Peek:
                    return "Peek";
                case EffectType.Breathe_Water:
                    return "Breathe Water";
                case EffectType.Feather_Fall:
                    return "Feather Fall";
                case EffectType.Night_Vision:
                    return "Night Vision";
                case EffectType.Blind:
                    return "Blind";
                case EffectType.HitsMax:
                    return "Added Max Health";
                case EffectType.Hits:
                    return "Added Hits";
                case EffectType.Stamina:
                    return "Added Stamina";
                case EffectType.Temporary_Strength:
                    return "Strength";
                case EffectType.Temporary_Dexterity:
                    return "Dexterity";
                case EffectType.Temporary_Intelligence:
                    return "Intelligence";
                case EffectType.Temporary_Wisdom:
                    return "Wisdom";
                case EffectType.Temporary_Constitution:
                    return "Constitution";
                case EffectType.Temporary_Charisma:
                    return "Charisma";
                case EffectType.Shield:
                    return "Shield";
                case EffectType.Resist_Blind:
                    return "Resist Blindness";
                case EffectType.Resist_Death:
                    return "Resist Death";
                case EffectType.Resist_Fear:
                    return "Resist Fear";
                case EffectType.Resist_Lightning:
                    return "Resist Lightning";
                case EffectType.Resist_Stun:
                    return "Resist Stun";
                case EffectType.Protection_from_Fire:
                    return "Protection from Fire";
                case EffectType.Protection_from_Cold:
                    return "Protection from Cold";
                case EffectType.Protection_from_Fire_and_Ice:
                    return "Protection from Fire and Ice";
                case EffectType.Protection_from_Poison:
                    return "Protection from Poison";
                case EffectType.Protection_from_Blind_and_Fear:
                    return "Protection from Blind and Fear";
                case EffectType.Protection_from_Stun_and_Death:
                    return "Protection from Stun and Death";
                case EffectType.Fear:
                    return "Fear";
                case EffectType.Poison:
                    return "Poison";
                case EffectType.Bless:
                    return "Blessing of the Faithful";
                case EffectType.Stamina_Restore:
                    return "Stamina Restoration";
                case EffectType.Nitro:
                    return "Nitroglycerin";
                case EffectType.Naphtha:
                    return "Naphtha";
                case EffectType.Mana_Restore:
                    return "Mana Restoration";
                case EffectType.Regenerate_Mana:
                    return "Power";
                case EffectType.Youth_Potion:
                    return "Youth";
                case EffectType.Permanent_Constitution:
                    return "Restore Constitution";
                case EffectType.Ale:
                    return "Ale";
                case EffectType.Beer:
                    return "Beer";
                case EffectType.Blindness_Cure:
                    return "Cure Blindness";
                case EffectType.Coffee:
                    return "Coffee";
                case EffectType.Exp:
                    return "Experience";
                case EffectType.Rum:
                    return "Rum";
                case EffectType.Whiskey:
                    return "Whiskey";
                case EffectType.Wine:
                    return "Wine";
                case EffectType.Water:
                    return "Water";
                //case EffectType.Balm:
                //    return "Healing";
                case EffectType.Drake_Potion:
                    return "Drake's Blood";
                case EffectType.OrcBalm:
                    return "Orc Urine";
                case EffectType.Permanent_Charisma:
                    return "Permanent Charisma";
                case EffectType.Permanent_Dexterity:
                    return "Permanent Dexterity";
                case EffectType.Permanent_Intelligence:
                    return "Permanent Intelligence";
                case EffectType.Permanent_Mana:
                    return "Permanent Mana";
                case EffectType.Permanent_Strength:
                    return "Permanent Strength";
                case EffectType.Permanent_Wisdom:
                    return "Permanent Wisdom";
                case EffectType.Resist_Zonk:
                    return "Resist Zonk";
                default:
                    return "Unknown";
            }
        }

        public static void DoAreaEffect(Cell cell, Effect effect)
        {
            int a;
            if (!DragonsSpineMain.ProcessEmptyWorld)
            {
                if (World.GetNumberPlayersInMap(cell.MapID) == 0)
                    return;
            }
            try
            {
                switch (effect.effectType)
                {
                    case EffectType.Dragon__s_Breath_Fire:
                    case EffectType.Fire:
                        for (a = 0; a < cell.Items.Count; a++)
                        {
                            Item flammableItem = cell.Items[a];

                            #region burn flammable items
                            try
                            {
                                Rules.SavingThrow itemSavingThrow = Rules.SavingThrow.Spell;
                                int savingThrowMod = 0;

                                if (flammableItem.effectType.Length > 0)
                                {
                                    savingThrowMod += flammableItem.effectType.Length;
                                }

                                if (cell.IsLair)
                                {
                                    savingThrowMod += 5;
                                }

                                if (effect.effectType == EffectType.Dragon__s_Breath_Fire)
                                {
                                    itemSavingThrow = Rules.SavingThrow.BreathWeapon;
                                    savingThrowMod -= 2;
                                }

                                if (flammableItem.flammable && (flammableItem.itemType == Globals.eItemType.Corpse || !Rules.DND_GetSavingThrow(null, itemSavingThrow, savingThrowMod)))
                                {
                                    if (flammableItem.itemType == Globals.eItemType.Corpse)
                                    {
                                        Item.dumpCorpse(flammableItem, cell);
                                        Character target = (Character)PC.GetOnline(flammableItem.special);
                                        if (target != null)
                                        {
                                            if (target.currentKarma > 0)
                                            {
                                                Rules.EnterUnderworld(target);
                                            }
                                            else if (target.Alignment == Globals.eAlignment.Evil)
                                            {
                                                if (Rules.RollD(1, 20) < target.Constitution)
                                                {
                                                    Rules.DeadRest(target);
                                                }
                                                else
                                                {
                                                    Rules.EnterUnderworld(target);
                                                }
                                            }
                                            else
                                            {
                                                Rules.DeadRest(target);
                                            }
                                        }
                                    } // end if corpse itemType
                                    cell.Remove(flammableItem);
                                } // end if flammable
                            }
                            catch (Exception e)
                            {
                                Utils.Log("Failure at DoFireEffect (flammable item) EffectType: " + effect.effectType.ToString(), Utils.LogType.SystemFailure);
                                Utils.LogException(e);
                            }
                            #endregion
                        }
                        break;
                    case EffectType.Whirlwind:
                        #region Whirlwind
                        //Utils.Log("Whirlwind: " + cell.X + "," + cell.Y + "," + cell.Z, Utils.LogType.Unknown);
                        List<Cell> celllist = Map.GetAdjacentCells(cell);
                        if (celllist != null)
                        {
                            Cell newcell = (Cell)celllist[Rules.dice.Next(celllist.Count)];
                            Effect newwhirl = new Effect(EffectType.Whirlwind, "&&", effect.effectAmount, effect.duration, effect.caster, newcell);
                            newcell.SendShout("a raging whirlwind.");
                            effect.duration = 1;
                        }
                        break;
                        #endregion
                    
                    case EffectType.Fire_Storm:
                        List<Cell> fireCells = Map.GetAdjacentCells(cell);
                        //int frand = Rules.dice.Next(fireCells.Count);
                        Cell newFireCell = (Cell)fireCells[Rules.dice.Next(fireCells.Count)];
                        Effect newFireball = new Effect(EffectType.Fire_Storm, "**", effect.effectAmount, effect.duration, effect.caster, newFireCell);
                        Spell.CastGenericAreaSpell(newFireCell, "", EffectType.Fire, effect.effectAmount, "fireball", effect.caster);
                        effect.duration = 1;
                        break;
                    default:
                        break;
                }

                ArrayList effectedCharacter = new ArrayList();

                for (a = 0; a < cell.Characters.Count; a++)
                {
                    Character target = cell.Characters[a];

                    if (!target.IsInvisible && !target.IsImmortal && !target.IsDead)
                    {
                        if (effect.effectType == EffectType.Turn_Undead)
                        {
                            if (target.IsUndead)
                            {
                                effectedCharacter.Add(target);
                            }
                        }
                        else
                        {
                            effectedCharacter.Add(target);
                        }
                    }
                }
                //
                if (effectedCharacter.Count > 0)
                {
                    foreach (Character eft in effectedCharacter)
                    {
                        if (effect.effectType == EffectType.Black_Fog)
                        {
                            int chance = Rules.dice.Next(1, 100);
                            
                            foreach(Character target in new List<Character>(cell.Characters))
                            {
                                if (Rules.RollD(1, 100) < 25) // 25% chance to move the character
                                {
                                    //locate a fog cell to put character in
                                    if (!target.IsDead && target.IsPC)
                                    {
                                        target.WriteToDisplay("You are enveloped in a black fog.");
                                        List<Cell> EcellList = Map.getAdjacentCells(cell, target, 6, effect.effectType);
                                        if (EcellList != null)
                                        {
                                            Cell newCell = EcellList[Rules.dice.Next(1, EcellList.Count)];
                                            target.CurrentCell = newCell;
                                        }
                                    }
                                }
                            }
                            continue;
                        }
                        if (Rules.DoSpellDamage(effect.caster, eft, null, effect.effectAmount, Utils.FormatEnumString(effect.effectType.ToString()).ToLower()) == 1)
                        {
                            if (effect.caster != null)
                            {
                                Rules.GiveAEKillExp(effect.caster, eft);
                                Skills.GiveSkillExp(effect.caster, eft, Globals.eSkillType.Magic);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        static void DoBalmEffect(Character target, Effect effect)
        {
            try
            {
                int balmAmount = (int)(effect.effectAmount / 2);
                effect.effectAmount -= balmAmount;
                target.Hits += balmAmount;
                if (target.Hits >= target.HitsFull || effect.duration <= 0 || effect.effectAmount <= 0)
                {
                    target.Hits = target.HitsFull;
                    effect.StopCharacterEffect();
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }
    }
}
