using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.GameObjects
{
    /// <summary>
    /// This class represents all non player character living objects in the game.
    /// Originally written by Michael Cohen (Ebony) on 29 August 2008.
    /// </summary>
    public class GameNPC : GameLiving
    {
        protected int m_npcID;

        protected int m_worldNPCID;

        protected bool m_canCommand;

        public GameNPC()
            : base()
        {
        }
    }
}
