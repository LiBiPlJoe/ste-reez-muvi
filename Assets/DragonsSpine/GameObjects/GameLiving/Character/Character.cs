using System;
using System.Timers;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.Data.SqlTypes;

namespace DragonsSpine
{
    public class Character
    {
        /// <summary>
        /// The professions enum
        /// The order of this enum cannot be changed
        /// </summary>
        public enum ClassType
        {
            None,
            Fighter,
            Thaumaturge,
            Wizard,
            Martial_Artist,
            Thief,
            Knight
        }

        #region Static Lists
        public static List<Character> loginList = new List<Character>();
        public static List<Character> charGenList = new List<Character>(); // list of all PCs at the character generator
        public static List<Character> menuList = new List<Character>(); // list of all PCs at the menu
        public static List<Character> confList = new List<Character>(); // list of all PCs in the conference rooms
        public static List<Character> allCharList = new List<Character>(); // list of all PCs and NPCs in the World
        public static List<Character> pcList = new List<Character>(); // list of all PCs in the World
        public static List<Character> NPCList = new List<Character>(); // list of all NPCs in the World 
        #endregion

        #region Constants
        protected const short DISPLAY_BUFFER_SIZE = 10;
        protected const double DEFAULT_COMMAND_INTERVAL = 5000;
        protected const int MAX_INPUT_LENGTH = 1024; //the longest line of input a person is allowed to enter

        public const short INACTIVITY_TIMEOUT = 25;
        public const short NAME_MIN_LENGTH = 4;
        public const short NAME_MAX_LENGTH = 14;
        public const short MAX_IGNORE = 30;
        public const short MAX_FRIENDS = 30;
        public const short MAX_UNARMED_WEIGHT = 20; // max weight allowed to receive unarmed combat benefits
        public const short MAX_MARKS = 5; // max marks allowed per account
        public const short MAX_CHARS = 8; // max chars allowed per account
        public const short MAX_BELT = 5; // max belt items allowed
        public const short MAX_LOCKER = 20; // max items allowed in locker
        public const short MAX_SACK = 20; // max items allowed in sack
        public const short MAX_RINGS = 8; // max rings worn
        public const short MAX_MACROS = 20;

        public const int UDP_OUTPUT_BUFFER = 1024;
        #endregion

        Timer roundTimer;
        Timer thirdRoundTimer;

        string[] displayBuffer = new string[DISPLAY_BUFFER_SIZE];
        string displayText = "";
        public int debug = 0; // this a tracker for command limits

        #region Private Data
        Globals.eAlignment alignment = Globals.eAlignment.None;
        bool ancestor = false; // is this character an ancestor (disables Underworld quests)
        int ancestorID = 0; // the player ID of this character's ancestor
        int attackRoll = 0; // record the character's most recent attack roll
        Cell cell = null;
        Command.CommandType commandType = Command.CommandType.None; // current command type
        bool dead = false;
        short facet = 0;
        Command.CommandType firstJoinedCommandType = Command.CommandType.None;
        string followName = ""; // used for "is looking at you", AI targetting, and follow mode of PCs ONLY
        Group group = null;
        int groupInviter = -1; // the group leader ID of the group this character has been invited into
        string hostName = ""; // resolved host name from IPAddress - not stored in the database
        string ipAddress = "0.0.0.0"; // IP address this character is connected from
        string email = "";
        bool isPC = true;
        short landID = 0; // current Land.landID
        string lastCommand;
        string name;
        short mapID = 0; // current Map.mapID
        string notes = "";
        string password; // account password
        Globals.ePlayerState pcState; // player characters only
        Character peekTarget;
        int playerID; // assigned by the database
        //int poisoned = 0;
        int shielding = 0; // magical shielding
        bool summoned = false; // true if the character is a summoned being
        string targetName = "";
        short timeout = INACTIVITY_TIMEOUT;
        bool undead = false; // usage: no corpse, turnundead/light spell damage, does not flee in combat
        short xcord = 0;
        short ycord = 0;
        int zcord = 0;
        int preppedRound = 0;
        List<Character> petList = new List<Character>();
        Character petOwner = null;
        double m_sackGold = 0;
        #endregion

        #region Public Properties
        #region Location Properties

        public Cell CurrentCell
        {
            get
            {
                return this.cell;
            }
            set
            {

                if (this.cell != null)
                {
                    this.cell.Remove(this);
                }

                this.cell = value;

                if (this.cell != null)
                {
                    this.FacetID = cell.FacetID;
                    this.LandID = cell.LandID;
                    this.MapID = cell.MapID;
                    this.X = cell.X;
                    this.Y = cell.Y;
                    this.Z = cell.Z;
                }

                if (this.cell != null && (!this.IsPC || this.PCState == Globals.ePlayerState.PLAYING))
                {
                    this.cell.Add(this);
                }
            }
        }

        public Facet Facet
        {
            get { return World.GetFacetByID(FacetID); }
        }

        public short FacetID
        {
            get { return this.facet; }
            set { this.facet = value; }
        }

        public short LandID
        {
            get { return this.landID; }
            set { this.landID = value; }
        }

        public short MapID
        {
            get { return this.mapID; }
            set { this.mapID = value; }
        }

        public short X
        {
            get { return this.xcord; }
            set { this.xcord = value; }
        }

        public short Y
        {
            get { return this.ycord; }
            set { this.ycord = value; }
        }

        public int Z
        {
            get { return this.zcord; }
            set { this.zcord = value; }
        }

        public Map Map
        {
            get
            {
                return World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID);
            }
        }

        public Land Land
        {
            get
            {
                return World.GetFacetByID(this.FacetID).GetLandByID(this.LandID);
            }
        }
          #endregion

        public object lockObject = new object();
        public Globals.eAlignment Alignment
        {
            get { return this.alignment; }
            set { this.alignment = value; }
        }
        public int AncestorID
        {
            get { return this.ancestorID; }
            set { this.ancestorID = value; }
        }
        public int AttackRoll
        {
            get { return this.attackRoll; }
            set { this.attackRoll = value; }
        }
        public Command.CommandType CommandType
        {
            get { return this.commandType; }
            set { this.commandType = value; }
        }
        public Command.CommandType FirstJoinedCommand
        {
            get { return this.firstJoinedCommandType; }
            set { this.firstJoinedCommandType = value; }
        }
        public string FollowName
        {
            get { return this.followName; }
            set { this.followName = value; }
        }
        public Group Group
        {
            get { return this.group; }
            set { this.group = value; }
        }
        public int GroupInviter
        {
            get { return this.groupInviter; }
            set { this.groupInviter = value; }
        }
        public string HostName
        {
            get { return this.hostName; }
            set { this.hostName = value; }
        }
        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }
        public bool InUnderworld
        {
            get
            {
                if (this.Map.Name == "Praetoseba")
                {
                    return true;
                }
                return false;
            }
        }
        public string IPAddress
        {
            get { return this.ipAddress; }
            set { this.ipAddress = value; }
        }
        public bool IsAncestor
        {
            get { return this.ancestor; }
            set { this.ancestor = value; }
        }
        public bool IsBeltLargeSlotAvailable
        {
            get
            {
                if (this.beltList.Count >= Character.MAX_BELT) { return false; }
                for (int a = 0; a < this.beltList.Count; a++)
                {
                    Item item = (Item)this.beltList[a];
                    if (item.size == Globals.eItemSize.Belt_Large_Slot_Only)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        public bool IsDead
        {
            get { return this.dead; }
            set { this.dead = value; }
        }
        public bool IsHybrid
        {
            get
            {
                if (Array.IndexOf(World.hybrids, this.BaseProfession) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsIntelligenceCaster
        {
            get
            {
                if (Array.IndexOf(World.intelligenceCasters, this.BaseProfession) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsLucky
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Dog_Luck))
                {
                    return true;
                }

                foreach (Item ring in this.GetRings())
                {
                    if (ring.special.IndexOf("lucky") != -1)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        public bool IsPC
        {
            get { return this.isPC; }
            set { this.isPC = value; }
        }
        public int PreppedRound
        {
            get { return preppedRound; }
            set { preppedRound = value; }
        }
        public bool IsPureMelee
        {
            get
            {
                if (Array.IndexOf(World.pureMelee, this.BaseProfession) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsSpellUser
        {
            get
            {
                if (Array.IndexOf(World.spellUsers, this.BaseProfession) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsSummoned
        {
            get { return this.summoned; }
            set { this.summoned = value; }
        }
        public bool IsUndead
        {
            get { return this.undead; }
            set { this.undead = value; }
        }
        public bool IsWisdomCaster
        {
            get
            {
                if (Array.IndexOf(World.wisdomCasters, this.BaseProfession) != -1)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsWyrmKin
        {
            get
            {
                if (Array.IndexOf(World.wyrmkin, this.species) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsHardHitter
        {
            get
            {
                if (Array.IndexOf(World.hardhitter, this.species) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsMagicSniffer
        {
            get
            {
                if (Array.IndexOf(World.magicsniffer, this.species) != -1)
                {
                    return true;
                }
                return false;
            }
        }
        public string LastCommand
        {
            get { return this.lastCommand; }
            set { this.lastCommand = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Notes
        {
            get { return this.notes; }
            set { this.notes = value; }
        }
        public string Password
        {
            get { return this.password; }
            set { this.password = value; }
        }
        public Globals.ePlayerState PCState
        {
            get { return this.pcState; }
            set { this.pcState = value; }
        }
        public Character PeekTarget
        {
            get { return this.peekTarget; }
            set { this.peekTarget = value; }
        }
        public Character PetOwner
        {
            get { return this.petOwner; }
            set { this.petOwner = value; }
        }
        public List<Character> Pets
        {
            get { return this.petList; }
        }
        public int PlayerID
        {
            get { return this.playerID; }
            set { this.playerID = value; }
        }
        public int Poisoned
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Poison))
                {
                    return this.effectList[Effect.EffectType.Poison].effectAmount;
                }
                return 0;

            }
            set
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Poison))
                {
                    if (value > 0)
                    {
                        this.effectList[Effect.EffectType.Poison].effectAmount = value;
                    }
                    else
                    {
                        this.effectList[Effect.EffectType.Poison].StopCharacterEffect();
                    }
                }
            }
        }
        public int Shielding
        {
            get { return this.shielding; }
            set { this.shielding = value; }
        }
        public string TargetName
        {
            get { return this.targetName; }
            set { this.targetName = value; }
        }
        public short Timeout
        {
            get { return this.timeout; }
            set { this.timeout = value; }
        }
        public double SackGold
        {
            get { return m_sackGold; }
            set { m_sackGold = value; }
        }
        public int SackCountMinusGold
        {
            get
            {
                int count = 0;
                foreach (Item itm in this.sackList)
                {
                    if (itm.name != "coins")
                    {
                        count++;
                    }
                }
                return count;
            }
        }
        #endregion

        #region Effect Properties
        public bool CanBreatheWater
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Breathe_Water))
                {
                    return true;
                }
                foreach (Effect effect in new List<Effect>(this.wornEffectList))
                {
                    if (effect.effectType == Effect.EffectType.Breathe_Water)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        public bool CanFly
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Flight))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Flight)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool IsHidden
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Hide_In_Shadows))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Hide_In_Shadows)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            set
            {
                if (value == false)
                {
                    if (this.effectList.ContainsKey(Effect.EffectType.Hide_In_Shadows))
                    {
                        if (!this.effectList[Effect.EffectType.Hide_In_Shadows].IsPermanent)
                        {
                            this.effectList[Effect.EffectType.Hide_In_Shadows].StopCharacterEffect();

                            if (this.IsPC && this.PCState == Globals.ePlayerState.PLAYING)
                            {
                                this.WriteToDisplay("You are no longer hidden!");
                            }
                        }
                    }
                    
                }
            }
        }
        public bool IsFeared
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Fear))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Fear)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool HasFeatherFall
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Feather_Fall))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Feather_Fall)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool IsBlind
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Blind))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Blind)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool IsPeeking
        {
            get
            {
                return this.effectList.ContainsKey(Effect.EffectType.Peek);
            }
        }
        public bool IsWizardEye
        {
            get
            {
                return this.effectList.ContainsKey(Effect.EffectType.Wizard_Eye);
            }
        }
        public bool HasNightVision
        {
            get
            {
                if (this.effectList.ContainsKey(Effect.EffectType.Night_Vision))
                {
                    return true;
                }
                else
                {
                    foreach (Effect effect in this.wornEffectList)
                    {
                        if (effect.effectType == Effect.EffectType.Night_Vision)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool knightRing = false;
        #endregion

        public Dictionary<Effect.EffectType, Effect> effectList = new Dictionary<Effect.EffectType, Effect>(); // temporary effects
        public List<Effect> wornEffectList = new List<Effect>(); // effects worn

        /* questList
         * For players this is their list of completed and current quests.
         * For NPCs this is their list of quests they give or are a part of.
         */

        public List<Quest> questList = new List<Quest>();
        public List<string> questFlags = new List<string>();
        public List<string> contentFlags = new List<string>();

        #region Protection
        protected int fireProtection = 0;
        public int FireProtection
        {
            get { return this.fireProtection; }
            set { this.fireProtection = value; }
        }
        protected int coldProtection = 0;
        public int ColdProtection
        {
            get { return this.coldProtection; }
            set { this.coldProtection = value; }
        }
        protected int lightningProtection = 0;
        public int LightningProtection
        {
            get { return this.lightningProtection; }
            set { this.lightningProtection = value; }
        }
        protected int deathProtection = 0;
        public int DeathProtection
        {
            get { return this.deathProtection; }
            set { this.deathProtection = value; }
        }
        protected int poisonProtection = 0;
        public int PoisonProtection
        {
            get { return this.poisonProtection; }
            set { this.poisonProtection = value; }
        }
        #endregion

        #region Resistance
        protected int fireResistance = 0;
        public int FireResistance
        {
            get { return this.fireResistance; }
            set { this.fireResistance = value; }
        }
        protected int coldResistance = 0;
        public int ColdResistance
        {
            get { return this.coldResistance; }
            set { this.coldResistance = value; }
        }
        private int lightningResistance = 0;
        public int LightningResistance
        {
            get { return this.lightningResistance; }
            set { this.lightningResistance = value; }
        }
        protected int deathResistance = 0;
        public int DeathResistance
        {
            get { return this.deathResistance; }
            set { this.deathResistance = value; }
        }
        protected int blindResistance = 0;
        public int BlindResistance
        {
            get { return this.blindResistance; }
            set { this.blindResistance = value; }
        }
        protected int fearResistance = 0;
        public int FearResistance
        {
            get { return this.fearResistance; }
            set { this.fearResistance = value; }
        }
        private int stunResistance = 0;
        public int StunResistance
        {
            get { return this.stunResistance; }
            set { this.stunResistance = value; }
        }
        protected int poisonResistance = 0;
        public int PoisonResistance
        {
            get { return this.poisonResistance; }
            set { this.poisonResistance = value; }
        }
        protected int zonkResistance = 0;
        public int ZonkResistance
        {
            get { return this.zonkResistance; }
            set { this.zonkResistance = value; }
        }
        #endregion
        
        #region Settings
        public bool displayCombatDamage = false;
        public int[] ignoreList = new int[MAX_IGNORE]; // array of player IDs that are ignored
        public string ignored = "";
        public bool filterProfanity = true; // profanity filter, default true
        public int[] friendsList = new int[MAX_FRIENDS]; // array of player IDs that are friends
        public string friends = "";
        public bool friendNotify = true; // display notification when friend logs on and off
        public bool receivePages = true;
        public bool receiveTells = true;
        public bool receiveGroupInvites = true;
        public bool afk = false; // sets AFK tag and AFK message when sent a private message or paged
        public string afkMessage = "I am currently A.F.K. (Away From Keyboard).";
        Globals.eImpLevel impLevel = Globals.eImpLevel.USER; // implementor level
        public string colorChoice = "brown";

        public Globals.eImpLevel ImpLevel
        {
            get { return this.impLevel; }
            set { this.impLevel = value; }
        }
        bool invisible = false;	// can't be seen or contacted
        public bool IsInvisible
        {
            get { return this.invisible; }
            set { this.invisible = value; }
        }
        bool wasInvisible = false;
        public bool WasInvisible // record invisible boolean for Peek spell
        {
            get { return this.wasInvisible; }
            set { this.wasInvisible = value; }
        }
        public bool showStaffTitle = true; // show implevel title in conference rooms and game [GM] [DEV]
        bool immortal = false; // when true heals impLevel >= GM to full stats each round
        public bool IsImmortal
        {
            get { return this.immortal; }
            set { this.immortal = value; }
        }
        bool wasImmortal = false;
        public bool WasImmortal
        {
            get { return this.wasImmortal; }
            set { this.wasImmortal = value; }
        }
        bool anonymous = false; // if player is anonymous will not be seen on WHO or in scores list
        public bool IsAnonymous
        {
            get { return this.anonymous; }
            set { this.anonymous = value; }
        }
        public bool echo = true;
        public ArrayList macros = new ArrayList();
        #endregion        
        
        public int npcID; // this is only for NPC's
        public int worldNpcID;
        public bool animal = false;	// true if this uses attackString and blockString in combat
        public int[] tanningResult;	// list of itemIDs this creature's corpse will produce if tanned
        public bool canCommand = false; // true if the character can be commanded
        public short poisonous = 0; // greater than 0 is a chance to poison and poison amount max
        public string special = ""; // used to handle special qualities of a creature (eg: blueglow)
        public Character MyClone; // Used for Wizard Eye and Peek spell
        public string account = ""; // account name
        public int accountID; // account identification number
        public string dirPointer = ".";
        public bool corpseIsCarried = false; // player's corpse is being carried
        short stunned = 0; // rounds of stun left
        public short Stunned
        {
            get { return this.stunned; }
            set
            {
                if (!this.IsPC || this.PCState == Globals.ePlayerState.PLAYING)
                {
                    if (value > this.stunned)
                    {
                        if (this.preppedSpell != null)
                        {
                            this.preppedSpell = null;
                            this.WriteToDisplay("You have lost your warmed spell.");
                            this.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                        }
                        if (!this.isPC)
                        {
                            if (this.Group != null)
                            {
                                this.Group.Remove((NPC)this);
                            }
                        }
                    }
                }

                this.stunned = value;

            }
        }

        public short floating = 3; // 2 more rounds until drowning
        public Spell preppedSpell;
        public string visualKey = "";
        public Globals.eGender gender = Globals.eGender.It;
        public string race = ""; // character's race
        public Globals.eSpecies species = Globals.eSpecies.Unknown;
        public string classFullName = "Fighter"; // full character class name

        ClassType baseProfession = ClassType.Fighter;

        public ClassType BaseProfession
        {
            get { return this.baseProfession; }
            set
            {
                this.baseProfession = value;
                Character.SetCharacterVisualKey(this); // images change if base profession changes
            }
        }


        #region Protocol and Client Specific
        public string protocol = "normal";
        public bool usingClient = false; // using the official Dragon's Spine client
        public bool sentWorldInformation = false; // sent world information
        public bool sentImplementorInformation = false; // sent implementor information
        #endregion

        #region Old Kesmai Protocol Update Bools
        public bool updateAll = true;
        public bool updateHits = true;
        public bool updateExp = true;
        public bool updateStam = true;
        public bool updateMP = true;
        public bool updateMap = true;
        public bool updateRight = true;
        public bool updateLeft = true;
        #endregion

        public IntStringMap spellList = new IntStringMap(); // the characters available spells
        
        public Merchant.TrainerType trainerType = Merchant.TrainerType.None;
        public Merchant.MerchantType merchantType = Merchant.MerchantType.None; //what itemType or baseType items this merchant will add to it's inventory
        public Merchant.InteractiveType interactiveType = Merchant.InteractiveType.None;
        public double baseArmorClass = 10.0; // relates to AD&D Armor Class
        public double encumbrance = 0;

        #region Immunities
        public bool immuneFire = false;			// true if immune to fire based attacks
        public bool immuneCold = false;			// true if immune to cold based attacks
        public bool immunePoison = false;		// true if immune to poison based attacks
        public bool immuneLightning = false;	// true if immune to lightning based attacks
        public bool immuneCurse = false;		// true if immune to curse magic (undead are already cursed)
        public bool immuneDeath = false;		// true if immune to death magic
        public bool immuneStun = false;			// true if immune to stun magic
        public bool immuneFear = false;			// true if immune to fear magic
        public bool immuneBlind = false;		// true if immune to blindness magic 
        #endregion

        #region Class Specific
        public Globals.eSkillType fighterSpecialization = Globals.eSkillType.None; // fighter specialization - increased skill gain, chance to hit, chance to block
        public long lastBashRound = 0; // fighter's last bash use round
        #endregion

        #region Regeneration
        public int hitsRegen = 0;
        public int staminaRegen = 0;
        public int manaRegen = 0; 
        #endregion

        #region Statistics
        short level;
        public short Level
        {
            get { return this.level; }
            set { this.level = value; }
        }
        int hits;
        public int Hits
        {
            get { return this.hits; }
            set { this.hits = value; }
        }
        int hitsmax;
        public int HitsMax
        {
            get { return this.hitsmax; }
            set { this.hitsmax = value; }
        }
        int hitsAdjustment = 0;
        public int HitsAdjustment
        {
            get { return this.hitsAdjustment; }
            set { this.hitsAdjustment = value; }
        }
        int hitsDoctored = 0;
        public int HitsDoctored
        {
            get { return this.hitsDoctored; }
            set { this.hitsDoctored = value; }
        }
        public int HitsFull
        {
            get { return hitsmax + hitsAdjustment + hitsDoctored; }
			set { this.hitsmax = value; this.hitsAdjustment = 0; this.hitsDoctored = 0; }
        }
        int mana;
        public int Mana
        {
            get { return this.mana; }
            set { this.mana = value; }
        }
        int manaMax;
        public int ManaMax
        {
            get { return this.manaMax; }
            set { this.manaMax = value; }
        }
        int manaAdjustment = 0;
        public int ManaAdjustment
        {
            get { return this.manaAdjustment; }
            set { this.manaAdjustment = value; }
        }
        public int ManaFull
        {
            get { return this.manaMax + this.manaAdjustment; }
        }
        long exp;
        public long Experience
        {
            get { return this.exp; }
            set
            {
                if (!this.IsPC)
                {
                    this.exp = value;
                }
                else
                {
                    int currentLevel = Rules.GetExpLevel(this.exp);

                    this.exp = value;

                    if (this.PCState == Globals.ePlayerState.PLAYING)
                    {
                        if (Rules.GetExpLevel(this.exp) > currentLevel)
                        {
                            if (this.Hits < this.HitsFull || this.Stamina < this.StaminaFull)
                            {
                                this.WriteToDisplay("You have earned enough experience for your next level! Type REST when you are at full health and stamina to advance.");
                            }
                            else
                            {
                                this.WriteToDisplay("You have earned enough experience for your next level! Type REST to advance.");
                            }
                        }
                    }
                }
            }
        }
        int staminaMax;
        public int StaminaMax
        {
            get { return this.staminaMax; }
            set { this.staminaMax = value; }
        }
        int stamina;
        public int Stamina
        {
            get { return this.stamina; }
            set { this.stamina = value; }
        }
        int staminaAdjustment = 0;
        public int StaminaAdjustment
        {
            get { return this.staminaAdjustment; }
            set
            {
                this.staminaAdjustment = value;
            }
        }
        public int StaminaFull
        {
            get { return staminaMax + staminaAdjustment; }
        }
        int numKills;
        public int Kills
        {
            get { return this.numKills; }
            set { this.numKills = value; }
        }
        int numDeaths;
        public int Deaths
        {
            get { return this.numDeaths; }
            set { this.numDeaths = value; }
        }
        int age;
        public int Age
        {
            get { return this.age; }
            set { this.age = value; }
        }
        long roundsPlayed;
        public long RoundsPlayed
        {
            get { return this.roundsPlayed; }
            set { this.roundsPlayed = value; }
        } 
        #endregion

        private int thac0Adjustment = 0;
        public int THAC0Adjustment
        {
            get { return this.thac0Adjustment; }
            set { this.thac0Adjustment = value; }
        }

        // underworld specific
        public int UW_hitsMax = 0;
        public int UW_hitsAdjustment = 0;
        public int UW_staminaMax = 0;
        public int UW_staminaAdjustment = 0;
        public int UW_manaMax = 0;
        public int UW_manaAdjustment = 0;
        public bool UW_hasLiver = false;
        public bool UW_hasLungs = false;
        public bool UW_hasIntestines = false;
        public bool UW_hasStomach = false;
        public ArrayList UWQuests = new ArrayList();

        #region Player vs. Player
        public int currentKarma = 0; // current karma amount
        public long lifetimeKarma = 0; // lifetime karma amount
        public int currentMarks = 0; // account current marks amount		
        public int lifetimeMarks = 0; // character lifetime marks amount (this is different than account lifetime marks)
        public long pvpNumKills = 0; // PvP kills amount
        public long pvpNumDeaths = 0; // PvP deaths amount

        List<int> playersFlagged = new List<int>(); // array of player IDs this character has flagged for self defense
        List<int> playersKilled = new List<int>(); // array of player IDs that were killed by this character without self defense flag
        #endregion

        public List<int> PlayersFlagged
        {
            get { return this.playersFlagged; }
        }

        public List<int> PlayersKilled
        {
            get { return this.playersKilled; }
        }

        #region Attributes
        int strength;
        public int Strength
        {
            get { return this.strength; }
            set { this.strength = value; }
        }
        int dexterity;
        public int Dexterity
        {
            get { return this.dexterity; }
            set { this.dexterity = value; }
        }
        int intelligence;
        public int Intelligence
        {
            get { return this.intelligence; }
            set { this.intelligence = value; }
        }
        int wisdom;
        public int Wisdom
        {
            get { return this.wisdom; }
            set { this.wisdom = value; }
        }
        int constitution;
        public int Constitution
        {
            get { return this.constitution; }
            set { this.constitution = value; }
        }
        int charisma;
        public int Charisma
        {
            get { return this.charisma; }
            set { this.charisma = value; }
        }

        int tempStrength = 0; // temporary strength
        public int TempStrength
        {
            get 
            {
                if (this.tempStrength > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempStrength;
                }
            }
        }
        public int TempStrengthRaw
        {
            get { return this.tempStrength; }
            set { this.tempStrength = value; }
        }
        int tempDexterity = 0; // temporary dexterity
        public int TempDexterity
        {
            get
            {
                if (this.tempDexterity > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempDexterity;
                }
            }
        }
        public int TempDexterityRaw
        {
            get { return this.tempDexterity; }
            set { this.tempDexterity = value; }
        }
        int tempIntelligence = 0; // temporary intelligence
        public int TempIntelligence
        {
            get
            {
                if (this.tempIntelligence > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempIntelligence;
                }
            }
        }
        public int TempIntelligenceRaw
        {
            get { return this.tempIntelligence; }
            set { this.tempIntelligence = value; }
        }
        int tempWisdom = 0; // temporary wisdom
        public int TempWisdom
        {
            get
            {
                if (this.tempWisdom > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempWisdom;
                }
            }
        }
        public int TempWisdomRaw
        {
            get { return this.tempWisdom; }
            set { this.tempWisdom = value; }
        }
        int tempConstitution = 0; // temporary constitution
        public int TempConstitution
        {
            get
            {
                if (this.tempConstitution > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempConstitution;
                }
            }
        }
        public int TempConstitutionRaw
        {
            get { return this.tempConstitution; }
            set { this.tempConstitution = value; }
        }
        int tempCharisma = 0; // temporary charisma
        public int TempCharisma
        {
            get
            {
                if (this.tempCharisma > this.Land.MaxTempAbilityScore)
                {
                    return this.Land.MaxTempAbilityScore;
                }
                else
                {
                    return this.tempCharisma;
                }
            }
        }
        public int TempCharismaRaw
        {
            get { return this.tempCharisma; }
            set { this.tempCharisma = value; }
        }

        public int strengthAdd;
        public int dexterityAdd; 
        #endregion

        public int numAttackers = 0; // used to determine skill risk

        #region Skills
        public long mace; // current skills
        public long bow;
        public long flail;
        public long dagger;
        public long rapier;
        public long twoHanded;
        public long staff;
        public long shuriken;
        public long sword;
        public long threestaff;
        public long halberd;
        public long unarmed;
        public long thievery;
        public long magic;

        public long bash;

        public long highMace; // highest skill level achieved
        public long highBow;
        public long highFlail;
        public long highDagger;
        public long highRapier;
        public long highTwoHanded;
        public long highStaff;
        public long highShuriken;
        public long highSword;
        public long highThreestaff;
        public long highHalberd;
        public long highUnarmed;
        public long highThievery;
        public long highMagic;

        public long highBash;

        public long trainedMace = 0; // current training amount
        public long trainedBow = 0;
        public long trainedFlail = 0;
        public long trainedDagger = 0;
        public long trainedRapier = 0;
        public long trainedTwoHanded = 0;
        public long trainedStaff = 0;
        public long trainedShuriken = 0;
        public long trainedSword = 0;
        public long trainedThreestaff = 0;
        public long trainedHalberd = 0;
        public long trainedUnarmed = 0;
        public long trainedThievery = 0;
        public long trainedMagic = 0;

        public long trainedBash = 0;
        #endregion

        public int cmdWeight = 0;
        public int damageRound = -3;

        bool newPC = false;
        public bool IsNewPC
        {
            get { return this.newPC; }
            set { this.newPC = value; }
        }
        public int confRoom = 0; // ChatRoom.rooms[]

        //MONEY
        public double bankGold;

        //USED FOR NPCS
        public string shortDesc; // short description of a NPC ( use LOOK AT command to see)
        public string longDesc; // long description of a NPC

        public List<Character> seenList;
        public List<Item> wearing; // worn items
        public List<Item> sackList; // sack for storage
        public List<Item> beltList; // belt for storage
        public List<Item> lockerList; // locker for storage
        
        public Item RightHand; // item in right hand
        public Item LeftHand; // item in left hand

        public Item RightRing1; // breaking rings down to each finger
        public Item RightRing2;
        public Item RightRing3;
        public Item RightRing4;
        public Item LeftRing1;
        public Item LeftRing2;
        public Item LeftRing3;
        public Item LeftRing4;

        public DateTime birthday; // time of player creation
        public DateTime lastOnline; // time of last login

        public static string[] possessive ={ "It's", "His", "Her" };
        public static string[] pronoun ={ "It", "He", "She" };

        public string attackSound;
        public string deathSound;
        public string idleSound;

        #region Socket
        private Socket socket; //the socket

        public void setSocket(Socket newSocket) 
        { 
            this.socket = newSocket;
            this.socket.LingerState = new LingerOption(true, 10);
            this.socket.Ttl = 42;
            this.socket.ReceiveTimeout = 1500;
            this.socket.SendTimeout = 1500;
        }

        public Socket getSocket() { return this.socket; }

        public int socketAvailable()
        {
            if (socket == null || !socket.Connected)
            {
                return 0;
            }
            return this.socket.Available;
        }

        public int socketReceive(byte[] buffer, int available, int loc)
        {
            return socket.Receive(buffer, available, 0);
        }

        public bool socketConnected()
        {
            return socket.Connected;
        }

        public int socketSend(byte[] b)
        {
            try
            {
                if (this.socketConnected())
                {
                    return socket.Send(b);
                }
                else
                {
                    return -1;
                }

            }
            catch (Exception)
            {
                return -1;
            }
        } 
        #endregion

        // since data is received from the player one char at a time, we store the chars in inputBuffer until
        // we receive a linefeed, at which case we know they are done with their command. At that point,
        // we copy the chars in inputBuffer into a String, which is stored in the inputCommandQueue until
        // it is time to process commands
        byte[] inputBuffer; //the input buffer for storing commands
        int inputPos; //our current position in the input buffer
        public Queue inputCommandQueue; // our queue for storing complete commands once they are entirely received
        Queue outputQueue; // output is stored in a queue also...this is necessary because of network latency

        #region Constructor
        public Character()
        {
            this.thirdRoundTimer = new Timer(DragonsSpineMain.MasterRoundInterval * 3);
            this.thirdRoundTimer.Elapsed += new ElapsedEventHandler(ThirdRoundEvent);
            this.thirdRoundTimer.AutoReset = true;
            this.thirdRoundTimer.Start();
            this.roundTimer = new Timer(DragonsSpineMain.MasterRoundInterval);
            this.roundTimer.Elapsed +=new ElapsedEventHandler(RoundEvent);
            this.roundTimer.AutoReset = true;
            this.roundTimer.Start();
            this.socket = null;
            this.PlayerID = -1;
            this.wearing = new List<Item>();
            this.sackList = new List<Item>();
            this.beltList = new List<Item>();
            this.lockerList = new List<Item>();
            this.seenList = new List<Character>();
            this.Name = "Nobody";
            this.inputBuffer = new byte[MAX_INPUT_LENGTH];
            this.inputCommandQueue = new Queue();
            this.outputQueue = new Queue();
            this.inputPos = 0;
            this.spellList = new IntStringMap();
        } 
        #endregion

        public int inputCommandQueueCount() { return inputCommandQueue.Count; }

        public string inputCommandQueueDequeue()
        {
            try
            {
                return (string)inputCommandQueue.Dequeue();
            }
            catch (InvalidOperationException e)
            {
                Utils.LogException(e);
                Command.ParseCommand(this, "rest", "");
                return (string)inputCommandQueue.Dequeue();
            }
        }

        public int outputQueueCount()
        {
            return outputQueue.Count;
        }

        public string outputQueueDequeue()
        {
            return (string)outputQueue.Dequeue();
        }

        public string getInputBuffer()
        {
            return Encoding.ASCII.GetString(inputBuffer, 0, inputPos);
        }

        public void clearInputBuffer()
        {
            inputPos = 0;
        }

        public void echoBytes(byte[] b, int count)
        {
            socket.Send(b, count, 0);
        }

        public void AddToLogin()
        {
            foreach (Character ch in Character.loginList)
            {
                if (ch.ipAddress == this.ipAddress)
                {
                    ch.RemoveFromLogin();
                }
            }
            IO.addToLogin.Add(this);
            IO.pplToAddToLogin = true;
        }

        public void RemoveFromLogin()
        {
            IO.removeFromLogin.Add(this);
            IO.pplToRemoveFromLogin = true;
        }

        public void AddToCharGen()
        {
            IO.addToCharGen.Add(this);
            IO.pplToAddToCharGen = true;
        }

        public void RemoveFromCharGen()
        {
            IO.removeFromCharGen.Add(this);
            IO.pplToRemoveFromCharGen = true;
        }

        public void AddToMenu()
        {
            IO.addToMenu.Add(this);
            IO.pplToAddToMenu = true;
        }

        public void RemoveFromMenu()
        {
            IO.removeFromMenu.Add(this);
            IO.pplToRemoveFromMenu = true;
        }

        public void AddToLimbo()
        {
            IO.addToLimbo.Add(this);
            IO.pplToAddToLimbo = true;
        }
        public void RemoveFromLimbo()
        {
            IO.removeFromLimbo.Add(this);
            IO.pplToRemoveFromLimbo = true;
        }

        virtual public void AddToWorld()
        {
            if (this.IsPC)
            {
                this.PCState = Globals.ePlayerState.PLAYING;
                if (this.protocol == DragonsSpineMain.APP_PROTOCOL)
                    this.Write(Protocol.GAME_ENTER);
            }
            this.CurrentCell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);
            IO.addToWorld.Add(this);
            IO.pplToAddToWorld = true;
        }

        virtual public void RemoveFromWorld()
        {
            if (this.CurrentCell != null)
            {
                this.CurrentCell.Remove(this);
                this.CurrentCell = null;
            }
            IO.removeFromWorld.Add(this);
            IO.pplToRemoveFromWorld = true;
        }

        public virtual void doAI() { }
        public virtual void SendMerchantList(Character chr) { return; }
        public virtual bool MerchantShowItem(Character chr, string args) { return false; }
        public virtual string MerchantSellItem(Character chr, string itemname) { return ""; }
        public virtual string MerchantBuyItem(Character chr, string args) { return ""; }
        public virtual string MerchantTrainSpell(Character chr, string spellname) { return ""; }
        public virtual string GetMerchantSpellList(Character chr) { return ""; }
        public virtual void MerchantTrain(Character chr) { return; }
        public virtual void MerchantSkin(Character chr, string itemname) { return; }
        public virtual void MerchantShowBalance(Character chr) { return; }
        public virtual void MerchantDeposit(Character chr, string amount) { return; }
        public virtual void MerchantWithdraw(Character chr, string amount) { return; }
        public virtual void MerchantAppraise(Character chr, string args) { return; }
        public virtual void MerchantCritique(Character chr, string skillName) { return; }

        public virtual void merchantClimb(Character chr) { return; }
        public virtual void merchantFollow(Character chr, string args) { return; }
        public virtual void merchantGo(Character chr, string args) { return; }
        public virtual void merchantAttack(Character chr, string args) { return; }

        #region Timer Related
        protected void ThirdRoundEvent(object obj, ElapsedEventArgs e)
        {
            if (this.IsPC && this.PCState != Globals.ePlayerState.PLAYING)
                return;

            if (!this.IsDead)
            {
                //TODO: modify stats gain to reflect additional stats due to temporary stat effects
                if (this.Hits < this.HitsFull) // gain a point of health
                {
                    this.Hits++;
                    this.updateHits = true;
                    if (this.hitsRegen > 0 && this.Hits < this.HitsFull) // gain additional mana if hpregen item/effect is active
                    {
                        this.Hits += this.hitsRegen;
                        if (this.Hits > this.HitsFull) { this.Hits = this.HitsFull; } // confirm we didn't regen more hits than we have
                    }
                }
                if (this.Stamina < this.StaminaFull) // gain a point of stamina
                {
                    if (this.Hits == this.HitsFull) // only gain stamina back if hits are at max
                    {
                        this.Stamina++;
                        this.updateStam = true;
                        if (this.staminaRegen > 0 && this.Stamina < this.StaminaFull)
                        {
                            this.Stamina += this.staminaRegen;
                            if (this.Stamina > this.StaminaFull) { this.Stamina = this.StaminaFull; } // confirm we didn't regen more stamina than we have
                        }
                    }
                }

                if (this.Mana < this.ManaFull && this.preppedSpell == null && this.CommandType != Command.CommandType.Cast) // gain a point of mana
                {
                    this.Mana++;
                    this.updateMP = true;
                    if (this.manaRegen > 0 && this.Mana < this.ManaFull) // gain additional mana if manaRegeneration item or effect is active
                    {
                        this.Mana += this.manaRegen;
                        if (this.Mana > this.ManaFull) { this.Mana = this.ManaFull; } // confirm we didn't regen more mana than we have
                    }
                }

                if (this.Poisoned > 0) // do poison every three rounds
                {
                    if (this.Hits - this.Poisoned <= 0)
                    {
                        this.WriteToDisplay("You have died from poison.");
                        this.SendToAllInSight(this.Name + " slumps to the ground and dies.");
                        Rules.GiveAEKillExp(this.effectList[Effect.EffectType.Poison].caster, this);
                        Rules.DoDeath(this, this.effectList[Effect.EffectType.Poison].caster);
                    }
                    else
                    {
                        this.Hits -= this.Poisoned;
                        this.WriteToDisplay("An involuntary tremor runs up and down your spine.");
                        this.updateHits = true;
                        this.Poisoned--;
                    }
                }
            }
        }
        protected void RoundEvent(object obj, ElapsedEventArgs e)
        {
            lock (lockObject)
            {
                if (!this.isPC)
                    return;
                if (Character.pcList.Contains(this))
                {
                    #region Player
                    Character ch = this;
                    if (ch.CurrentCell != null)
                        ch.Map.UpdateCellVisible(ch.CurrentCell);

                    Character.ValidatePlayer(ch);

                    if (ch.IsDead && !ch.CurrentCell.ContainsPlayerCorpse(ch.Name))
                    {
                        foreach (Character chold in new List<Character>(Character.pcList))
                        {
                            if (chold.RightHand != null && chold.RightHand.itemType == Globals.eItemType.Corpse &&
                                chold.RightHand.special == ch.Name)
                            {
                                ch.CurrentCell = chold.CurrentCell;
                                ch.corpseIsCarried = true;
                            }
                            else if (chold.LeftHand != null && chold.LeftHand.itemType == Globals.eItemType.Corpse &&
                                chold.LeftHand.special == ch.Name)
                            {
                                ch.CurrentCell = chold.CurrentCell;
                                ch.corpseIsCarried = true;
                            }
                        }
                    }
                    else ch.corpseIsCarried = false;

                    ch.cmdWeight = 0; // reset the number of commands entered

                    if (!ch.InUnderworld && !ch.IsDead) // age the character if they are not in the Underworld and not dead
                    {
                        ch.Age++;
                        Rules.DoAgingEffect(ch);
                    }

                    ch.RoundsPlayed++; // add to the total roundsPlayed

                    // every x rounds the player suffers skill loss from time - DISABLED
                    //if (!ch.IsDead && !ch.InUnderworld && ch.RoundsPlayed % Globals.SKILL_LOSS_DIVISOR == 0)
                    //{
                    //    Skills.SkillLossOverTime(ch);
                    //}

                    if (ch.preppedSpell != null)
                    {
                        if (DragonsSpineMain.GameRound - ch.preppedRound > 1)
                        {
                            ch.Mana--;
                            ch.updateMP = true;
                            if (ch.Mana <= 0)
                            {
                                ch.Mana = 0;
                                ch.preppedSpell = null;
                                ch.WriteToDisplay("Your spell has been lost.");
                            }
                        }
                    }
                    if (ch.Stunned == 0 && !ch.IsFeared)
                    {
                        //IO.ProcessCommands(ch);

                        #region Follow Mode for Players
                        if (ch.IsPC)
                        {
                            if (ch.FollowName != "" && Array.IndexOf(Command.breakFollowCommands, ch.CommandType) != -1)
                            {
                                ch.BreakFollowMode();
                            }
                            else if (ch.FollowName != "")
                            {
                                PC pc = PC.GetOnline(ch.FollowName);
                                if (pc == null)
                                {
                                    ch.BreakFollowMode();
                                }
                                else if (pc.IsDead)
                                {
                                    ch.BreakFollowMode();
                                }
                                else
                                {
                                    if (Map.FindTargetInView(ch, pc.Name, false, true) != null)
                                    {
                                        ch.CurrentCell = pc.CurrentCell;
                                        ch.Timeout = Character.INACTIVITY_TIMEOUT;
                                    }
                                    else
                                    {
                                        ch.BreakFollowMode();
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (ch.Stunned > 0)
                            ch.Stunned -= 1;
                        else if (ch.IsFeared)
                            Creature.AIMakePCMove(ch);
                    }

                    ch.numAttackers = 0; // reset the number of attackers after commands are processed

                    //Character.ValidatePlayer(ch);

                    // update the visible cells
                    if (ch.CurrentCell != null)
                        ch.Map.UpdateCellVisible(ch.CurrentCell);

                    if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                        Protocol.ShowMap(ch);
                    else if (ch.protocol == "old-kesmai")
                        ch.CurrentCell.showMapOldKesProto(ch);
                    else if (ch.CurrentCell != null)
                        ch.CurrentCell.showMap(ch);
                    #endregion
                }
            }
        }
        #endregion

        public void RemoveFromServer()
        {
            try
            {
                if (protocol == DragonsSpineMain.APP_PROTOCOL)
                    WriteLine(Protocol.LOGOUT);

                PC pc = (PC)this;

                #region Disband From Group
                if (this.Group != null)
                {
                    this.Group.DisbandPlayerGroupMember(this.PlayerID, false);
                } 
                #endregion

                Protocol.UpdateUserLists(); // send updated user lists to protocol users

                Conference.FriendNotify(this, false); // notify friends of logoff                

                if (pc.RightHand != null && pc.RightHand.itemType == Globals.eItemType.Corpse)
                    pc.RightHand = null;
                if (pc.LeftHand != null && pc.LeftHand.itemType == Globals.eItemType.Corpse)
                    pc.LeftHand = null;

                pc.DisconnectSocket(); // clean up the socket

                System.Threading.Thread saveThread = new System.Threading.Thread(pc.Save);
                saveThread.Name = "PC Save Thread - Server removed player";
                saveThread.Start();                
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void DisconnectSocket()
        {
            if (socketConnected())
            {
                try
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
                catch (ObjectDisposedException e)
                {
                    Utils.LogException(e);
                }
            }
        }

        public void AddInput(byte[] buf, int length) // add characters to a pc's input buffer
        {
            if (inputPos + length >= MAX_INPUT_LENGTH) //too many chars, it will overrun our inputBuffer
            {
                return;
            }

            for (int a = 0; a < length; a++)
            {
                if (buf[a] == '\r' || buf[a] == '\0' || buf[a] == '\n')
                {
                    if (inputPos > 0)
                    {
                        String str = System.Text.Encoding.ASCII.GetString(inputBuffer, 0, inputPos);

                        inputCommandQueue.Enqueue(str); //add it to the command queue

                        if (str.ToLower() != "a" && str.ToLower() != "ag" && str.ToLower() != "aga" && str.ToLower() != "again")
                        {
                            this.LastCommand = str;
                        }
                    }
                    inputPos = 0; //start over in the inputBuffer
                    continue;
                }

                if (inputPos > 0 && inputBuffer[inputPos - 1] == ' ' && buf[a] == ' ')
                    continue; //in case they put extra spaces in a command

                // handle backspaces
                if (buf[a] == 8)
                {
                    if (inputPos > 0)
                        inputPos--;
                }
                else inputBuffer[inputPos++] = buf[a]; //add this char to the inputBuffer
            }

            if (this.echo && this.PCState == Globals.ePlayerState.PLAYING)
                echoBytes(buf, length);
        }

        public void NPCSpeech(string command, string args)
        {
            if (this.FirstJoinedCommand != Command.CommandType.None || args.Contains(";"))
            {
                this.WriteToDisplay("Interaction with an NPC cannot be joined with another command.");
                return;
            }

            try
            {
                Character target = Map.FindTargetInView(this, command.Substring(0, command.Length - 1), false, false);

                NPC npc = (NPC)target;

                if (target == null)
                {
                    this.WriteToDisplay("You don't see " + command.Substring(0, command.Length - 1) + " here.");
                    return;
                }

                bool targetResponded = false;

                args = args.Trim();

                string[] sArgs = args.Split(" ".ToCharArray());

                if (target.questList.Count > 0)
                {
                    for (int a = 0; a < target.questList.Count; a++)
                    {
                        Quest q = target.questList[a];

                        Quest activeQuest = this.GetQuest(q.QuestID);
                        
                        if(activeQuest != null)
                        {
                            if (!activeQuest.IsRepeatable && activeQuest.TimesCompleted > 0)
                            {
                                continue;
                            }
                        }

                        #region Handle Quest Step Start Strings
                        foreach (string stepString in q.StepStrings.Keys)
                        {
                            if (args.ToLower().Contains(stepString.ToLower()))
                            {
                                q.BeginQuest((PC)this, true);
                                if (activeQuest == null)
                                {
                                    activeQuest = this.GetQuest(q.QuestID);
                                }
                                activeQuest.FinishStep(target, (PC)this, q.StepStrings[args]);
                                targetResponded = true;
                                goto questResponses;
                            }
                        }
                        #endregion

                    questResponses:
                        #region Handle Quest Responses
                        foreach (string respString in q.ResponseStrings.Keys)
                        {
                            if (args.ToLower().Contains(respString.ToLower()))
                            {
                                string emote = Utils.ParseEmote(q.ResponseStrings[respString]);
                                string response = q.ResponseStrings[respString];
                                if (emote.Length > 0)
                                {
                                    response = response.Replace("{" + emote + "}", "");
                                    this.WriteToDisplay(target.name + " " + emote);
                                }
                                if (response.Length > 0)
                                {
                                    this.WriteToDisplay(target.Name + ": " + response);
                                }
                                Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, target, Rules.dice.Next(28, 40), null);
                                targetResponded = true;
                            }
                        }
                        #endregion

                        #region Handle Quest Flags
                        foreach (string flagString in q.FlagStrings.Keys)
                        {
                            if (args.ToLower().Contains(flagString.ToLower()))
                            {
                                if (!this.questFlags.Contains(q.RewardFlags[q.FlagStrings[flagString]]))
                                {
                                    if (!q.StepOrder || (activeQuest != null && activeQuest.CurrentStep == q.FlagStrings[flagString]))
                                    {
                                        if (activeQuest == null)
                                        {
                                            q.BeginQuest((PC)this, true);
                                            activeQuest = this.GetQuest(q.QuestID);
                                        }

                                        if (activeQuest != null && !activeQuest.CompletedSteps.Contains(q.FlagStrings[flagString]))
                                        {
                                            activeQuest.FinishStep(target, (PC)this, q.FlagStrings[flagString]);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (activeQuest != null)
                                        {
                                            if (q.FailStrings.ContainsKey(activeQuest.CurrentStep))
                                            {
                                                string emote = Utils.ParseEmote(q.FailStrings[activeQuest.CurrentStep]);
                                                string failure = q.FailStrings[activeQuest.CurrentStep];
                                                if (emote.Length > 0)
                                                {
                                                    failure = failure.Replace("{" + emote + "}", "");
                                                    this.WriteToDisplay(target.name + " " + emote);
                                                }
                                                if (failure.Length > 0)
                                                {
                                                    this.WriteToDisplay(target.Name + ": " + failure);
                                                }
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            this.WriteToDisplay(target.Name + ": I cannot help you with that, " + this.Name + ".");
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    this.WriteToDisplay(target.Name + ": I have already helped you with that, " + this.Name + ".");
                                    return;
                                }
                            }
                        }
                        #endregion
                    }
                }

                switch (sArgs[0].ToLower())
                {
                    case "hail":
                    case "hello":
                    case "hi":
                        if (target.questList.Count > 0)
                        {
                            int qCount = 0;
                            Quest q = target.questList[qCount];

                            // only get a hint string from a quest that the player can do
                            do
                            {
                                q = target.questList[qCount];
                                qCount++;
                            }
                            while (!q.PlayerMeetsRequirements((PC)this, false) && qCount < this.questList.Count);

                            if (q.HintStrings.Count > 0 && q.PlayerMeetsRequirements((PC)this, false))
                            {
                                int selection = Rules.dice.Next(0, q.HintStrings.Count);
                                string emote = Utils.ParseEmote(q.HintStrings[selection]);
                                string hint = q.HintStrings[selection];
                                if (emote.Length > 0)
                                {
                                    hint = hint.Replace("{" + emote + "}", "");
                                    this.WriteToDisplay(target.name + " " + emote);
                                }
                                if (hint.Length > 0)
                                {
                                    this.WriteToDisplay(target.Name + ": " + hint);
                                }
                                break;
                            }
                            else
                            {
                                this.WriteToDisplay(target.Name + ": Hello, " + this.Name + ".");
                            }
                        }
                        else // responses if the target does not have a quest to give
                        {
                            if (target.merchantType > Merchant.MerchantType.None)
                            {
                                this.WriteToDisplay(target.Name + ": Hello.");
                            }
                            else this.WriteToDisplay(target.Name + ": Oloth plynn dos!");
                        }
                        break;
                    case "show":
                        #region show

                        if (sArgs.Length < 2)
                        {
                            this.WriteToDisplay(target.Name + ": What do you want me to show you?");
                            break;
                        }

                        if (sArgs[1].Equals("list") || sArgs[1].Equals("prices"))
                        {
                            if (target.merchantType > Merchant.MerchantType.None) { target.SendMerchantList(this); }
                            else { this.WriteToDisplay(target.Name + ": I do not have anything to sell."); }
                        }
                        else if (sArgs[1].Equals("spells"))
                        {
                            if (target.trainerType == Merchant.TrainerType.Spell && this.BaseProfession == target.BaseProfession)
                            {
                                this.WriteToDisplay(target.GetMerchantSpellList(this));
                            }
                            else
                            {
                                if (target.trainerType == Merchant.TrainerType.Spell)
                                {
                                    switch (this.BaseProfession)
                                    {
                                        case ClassType.Fighter:
                                        case ClassType.Martial_Artist:
                                        case ClassType.Knight:
                                            this.WriteToDisplay(target.Name + ": I cannot teach magic to a " + this.classFullName + ", therefore I do not have any spells to show you.");
                                            break;
                                        case ClassType.Thaumaturge:
                                            if (this.LandID == 0)
                                            {
                                                if (this.MapID == 0)
                                                { this.WriteToDisplay(target.Name + ": I am not skilled in the ways of thaumaturgy. Speak with Sven in the temple."); }
                                                else if (this.MapID == 1)
                                                { this.WriteToDisplay(target.Name + ": I am not a thaumaturgist. You should visit the priest that dwells in the east, beyond the dust plains."); }
                                                else if (this.MapID == 2)
                                                { this.WriteToDisplay(target.Name + ": I am not a thaumaturge. Travel to the temple near the portal, or head up to Lockpick Town and speak to the priests there."); }
                                                else if (this.MapID == 3)
                                                { this.WriteToDisplay(target.Name + ": Go to the temple and speak to the priests there. I do not practice thaumaturgy."); }
                                                else { this.WriteToDisplay(target.Name + ": I do not practice thaumaturgy. Ask a priest in one of the temples to help you."); }
                                            }
                                            break;
                                        case ClassType.Wizard:
                                            this.WriteToDisplay(target.Name + ": I am not versed in the ways of wizardry. Search elsewhere for assistance.");
                                            break;
                                        case ClassType.Thief:
                                            if (target.BaseProfession == ClassType.Thaumaturge) { this.WriteToDisplay(target.Name + ": I do not dabble in nor do I tolerate the foul art of shadow magic. Begone!"); }
                                            else { this.WriteToDisplay(target.Name + ": I do not recognize your aura... shadow magic I bet. I cannot help you."); }
                                            break;
                                    }
                                }
                                else { this.WriteToDisplay(target.Name + ": I do not know anything about magic."); }
                            }
                        }
                        else if (sArgs[1].Equals("balance"))
                        {
                            if (target.interactiveType == Merchant.InteractiveType.Banker) { target.MerchantShowBalance(this); }
                            else { this.WriteToDisplay(target.Name + ": I am not a banker."); }
                        }
                        else if(target.MerchantShowItem(this, args))
                        {
                            break;
                        }
                        break; 
                        #endregion
                    case "sell":
                        #region sell
                        if (sArgs.Length < 2 || sArgs[1].Equals(""))
                        {
                            if (target.merchantType != Merchant.MerchantType.None)
                            { this.WriteToDisplay(target.Name + ": What do you want me to sell to you?"); }
                            else { this.WriteToDisplay(target.Name + ": I have nothing to sell. Speak with a merchant."); }
                            break;
                        }
                        if (target.merchantType != Merchant.MerchantType.None || target.trainerType == Merchant.TrainerType.Spell)
                        {
                            if (target.BaseProfession == this.BaseProfession && target.trainerType == Merchant.TrainerType.Spell)
                            {
                                if (sArgs[1] == "spellbook" || sArgs[1] == "book")
                                {
                                    this.TargetName = target.Name;
                                    this.WriteToDisplay(target.MerchantSellItem(this, args));
                                }
                                else
                                {
                                    this.WriteToDisplay(target.Name + ": I do not sell that. However I do sell spellbooks.");
                                }
                                break;
                            }
                            else if (target.BaseProfession != this.BaseProfession && target.trainerType == Merchant.TrainerType.Spell)
                            {
                                switch (this.BaseProfession)
                                {
                                    case ClassType.Fighter:
                                    case ClassType.Martial_Artist:
                                    case ClassType.Knight:
                                        this.WriteToDisplay(target.Name + ": I have nothing to sell to a " + this.classFullName.ToLower() + ".");
                                        break;
                                    case ClassType.Thaumaturge:
                                    case ClassType.Wizard:
                                    case ClassType.Thief:
                                        if (sArgs[1] == "spellbook" || sArgs[1] == "book")
                                        {
                                            this.WriteToDisplay(target.Name + ": You should speak to someone of your profession about scribing a new spellbook.");
                                        }
                                        else
                                        {
                                            this.WriteToDisplay(target.Name + ": I have nothing to sell to you.");
                                        }
                                        break;
                                }
                                break;
                            }
                            else
                            {
                                target.TargetName = this.Name;
                                this.WriteToDisplay(target.MerchantSellItem(this, args));
                                break;
                            }
                        }
                        else
                        {
                            this.WriteToDisplay(target.Name + ": I have nothing to sell. Speak to a merchant.");
                        }
                        break; 
                        #endregion
                    case "buy":
                        #region buy
                        if (target.merchantType == Merchant.MerchantType.None)
                        {
                            this.WriteToDisplay(target.Name + ": I am not a merchant.");
                        }
                        else
                        {
                            if (sArgs.Length <= 1)
                            {
                                this.WriteToDisplay(target.Name + ": What do you want me to buy from you?");
                            }
                            else
                            {
                                this.WriteToDisplay(target.MerchantBuyItem(this, args));
                            }
                        }
                        break; 
                        #endregion
                    case "specialize":
                        #region specialize
                        if (target.trainerType != Merchant.TrainerType.Weapon)
                        {
                            this.WriteToDisplay(target.Name + ": I do not know how to do that.");
                            break;
                        }
                        if (this.BaseProfession != ClassType.Fighter)
                        {
                            this.WriteToDisplay(target.Name + ": I only teach pure fighters how to hone their skills.");
                            break;
                        }
                        if (this.Level < 9)
                        {
                            this.WriteToDisplay(target.Name + ": I admire your ambition, " + this.Name + ", however you will need to return to me after you have learned more.");
                            break;
                        }
                        if (this.RightHand == null)
                        {
                            this.WriteToDisplay(target.Name + ": Wield the weapon you wish to specialize with in your right hand and ask me again.");
                            break;
                        }
                        if (this.RightHand.skillType == Globals.eSkillType.Unarmed)
                        {
                            this.WriteToDisplay(target.Name + ": That weapon is better suited for a martial artist.");
                            break;
                        }
                        if (this.RightHand.skillType == Globals.eSkillType.Bash)
                        {
                            this.WriteToDisplay(target.Name + ": You may not specialize with " + this.RightHand.shortDesc +"...yet.");
                            break;
                        }
                        if (this.fighterSpecialization == Globals.eSkillType.None)
                        {
                            this.WriteToDisplay(target.Name + ": You have specialized your " + Utils.FormatEnumString(this.RightHand.skillType.ToString()).ToLower() + " skill.");
                            this.fighterSpecialization = this.RightHand.skillType;
                            break;
                        }
                        else
                        {
                            this.WriteToDisplay(target.Name + ": You are already specialized with that. (" + Utils.FormatEnumString(this.fighterSpecialization.ToString()).ToLower() + ")");
                            break;
                        } 
                        #endregion
                    case "train":
                        #region train
                        if (target.trainerType == Merchant.TrainerType.None)
                        {
                            this.WriteToDisplay(target.Name + ": I am not a trainer.");
                        }
                        else
                        {
                            target.MerchantTrain(this);
                        }
                        break; 
                        #endregion
                    case "teach":
                        #region teach
                        if (targetResponded)
                            break;

                        if (sArgs.Length == 1)
                        {
                            this.WriteToDisplay(target.Name + ": What do you want me to teach you?");
                            break;
                        }

                        //if (target.trainerType == Merchant.TrainerType.Knight)
                        //{
                        //    target.merchantTrain(this);
                        //    break;
                        //}

                        if (target.trainerType != Merchant.TrainerType.Spell)
                        {
                            this.WriteToDisplay(target.Name + ": I am not a spell trainer.");
                            break;
                        }

                        if (target.BaseProfession != this.BaseProfession)
                        {
                            this.WriteToDisplay(target.Name + ": I do not know how to teach you.");
                            break;
                        }

                        if (this.RightHand != null && this.RightHand.baseType == Globals.eItemBaseType.Book)
                        {
                            Book book = (Book)this.RightHand;
                            if (book.bookType == Book.BookType.Spellbook)
                            {
                                if (book.attunedID == this.PlayerID)
                                {
                                    args = args.Substring(args.IndexOf(' ') + 1);

                                    Spell spell = Spell.GetSpell(args.ToLower());

                                    if (spell == null)
                                    {
                                        this.WriteToDisplay(target.Name + ": I do now know a spell called \"" + args + "\".");
                                        break;
                                    }

                                    if (this.spellList.SpellIDExists(spell.SpellID))
                                    {
                                        this.WriteToDisplay(target.Name + ": You already know the spell " + spell.Name + ".");
                                        break;
                                    }

                                    if (Skills.GetSkillLevel(this.magic) < spell.RequiredLevel)
                                    {
                                        this.WriteToDisplay(target.Name + ": You are not ready to learn the spell " + spell.Name + " yet.");
                                        break;
                                    }

                                    Item coins = Item.GetItemFromGround("coins", target.FacetID, target.LandID, target.MapID, target.X, target.Y, target.Z);

                                    if (coins == null)
                                    {
                                        coins = Map.GetItemFromCounter(target, "coins");
                                        if (coins == null)
                                        {
                                            this.WriteToDisplay(target.Name + ": I do not see any coins here.");
                                            break;
                                        }
                                    }

                                    if (coins != null && coins.coinValue == spell.TrainingPrice)
                                    {
                                        this.TargetName = target.Name;
                                        Spell.TeachSpell(this, args.ToLower());
                                    }
                                    else if (coins != null && coins.coinValue > spell.TrainingPrice)
                                    {
                                        coins.coinValue = coins.coinValue - spell.TrainingPrice;
                                        if (Map.nearCounter(target))
                                        {
                                            Map.PutItemOnCounter(target, coins);
                                        }
                                        else
                                        {
                                            target.CurrentCell.Add(coins);
                                        }
                                        this.TargetName = target.Name;

                                        Spell.TeachSpell(this, spell.SpellCommand);
                                    }
                                    else
                                    {
                                        if (Map.nearCounter(target))
                                        {
                                            Map.PutItemOnCounter(target, coins);
                                        }
                                        else
                                        {
                                            target.CurrentCell.Add(coins);
                                        }
                                        this.WriteToDisplay(target.Name + ": There aren't enough coins here to cover the cost of that spell.");
                                    }
                                }
                                else
                                {
                                    this.WriteToDisplay(target.Name + ": That spellbook is soulbound to another being.");
                                }
                            }
                            else
                            {
                                this.WriteToDisplay(target.Name + ": You must have your spellbook in your right hand.");
                            }
                        }
                        else
                        {
                            this.WriteToDisplay(target.Name + ": You must have your spellbook in your right hand.");
                        }
                        break; 
                        #endregion
                    case "give":
                        #region give
                        this.WriteToDisplay(target.Name + ": Perhaps you should give me something first.");
                        return;
                        //target.merchantGive(this, sArgs[1]);
                        #endregion
                    case "critique":
                        #region critique
                        if (sArgs.Length == 1) // trainer, critique
                        {
                            if (this.RightHand != null)
                            {
                                target.MerchantCritique(this, Utils.FormatEnumString(this.RightHand.skillType.ToString()));
                            }
                            else
                            {
                                target.MerchantCritique(this, "unarmed");
                            }
                            break;
                        }
                        if (sArgs.Length > 1) // trainer, critique <skill>
                        {
                            if (sArgs[1].ToLower() == "greatsword" || sArgs[1].ToLower() == "two" ||
                                sArgs[1].ToLower() == "twohanded")
                                sArgs[1] = "two handed";

                            target.MerchantCritique(this, sArgs[1]);
                            break;
                        }
                        else // catch
                        {
                            if (this.RightHand == null)
                            {
                                target.MerchantCritique(this, Globals.eSkillType.Unarmed.ToString());
                                break;
                            }
                            target.MerchantCritique(this, Utils.FormatEnumString(this.RightHand.skillType.ToString()));
                        }
                        break; 
                        #endregion
                    case "deposit":
                        #region deposit
                        if (target.interactiveType != Merchant.InteractiveType.Banker)
                        {
                            this.WriteToDisplay(target.Name + ": I am not a banker.");
                            return;
                        }
                        if (sArgs.Length > 1)
                        {
                            target.MerchantDeposit(this, sArgs[1]);
                        }
                        else
                        {
                            target.MerchantDeposit(this, "all");
                        }
                        break; 
                        #endregion
                    case "withdraw":
                        #region withdraw
                        if (target.interactiveType != Merchant.InteractiveType.Banker)
                        {
                            this.WriteToDisplay(target.Name + ": I am not a banker.");
                            return;
                        }
                        target.MerchantWithdraw(this, sArgs[1]);
                        break; 
                        #endregion
                    case "climb":
                        ArtificialIntel.Go_Climb((NPC)target, this, sArgs[1]);
                        break;
                    case "follow":
                        ArtificialIntel.Follow((NPC)target, this);
                        break;
                    case "go":
                        ArtificialIntel.Go_Direction((NPC)target, this, sArgs);
                        break;
                    case "attack":
                        target.merchantAttack(this, sArgs[1]);
                        break;
                    case "appraise":
                        if (target.merchantType == Merchant.MerchantType.None)
                        {
                            this.WriteToDisplay(target.Name + ": Speak to a merchant about that.");
                            return;
                        }
                        else
                        {
                            if (sArgs.Length <= 1)
                            {
                                this.WriteToDisplay(target.Name + ": What do you want me to appraise?");
                                return;
                            }
                            target.MerchantAppraise(this, args);
                        }
                        break;
                    default:
                        targetResponded = false;
                        break;
                }

                if (targetResponded) { this.CommandType = Command.CommandType.NPCInteraction; }

                if (target != null)
                {
                    Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, target, Rules.dice.Next(28, 40), null);
                }
            }
            catch (Exception e)
            {
                Utils.Log("Character.npcTalk [" + command + " " + args + "]", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
        }

        public int GetWeaponSkillLevel(Item weapon)
        {
            if (!this.IsPC)
            {
                if (weapon == null)
                {
                    if (GetSkillExperience(Globals.eSkillType.Unarmed) == 0)
                    {
                        return this.level;
                    }
                    else
                    {
                        return Skills.GetSkillLevel(this.unarmed);
                    }
                }
                else
                {
                    if (GetSkillExperience(weapon.skillType) == 0)
                    {
                        return this.level;
                    }
                    else
                    {
                        return this.level;
                    }
                }
            }
            if (weapon == null)
            {
                return Skills.GetSkillLevel(this.unarmed);
            }
            else
            {
                return Skills.GetSkillLevel(GetSkillExperience(weapon.skillType));
            }
        }

        public long GetHighSkillExperience(Globals.eSkillType skillType)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Bow:
                    return this.highBow;
                case Globals.eSkillType.Sword:
                    return this.highSword;
                case Globals.eSkillType.Two_Handed:
                    return this.highTwoHanded;
                case Globals.eSkillType.Unarmed:
                    return this.highUnarmed;
                case Globals.eSkillType.Staff:
                    return this.highStaff;
                case Globals.eSkillType.Dagger:
                    return this.highDagger;
                case Globals.eSkillType.Halberd:
                    return this.highHalberd;
                case Globals.eSkillType.Rapier:
                    return this.highRapier;
                case Globals.eSkillType.Shuriken:
                    return this.highShuriken;
                case Globals.eSkillType.Magic:
                    return this.highMagic;
                case Globals.eSkillType.Mace:
                    return this.highMace;
                case Globals.eSkillType.Flail:
                    return this.highFlail;
                case Globals.eSkillType.Threestaff:
                    return this.highThreestaff;
                case Globals.eSkillType.Thievery:
                    return this.highThievery;
                case Globals.eSkillType.Bash:
                    return this.highBash;
                default:
                    return -1;
            }
        }

        public long GetSkillExperience(Globals.eSkillType skillType)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Bow:
                    return this.bow;
                case Globals.eSkillType.Sword:
                    return this.sword;
                case Globals.eSkillType.Two_Handed:
                    return this.twoHanded;
                case Globals.eSkillType.Unarmed:
                    return this.unarmed;
                case Globals.eSkillType.Staff:
                    return this.staff;
                case Globals.eSkillType.Dagger:
                    return this.dagger;
                case Globals.eSkillType.Halberd:
                    return this.halberd;
                case Globals.eSkillType.Rapier:
                    return this.rapier;
                case Globals.eSkillType.Shuriken:
                    return this.shuriken;
                case Globals.eSkillType.Magic:
                    return this.magic;
                case Globals.eSkillType.Mace:
                    return this.mace;
                case Globals.eSkillType.Flail:
                    return this.flail;
                case Globals.eSkillType.Threestaff:
                    return this.threestaff;
                case Globals.eSkillType.Thievery:
                    return this.thievery;
                case Globals.eSkillType.Bash:
                    return this.bash;
                default:
                    return -1;
            }
        }

        public void SetSkillExperience(Globals.eSkillType skillType, long amount)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Bow:
                    this.bow = amount;
                    break;
                case Globals.eSkillType.Sword:
                    this.sword = amount;
                    break;
                case Globals.eSkillType.Two_Handed:
                    this.twoHanded = amount;
                    break;
                case Globals.eSkillType.Unarmed:
                    this.unarmed = amount;
                    break;
                case Globals.eSkillType.Staff:
                    this.staff = amount;
                    break;
                case Globals.eSkillType.Dagger:
                    this.dagger = amount;
                    break;
                case Globals.eSkillType.Halberd:
                    this.halberd = amount;
                    break;
                case Globals.eSkillType.Rapier:
                    this.rapier = amount;
                    break;
                case Globals.eSkillType.Shuriken:
                    this.shuriken = amount;
                    break;
                case Globals.eSkillType.Magic:
                    this.magic = amount;
                    break;
                case Globals.eSkillType.Mace:
                    this.mace = amount;
                    break;
                case Globals.eSkillType.Flail:
                    this.flail = amount;
                    break;
                case Globals.eSkillType.Threestaff:
                    this.threestaff = amount;
                    break;
                case Globals.eSkillType.Thievery:
                    this.thievery = amount;
                    break;
                case Globals.eSkillType.Bash:
                    this.bash = amount;
                    break;
            }
        }

        public long GetTrainedSkillExperience(Globals.eSkillType skillType)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Bow:
                    return this.trainedBow;
                case Globals.eSkillType.Sword:
                    return this.trainedSword;
                case Globals.eSkillType.Two_Handed:
                    return this.trainedTwoHanded;
                case Globals.eSkillType.Unarmed:
                    return this.trainedUnarmed;
                case Globals.eSkillType.Staff:
                    return this.trainedStaff;
                case Globals.eSkillType.Dagger:
                    return this.trainedDagger;
                case Globals.eSkillType.Halberd:
                    return this.trainedHalberd;
                case Globals.eSkillType.Rapier:
                    return this.trainedRapier;
                case Globals.eSkillType.Shuriken:
                    return this.trainedShuriken;
                case Globals.eSkillType.Magic:
                    return this.trainedMagic;
                case Globals.eSkillType.Mace:
                    return this.trainedMace;
                case Globals.eSkillType.Flail:
                    return this.trainedFlail;
                case Globals.eSkillType.Threestaff:
                    return this.trainedThreestaff;
                case Globals.eSkillType.Thievery:
                    return this.trainedThievery;
                case Globals.eSkillType.Bash:
                    return this.trainedBash;
                default:
                    return -1;
            }
        }

        public void SetTrainedSkillExperience(Globals.eSkillType skillType, long amount)
        {
            switch (skillType)
            {
                case Globals.eSkillType.Bow:
                    this.trainedBow = amount;
                    break;
                case Globals.eSkillType.Sword:
                    this.trainedSword = amount;
                    break;
                case Globals.eSkillType.Two_Handed:
                    this.trainedTwoHanded = amount;
                    break;
                case Globals.eSkillType.Unarmed:
                    this.trainedUnarmed = amount;
                    break;
                case Globals.eSkillType.Staff:
                    this.trainedStaff = amount;
                    break;
                case Globals.eSkillType.Dagger:
                    this.trainedDagger = amount;
                    break;
                case Globals.eSkillType.Halberd:
                    this.trainedHalberd = amount;
                    break;
                case Globals.eSkillType.Rapier:
                    this.trainedRapier = amount;
                    break;
                case Globals.eSkillType.Shuriken:
                    this.trainedShuriken = amount;
                    break;
                case Globals.eSkillType.Magic:
                    this.trainedMagic = amount;
                    break;
                case Globals.eSkillType.Mace:
                    this.trainedMace = amount;
                    break;
                case Globals.eSkillType.Flail:
                    this.trainedFlail = amount;
                    break;
                case Globals.eSkillType.Threestaff:
                    this.trainedThreestaff = amount;
                    break;
                case Globals.eSkillType.Thievery:
                    this.trainedThievery = amount;
                    break;
                case Globals.eSkillType.Bash:
                    this.trainedBash = amount;
                    break;
            }
        }        

        public void SendToAllInConferenceRoom(string text, Protocol.TextType textType)
        {
            for (int a = 0; a < Character.confList.Count; a++)
            {
                Character chr = confList[a];

                if (chr.PCState == Globals.ePlayerState.CONFERENCE)
                {
                    if (this.confRoom == chr.confRoom && Array.IndexOf(chr.ignoreList, this.PlayerID) == -1) // in same room and not ignored
                    {
                        switch (textType)
                        {
                            case Protocol.TextType.Enter:
                                if (this.Name == chr.Name) { break; } // do not send enter messages to the player that is entering
                                chr.WriteLine(text, Protocol.TextType.Enter); break;
                            case Protocol.TextType.Exit:
                                if (this.Name == chr.Name) { break; } // do not send exit messages to the player that is exiting
                                chr.WriteLine(text, Protocol.TextType.Exit); break;
                            case Protocol.TextType.PlayerChat:
                                if (chr.filterProfanity) { text = Conference.FilterProfanity(text); } // send the text through the filter
                                chr.WriteLine(text, Protocol.TextType.PlayerChat);
                                //if (this.echo && this.Name == chr.Name)
                                //{
                                //    this.WriteLine(text, Protocol.TextType.PlayerChat);
                                //}
                                //else if (this.Name != chr.Name)
                                //{
                                //    chr.WriteLine(text, Protocol.TextType.PlayerChat);
                                //}
                                break;
                            case Protocol.TextType.Status:
                                chr.WriteLine(text, Protocol.TextType.Status); break; 
                            case Protocol.TextType.System:
                                chr.WriteLine(text, Protocol.TextType.System); break;
                        } // end switch
                    } // end if
                } // end if
            } // end foreach
        }

        public void SendToAllInSight(string text) // send text to everyone in sight, except this character
        {
            int bitcount = 0;
            Cell curCell = null;

            for (int ypos = -3; ypos <= 3; ypos++)
            {
                for (int xpos = -3; xpos <= 3; xpos++)
                {
                    if (Cell.CellRequestInBounds(this.CurrentCell, xpos, ypos))
                    {
                        if (this.CurrentCell.visCells[bitcount]) // check the PC list, and char list of the cell
                        {
                            curCell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                            if (curCell != null)
                            {
                                foreach (Character chr in curCell.Characters) // search for the character in the charlist of the cell
                                {
                                    if (chr != this && chr.IsPC && this.IsPC) // players sending messages to other players
                                    {
                                        if (Array.IndexOf(chr.ignoreList, this.PlayerID) == -1) // ignore list
                                        {
                                            if (chr.filterProfanity) // profanity filter
                                            {
                                                text = Conference.FilterProfanity(text);
                                            }
                                            chr.WriteToDisplay(text);
                                        }
                                    }
                                    else if (chr != this && chr.IsPC && !this.IsPC) // non players sending messages to other players
                                    {
                                        chr.WriteToDisplay(text);
                                    }
                                }//end foreach
                            }
                        }
                        bitcount++;
                    }
                }
            }
        }

        public void SendSoundToAllInRange(string soundFile) // everyone but the sound source hears the sound
        {
            Cell cell = null;

            for (int ypos = -6; ypos <= 6; ypos += 1)
            {
                for (int xpos = -6; xpos <= 6; xpos += 1)
                {
                    if (Cell.CellRequestInBounds(this.CurrentCell, xpos, ypos))
                    {
                        cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                        if (cell != null)
                        {
                            foreach (Character chr in cell.Characters) // search for the character in the charlist of the cell
                            {
                                if (chr != this)
                                {
                                    if (chr.IsPC)
                                    {
                                        if (Array.IndexOf(chr.ignoreList, this.PlayerID) == -1)
                                        {
                                            chr.SendSound(soundFile);
                                        }
                                    }
                                    else
                                    {
                                        chr.SendSound(soundFile);
                                    }
                                }
                            }//end foreach
                        }
                    }
                }
            }
        }

        public void SendSound(string soundFile) // only the sound source hears the sound
        {
            try
            {
                if (soundFile == null || soundFile == "")
                {
                    return;
                }
                else if (soundFile.Length < 4)
                {
                    Utils.Log("Sound file length less than 4 at sendSound(" + soundFile + ")", Utils.LogType.SystemWarning);
                    return;
                }

                if (this.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    this.Write(Protocol.SOUND + soundFile + Protocol.VSPLIT + "0" + Protocol.VSPLIT + "0" + Protocol.SOUND_END);
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public void EmitSound(string soundFile) // everyone hears the sound
        {
            try
            {
                if (soundFile == null || soundFile == "")
                {
                    return;
                }

                else if (soundFile.Length < 4)
                {
                    Utils.Log("Sound file length less than 4 at character.EmitSound(" + soundFile + ")", Utils.LogType.SystemWarning);
                    return;
                }

                Cell cell = null;

                for (int ypos = -6; ypos <= 6; ypos += 1)
                {
                    for (int xpos = -6; xpos <= 6; xpos += 1)
                    {
                        if (Cell.CellRequestInBounds(this.CurrentCell, xpos, ypos))
                        {
                            cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);
                            if (cell != null && cell.Characters.Count > 0)
                            {
                                int distance = 0;
                                int direction = 0;

                                for (int a = 0; a < cell.Characters.Count; a++)
                                {
                                    Character chr = (Character)cell.Characters[a];
                                    if (chr.protocol == DragonsSpineMain.APP_PROTOCOL)
                                    {
                                        distance = Cell.GetCellDistance(this.X, this.Y, chr.X, chr.Y);
                                        direction = Convert.ToInt32(Map.GetDirection(this.CurrentCell, chr.CurrentCell));

                                        if (chr != this)
                                        {
                                            if (Array.IndexOf(chr.ignoreList, this.PlayerID) == -1) // check ignore list
                                            {
                                                chr.Write(Protocol.SOUND + soundFile + Protocol.VSPLIT + distance + Protocol.VSPLIT + direction + Protocol.SOUND_END);
                                            }
                                        }
                                        else
                                        {
                                            chr.Write(Protocol.SOUND + soundFile + Protocol.VSPLIT + distance + Protocol.VSPLIT + direction + Protocol.SOUND_END);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.Log("Sound failure at EmitSound(" + soundFile + ")", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
        }

        public void SendShout(string text)
        {
            Cell cell = null;

            for (int ypos = -6; ypos <= 6; ypos += 1)
            {
                for (int xpos = -6; xpos <= 6; xpos += 1)
                {
                    try
                    {
                        if (Cell.CellRequestInBounds(this.CurrentCell, xpos, ypos))
                        {
                            cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                            if (cell != null && cell.Characters.Count > 0)
                            {
                                foreach (Character chr in cell.Characters)
                                {
                                    if (chr != this && chr.IsPC && this.IsPC) // players sending shouts to other players
                                    {
                                        if (chr.ignoreList != null && Array.IndexOf(chr.ignoreList, this.PlayerID) == -1)
                                        {
                                            if (chr.filterProfanity) // filter profanity if setting is true
                                            {
                                                text = Conference.FilterProfanity(text);
                                            }
                                            chr.WriteToDisplay(GetTextDirection(chr, this.X, this.Y) + text);
                                        }
                                    }
                                    else if (chr != this && chr.IsPC && !this.IsPC) // non players sending shouts to players
                                    {
                                        chr.WriteToDisplay(GetTextDirection(chr, this.X, this.Y) + text);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utils.LogException(e);
                    }
                }
            }
            return;

        }        

        public static string GetTextDirection(Character ch, int x, int y)
        {
            if (x < ch.X && y < ch.Y)
            {
                return "To the northwest you hear ";
            }
            else if (x < ch.X && y > ch.Y)
            {
                return "To the southwest you hear ";
            }
            else if (x > ch.X && y < ch.Y)
            {
                return "To the northeast you hear ";
            }
            else if (x > ch.X && y > ch.Y)
            {
                return "To the southeast you hear ";
            }
            else if (x == ch.X && y > ch.Y)
            {
                return "To the south you hear ";
            }
            else if (x == ch.X && y < ch.Y)
            {
                return "To the north you hear ";
            }
            else if (x < ch.X && y == ch.Y)
            {
                return "To the west you hear ";
            }
            else if (x > ch.X && y == ch.Y)
            {
                return "To the east you hear ";
            }
            return "You hear ";
        }

        public void SendToAll(string text) // send to everyone in the world
        {
            for (int a = 0; a < pcList.Count; a++)
            {
                Character ch = Character.pcList[a];
                if (ch != null)
                {
                    ch.WriteToDisplay(text);
                }
            }
        }

        public void Write(string message) // sends a text message to the player
        {
            outputQueue.Enqueue(message);
        }
        
        public void WriteLine(string message, Protocol.TextType textType) // write a line with a carriage return, include protocol
        {
            try
            {
                message = message + "\r\n"; // add a carriage return line feed to message

                if (this.protocol != DragonsSpineMain.APP_PROTOCOL || this.PCState == Globals.ePlayerState.PLAYING) // if not using the protocol send the message
                {
                    if (this.PCState == Globals.ePlayerState.PLAYING)
                    {
                        this.WriteToDisplay(message);
                    }
                    else 
                    {
                        this.Write(message);
                    }
                    return;
                }

                // Format the protocol message.
                message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);

                // Send the protocol message.
                this.Write(message);

            }
            catch (Exception e)
            {
                this.Write(message);
                Utils.LogException(e);
            }
        }
        public void WriteToDisplay(string message, Protocol.TextType textType) // write a line with a carriage return, include protocol
        {
            try
            {
                message = message + "\r\n"; // add a carriage return line feed to message

                if (this.protocol != DragonsSpineMain.APP_PROTOCOL || this.PCState == Globals.ePlayerState.PLAYING) // if not using the protocol send the message
                {
                    if (this.PCState == Globals.ePlayerState.PLAYING)
                    {
                        this.WriteToDisplay(message);
                    }
                    else
                    {
                        this.Write(message);
                    }
                    return;
                }

                message = Protocol.GetTextProtocolString(textType, true) + message + Protocol.GetTextProtocolString(textType, false);
                this.Write(message); // send the protocol message

            }
            catch (Exception e)
            {
                this.Write(message);
                Utils.LogException(e);
            }
        }
        public void WriteLine(string message) // write a line with a carriage return line feed, exclude protocol
        {
            Write(message + "\r\n");
        }

        public string FilterMessage(string message)
        {
            message = message.Replace("%n", this.Name);
            message = message.Replace("%r", this.race);
            message = message.Replace("%R", this.race);
            message = message.Replace("%a", Utils.FormatEnumString(this.Alignment.ToString()).ToLower());
            message = message.Replace("%A", Utils.FormatEnumString(this.Alignment.ToString()));
            message = message.Replace("%c", this.classFullName.ToLower());
            message = message.Replace("%C", this.classFullName);
            message = message.Replace("%t", World.CurrentDailyCycle.ToString().ToLower());
            message = message.Replace("%T", World.CurrentDailyCycle.ToString());
            message = message.Replace("%G", this.gender.ToString());
            message = message.Replace("%g", this.gender.ToString().ToLower());
            message = message.Replace("%p", Character.pronoun[Convert.ToInt32(this.gender)].ToLower());
            message = message.Replace("%P", Character.pronoun[Convert.ToInt32(this.gender)]);

            return message;
        }

        public void WriteToDisplay(string message) // text that will be displayed at the end of the round
        {
            // if message is empty, log it and return
            if (message == "")
            {
                Utils.Log("WriteToDisplay message = null.", Utils.LogType.Unknown);
                return;
            }

            // filter message by replacing strings
            message = FilterMessage(message);

            // if receiver of message is not a player, log it
            if (!this.IsPC)
            {
                //Utils.Log(this.GetLogString() + " >> " + message, Utils.LogType.DisplayCreature);
                return;
            }
            else
            {
                // if player is not playing then use WriteLine method
                if (this.PCState != Globals.ePlayerState.PLAYING)
                {
                    this.WriteLine(message, Protocol.TextType.Status);
                    return;
                }

                if (this.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    this.Write(Protocol.GAME_TEXT + message + Protocol.GAME_TEXT_END);
                    return;
                }

                int ctr;
                
                for (ctr = DISPLAY_BUFFER_SIZE - 1; ctr > 0; ctr--)
                    displayBuffer[ctr] = displayBuffer[ctr - 1];

                displayBuffer[0] = message + "\n\r";
                displayText = message + "\n\r";
                for (ctr = 1; ctr < DISPLAY_BUFFER_SIZE; ctr++)
                    displayText = displayBuffer[ctr] + displayText;
                
            }

            //Utils.Log(this.GetLogString() + " >> " + message, Utils.LogType.DisplayPlayer);
        }

        public void clearDisplayBuffer() // clear the display
        {
            displayText = " ";
            for (int ctr = 0; ctr < DISPLAY_BUFFER_SIZE; ctr++)
                displayBuffer[ctr] = " ";
        }

        public string getDisplayText()
        {
            return this.displayText;
        }

        public void ClearDisplay()
        {
            int ctr;
            if (!this.IsPC)
            {
                return;
            }

            for (ctr = DISPLAY_BUFFER_SIZE - 1; ctr > 0; ctr--)
                displayBuffer[ctr] = "";

            displayBuffer[0] = "";
            displayText = "";
            for (ctr = 1; ctr < DISPLAY_BUFFER_SIZE; ctr++)
                displayText += "";
        }

        public int inHand(string itemName)
        {
            int rslt = 0;

            if (this.LeftHand != null)
            {
                if (this.LeftHand.name.ToLower() == itemName || itemName.ToLower() == "left")
                {
                    rslt = 2;
                }
            }

            if (this.RightHand != null)
            {
                if (this.RightHand.name.ToLower() == itemName || itemName.ToLower() == "right")
                {
                    rslt = 1;
                }
            }

            return rslt;

        }

        public Item getFromHand(string itemName)
        {
            Item tmpItem;
            int itemLoc = inHand(itemName);

            if (itemLoc == 0)
            {
                return null;
            }
            else if (itemLoc == 1)
            {
                tmpItem = this.RightHand;
                this.RightHand = null;
                return tmpItem;

            }
            else if (itemLoc == 2)
            {
                tmpItem = this.LeftHand;
                this.LeftHand = null;
                return tmpItem;

            }
            return null;
        }

        public Item getItemFromHand(string itemName)
        {
            int itemLoc = inHand(itemName);

            if (itemLoc == 1)
            {
                return RightHand;
            }
            else if (itemLoc == 2)
            {
                return LeftHand;
            }
            return null;
        }

        public Item getAnyItemInSight(string itemName)
        {
            try
            {
                // hands
                if (this.inHand(itemName) == 1)
                {
                    return RightHand;
                }
                if (this.inHand(itemName) == 2)
                {
                    return LeftHand;
                }

                // ground
                if (Item.FindItemOnGround(itemName.ToLower(), this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z) != null)
                {
                    return Item.FindItemOnGround(itemName.ToLower(), this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);
                }

                // inventory
                foreach (Item item in wearing)
                {
                    if (item != null && item.name.ToLower().Equals(itemName.ToLower()))
                    {
                        return item;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        public int findFirstLeftRing() //returns first finger with a ring on left hand
        {
            if (this.LeftRing1 != null) return 1;
            if (this.LeftRing2 != null) return 2;
            if (this.LeftRing3 != null) return 3;
            if (this.LeftRing4 != null) return 4;
            return 0;
        }

        public int findFirstRightRing() //returns first finger with a ring on right hand
        {
            if (this.RightRing1 != null) return 1;
            if (this.RightRing2 != null) return 2;
            if (this.RightRing3 != null) return 3;
            if (this.RightRing4 != null) return 4;
            return 0;
        }

        public void prepSpell(int spellID)
        {
            preppedSpell = Spell.GetSpell(spellID); // load the prepped spell
            if (preppedSpell == null) // failed to load the spell
            {
                Utils.Log("Failed at Character.prepSpell("+spellID+") Could not find spellID.", Utils.LogType.SystemFailure);
                return;
            }
            preppedSpell.WarmSpell(this); // send warming message
            if (this.IsPC) // log
            {
                Utils.Log(this.GetLogString() + " warmed " + Spell.GetLogString(preppedSpell) + ".", Utils.LogType.SpellWarmingFromPlayer);
            }
            else
            {
                Utils.Log(this.GetLogString() + " warmed " + Spell.GetLogString(preppedSpell) + ".", Utils.LogType.SpellWarmingFromCreature);
            }
        }

        public void RemoveWornItem(Item item)
        {
            if (this.wearing.Contains(item))
            {
                this.wearing.Remove(item);
                Effect.RemoveWornEffectFromCharacter(this, item);
            }
        }

        public Item RemoveWornItem(string name) // remove item worn
        {
            foreach (Item item in this.wearing)
            {
                if (item.name == name)
                {
                    this.wearing.Remove(item);
                    Effect.RemoveWornEffectFromCharacter(this, item);
                    return item;
                }
            }
            return null;
        }

        public Item RemoveFromSack(Item item)
        {
            this.sackList.Remove(item);
            return item;
        }

        public Item RemoveFromSack(string name) // remove item from sack
        {
            foreach (Item item in this.sackList)
            {
                if (item.name == name)
                {
                    this.sackList.Remove(item);
                    return item;
                }
            }
            return null;
        }

        public Item RemoveFromLocker(string name) // remove item from locker
        {
            foreach (Item item in this.lockerList)
            {
                if (item.name == name)
                {
                    this.lockerList.Remove(item);
                    return item;
                }
            }
            return null;
        }

        public Item RemoveFromBelt(string name) // remove item from belt
        {
            foreach (Item item in this.beltList)
            {
                if (item.name == name)
                {
                    this.beltList.Remove(item);
                    return item;
                }
            }
            return null;
        }

        public string rightHandItemName() // for internal use
        {
            string righthand;

            righthand = "          ";

            if (this.RightHand != null)
            {
                righthand = this.RightHand.name;
                if (this.RightHand.name == "crossbow" && this.RightHand.nocked)
                {
                    righthand = righthand + "*";
                }
            }
            return righthand;
        }

        public string leftHandItemName() // for internal use
        {
            string lefthand;

            lefthand = "          ";
            if (this.RightHand != null && this.RightHand.nocked && this.RightHand.name != "crossbow")
            {
                lefthand = "*";
            }
            else
            {
                if (this.LeftHand != null)
                {
                    lefthand = this.LeftHand.name;
                    if (this.LeftHand.name == "crossbow" && this.LeftHand.nocked)
                    {
                        lefthand = lefthand + "*";
                    }
                }
            }
            return lefthand;
        }

        public string GetVisibleArmorName() // for internal use
        {
            string armor = "none";

            foreach (Item item in this.wearing)
            {
                //this checks if there is already something in that location, if so 
                //we cant wear this item, so canWear is false
                if (item != null)
                {
                    if (item.wearLocation == Globals.eWearLocation.Torso)
                    {
                        if (this.animal) { armor = item.armorType.ToString().ToLower(); }
                        else if(this.tanningResult != null && Array.IndexOf(this.tanningResult, item.itemID) == 1)
                        {
                            armor = "none";
                        }
                        else { armor = item.name; }
                    }
                    if (item.wearLocation == Globals.eWearLocation.Back && item.baseType == Globals.eItemBaseType.Armor) // override base armor
                    {
                        if (this.animal) { armor = item.armorType.ToString().ToLower(); }
                        else if (this.tanningResult != null && Array.IndexOf(this.tanningResult, item.itemID) == 1)
                        {
                            armor = "none";
                        }
                        else { armor = item.name; }
                    }
                    if (item.wearLocation == Globals.eWearLocation.Back && item.baseType != Globals.eItemBaseType.Armor)
                    {
                        armor = "scabbard";
                    }
                    if (item.wearLocation == Globals.eWearLocation.Shoulders)
                    {
                        if (this.animal) { armor = item.armorType.ToString().ToLower(); }
                        else if (this.tanningResult != null && Array.IndexOf(this.tanningResult, item.itemID) == 1)
                        {
                            armor = "none";
                        }
                        else { armor = item.name; }
                    }
                }
            }
            return armor;
        }

        public static string ConvertNumberToString(double qty)
        {
            //if (qty == 1) { return "a "; }
            if (qty == 2) { return "two "; }
            if (qty == 3) { return "three "; }
            if (qty == 4) { return "four "; }
            if (qty == 5) { return "five "; }
            if (qty == 6) { return "six "; }
            if (qty > 6 && qty <= 10) { return "several "; }
            if (qty > 10) { return "many "; }
            return "";
        }

        public static string GetLookShortDesc(Item item, double quantity)
        {
            if (item.itemType == Globals.eItemType.Coin)
            {
                if (item.coinValue == 1)
                    return "a coin";
                else return "coins";
            }

            string what = item.shortDesc;

            if (quantity > 1)
            {
                if (what.Contains("scales")) { return "vests made of scales"; }
                if (what.Contains("boots")) { return "pairs of boots"; }
                if (what.Contains("greaves")) { return "pairs of greaves"; }
                if (what.Contains("leggings")) { return "pairs of leggings"; }
                if (what.Contains("gauntlets")) { return "pairs of gauntlets"; }
                if (what.Contains("pantaloons")) { return "pairs of pantaloons"; }
                if (what.Contains("berries")) { return "bunches of berries"; }
                if (what.Contains("threestaff")) { return "threestaves"; }
                if (what.Contains("staff")) { return "staves"; }

                what = item.name + "s";
            }
            else
            {
                if (what.Contains("scales")) { return "a vest made of scales"; }
                if (what.Contains("boots")) { return "a pair of boots"; }
                if (what.Contains("greaves")) { return "a pair of greaves"; }
                if (what.Contains("leggings")) { return "a pair of leggings"; }
                if (what.Contains("gauntlets")) { return "a pair of gauntlets"; }
                if (what.Contains("pantaloons")) { return "a pair of pantaloons"; }
            }

            return what;
        }

        public static void ValidatePlayer(Character ch)
        {
            if (!ch.IsPC) { return; }

            if (ch.PCState == Globals.ePlayerState.PLAYING && ch.CurrentCell == null)
            {
                ch.CurrentCell = Cell.GetCell(ch.FacetID, ch.LandID, ch.MapID, ch.Map.KarmaResX, ch.Map.KarmaResY, ch.Map.KarmaResZ);
                ch.WriteToDisplay("You are currently out of map bounds. Please report this. Attempting to place you at a safe point...");
                if (ch.CurrentCell == null)
                {
                    ch.WriteToDisplay("Current map safe point failed. Placing you at default safe point.");
                    ch.CurrentCell = Cell.GetCell(ch.FacetID, ch.LandID, 0, 53, 7, 0);
                }
                else
                {
                    ch.WriteToDisplay("Safe point placement successful.");
                }
            }

            if (!ch.IsDead)
            {
                if (ch.IsBlind)
                {
                    if (ch.FollowName != "")
                    {
                        ch.BreakFollowMode();
                    }
                }

                if (ch.RightHand != null)
                {
                    if (ch.RightHand.itemType == Globals.eItemType.Corpse)
                    {
                        if (ch.RightHand.itemID >= 600000) // one way to tell if it's a player's corpse
                        {
                            PC pc = PC.GetOnline(ch.RightHand.special);
                            if (pc == null || (pc != null && !pc.IsDead))
                            {
                                ch.WriteToDisplay("The corpse of " + ch.RightHand.special + " disintegrates.");
                                ch.UnEquipRightHand(ch.RightHand);
                            }
                            else
                            {
                                if (pc.CurrentCell != ch.CurrentCell)
                                {
                                    pc.CurrentCell = ch.CurrentCell;
                                    pc.Timeout = Character.INACTIVITY_TIMEOUT;
                                }
                            }
                        }
                    }
                    else if (ch.RightHand.itemType == Globals.eItemType.Coin)
                    {
                        Utils.Log(ch.GetLogString() + " is carrying " + ch.RightHand.coinValue + " coins in their right hand.", Utils.LogType.CoinLogging);
                    }
                }
                if (ch.LeftHand != null)
                {
                    if (ch.LeftHand.itemType == Globals.eItemType.Corpse)
                    {
                        if (ch.LeftHand.itemID > 600000)
                        {
                            PC pc = PC.GetOnline(ch.LeftHand.special);
                            if (pc == null || (pc != null && !pc.IsDead))
                            {
                                ch.WriteToDisplay("The corpse of " + ch.LeftHand.special + " disintegrates.");
                                ch.UnEquipLeftHand(ch.LeftHand);
                            }
                            else
                            {
                                if (pc.CurrentCell != ch.CurrentCell)
                                {
                                    pc.CurrentCell = ch.CurrentCell;
                                    pc.Timeout = Character.INACTIVITY_TIMEOUT;
                                }
                            }
                        }
                    }
                    else if (ch.LeftHand.itemType == Globals.eItemType.Coin)
                    {
                        Utils.Log(ch.GetLogString() + " is carrying " + ch.LeftHand.coinValue + " coins in their left hand.", Utils.LogType.CoinLogging);
                    }
                }
            }

            #region Set High Skills
            if (ch.mace > ch.highMace) { ch.highMace = ch.mace; }
            if (ch.bow > ch.highBow) { ch.highBow = ch.bow; }
            if (ch.flail > ch.highFlail) { ch.highFlail = ch.flail; }
            if (ch.dagger > ch.highDagger) { ch.highDagger = ch.dagger; }
            if (ch.rapier > ch.highRapier) { ch.highRapier = ch.rapier; }
            if (ch.twoHanded > ch.highTwoHanded) { ch.highTwoHanded = ch.twoHanded; }
            if (ch.staff > ch.highStaff) { ch.highStaff = ch.staff; }
            if (ch.shuriken > ch.highShuriken) { ch.highShuriken = ch.shuriken; }
            if (ch.sword > ch.highSword) { ch.highSword = ch.sword; }
            if (ch.threestaff > ch.highThreestaff) { ch.highThreestaff = ch.threestaff; }
            if (ch.halberd > ch.highHalberd) { ch.highHalberd = ch.halberd; }
            if (ch.unarmed > ch.highUnarmed) { ch.highUnarmed = ch.unarmed; }
            if (ch.thievery > ch.highThievery) { ch.highThievery = ch.thievery; }
            if (ch.magic > ch.highMagic) { ch.highMagic = ch.magic; }
            if (ch.bash > ch.highBash) { ch.highBash = ch.bash; }
            #endregion

            if (ch.Shielding < 0)
            {
                ch.Shielding = 0;
                //Utils.Log(ch.GetLogString() + " shield is below 0. ch.Shielding = " + ch.Shielding, Utils.LogType.SystemWarning);
            }

            if (!ch.IsImmortal)
            {
                #region Validate Encumbrance
                ch.encumbrance = ch.GetEncumbrance();
                #endregion
            }
            else
            {
                ch.encumbrance = 0;
            }

            if (Rules.BreakHideSpell(ch))
                ch.IsHidden = false;

            if (ch.ImpLevel < Globals.eImpLevel.GM)
            {
                #region Confirm Not Invisible If Not Dead
                if (!ch.IsDead && ch.ImpLevel < Globals.eImpLevel.GM)
                {
                    if (ch.IsInvisible)
                    {
                        ch.IsInvisible = false;
                    }
                }
                #endregion

                #region Validate Maximum Shield
                if (ch.Shielding > ch.Land.MaxShielding)
                    ch.Shielding = ch.Land.MaxShielding;
                #endregion


                #region Validate Maximum Hits, Mana and Stamina
                int maximumHits = Rules.GetMaximumHits(ch);
                int maximumMana = Rules.GetMaximumMana(ch);
                int maximumStamina = Rules.GetMaximumStamina(ch);

                if (ch.HitsDoctored > 0)
                {
                    maximumHits += ch.HitsDoctored;
                }

                if (ch.BaseProfession == ClassType.Knight)
                    maximumMana = 3;

                //validate character hp, stamina, mana
                if (ch.HitsMax > maximumHits)
                {
                    ch.HitsMax = maximumHits;
                    ch.Hits = ch.HitsFull;
                }
                if (ch.StaminaMax > maximumStamina)
                {
                    ch.StaminaMax = maximumStamina;
                    ch.Stamina = ch.StaminaFull;
                }
                if (ch.ManaMax > maximumMana)
                {
                    ch.ManaMax = maximumMana;
                    ch.Mana = ch.ManaFull;
                }
                #endregion
            }

            if (ch.ImpLevel >= Globals.eImpLevel.GM && ch.immortal) // keeps ghods at max hp, stamina, mana if immortal is true
            {
                ch.Hits = ch.HitsFull;
                ch.Stamina = ch.StaminaFull;
                ch.Mana = ch.ManaFull;
                ch.Stunned = 0;
                ch.floating = 4;
                ch.encumbrance = 0;
            }

            Character.SetCharacterVisualKey(ch);
        }

        public bool WearItem(Item item)
        {
            if (item != null)
            {
                if (item.IsAttunedToOther(this))
                {
                    string isare = "is";
                    if (item.name.ToLower().EndsWith("s")) { isare = "are"; }
                    this.WriteToDisplay("The " + item.name + " " + isare + " soulbound to another individual.");
                    return false;
                }

                if (!item.AlignmentCheck(this))
                {
                    this.WriteToDisplay("The " + item.name + " does not match your alignment.");
                    return false;
                }

                this.wearing.Add(item);

                if (item.attuneType == Globals.eAttuneType.Wear) { item.AttuneItem(this); }

                Effect.AddWornEffectToCharacter(this, item); // add worn effect

                if (item == this.RightHand) // empty the hand the item came from
                    this.UnEquipRightHand(item);
                else if (item == this.LeftHand)
                    this.UnEquipLeftHand(item);
                return true;
            }
            else return false;
        }

        public bool BeltItem(Item item)
        {
            if (item != null)
            {
                this.beltList.Add(item);

                if (item.attuneType == Globals.eAttuneType.Take) { item.AttuneItem(this); }

                return true;
            }
            else return false;
        }

        public bool AddGoldToSack(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            SackGold += amount;
            return true;
        }

        public bool SackItem(Item item)
        {
            
            if (item != null)
            {
                if (item.itemType == Globals.eItemType.Coin)
                {
                    foreach (Item coins in new List<Item>(this.sackList))
                    {
                        if (coins.itemType == Globals.eItemType.Coin)
                        {
                            coins.coinValue += item.coinValue;
                            SackGold += item.coinValue;
                            return true;
                        }
                    }
                    this.sackList.Add(item);
                    return true;
                }

                if (this.SackCountMinusGold >= MAX_SACK)
                {
                    return false;
                }

                if (item.attuneType == Globals.eAttuneType.Take) { item.AttuneItem(this); }

                this.sackList.Add(item);

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EquipRightHand(Item item)
        {
            if (item != null)
            {
                if (this.LeftHand != null && this.LeftHand.nocked && this.LeftHand.name != "crossbow") // un-nock opposite hand if not crossbow
                {
                    this.LeftHand.nocked = false;
                }
                this.RightHand = item; // place the item in character's right hand
                if (item.attuneType == Globals.eAttuneType.Take) { item.AttuneItem(this); } // attune the item
                if (!item.IsAttunedToOther(this) && item.AlignmentCheck(this)) // add effects
                {
                    if (item.effectType.Length > 0 && item.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.AddWornEffectToCharacter(this, item);
                    }
                }
                if (item.size == Globals.eItemSize.Belt_Large_Slot_Only || item.size == Globals.eItemSize.No_Container)
                {
                    if (this.IsHidden)
                    {
                        if (this.effectList.ContainsKey(Effect.EffectType.Hide_In_Shadows))
                        {
                            if (!this.effectList[Effect.EffectType.Hide_In_Shadows].IsPermanent)
                            {
                                this.IsHidden = false;
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public bool EquipLeftHand(Item item)
        {
            if (item != null)
            {
                if (this.RightHand != null && this.RightHand.nocked && this.RightHand.name != "crossbow") // un-nock opposite hand if not crossbow
                {
                    this.RightHand.nocked = false;
                } 
                this.LeftHand = item; // place the item in character's left hand
                if (item.attuneType == Globals.eAttuneType.Take) { item.AttuneItem(this); } // attune the item

                if (!item.IsAttunedToOther(this) && item.AlignmentCheck(this)) // add effects if not soulbound to another character
                {
                    if (item.effectType.Length > 0 && item.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.AddWornEffectToCharacter(this, item);
                    }
                }
                if (item.size == Globals.eItemSize.Belt_Large_Slot_Only || item.size == Globals.eItemSize.No_Container)
                {
                    if (this.IsHidden)
                    {
                        if (this.effectList.ContainsKey(Effect.EffectType.Hide_In_Shadows))
                        {
                            if (!this.effectList[Effect.EffectType.Hide_In_Shadows].IsPermanent)
                            {
                                this.IsHidden = false;
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public bool UnEquipRightHand(Item item)
        {
            if (item != null)
            {
                this.RightHand = null; // empty right hand
                if (!item.IsAttunedToOther(this) && item.AlignmentCheck(this))
                {
                    if (item.effectType.Length > 0 && item.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.RemoveWornEffectFromCharacter(this, item);
                    }
                }
                return true;
            }
            return false;
        }

        public bool UnEquipLeftHand(Item item)
        {
            if (item != null)
            {
                this.LeftHand = null; // empty left hand
                if (!item.IsAttunedToOther(this) && item.AlignmentCheck(this))
                {
                    if (item.effectType.Length > 0 && item.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.RemoveWornEffectFromCharacter(this, item);
                    }
                }
                return true;
            }
            return false;
        }

        public bool EquipEitherHand(Item item)
        {
            if (this.RightHand == null)
            {
                return this.EquipRightHand(item);
            }
            else if (this.LeftHand == null)
            {
                return this.EquipLeftHand(item);
            }
            else
            {
                this.WriteToDisplay("Your hands are full.");
                return false;
            }
        }

        public int GetFirstFreeHand()
        {
            if (this.RightHand == null)
            {
                return 1;
            }
            if (this.LeftHand == null)
            {
                return 2;
            }
            return 0;
        }

        public bool EquipEitherHandOrDrop(Item item)
        {
            if (this.RightHand == null)
            {
                return this.EquipRightHand(item);
            }
            else if (this.LeftHand == null)
            {
                return this.EquipLeftHand(item);
            }
            else
            {
                this.CurrentCell.Add(item);
                return false;
            }
        }

        public List<Item> GetRings()
        {
            List<Item> ringsList = new List<Item>();

            if (this.RightRing1 != null)
            {
                ringsList.Add(this.RightRing1);
            }
            if (this.RightRing2 != null)
            {
                ringsList.Add(this.RightRing2);
            }
            if (this.RightRing3 != null)
            {
                ringsList.Add(this.RightRing3);
            }
            if (this.RightRing4 != null)
            {
                ringsList.Add(this.RightRing4);
            }
            if (this.LeftRing1 != null)
            {
                ringsList.Add(this.LeftRing1);
            }
            if (this.LeftRing2 != null)
            {
                ringsList.Add(this.LeftRing2);
            }
            if (this.LeftRing3 != null)
            {
                ringsList.Add(this.LeftRing3);
            }
            if (this.LeftRing4 != null)
            {
                ringsList.Add(this.LeftRing4);
            }
            return ringsList;
        }        
        
        public Character CloneCharacter()
        {
            return (Character)this.MemberwiseClone();
        }

        public string GetLogString()
        {
            try
            {
                if (this.IsPC)
                {
                    return "[PCID: " + this.PlayerID + "] " + this.Name + " [" + Utils.FormatEnumString(this.Alignment.ToString()) + " " + Utils.FormatEnumString(this.BaseProfession.ToString()) + "(" + this.Level + ")] (" + this.account + ") (" +
                        this.Land.ShortDesc + " - " + this.Map.Name + " " + this.X + ", " + this.Y + ", " + this.Z + ")";
                }
                return "[NPCID: " + this.npcID + " | WorldNPCID: " + this.worldNpcID + "] " + this.Name + " [" + Utils.FormatEnumString(this.Alignment.ToString()) + " " + Utils.FormatEnumString(this.BaseProfession.ToString()) + "(" + this.Level + ")] (" + this.Land.ShortDesc +
                    " - " + this.Map.Name + " " + this.X + ", " + this.Y + ", " + this.Z + ")";
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                try
                {
                    return "NPCID: " + this.npcID + " (null)";
                }
                catch
                {
                    return "(null)";
                }
            }
        }

        public void ResetAFK()
        {
            this.afk = false;
            this.afkMessage = "I am currently A.F.K. (Away From Keyboard).";
        }

        public static void fillFriendsList(Character ch, int[] array)
        {
            ch.friends = "";
            if (array == null)
                return;
            for (int a = 0; a < array.Length; a++)
            {
                ch.friendsList[a] = array[a];
                ch.friends += ch.friendsList[a] + Protocol.ASPLIT;
            }
            ch.friends = ch.friends.Substring(0, ch.friends.Length - Protocol.ASPLIT.Length); // remove last VSPLIT
        }

        public static void fillIgnoreList(Character ch, int[] array)
        {
            ch.ignored = ""; // clear ignore names list
            for (int a = 0; a < array.Length; a++) // loop to fill ignore list and ignore names list
            {
                ch.ignoreList[a] = array[a];
                ch.ignored += ch.ignoreList[a] + Protocol.ASPLIT;
            }
            ch.ignored = ch.ignored.Substring(0, ch.ignored.Length - Protocol.ASPLIT.Length); // remove last VSPLIT
        }

        public string getAgeDescription(bool fullDescription)
        {
            int index = 0;

            if (this.Age < World.AgeCycles[0])
            {
                index = 0;
            }
            else if (this.Age >= World.AgeCycles[0] && this.Age < World.AgeCycles[1])
            {
                index = 1;
            }
            else if (this.Age >= World.AgeCycles[1] && this.Age < World.AgeCycles[2])
            {
                index = 2;
            }
            else if (this.Age >= World.AgeCycles[2] && this.Age < World.AgeCycles[3])
            {
                index = 3;
            }
            else if (this.Age >= World.AgeCycles[3] && this.Age < World.AgeCycles[4])
            {
                index = 4;
            }
            else
            {
                index = 5;
            }

            string ageDescription;

            if (this.IsWyrmKin)
            {
                ageDescription = World.age_wyrmKin[index];
            }
            else
            {
                ageDescription = World.age_humanoid[index];
            }

            if (!fullDescription)
            {
                ageDescription = ageDescription.Substring(ageDescription.IndexOf(" "));
            }
            return ageDescription;
        }        

        public void BreakFollowMode()
        {
            if (!this.IsDead && this.IsPC)
            {
                this.WriteToDisplay("You are no longer following " + this.FollowName + ".");
            }
            this.FollowName = "";
        }

        public Item GetInventoryItem(Globals.eWearLocation wearLoc)
        {
            foreach (Item item in this.wearing)
            {
                if (item.wearLocation == wearLoc)
                {
                    return item;
                }
            }
            return null;
        }

        public Item GetInventoryItem(Globals.eWearLocation wearLoc, Globals.eWearOrientation wearOrientation)
        {
            foreach (Item item in this.wearing)
            {
                if (item.wearLocation == wearLoc && item.wearOrientation == wearOrientation)
                {
                    return item;
                }
            }
            return null;
        }

        public Item GetSpecificRing(bool right, int number)
        {
            switch (number)
            {
                case 1:
                    if (right)
                    {
                        return this.RightRing1;
                    }
                    return this.LeftRing1;
                case 2:
                    if (right)
                    {
                        return this.RightRing2;
                    }
                    return this.LeftRing2;
                case 3:
                    if (right)
                    {
                        return this.RightRing3;
                    }
                    return this.LeftRing3;
                case 4:
                    if (right)
                    {
                        return this.RightRing4;
                    }
                    return this.LeftRing4;
                default:
                    return null;
            }
        }

        public void SetSpecificRing(bool right, int number, Item ring)
        {
            switch (number)
            {
                case 1:
                    if (right)
                    {
                        this.RightRing1 = ring;
                    }
                    this.LeftRing1 = ring;
                    break;
                case 2:
                    if (right)
                    {
                        this.RightRing2 = ring;
                    }
                    this.LeftRing2 = ring;
                    break;
                case 3:
                    if (right)
                    {
                        this.RightRing3 = ring;
                    }
                    this.LeftRing3 = ring;
                    break;
                case 4:
                    if (right)
                    {
                        this.RightRing4 = ring;
                    }
                    this.LeftRing4 = ring;
                    break;
            }
        }

        public double GetEncumbrance()
        {
            double encumbrance = 0;

            foreach (Item rItem in this.GetRings())
            {
                encumbrance += rItem.weight;
            }

            foreach (Item sItem in this.sackList)
            {
                if (sItem.itemType != Globals.eItemType.Coin)
                {
                    encumbrance += sItem.weight;
                }
                else
                {
                    encumbrance += (int)(sItem.coinValue / 100);
                    //if(this.IsPC && sItem.coinValue >= 10000)
                    //    Utils.Log(this.GetLogString() + " is carrying " + sItem.coinValue + " coins in their sack.", Utils.LogType.CoinLogging);
                }
            }

            foreach (Item bItem in this.beltList)
            {
                encumbrance += bItem.weight;
            }

            foreach (Item wItem in this.wearing)
            {
                encumbrance += wItem.weight;
            }

            if (this.RightHand != null)
            {
                if (this.RightHand.itemType != Globals.eItemType.Coin)
                {
                    encumbrance += this.RightHand.weight;
                }
                else
                {
                    encumbrance += (int)(this.RightHand.coinValue / 100);
                }
            }

            if (this.LeftHand != null)
            {
                if (this.LeftHand.itemType != Globals.eItemType.Coin)
                {
                    encumbrance += this.LeftHand.weight;
                }
                else
                {
                    encumbrance += (int)(this.LeftHand.coinValue / 100);
                }
            }

            return encumbrance;
        }

        public Quest GetQuest(int questID)
        {
            foreach (Quest q in this.questList)
            {
                if (q.QuestID == questID)
                {
                    return q;
                }
            }
            return null;
        }

        public static string RaceToString(string race)
        {
            if (race == "Barbarian") { return "the plains"; }
            return race;
        }

        public int GetID()
        {
            if (this.isPC) { return this.playerID; }
            return this.worldNpcID;
        }

        public static void SetCharacterVisualKey(Character ch)
        {
            if (ch.IsPC)
            {
                ch.visualKey = ch.gender.ToString().ToLower() + "_" + ch.BaseProfession.ToString().ToLower() + "_pc_" + ch.colorChoice.ToLower();

                if (ch.IsDead)
                    ch.visualKey = "ghost";

                if (ch.Name == "rat")
                    ch.visualKey = "rat";
                else if (ch.Name == "toad")
                    ch.visualKey = "toad";
            }
        }
    }
}