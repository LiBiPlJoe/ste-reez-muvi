using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.GameObjects
{
    public static class ObjectManager
    {
        /// <summary>
        /// Holds a list of all players at the login prompt.
        /// </summary>
        public static List<GamePlayer> LoginList = new List<GamePlayer>();

        /// <summary>
        /// Holds a list of all players in character generation.
        /// </summary>
        public static List<GamePlayer> CharGenList = new List<GamePlayer>();

        /// <summary>
        /// Holds a list of all players at the menu system.
        /// </summary>
        public static List<GamePlayer> MenuList = new List<GamePlayer>();

        /// <summary>
        /// Holds a list of all players in conference rooms.
        /// </summary>
        public static List<GamePlayer> ConfList = new List<GamePlayer>();

        /// <summary>
        /// Holds a list of all GameLiving objects currently in the world.
        /// </summary>
        public static List<GameLiving> AllLivingList = new List<GameLiving>();

        /// <summary>
        /// Holds a list of all GamePlayer objects currently in the world.
        /// </summary>
        public static List<GamePlayer> PlayerList = new List<GamePlayer>();

        /// <summary>
        /// Holds a list of all GameNPC objects currently in the world.
        /// </summary>
        public static List<GameNPC> NPCList = new List<GameNPC>();

        /// <summary>
        /// Holds a list of all Game objects currently in the world.
        /// </summary>
        public static List<GameObject> ObjectList = new List<GameObject>();
    }
}
