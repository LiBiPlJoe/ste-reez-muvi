//using System;
//using System.Collections.Generic;
//using System.Text;
//
//namespace DragonsSpine.GameObjects
//{
//    public class GameWearable : GameItem
//    {
//        /// <summary>
//        /// The item recall properties structure.
//        /// </summary>
//        protected struct RecallProperties
//        {
//            public bool IsRecall;
//            public bool WasRecall;
//            public short RecallLand;
//            public short RecallMap;
//            public int RecallX;
//            public int RecallY;
//            public int RecallZ;
//        }
//
//        /// <summary>
//        /// Holds where the item may be worn.
//        /// </summary>
//        protected Globals.eWearLocation m_wearLocation;
//
//        /// <summary>
//        /// Holds the type of armor the item is.
//        /// </summary>
//        protected Globals.eArmorType m_armorType;
//
//        /// <summary>
//        /// Holds the recall properties for the item.
//        /// </summary>
//        protected RecallProperties m_recallProps;
//
//        /// <summary>
//        /// Holds the slot the item may be worn on.
//        /// </summary>
//        protected Globals.eInventorySlot m_slot;
//        protected Globals.eWearOrientation m_orientation;
//
//        /// <summary>
//        /// Gets the eInventorySlot the item may be worn on.
//        /// </summary>
//        public Globals.eInventorySlot Slot
//        {
//            get { return m_slot; }
//            set { m_slot = value; }
//        }
//        public Globals.eWearOrientation WearOrientation
//        {
//            get { return m_orientation; }
//            set { m_orientation = value; }
//        }
//        public Globals.eWearLocation WearLocation
//        {
//            get { return m_wearLocation; }
//        }
//        public GameWearable()
//            : base()
//        {
//            m_wearLocation = Globals.eWearLocation.None;
//            m_armorType = Globals.eArmorType.None;
//            m_recallProps = new RecallProperties();
//            m_slot = Globals.eInventorySlot.None;
//        }
//
//        public static void LoadGameWearable(ref GameWearable gitem, System.Data.DataRow dr)
//        {
//            GameItem item = gitem as GameItem;
//            GameItem.LoadGameItem(ref item, dr);
//            gitem = item as GameWearable;
//            gitem.m_wearLocation = (Globals.eWearLocation)Enum.Parse(typeof(Globals.eWearLocation), dr["wearLocation"].ToString(), true);
//            gitem.m_armorType = (Globals.eArmorType)Enum.Parse(typeof(Globals.eArmorType), dr["armorType"].ToString(), true);
//            if (Convert.ToBoolean(dr["isRecall"]))
//                LoadRecallProperties(ref gitem, dr);
//        }
//
//        public static void LoadRecallProperties(ref GameWearable gitem, System.Data.DataRow dr)
//        {
//            try
//            {
//                gitem.m_recallProps.IsRecall = Convert.ToBoolean(dr["isRecall"]);
//                gitem.m_recallProps.WasRecall = Convert.ToBoolean(dr["wasRecall"]);
//                gitem.m_recallProps.RecallLand = Convert.ToInt16(dr["recallLand"]);
//                gitem.m_recallProps.RecallMap = Convert.ToInt16(dr["recallMap"]);
//                gitem.m_recallProps.RecallX = Convert.ToInt32(dr["recallX"]);
//                gitem.m_recallProps.RecallY = Convert.ToInt32(dr["recallY"]);
//                gitem.m_recallProps.RecallZ = Convert.ToInt32(dr["recallZ"]);
//            }
//            catch (Exception)
//            {
//                Utils.Log("Error in wearable.LoadRecallProperties", Utils.LogType.Exception);
//            }
//        }
//    }
//}
