

namespace DragonsSpine
{
	using System;
	using System.Collections;
	using System.IO;
	
	public static class Food
	{
		public static void EatFood(Item food, Character ch)
		{
            ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.EatFood));

			string eatText = "You eat "+food.longDesc+".";
			switch(food.special.ToLower())
			{
				case "balmberry":
					ch.WriteToDisplay(eatText);
					if(ch.HitsFull - ch.Hits > 6){ch.Hits += 6;}
					else{ch.Hits = ch.HitsFull;}
					break;
				case "manaberry":
					ch.WriteToDisplay(eatText+" Mmmm. Those were very sweet!");
					if(ch.ManaFull - ch.Mana > 6){ch.Mana += 6;}
					else{ch.Mana = ch.ManaFull;}
					break;
				case "poisonberry":
					ch.WriteToDisplay(eatText+" Ugh those tasted horrible!");
					ch.Poisoned = 3;
					break;
				case "stamberry":
					ch.WriteToDisplay(eatText);
					if(ch.StaminaFull - ch.Stamina > 6){ch.Stamina += 6;}
					else{ch.Stamina = ch.StaminaFull;}
					break;
				case "neutralize":
					if(ch.Poisoned > 0)
					{
						ch.Poisoned = 0;
						ch.WriteToDisplay(eatText+" Your poison has been neutralized.");
					}
					else
					{
						ch.WriteToDisplay(eatText);
					}
					break;
			}
		}
	}
}
