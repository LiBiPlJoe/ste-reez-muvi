namespace DragonsSpine
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using System.IO;

	/// <summary>
	/// Summary description for Book.
	/// </summary>
	public class Book : Item
	{
        public enum BookType { None, Normal, Spellbook, Scroll }
		
		public Book() : base()
		{
            currentPage = 0;
		}

        public Book(Item item) : base()
        {
            this.catalogID = item.catalogID;
            this.notes = item.notes;
            this.combatAdds = item.combatAdds;
            this.itemID = item.itemID;
            this.worldItemID = item.worldItemID;
            this.itemType = item.itemType;
            this.baseType = item.baseType;
            this.name = item.name;
            this.shortDesc = item.shortDesc;
            this.longDesc = item.longDesc;
            this.visualKey = item.visualKey;
            this.wearLocation = item.wearLocation;
            this.weight = item.weight;
            this.coinValue = item.coinValue;
            this.size = item.size;
            this.effectType = item.effectType;
            this.effectAmount = item.effectAmount;
            this.effectDuration = item.effectDuration;
            this.special = item.special;
            this.minDamage = item.minDamage;
            this.maxDamage = item.maxDamage;
            this.skillType = item.skillType;
            this.vRandLow = item.vRandLow;
            this.vRandHigh = item.vRandHigh;
            this.key = item.key;
            this.isRecall = item.isRecall;
            this.alignment = item.alignment;
            this.spell = item.spell;
            this.spellPower = item.spellPower;
            this.charges = item.charges;
            this.attackType = item.attackType;
            this.blueglow = item.blueglow;
            this.flammable = item.flammable;
            this.fragile = item.fragile;
            this.lightning = item.lightning;
            this.returning = item.returning;
            this.silver = item.silver;
            this.attuneType = item.attuneType;
            this.figExp = item.figExp;
            this.armorClass = item.armorClass;
            this.armorType = item.armorType;
            //
            this.bookType = item.bookType;
            this.maxPages = item.maxPages;
            this.pages = item.pages;
        }

		public string ReadPage()
		{
			//Will read the same page over and over again

            int mp = pages.Length / 2; // do this to filter out the blank pages

			if(this.currentPage + 1 > mp)
			{
				this.currentPage = 0;
			}

			if(this.currentPage < 0)
			{
				this.currentPage = 0;
			}

            List<string> bookPages = new List<string>();

            foreach (string text in this.pages)
            {
                if (text == "")
                {
                    continue;
                }
                bookPages.Add(text);
            }

            return bookPages[this.currentPage];
		}

		//Turns to the next page
		public string nextPage(int pageNumber)
		{
			if(this.currentPage != this.maxPages)
			{
				this.currentPage += 1;
				return this.pages[this.currentPage];
			}
			else
			{
				return "You are at the end of the book.";
			}
			
		}

		//Turns to the previous page
		public string prevPage(int pageNumber)
		{
			if(this.currentPage > 1)
			{
				this.currentPage -= 1;
				return this.pages[this.currentPage];
			}
			else
			{
				return "You are at the beginning of the book.";
			}
		}
	}
}
