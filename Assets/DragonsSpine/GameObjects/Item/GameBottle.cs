//using System;
//using System.Collections.Generic;
//using System.Text;
//
//namespace DragonsSpine.GameObjects
//{
//    public class GameBottle : GameItem
//    {
//        /// <summary>
//        /// Holds the description given to a player when the the bottle is drank.
//        /// </summary>
//        protected string m_drinkDesc;
//
//        /// <summary>
//        /// Holds the description given when a player looks at the bottle.
//        /// </summary>
//        protected string m_fluidDesc;
//        protected Effect.EffectType m_effect;
//        /// <summary>
//        /// Holds whether the bottle is open.
//        /// </summary>
//        protected bool m_open;
//
//        public string FluidDesc
//        {
//            get { return m_fluidDesc; }
//        }
//        public string DrinkDesc
//        {
//            get { return m_drinkDesc; }
//        }
//        public Effect.EffectType EffectType
//        {
//            get { return m_effect; }
//            set { m_effect = value; }
//        }
//        public GameBottle()
//            : base()
//        {
//            m_drinkDesc = "";
//            m_fluidDesc = "";
//            m_open = false;
//        }
//
//        public static void LoadGameBottle(ref GameBottle gbottle, System.Data.DataRow dr)
//        {
//            GameItem bot = gbottle as GameItem;
//            GameItem.LoadGameItem(ref bot, dr);
//            gbottle = bot as GameBottle;
//            gbottle.m_size = Globals.eItemSize.Sack_Only;
//            gbottle.m_drinkDesc = dr["drinkDesc"].ToString();
//            gbottle.m_fluidDesc = dr["fluidDesc"].ToString();
//        }
//    }
//}
