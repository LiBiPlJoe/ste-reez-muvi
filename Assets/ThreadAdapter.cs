﻿using UnityEngine;
using System;
using System.Collections;

internal class ThreadAdapter : MonoBehaviour {

    private volatile bool waitCall = true;

    public static int x = 0;

    //this will hold the reference to delegate which will be
    //executed on ui thread
    private volatile Action theAction;

    public void Awake() {
        DontDestroyOnLoad(gameObject);
        this.name = "ThreadAdapter-" + (x++);
    }

    public IEnumerator Start() {
        while (waitCall) {
            yield return new WaitForSeconds(.05f);
        }
        theAction();
        Destroy(gameObject);
    }

    public void ExecuteOnUi(Action action) {
        this.theAction = action;
        waitCall = false;
    }
}