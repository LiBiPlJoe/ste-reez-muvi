﻿using UnityEngine;
using System.Collections;

public class NpcScript : MonoBehaviour {

    public CellLoc cell;
	public Vector3 newPosition, newScale;
	Vector3 oldPosition, oldScale;
	float timeSinceStable;
	public bool toBeSeen; // independent of local concerns, does the UI want to see me?

	public int npcId;
	public string name;
	public int lastActiveRound;
	private int hitsFull;
	private int hits;
	public bool presumedDead; // Gone too quick to register as dead in the DB?
	private bool hasMostHated;
	private int mostHatedId;
	public Vector3 hatedLocation;

	public Material liveMaterial;
	public Material deadMaterial;
	public Transform damagePrefab;
	public Transform hatredPrefab;

    private Material currMaterial;
    public Material CurrMaterial {
        get { return currMaterial; }
        set {
            if (currMaterial != value && myRend != null) {
                currMaterial = value;
                myRend.material = currMaterial;
                myRend.material.mainTextureScale = new Vector2(-1f,-1f);
            }
        }
    }

    private Renderer myRend;
	private Transform myDamage;
	private Renderer myDamageRend;
	private Transform myHatred;
	private Renderer myHatredRend;

	public NpcManager npcManager;

	public int Hits {
		get { return hits; }
		set {
			if (hits != value) {
				hits = value;
				StopCoroutine("Damage");
				StartCoroutine("Damage");
			}
		}
	}

	public int HitsFull {
		get { return hitsFull; }
		set {
			if (hitsFull != value) {
				hitsFull = value;
				StopCoroutine("Damage");
				StartCoroutine("Damage");
			}
		}
	}

	public float Health {
		get { return Mathf.Clamp((float) hits / (float) hitsFull, 0f, 1f); }
	}
	
	public bool HasMostHated {
		get { return hasMostHated; }
		set {
			if (hasMostHated != value) {
				hasMostHated = value;
				StopCoroutine("Hatred");
				StartCoroutine("Hatred");
			}
		}
	}
	public int MostHatedId {
		get { return mostHatedId; }
		set {
			if (mostHatedId != value) {
				mostHatedId = value;
				StopCoroutine("Hatred");
				StartCoroutine("Hatred");
			}
		}
	}

	void Awake () {
		Reset();
        myRend = GetComponent<Renderer>();
	}

	public void Reset() {
		oldPosition = newPosition = transform.position;
		oldScale = newScale = transform.localScale;
		timeSinceStable = 0f;
		toBeSeen = true;
		npcId = 0;
		name = "undefined";
		lastActiveRound = 0;
		hitsFull = 1; hits = int.MaxValue; // So that during initialization our health is always full.
		presumedDead = false;
		hasMostHated = false;
		mostHatedId = 0;
	}

	void Update () {
		timeSinceStable += Time.deltaTime;
		if (transform.position != newPosition) {
			transform.position = Vector3.Slerp(oldPosition,newPosition,timeSinceStable);
			if (timeSinceStable >= 1f) transform.position = newPosition;
		} else {
			if (transform.localScale != newScale) {
				transform.localScale = Vector3.Slerp(oldScale,newScale,timeSinceStable - 1f);
				if (timeSinceStable >= 2f) transform.localScale = newScale;
			} else {
					timeSinceStable = 0f;
					oldPosition = transform.position;
					oldScale = transform.localScale;
			}
		}

		// disappear if desired
		myRend.enabled = toBeSeen;
	}

	public void UpdateMaterial(int currRound) {
		if (lastActiveRound >= currRound-1 && Health > 0f) { // one round leeway in case we catch the DB in mid-update
            presumedDead = false;
			CurrMaterial = liveMaterial;
        } else {
			presumedDead = true;
			CurrMaterial = deadMaterial;
		}
	}

	IEnumerator Damage () {
		while (Health < 1f && !presumedDead && hits > 0f) {
			if (myDamage == null) {
				myDamage = Instantiate(damagePrefab, 
				                       Vector3.zero,
				                       Quaternion.identity) as Transform;
				myDamage.parent = this.transform;
				myDamage.localPosition = new Vector3(0f, 0f, -0.1f);
				myDamageRend = myDamage.GetComponent<Renderer>();
			}
			myDamage.localScale = new Vector3(1f, 1f - Health, 1f);
			myDamageRend.enabled = toBeSeen;
			yield return null;
		}
		if (myDamage != null) myDamageRend.enabled = false;
	}
	
	IEnumerator Hatred () {
		while (hasMostHated && !presumedDead && hits > 0f) {
			if (myHatred == null) {
				myHatred = Instantiate(hatredPrefab,
				                       Vector3.zero,
				                       Quaternion.identity) as Transform;
				myHatred.parent = this.transform;
				myHatred.localPosition = new Vector3(0f, 0f, -0.2f);
				myHatredRend = myHatred.GetComponent<Renderer>();
			}
			hatedLocation = npcManager.locateNpcV3(mostHatedId);
			
			Vector3 hatredDir = hatedLocation - transform.position;
			hatredDir.z = 0f;
			hatredDir = hatredDir.normalized;
			
			Quaternion qDir = new Quaternion();
			qDir.SetLookRotation(hatredDir, new Vector3(0f, 0f, 1f));
			myHatred.rotation = qDir;
			
			myHatred.localScale = new Vector3(0.3f, 0.1f, 1f);
			Vector3 startPos = transform.position + hatredDir * transform.localScale.x * 0.5f;
			Vector3 endPos = hatedLocation - hatredDir * transform.localScale.x * 0.5f;
			startPos.z -= 0.2f; endPos.z -= 0.2f;
			myHatred.position = Vector3.Lerp(startPos, endPos, Time.time % 1f);
			myHatredRend.enabled = toBeSeen;

			yield return null;
		} 
		if (myHatred != null) {
			myHatredRend.enabled = false;
		}
	}


}

