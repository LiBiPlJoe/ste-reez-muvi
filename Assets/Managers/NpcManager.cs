﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using System.Text;

using DragonsSpine;


public class NpcManager : MonoBehaviour {
    public ObjectPooler npcPooler;

	public MapManager mapManager;
	public NpcScript npcScript;
    private int mapNum = -1;
    public int MapNum {
        get { return mapNum; }
        set {
            if (mapNum != value) {
                mapNum = value;
                StopCoroutine("LoadNpcs");
                if (mapNum != -1) StartCoroutine("LoadNpcs");
            }
        }
    }

	public float nextDBUpdate;
	public float dbUpdateDelta = 2f;
    public bool dbUpdateComplete;
    public List<NPCLite> npcsFromDb;

	System.Random myRandom = new System.Random();

    public List<int> activePcs;
	private Dictionary<int,NpcScript> npcScripts;
	private Dictionary<int,CellLoc> npcLocations;
	private Dictionary<CellLoc,List<int>> cellsContainingNpcs;
    private Dictionary<String, Material> npcLiveMaterials;
    private Dictionary<String, Material> npcDeadMaterials;

	public int NumNpcScripts {
		get { 
            if (this.npcScripts != null) return this.npcScripts.Count;
            return 0;
        }
	}
	public int numActiveNpcScripts = 0;

    int numNpcsInDb;
	public int NumNpcsInDb {
		get { return numNpcsInDb; }
	}

	public HashSet<int> activeGameRounds = new HashSet<int>();
	public int maxGameRound = 0;
	
	void Awake () {
		npcScripts = new Dictionary<int,NpcScript>();
		npcLocations = new Dictionary<int,CellLoc>();
        npcsFromDb = new List<NPCLite>();
        cellsContainingNpcs = new Dictionary<CellLoc, List<int>>();
        npcLiveMaterials = new Dictionary<String, Material>();
        npcDeadMaterials = new Dictionary<String, Material>();
        mapNum = -1;
        dbUpdateComplete = false;
		nextDBUpdate = Time.time + dbUpdateDelta;
	}
	
	void Update () {
		if (mapNum != -1 && dbUpdateComplete && Time.time > nextDBUpdate) {
			print("Time to read from the DB! " + Time.time);
			nextDBUpdate = Time.time + dbUpdateDelta;
            dbUpdateComplete = false;

            UpdateNpcsFromDb();
		} 
	}

    void DbUpdateIsComplete(List<NPCLite> npcs)
    {
        if (npcs == null) {
            print("DB update of NPCs failed!");
        } else {
            print("DB update complete! " + npcs.Count + " NPCs updated!");
            npcsFromDb = npcs;
        }
        activeGameRounds.Clear();

        foreach (var key in cellsContainingNpcs.Keys) {
            cellsContainingNpcs[key].Clear();
        }
        UpdateAllNpcs();
        UpdateNpcCohab();
        dbUpdateComplete = true;
    }

    public static void DbUpdateAsync(int param, DbUpdateResultHandler resultHandler = null)
    {
        //If callback is null; we do not need unity adapter, otherwise we need to create it in ui thread.
        ThreadAdapter adapter = resultHandler == null ? null : CreateUnityAdapter();

        System.Threading.ThreadPool.QueueUserWorkItem(
            //jobResultHandler is a function reference; which will be called by UIThread with result data
         new System.Threading.WaitCallback(ExecuteDbUpdateJob), new object[] { param, adapter, resultHandler });
    }

    private static void ExecuteDbUpdateJob(object state) {
        object[] array = state as object[];

        int mapNum = (int) array[0];
        ThreadAdapter adapter = array[1] as ThreadAdapter;
        DbUpdateResultHandler callback = array[2] as DbUpdateResultHandler;

        //... time consuming job is performed here...
        List<NPCLite> result = new List<NPCLite>(500);
        try {
            result = DragonsSpine.DAL.DBNPC.GetAllNpcsAndPcs(mapNum);
        } catch (Exception e) {
            Utils.Log("Exception updating NPCs:" + e.Message, Utils.LogType.Exception);
            result = null;
        } 
        // if adapter is not null; callback is also not null.
        if (adapter != null) {
            adapter.ExecuteOnUi(delegate {
                callback(result);
            });
        }
        
    }

    public delegate void DbUpdateResultHandler(List<NPCLite> result);

    void UpdateNpcsFromDb()
    {
        DbUpdateResultHandler jrh = new DbUpdateResultHandler(DbUpdateIsComplete);
        DbUpdateAsync(mapNum, jrh);
    }

    internal static ThreadAdapter CreateUnityAdapter()
    {
        GameObject gameObject = new GameObject();
        return gameObject.AddComponent<ThreadAdapter>();
    }

    void UpdateAllNpcs() {
        List<NPCLite> npcs = npcsFromDb;
		// print("I found " + npcs.Count + " NPCs");
		numNpcsInDb = npcs.Count;
		foreach (NPCLite npc in npcs) {
			if (npc.lastActiveRound > maxGameRound) maxGameRound = npc.lastActiveRound;
		}
		foreach (NPCLite npc in npcs) {
			// skip inactive NPCs, and make sure they know they are inactive
			if (npc.lastActiveRound < maxGameRound - 10) {
                NpcScript tempNpcScript;
				if (npcScripts.TryGetValue(npc.worldNpcID, out tempNpcScript)) {
					tempNpcScript.gameObject.SetActive(false);
				}
                npcScripts.Remove(npc.worldNpcID);
				npcLocations.Remove(npc.worldNpcID);
                if (npc.isPC) activePcs.Remove(npc.worldNpcID);
			} else {
				activeGameRounds.Add(npc.lastActiveRound);
				CellLoc cell = new CellLoc(npc.X, npc.Y, npc.Z);
				npcLocations[npc.worldNpcID] = cell;
				if (!cellsContainingNpcs.ContainsKey(cell)) 
					cellsContainingNpcs[cell] = new List<int>();
				cellsContainingNpcs[cell].Add(npc.worldNpcID);
				Vector3 position = CLUtils.CellLocToVector3(cell, 1);

                NpcScript tempNpc = null;
                bool recycledNpc = false;
				if (npcScripts.ContainsKey(npc.worldNpcID)) {
                    tempNpc = npcScripts[npc.worldNpcID];
                    if (!tempNpc.gameObject.activeInHierarchy) {
                        print("NPC " + npc.worldNpcID + " not active!");
                        Debug.Break();
                    }
                } else {
                    tempNpc = npcPooler.GetPooledObject().GetComponent<NpcScript>();
                    recycledNpc = true;
                    tempNpc.Reset();
                    tempNpc.npcManager = this;
                    tempNpc.npcId = npc.worldNpcID;
                    tempNpc.name = npc.Name;
                    SetMaterials(tempNpc);
                    tempNpc.toBeSeen = (position.z <= mapManager.zTop + 1);                    
                    npcScripts[npc.worldNpcID] = tempNpc;
				}
                tempNpc.gameObject.SetActive(true);
                if (recycledNpc) print("Recycled NPC has ID " + tempNpc.npcId + " when it should be " + npc.worldNpcID);
                if (tempNpc.npcId != npc.worldNpcID) Debug.Break();
                tempNpc.cell = cell;
                tempNpc.newPosition = position;
                tempNpc.lastActiveRound = npc.lastActiveRound;
				tempNpc.UpdateMaterial(maxGameRound);
				tempNpc.Hits = npc.Hits;
				tempNpc.HitsFull = npc.HitsFull;
				tempNpc.MostHatedId = npc.mostHatedId;
				tempNpc.HasMostHated = npc.hasMostHated;
                if (npc.isPC && !activePcs.Contains(npc.worldNpcID)) activePcs.Add(npc.worldNpcID);
			}
		}
	}

	void SetMaterials(NpcScript npc) {
        string nameToUse = npc.name;
        if (nameToUse.ToLower().StartsWith("dude")) nameToUse = "dude";

        if (!npcLiveMaterials.ContainsKey(nameToUse)) {
            Material npcLiveMaterial = Resources.Load("Materials/" + nameToUse, typeof(Material)) as Material;
            if (npcLiveMaterial == null) {
                print("Couldn't find live Material for NPC name: " + nameToUse);
                npcLiveMaterial = Resources.Load("Materials/anonymous", typeof(Material)) as Material;
            }
            npcLiveMaterials[nameToUse] = npcLiveMaterial;
        }
        npc.liveMaterial = npcLiveMaterials[nameToUse];

        if (!npcDeadMaterials.ContainsKey(nameToUse)) {
            Material npcDeadMaterial = Resources.Load("Materials/ex" + nameToUse, typeof(Material)) as Material;
            if (npcDeadMaterial == null) {
                print("Couldn't find Dead Material for NPC name: " + nameToUse);
                npcDeadMaterial = Resources.Load("Materials/exanonymous", typeof(Material)) as Material;
            }
            npcDeadMaterials[nameToUse] = npcDeadMaterial;
        }
        npc.deadMaterial = npcDeadMaterials[nameToUse];
	}
		
	private void UpdateNpcCohab() {
        short dimension = 0, idx = 0; int population = 0;
        NpcScript thisNpcScript;
        foreach (KeyValuePair<CellLoc, List<int>> pair in cellsContainingNpcs) {
            population = pair.Value.Count;
            // print("Cell " + cell + " has an NPC population of " + population);
            dimension = (short)Math.Ceiling(Math.Sqrt(population));
            idx = 0;
            foreach (int npcId in pair.Value) {
                thisNpcScript = npcScripts[npcId];
                thisNpcScript.newScale = new Vector3(1.0f / dimension, 1.0f / dimension, 0.1f);
                thisNpcScript.cell.idx = idx;
                thisNpcScript.cell.dim = dimension;
                thisNpcScript.newPosition = CLUtils.CellLocToVector3(thisNpcScript.cell, 1);
                idx += 1;
            }
        }
	}

    public int getRandomNpc() {
        List<int> npcIds = new List<int>(npcScripts.Keys);
        if (npcIds.Count == 0) return 0;
        return npcIds[myRandom.Next(npcIds.Count)];
    }
    public int getRandomPc() {
        if (activePcs.Count == 0) return 0;
        return activePcs[myRandom.Next(activePcs.Count)];
    }
    public bool npcExists(int npcId) {
        return npcLocations.ContainsKey(npcId);
    }
    public CellLoc locateNpc(int npcId) {
        CellLoc npcLoc;
        if (npcLocations.TryGetValue(npcId, out npcLoc))
            return npcLoc;
        else
            return new CellLoc(0, 0, 0);
    }


	public CellLoc locateRandomNpc() {
		if (cellsContainingNpcs.Count == 0) return new CellLoc(0,0,0);
		List<CellLoc> cells = new List<CellLoc>(cellsContainingNpcs.Keys);
		return cells[myRandom.Next(cells.Count)];
	}
    

	public Vector3 locateNpcV3(int npcId) {
		if (npcScripts.ContainsKey(npcId))
			return npcScripts[npcId].transform.position;
		else
			return Vector3.zero;
	}

	public void UpdateZ(float z) {
		foreach (CellLoc c in cellsContainingNpcs.Keys) {
			foreach (int npcId in cellsContainingNpcs[c]) {
				npcScripts[npcId].toBeSeen = (c.z <= z); 
			}
		}
	}

    IEnumerator LoadNpcs() {
        foreach (NpcScript ns in npcScripts.Values)
            ns.gameObject.SetActive(false);
        npcScripts.Clear();
        npcLocations.Clear();
        cellsContainingNpcs.Clear();

        dbUpdateComplete = false;
        UpdateNpcsFromDb();
        while (!dbUpdateComplete)
           yield return null;
    }

}
