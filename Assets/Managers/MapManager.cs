using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DragonsSpine;

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public class MapManager : MonoBehaviour {

    public NpcManager npcManager;
    public CellScript cellScript;
    public ObjectPooler cellPooler;

    private static object cellLock = new object();

    private int mapNum = -1;
    public int MapNum {
        get { return mapNum; }
        set {
            if (mapNum != value) {
                mapNum = value;
                StopCoroutine("LoadMap");
                if (mapNum != -1) StartCoroutine("LoadMap");
            }
        }
    }

    public int num_cells = 0;
    public int num_blocks = 0;
    public float zTop = 0;

    public int[] zPlanes; // Array of elevations of Z planes
    public int currentZTopIdx; // Index into zPlanes for highest elevation currently visible

    public float nextDBUpdate;
    public float dbUpdateDelta = 2.5f;
    public int lastRoundSeen = 0;

    private Dictionary<CellLoc, CellScript> cell_objects;
    private Dictionary<String, Material> cellMaterials;
    private List<string> cellKeyList;
    private Dictionary<CellLoc, CellLite> cells;
    private List<CellLoc> cellsKeys;

    System.Random myRandom = new System.Random();

    public bool updateComplete;

    void Awake() {
        mapNum = -1;
        cell_objects = new Dictionary<CellLoc, CellScript>();
        cellMaterials = new Dictionary<String, Material>();
        cellKeyList = new List<string>();
        cells = new Dictionary<CellLoc, CellLite>();
        cellsKeys = new List<CellLoc>();
    }

    void Start() {
        nextDBUpdate = Time.time + dbUpdateDelta;
        updateComplete = true;
    }

    void InitialMapLoading() {
        lock (cellLock) {
            DragonsSpine.DAL.DBWorld.UpdateCellDict(cells, mapNum, 0);

            Dictionary<CellLoc, bool> cellNeedsTransform = new Dictionary<CellLoc, bool>();
            List<int> tempZPlanes = new List<int>(5);
            foreach (CellLoc c in cells.Keys) {
                if (!tempZPlanes.Contains(c.z)) tempZPlanes.Add(c.z);
                cellNeedsTransform[c] = true; // Every cell needs a transform by default
            }
            tempZPlanes.Sort();
            zPlanes = tempZPlanes.ToArray();

            currentZTopIdx = zPlanes.Length - 1;

            // Make a list of the cells sorted by coordinates:
            cellsKeys = new List<CellLoc>(cells.Keys);
            cellsKeys.Sort(
                delegate(CellLoc p1, CellLoc p2) {
                    int compareV = p1.z.CompareTo(p2.z);
                    if (compareV == 0) {
                        compareV = p1.y.CompareTo(p2.y);
                        if (compareV == 0) {
                            compareV = p1.x.CompareTo(p2.x);
                        }
                    }
                    return compareV;
                }
            );

            // Look through and only create the transforms we need
            cell_objects = new Dictionary<CellLoc, CellScript>();
            CellScript tempCell = null;
            foreach (CellLoc c in cellsKeys) {
                num_cells++;
                if (cellNeedsTransform[c]) {
                    // Instantiate this one
                    Vector3 position = CLUtils.CellLocToVector3(new CellLoc(cells[c].x, cells[c].y, cells[c].z), 0);

                    tempCell = cellPooler.GetPooledObject().GetComponent<CellScript>();
                    tempCell.mapManager = this;
                    tempCell.newPosition = position;

                    cell_objects[c] = tempCell;
                    Renderer rend = tempCell.GetComponent<Renderer>();
                    rend.enabled = false;
                    if (!cellMaterials.ContainsKey(cells[c].displayGraphic)) {
                        String materialName = StringToAsciiHex(cells[c].displayGraphic);
                        Material cellMaterial = Resources.Load("Materials/Cells/" + materialName, typeof(Material)) as Material;
                        if (cellMaterial == null) {
                            print("Couldn't find material " + materialName + " for display graphic \"" + cells[c].displayGraphic + "\"!");
                            Debug.Break();
                            cellMaterial = Resources.Load("Materials/Cells/20-20", typeof(Material)) as Material; // TEMP
                            cellMaterials[cells[c].displayGraphic] = cellMaterial;
                        } else {
                            cellMaterials[cells[c].displayGraphic] = cellMaterial;
                        }
                    }
                    tempCell.CurrMaterial = cellMaterials[cells[c].displayGraphic];
                    rend.enabled = true;

                    /* no grouping for now:
                     * 
                    // Now check to find the best chunk:
                    short bestXSize = 1, bestYSize = 1, currXSize = 1, currYSize = 1;
                    // 	1. Find the maximum X size we could possible be
                    CellLoc nextCell = c; nextCell.x += 1;
                    while (cells.ContainsKey(nextCell) && cellNeedsTransform[nextCell] && cells[nextCell].displayGraphic == cells[c].displayGraphic) {
                        currXSize++;
                        nextCell.x += 1;
                    }
                    //  2. Work down from the maximum possible X, evaluating the max possible Y at each X size
                    while (currXSize >= 1) {
                        bool nextYGood = true;
                        while (nextYGood) {
                            for (short xOffset = 0; xOffset < currXSize; xOffset++) {
                                nextCell = c; nextCell.x += xOffset; nextCell.y += currYSize;
                                if (!(cells.ContainsKey(nextCell) && cellNeedsTransform[nextCell] && cells[nextCell].displayGraphic == cells[c].displayGraphic)) {
                                    nextYGood = false;
                                }
                            }
                            if (nextYGood) currYSize++;
                        }
                        //  3. Keep track of the highest X*Y we've found so far
                        if (currXSize * currYSize > bestXSize * bestYSize) {
                            bestXSize = currXSize; bestYSize = currYSize;
                        }
                        currXSize--;
                    }
                    // Uncomment me to override grouping: 
                    bestXSize = 1; bestYSize = 1;
                    // print("Best size for cell " + v + " is " + bestXSize + "x, " + bestYSize + "y.");
                    //  4. Mark each cell we're absorbing
                    for (short yOffset = 0; yOffset < bestYSize; yOffset++) {
                        for (short xOffset = 0; xOffset < bestXSize; xOffset++) {
                            if (xOffset == 0 && yOffset == 0) continue;
                            cellNeedsTransform[new CellLoc(c.x + xOffset, c.y + yOffset, c.z)] = false;
                        }
                    }
                    //  5. Update the position, scale, and texture scale.
                    Vector3 firstCellPosition = CLUtils.CellLocToVector3(c, 0);
                    Vector3 lastCellPosition = CLUtils.CellLocToVector3(new CellLoc(c.x + (bestXSize - 1), c.y + (bestYSize - 1), c.z), 0);
                    tempCell.newPosition = (firstCellPosition + lastCellPosition) / 2f;
                    // tempts.position += new Vector3(-0.5f * (bestXSize - 1), -0.5f * (bestYSize - 1), 0f);
                    tempCell.newScale += new Vector3((float)(bestXSize - 1), (float)(bestYSize - 1), 0f);
                    // Uncomment for borders between the blocks:
                    // tempts.localScale -= new Vector3(0.1f, 0.1f, 0f);
                    rend.material.mainTextureScale = new Vector2((-1f) * tempCell.newScale.x, (-1f) * tempCell.newScale.y);
                     * */

                    tempCell.gameObject.SetActive(true);
                }
            }

            num_blocks = cell_objects.Count;
            print("Instantiated " + num_cells + " cells in " + cell_objects.Count + " transforms.");
            UpdateZ(zPlanes[currentZTopIdx]);
        }

    }

    void Update() {
        if (mapNum != -1 && updateComplete && Time.time > nextDBUpdate) {
            print("Time to read from the DB! " + Time.time);
            nextDBUpdate = Time.time + dbUpdateDelta;
            updateComplete = false;
            UpdateCellContents();
        }
    }



    public void UpdateZ(float z) {
        lock (cellLock) {
            foreach (CellLoc c in cell_objects.Keys) {
                cell_objects[c].toBeSeen = (c.z <= z);
            }
        }
        npcManager.UpdateZ(z);
        zTop = z;
    }

    public void CycleZ(bool up) {
        currentZTopIdx += (up ? 1 : -1);
        if (currentZTopIdx < 0) currentZTopIdx = 0;
        if (currentZTopIdx >= zPlanes.Length) currentZTopIdx = zPlanes.Length - 1;
        UpdateZ(zPlanes[currentZTopIdx]);
    }

    private bool CoinFlip() {
        return (UnityEngine.Random.Range(0f, 1f) >= 0.5);
    }

    private void GameStart() {
        enabled = true;
    }

    private void GameOver() {
        enabled = false;
    }

    void UpdateIsComplete(int numCellUpdated) {
        print("We updated " + numCellUpdated + " cells from the DB!");
        lock (cellLock) {
            foreach (KeyValuePair<CellLoc, CellLite> pair in cells) {

                if (!cellMaterials.ContainsKey(pair.Value.displayGraphic)) {
                    String materialName = StringToAsciiHex(pair.Value.displayGraphic);
                    Material cellMaterial = Resources.Load("Materials/Cells/" + materialName, typeof(Material)) as Material;
                    if (cellMaterial == null) {
                        print("Couldn't find material " + materialName + " for display graphic \"" + pair.Value.displayGraphic + "\"!");
                        Debug.Break();
                    } else {
                        cellMaterials[pair.Value.displayGraphic] = cellMaterial;
                    }
                }
                cell_objects[pair.Key].CurrMaterial = cellMaterials[pair.Value.displayGraphic];
            }
        }
        updateComplete = true;
    }

    public static void DoUpdateAsync(Dictionary<CellLoc,CellLite> param, int mapNum, UpdateResultHandler jobResultHandler = null) {
        //If callback is null; we do not need unity adapter, otherwise we need to create it in ui thread.
        ThreadAdapter adapter = jobResultHandler == null ? null : CreateUnityAdapter();

        System.Threading.ThreadPool.QueueUserWorkItem(
            //jobResultHandler is a function reference; which will be called by UIThread with result data
         new System.Threading.WaitCallback(ExecuteUpdateJob), new object[] { param, mapNum, adapter, jobResultHandler });
    }

    private static void ExecuteUpdateJob(object state) {
        object[] array = state as object[];

        var param = (Dictionary<CellLoc,CellLite>) array[0];
        var mapNum = (int)array[1];
        ThreadAdapter adapter = array[2] as ThreadAdapter;
        UpdateResultHandler callback = array[3] as UpdateResultHandler;

        //... time consuming job is performed here...
        int result;
        int highestRound = 0;
        lock (cellLock) {
            foreach (KeyValuePair<CellLoc, CellLite> pair in param) {
                if (pair.Value.lastRoundChanged > highestRound) highestRound = pair.Value.lastRoundChanged;
            }
            try {
                result = DragonsSpine.DAL.DBWorld.UpdateCellDict(param, mapNum, highestRound);
            } catch (Exception e) {
                Utils.Log("Exception updating Cells:" + e.Message, Utils.LogType.Exception);
                result = 0;
            }
        }
        // if adapter is not null; callback is also not null.
        if (adapter != null) {
            adapter.ExecuteOnUi(delegate {
                callback(result);
            });
        }
    }

    public delegate void UpdateResultHandler(int result);

    void UpdateCellContents() {
        UpdateResultHandler jrh = new UpdateResultHandler(UpdateIsComplete);
        DoUpdateAsync(cells, mapNum, jrh);
    }

    String StringToAsciiHex(String s) {
        byte[] ASCIIValues = Encoding.Convert(Encoding.Unicode, Encoding.ASCII, (Encoding.Unicode).GetBytes(s));
        return BitConverter.ToString(ASCIIValues);
    }

    // see http://blog.yamanyar.com/2015/05/unity-creating-c-thread-with-callback.html

    /// <summary>
    /// Must be called from an ui thread
    /// </summary>
    /// <returns>The unity adapter.</returns>
    internal static ThreadAdapter CreateUnityAdapter() {
        GameObject gameObject = new GameObject();
        return gameObject.AddComponent<ThreadAdapter>();
    }

    IEnumerator LoadMap() {
        lock (cellLock) {
            lastRoundSeen = 0;
            foreach (CellScript cs in cell_objects.Values)
                cs.gameObject.SetActive(false);
            num_cells = 0;
            cell_objects.Clear();
            cellKeyList.Clear();
            cells.Clear();
            cellsKeys.Clear();

            InitialMapLoading();
            nextDBUpdate = Time.time + dbUpdateDelta;
        }
        yield return null;
    }
}
