using UnityEngine;
using System.Text;

public class GUIManager : MonoBehaviour {
	
	private static GUIManager instance;
	public MapManager mapManager;
	public NpcManager npcManager;
	public CameraControlScript mainCamera;
    public int mapNum;
    public Vector3[] mapOverlooks;
    public int followingCharId;
    public Vector3 followingV3;
    public CellLoc followingCL;

	void Awake () {
		instance = this;
	}
	
	void Update () {
        if (followingCharId != 0 && npcManager.npcExists(followingCharId)) {
            CellLoc newFollowingCL = npcManager.locateNpc(followingCharId);
            if (!newFollowingCL.Equals(followingCL)) {
                followingV3 = CLUtils.CellLocToVector3(newFollowingCL, 1);
                mainCamera.lookAtNpc(followingV3);
                if (!newFollowingCL.z.Equals(followingCL.z)) {
                    mapManager.UpdateZ(newFollowingCL.z);
                }
                followingCL = newFollowingCL;
            }
        }
        if (Input.GetKeyDown(KeyCode.N)) {
            followingCharId = npcManager.getRandomNpc();
        }
        if (Input.GetKeyDown(KeyCode.P)) {
            followingCharId = npcManager.getRandomPc();
        }
        if (Input.GetKeyDown(KeyCode.F)) {
            followingCharId = 0;
        }
        if (Input.GetKeyDown (KeyCode.Z)) {
			mapManager.CycleZ(false);
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			mapManager.CycleZ(true);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			mainCamera.ReturnToHome();
		}
        if (Input.GetKeyDown(KeyCode.M)) {
            followingCharId = 0;
            mapNum += 1;
            if (mapNum > 12) mapNum = 0;
            if (mapNum == 9) mapNum = 10; // for some reason there's no map 9
            if (mapOverlooks.Length > mapNum)
                mainCamera.MoveToPosition(mapOverlooks[mapNum]);
            mapManager.MapNum = mapNum;
            npcManager.MapNum = mapNum;
        }
	}
			
	void OnGUI(){
		GUI.Box (new Rect (0,0,100,50), "# of cells\n" + mapManager.num_cells);
		GUI.Box (new Rect (100,0,100,50), "NPC Instances\n" + npcManager.NumNpcScripts);
		GUI.Box (new Rect (200,0,100,50), "Active NPC Insts\n" + npcManager.numActiveNpcScripts);
		GUI.Box (new Rect (300,0,100,50), "NPCs in DB\n" + npcManager.NumNpcsInDb);
		GUI.Box (new Rect (Screen.width - 100,0,100,50), "Map #\n" + mapNum);
		GUI.Box (new Rect (0,Screen.height - 50,100,50), "Z top\n" + mapManager.zTop);
		StringBuilder builder = new StringBuilder();
		foreach (int gameRound in npcManager.activeGameRounds)
			builder.Append(gameRound).Append(" ");
		GUI.Box (new Rect (Screen.width - 100,Screen.height - 50,100,50), "Game Round\n" + builder.ToString());
	}
	
}
