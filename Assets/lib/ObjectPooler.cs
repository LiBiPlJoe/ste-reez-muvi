﻿// With thanks to Mike Geig and http://unity3d.com/learn/tutorials/modules/beginner/live-training-archive/object-pooling

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

    // public static ObjectPooler current;
    public GameObject pooledObject;
    public int pooledAmount = 400;
    public bool willGrow = true;
    public int currentSize = 0;

    List<GameObject> pooledObjects;

    void Awake() {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++) {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
        currentSize = pooledObjects.Count;

    }

	void Start () {
	}

    public GameObject GetPooledObject() {
        for (int i = 0; i < pooledObjects.Count; i++) {
            if (!pooledObjects[i].activeInHierarchy) {
                return pooledObjects[i];
            }
        }
        if (willGrow) {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            pooledObjects.Add(obj);
            currentSize = pooledObjects.Count;
            return obj;
        }
        return null;
    }
}
