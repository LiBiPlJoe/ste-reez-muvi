﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public struct CellLoc {
    public short x;
    public short y;
    public int z;
    public short idx; // index of this space in the cell (zero-based)
    public short dim; // number of subcells on a row, and also number of rows

    public CellLoc(short x, short y, int z) {
        this.x = x; this.y = y; this.z = z;
        this.idx = 0; this.dim = 1;
    }

    public CellLoc(int x, int y, int z) {
        this.x = (short) x; this.y = (short) y; this.z = z;
        this.idx = 0; this.dim = 1;
    }

    public string ToString() {
        return "CellLoc(" + this.x + "," + this.y + "," + this.z + ")";
    }
}

public class CLUtils {
    public static Vector3 CellLocToVector3(CellLoc c, int layer) {
        // note: layer 0 for ground, 1 for characters, etc
        if (c.dim == 1) { // the simple case
            return new Vector3((1f) * c.x,
                               (-1f) * c.y,
                               (-0.1f) * (c.z + layer));
        } else {
            Vector3 position;
            int xPos = c.idx % c.dim;
            int yPos = c.idx / c.dim;
            position.x = c.x - 0.5f; // left side
            position.x += (1f / (c.dim * 2f)); // first position
            position.x += xPos * (1f / c.dim);
            position.y = (-1f) * c.y + 0.5f; // top side
            position.y -= (1f / (c.dim * 2f)); // first position
            position.y -= yPos * (1f / c.dim);
            position.z = (-0.1f) * (c.z + layer);
            return position;
        }
    }
}