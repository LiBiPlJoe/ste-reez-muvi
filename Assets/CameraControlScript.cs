
// Source: http://answers.unity3d.com/questions/9217/free-view-camera-script.html answer by user MGB

using UnityEngine; using System.Collections;

public class CameraControlScript : MonoBehaviour {

     public float sensitivityX = 8F;
     public float sensitivityY = 8F;
     
     float mHdg = 0F;
     float mPitch = 0F;
     
	Vector3 originalPosition; Quaternion originalRotation;
    public Vector3 newPosition; public Quaternion newRotation;
    
	void Start()
     {
		originalPosition = transform.position;
		originalRotation = transform.rotation;
     }
     
	public void ReturnToHome() {
		transform.position = originalPosition;
		transform.rotation = originalRotation;
	}

     void Update()
     {
         if (!(Input.GetMouseButton(0) || Input.GetMouseButton(1)))
             return;
     
         float deltaX = Input.GetAxis("Mouse X") * sensitivityX;
         float deltaY = Input.GetAxis("Mouse Y") * sensitivityY;
     
         if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
         {
             Strafe(deltaX);
             ChangeHeight(deltaY);
         }
         else
         {
             if (Input.GetMouseButton(0))
             {
                 MoveForwards(deltaY);
                 ChangeHeading(deltaX);
             }
             else if (Input.GetMouseButton(1))
             {
                 ChangeHeading(deltaX);
                 ChangePitch(-deltaY);
             }
         }
     }
     
     void MoveForwards(float aVal)
     {
         Vector3 fwd = transform.forward;
         fwd.y = 0;
         fwd.Normalize();
         transform.position += aVal * fwd;
     }
     
     void Strafe(float aVal)
     {
         transform.position += aVal * transform.right;
     }
     
     void ChangeHeight(float aVal)
     {
         transform.position += aVal * Vector3.up;
     }
     
     void ChangeHeading(float aVal)
     {
         mHdg += aVal;
         WrapAngle(ref mHdg);
         transform.localEulerAngles = new Vector3(mPitch, mHdg, 0);
     }
     
     void ChangePitch(float aVal)
     {
         mPitch += aVal;
         WrapAngle(ref mPitch);
         transform.localEulerAngles = new Vector3(mPitch, mHdg, 0);
     }
     
     public static void WrapAngle(ref float angle)
     {
         if (angle < -360F)
             angle += 360F;
         if (angle > 360F)
             angle -= 360F;
     }

	public void lookAtNpc(Vector3 npcPos) {
		transform.position = new Vector3(npcPos.x, npcPos.y, npcPos.z - 15f);
		transform.LookAt(npcPos);
	}
    public void MoveToPosition(Vector3 newPos) {
        transform.position = newPos;
        transform.rotation = originalRotation;
    }
}
